package com.airvtinginc.airvtinglive.app;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.airvtinginc.airvtinglive.tools.fonts.FontsManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class MainApplication extends Application {

    public static String DEFAULT_LOG_TAG;
    private static MainApplication mApplication;
    static Typeface MEDIUM;
    static Typeface REGULAR;

    static {
        DEFAULT_LOG_TAG = "AirVTing";
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static MainApplication getContext() {
        return mApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mApplication = this;
        Log.wtf(DEFAULT_LOG_TAG, "MainApplication.onCreate() uptime: " + SystemClock.uptimeMillis() + " elapsedRealtime: " + SystemClock.elapsedRealtime());
        //initFont();
    }

    private void initFont() {
        MEDIUM = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        REGULAR = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        FontsManager.init(REGULAR, MEDIUM);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
