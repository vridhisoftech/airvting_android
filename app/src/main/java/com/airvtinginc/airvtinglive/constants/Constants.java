package com.airvtinginc.airvtinglive.constants;

public class Constants {
    // TODO: SHARED PREFERENCE
    public static final String PREF_CURRENT_USER_TOKEN_ID = "prefCurrentUserTokenId";
    public static final String PREF_CURRENT_USER_ID = "prefCurrentUserId";
    public static final String PREF_CURRENT_USER_USERNAME = "prefCurrentUserName";
    public static final String PREF_CURRENT_USER_EMAIL = "prefCurrentUserEmail";
    public static final String PREF_CURRENT_USER_FIRST_NAME = "prefCurrentUserFirstName";
    public static final String PREF_CURRENT_USER_LAST_NAME = "prefCurrentUserLastName";
    public static final String PREF_CURRENT_USER_DISPLAY_NAME = "prefCurrentUserDisplayName";
    public static final String PREF_CURRENT_USER_PHONE_NUMBER = "prefCurrentUserPhoneNumber";
    public static final String PREF_CURRENT_USER_PHOTO_URL = "prefCurrentUserURLPhotoUrl";
    public static final String PREF_CURRENT_USER_NUMBER_FOLLOW_POST = "prefCurrentUserNumberFollowPost";
    public static final String PREF_CURRENT_USER_TOKEN = "prefCurrentUserToken";
    public static final String PREF_CURRENT_POST_ID = "prefCurrentPostId";
    public static final String PREF_CURRENT_VIEWER_ID = "prefCurrentViewerId";
    public static final String PREF_CURRENT_USER_GIFTS = "prefCurrentUserGifts";
    public static final String PREF_CURRENT_USER_HAS_PAYMENT_METHOD = "prefCurrentUserHasPaymentMethod";
    public static final String PREF_CURRENT_USER_PAYMENT_METHOD = "prefCurrentUserPaymentMethod";
    public static final String PREF_CURRENT_USER_CREATE_STRIPE_ACCOUNT_URL = "prefCurrentUserCreateStripeAccountUrl";
    public static final String PREF_PURCHASE = "purchase";
    public static final String PREF_CURRENT_FCM_TOKEN = "prefFCMToken";
    public static final String PREF_CURRENT_USER_IS_VERIFIED_EMAIL = "prefCurrentUserIsVerifiedEmail";
    public static final String PREF_NOTIFICATION = "prefNotification";
    public static final String PREF_NOTIFICATION_LIVE_STREAM = "prefNotificationLiveStream";
    public static final String PREF_NOTIFICATION_MESSAGE = "prefNotificationMessage";
    public static final String PREF_NOTIFICATION_SYSTEM = "prefNotificationSystem";
    public static final String PREF_RED5PRO_LICENSE_KEY = "prefConfigsRed5ProLicenseKey";

    public static final String CURRENCY = " SGD";
    public static final int NUMBER_OF_USERS_DISPLAY_IN_PEOPLE_GROUP = 4;
    public static final int NUMBER_OF_PHOTOS_DISPLAY_IN_PEOPLE_SUGGESTED = 4;
    public static final int MAX_MINUTE_ON_DEAL_COUNTDOWN = 60;
    public static final int MAX_CART_QUANTITY = 99;

    public static final String EXTRA_USER = "extra_user";
    public static final String EXTRA_USER_ID = "extra_user_id";
    public static final String EXTRA_USER_IS_CURRENT_USER = "extra_is_current_user";
    public static final String EXTRA_GALLERY_ID = "extra_gallery";
    public static final String EXTRA_IMAGE_URI = "extra_image_uri";
    public static final String EXTRA_CONVERSATION_ID = "extra_conversations_id";
    public static final String EXTRA_USERNAME = "extra_username";
    public static final String EXTRA_POST_ID = "extra_post_id";
    public static final String EXTRA_CAMERA_FACING = "extra_camera_facing";
    public static final String EXTRA_DEVICE_NAME = "extra_device_name";
    public static final String EXTRA_MEDIA_URL = "extra_media_url";
    public static final String EXTRA_WEB_VIEW_URL = "extra_web_view_id";
    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_PRODUCT_ID = "extra_product_id";
    public static final String EXTRA_SHOW_MENU_GIFT_STORE = "extra_show_menu_gift_store";
    public static final String EXTRA_SHOW_WALLET_TOKEN = "extra_show_wallet_token";
    public static final String EXTRA_COMMENT_COUNT = "extra_comment_count";
    public static final String EXTRA_LIKE_COUNT = "extra_like_count";
    public static final String EXTRA_GIFT_COUNT = "extra_gift_count";
    public static final String EXTRA_CURRENT_CAMERA = "current_camera";
    public static final String EXTRA_PAYMENT_DETAILS = "extra_payment_details";
    public static final String EXTRA_LIVE_STATUS = "extra_live_status";
    public static final String EXTRA_IS_HIDE_ADD_TO_CART = "extra_is_hide_add_to_cart";
    public static final String EXTRA_FOLLOW_TYPE = "extra_follow_type";
    public static final String REALM_MY_CART = "my_cart.realm";
    public static final String REALM_PRODUCT_ID_PRIMARY_KEY = "productId";

    //FireStore
    public static final int COMMENT_NUMBER_OLD_MESSAGE = 20;
    public static final String COLLECTION_COMMENTS = "comments";
    public static final String COLLECTION_POSTS = "posts";
    public static final String COLLECTION_USERS = "users";
    public static final String COLLECTION_VIEWERS = "viewers";
    public static final String COLLECTION_GIFTS = "gifts";
    public static final String COLLECTION_CONFIGS = "configs";
    public static final String DOCUMENT_RED_5_PRO = "red5pro";
    public static final String FIELD_IS_LIVE = "isLive";
    public static final String FIELD_COUNT_COMMENTS = "countComments";
    public static final String FIELD_COUNT_VIEWERS = "countViewers";
    public static final String FIELD_IS_LIKE = "isLike";
    public static final String FIELD_IS_ONLINE = "isOnline";
    public static final String FIELD_IS_VIEW = "isView";
    public static final String FIELD_PRODUCT = "product";
    public static final String FIELD_PRODUCT_ID = "productId";
    public static final String FIELD_PRODUCT_EXPIRE_AT = "expiredAt";
    public static final String FIELD_PRODUCT_DISCOUNT = "discount";
    public static final String FIELD_PRODUCT_PRICE_DISCOUNT = "priceDiscount";
    public static final String FIELD_PRODUCT_QUANTITY = "quantity";
    public static final String FIELD_CREATE_AT = "createdAt";
    public static final String FIELD_POST_ID = "postId";
    public static final String FIELD_USER_ID = "userId";

    public static final int DEVICE_TYPE_ID_ANDROID = 2; //1 ios , 2 android
    public static final int LIMIT_DISPLAY_NAME_LENGTH = 15;
    public static final String STRING_ELLIPS_END = "...";
}
