package com.airvtinginc.airvtinglive.tools;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;

public class AppPermission {

    public static final int REQUEST_CODE_GALLERY = 9001;
    public static final int REQUEST_CODE_CAMERA = 9002;

    private Activity activity;

    private Fragment fragment;

    private PermissionCallbackGranted permissionCallbackGranted;

    private CustomDialogDenied customDialogDenied;

    private int numberPermission;

    public AppPermission(Activity activity, PermissionCallbackGranted permissionCallbackGranted, CustomDialogDenied customDialogDenied) {
        this.activity = activity;
        this.permissionCallbackGranted = permissionCallbackGranted;
        this.customDialogDenied = customDialogDenied;
    }

    public AppPermission(Fragment fragment, PermissionCallbackGranted permissionCallbackGranted, CustomDialogDenied customDialogDenied) {
        this.fragment = fragment;
        this.permissionCallbackGranted = permissionCallbackGranted;
        this.customDialogDenied = customDialogDenied;
    }

    private Context getContext() {
        return activity != null ? activity : fragment.getActivity();
    }

    private Activity getActivity() {
        return activity != null ? activity : fragment.getActivity();
    }

    private boolean hasPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void requestPermissions(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity != null) {
                activity.requestPermissions(permissions, requestCode);
            } else {
                fragment.requestPermissions(permissions, requestCode);
            }
        }
    }

    private void showDialogDenied() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.PERMISSION);
        messageDialog.setCancel(false);
        messageDialog.setDialogTitle(getContext().getString(R.string.dialog_permission_title));
        messageDialog.setDialogMessage(getContext().getString(R.string.permissions_denied_feedback));
        messageDialog.setDialogPositiveButtonTitle(getContext().getString(R.string.setting));
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
            }

            @Override
            public void onCloseActionButtonClick() {
                getActivity().finish();
            }
        });
        messageDialog.show(((AppCompatActivity) getContext()).getSupportFragmentManager(), "Show Permission Settings Dialog");
    }

    public void checkPermissionCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};
            numberPermission = permissions.length;
            if (hasPermissions(getContext(), permissions)) {
                permissionCallbackGranted.onGranted(REQUEST_CODE_CAMERA);
            } else {
                for (String permission : permissions) {
                    if (getContext().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(permissions, REQUEST_CODE_CAMERA);
                    }
                }
            }
        } else {
            permissionCallbackGranted.onGranted(REQUEST_CODE_CAMERA);
        }
    }

    public void checkPermissionGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
            numberPermission = permissions.length;
            if (hasPermissions(getContext(), permissions)) {
                permissionCallbackGranted.onGranted(REQUEST_CODE_GALLERY);
            } else {
                for (String permission : permissions) {
                    if (getContext().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(permissions, REQUEST_CODE_GALLERY);
                    }
                }
            }
        } else {
            permissionCallbackGranted.onGranted(REQUEST_CODE_GALLERY);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
            case REQUEST_CODE_CAMERA: {
                if (numberPermission == permissions.length) {
                    // If request is cancelled, the result arrays are empty.
                    if (checkPermissionGranted(permissions, grantResults)) {
                        // permission was granted, yay!
                        permissionCallbackGranted.onGranted(requestCode);
                    } else {
                        // permission denied, boo!
                        if (checkShowRationale(permissions, grantResults)) {
                            if (customDialogDenied != null) {
                                customDialogDenied.onDined(requestCode);
                            } else {
                                getActivity().finish();
                            }
                        } else {
                            showDialogDenied();
                        }
                    }
                }
            }
        }
    }

    private boolean checkPermissionGranted(@NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean isPermissionGranted = true;
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                isPermissionGranted = false;
            }
        }
        return isPermissionGranted;
    }

    private boolean checkShowRationale(@NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean showRationale = true;
        for (int i = 0; i < permissions.length; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !getActivity().shouldShowRequestPermissionRationale(permissions[i]) && grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                showRationale = false;
            }
        }
        return showRationale;
    }

    public interface PermissionCallbackGranted {
        void onGranted(int requestCode);
    }

    public interface CustomDialogDenied {
        void onDined(int requestCode);
    }
}
