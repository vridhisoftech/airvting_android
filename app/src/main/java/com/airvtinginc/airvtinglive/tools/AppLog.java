package com.airvtinginc.airvtinglive.tools;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.IllegalFormatException;
import java.util.Locale;

public class AppLog {
    public static final boolean DEBUG;
    private static String TAG = "AirVTing";
    private static long sStartTime;

    static {
        DEBUG = Log.isLoggable(TAG, Log.VERBOSE);
        sStartTime = System.currentTimeMillis();
    }

    private static String buildMessage(String message, Object... objects) {
        String msg;
        if (objects == null) {
            msg = message;
        } else {
            try {
                msg = String.format(Locale.US, message, objects);
            } catch (IllegalFormatException var14) {
                wtf("IllegalFormatException: formatString=\'%s\' numArgs=%d", message, objects.length);
                msg = message + " (An error occurred while formatting the message.)";
            }
        }

        StackTraceElement[] traceElements = (new Throwable()).fillInStackTrace().getStackTrace();
        String addr = "<unknown>";

        for (int i = 2; i < traceElements.length; i++) {
            String className = traceElements[i].getClassName();
            if (!className.equals(AppLog.class.getName())) {
                String sq = className.substring(1 + className.lastIndexOf(46));
                String s2 = sq.substring(1 + sq.lastIndexOf(36));
                addr = s2 + "." + traceElements[i].getMethodName();
                break;
            }
        }

        return String.format(Locale.US, "[%d] %s: %s", Thread.currentThread().getId(), addr, msg);
    }

    public static void d(String msg, Object... objects) {
        Log.d(TAG, buildMessage(msg, objects));
    }

    public static void e(String var0, Object... var1) {
        Log.e(TAG, buildMessage(var0, var1));
    }

    @SuppressLint("WrongConstant")
    public static void logTiming(String msg, Object... objects) {
        if (Log.isLoggable(TAG, 2)) {
            if (objects != null) {
                msg = String.format(Locale.US, msg, objects);
            }

            v("%4dms: %s", System.currentTimeMillis() - sStartTime, msg);
        }

    }

    public static void v(String msg, Object... objects) {
        Log.v(TAG, buildMessage(msg, objects));
    }

    public static void w(String msg, Object... objects) {
        Log.w(TAG, buildMessage(msg, objects));
    }

    public static void wtf(String msg, Object... objects) {
        Log.e(TAG, buildMessage(msg, objects));
        Log.wtf(TAG, buildMessage(msg, objects));
    }
}
