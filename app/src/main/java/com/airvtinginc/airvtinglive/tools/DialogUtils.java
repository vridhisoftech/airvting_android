package com.airvtinginc.airvtinglive.tools;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;

public class DialogUtils {
    private static MyProgressDialog progress;
    private static MessageDialog messageDialog;
    private static MessageDialog emailMessageDialog;
    private static MessageDialog verifyEmailmessageDialog;
    private static MessageDialog connectMessageDialog;
    private static MessageDialog paymentMessageDialog;
    private static MessageDialog paymentStripMessageDialog;
    private static MessageDialog authorizedMessageDialog;
    private static MessageDialog noNetwork;

    public static void showLoadingProgress(Context context, boolean isCancel) {
        try {
            if (progress == null) {
                progress = new MyProgressDialog(context, R.style.dialog_full_transparent_background);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setCancelable(isCancel);
            }
            if (progress != null) {
                if (!progress.isShowing()) {
                    progress.show();
                    progress.showLoading();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void hideLoadingProgress() {
        try {
            if (progress != null) {
                progress.closeLoading();
                progress.dismiss();
                progress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showUpdateEmailDialog(Context context, FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            emailMessageDialog = MessageDialog.newInstance();
            emailMessageDialog.setDialogType(MessageDialogType.UPDATE_EMAIL);
            emailMessageDialog.setDialogTitle(context.getString(R.string.dialog_update_email_title));
            emailMessageDialog.setDialogMessage(context.getString(R.string.dialog_update_email_required));
            emailMessageDialog.setDialogPositiveButtonTitle(context.getString(R.string.dialog_ok));
            emailMessageDialog.show(fragmentManager, "Show Update Email Dialog");
        }
    }

    public static void showVerifyEmailDialog(Context context, FragmentManager fragmentManager) {
        if (fragmentManager != null ) {
            verifyEmailmessageDialog = MessageDialog.newInstance();
            verifyEmailmessageDialog.setDialogType(MessageDialogType.VERIFY_EMAIL);
            verifyEmailmessageDialog.setDialogTitle(context.getString(R.string.dialog_verify_email_title));
            verifyEmailmessageDialog.setDialogMessage(context.getString(R.string.dialog_verify_email_msg));
            verifyEmailmessageDialog.setDialogPositiveButtonTitle(context.getString(R.string.dialog_resend));
            verifyEmailmessageDialog.show(fragmentManager, "Show Verify Email Dialog");
        }
    }

    public static void showConnectStripeDialog(Context context, FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            connectMessageDialog = MessageDialog.newInstance();
            connectMessageDialog.setDialogType(MessageDialogType.CONNECT_WITH_STRIPE);
            connectMessageDialog.setDialogTitle(context.getString(R.string.dialog_connect_with_stripe_title));
            connectMessageDialog.setDialogMessage(context.getString(R.string.dialog_connect_with_stripe_msg));
            connectMessageDialog.setDialogPositiveButtonTitle(context.getString(R.string.dialog_connect));
            connectMessageDialog.show(fragmentManager, "Show Connect With Stripe Dialog");
        }
    }

    public static void showAddPaymentMethodDialog(Context context, FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            paymentMessageDialog = MessageDialog.newInstance();
            paymentMessageDialog.setDialogType(MessageDialogType.ADD_PAYMENT_METHOD);
            paymentMessageDialog.setDialogTitle(context.getString(R.string.dialog_my_cart_payment));
            paymentMessageDialog.setDialogMessage(context.getString(R.string.require_payment));
            paymentMessageDialog.setDialogPositiveButtonTitle(context.getString(R.string.dialog_ok));
            paymentMessageDialog.show(fragmentManager, "Show Add Payment Method Dialog");
        }
    }

    public static void showConnectStripeAndAddPaymentDialog(Context context, FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            paymentStripMessageDialog = MessageDialog.newInstance();
            paymentStripMessageDialog.setDialogType(MessageDialogType.CONNECT_STRIPE_AND_ADD_PAYMENT);
            paymentStripMessageDialog.setDialogTitle(context.getString(R.string.dialog_stripe_connect_title));
            paymentStripMessageDialog.setDialogMessage(context.getString(R.string.dialog_stripe_connect_msg));
            paymentStripMessageDialog.setDialogPositiveButtonTitle(context.getString(R.string.dialog_go));
            paymentStripMessageDialog.show(fragmentManager, "Show Connect Stripe & Add Payment Method Dialog");
        }
    }

    public static void showUnauthorizedDialog(Context context, FragmentManager fragmentManager, String message) {
        if (fragmentManager != null) {
            authorizedMessageDialog = MessageDialog.newInstance();
            authorizedMessageDialog.setDialogType(MessageDialogType.UNAUTHORIZED);
            authorizedMessageDialog.setDialogTitle(context.getString(R.string.error_401_authorization_title));
            authorizedMessageDialog.setDialogMessage(message);
            authorizedMessageDialog.setDialogPositiveButtonTitle(context.getString(R.string.dialog_ok));
            authorizedMessageDialog.setCancel(false);
            authorizedMessageDialog.show(fragmentManager, "Show Unauthorized Dialog");
        }
    }

    public static void showMessageDialog(Context context, FragmentManager fragmentManager, String title, String errorMsg) {
        if (fragmentManager != null) {
            messageDialog = MessageDialog.newInstance();
            messageDialog.setDialogType(MessageDialogType.DEFAULT);
            messageDialog.setDialogTitle(title);
            messageDialog.setDialogMessage(errorMsg);
            messageDialog.setDialogPositiveButtonTitle(context.getString(R.string.dialog_ok));
            messageDialog.show(fragmentManager, "Show Message Dialog");
        }
    }
}
