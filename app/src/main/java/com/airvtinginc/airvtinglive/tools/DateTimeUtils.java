package com.airvtinginc.airvtinglive.tools;

import android.content.Context;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.models.response.Schedule;
import com.airvtinginc.airvtinglive.models.response.ScheduleDate;
import com.wdullaer.materialdatetimepicker.date.DateRangeLimiter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateTimeUtils {
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_YYYY_MM_DD_FORMAT = "yyyy-MM-dd";
    public static final String DATE_DD_MM_YY_FORMAT = "dd/MM/yy";
    public static final String TIME_FORMAT = "hh:mmaa";
    public static final String TIME_HOUR_MINUTE_12_FORMAT = "hh:mm";
    public static final String TIME_HOUR_MINUTE_24_FORMAT = "HH:mm";
    public static final int TIME_MILLISECOND = 1000;
    public static final int TIME_SECOND = 60;
    public static final int TIME_MINUTE = 60;
    public static final int TIME_HOUR = 60;
    public static final int TIME_DAY = 24;
    public static final int TIME_DAYS_IN_WEEK = 7;
    public static final int TIME_DAYS_IN_MONTH = 30;
    public static final int TIME_DAYS_IN_YEAR = 365;

    public static String getDisplaySchedule(Context context, Schedule schedule) {
        String result = "";
        StringBuilder dayInWeek = new StringBuilder();
        String postfix = ", ";
        for (ScheduleDate scheduleDate : schedule.getScheduleDates()) {
            switch (scheduleDate.getDate()) {
                case 2:
                    dayInWeek.append(context.getString(R.string.day_in_week_mon)).append(postfix);
                    break;
                case 3:
                    dayInWeek.append(context.getString(R.string.day_in_week_tue)).append(postfix);
                    break;
                case 4:
                    dayInWeek.append(context.getString(R.string.day_in_week_wed)).append(postfix);
                    break;
                case 5:
                    dayInWeek.append(context.getString(R.string.day_in_week_thu)).append(postfix);
                    break;
                case 6:
                    dayInWeek.append(context.getString(R.string.day_in_week_fri)).append(postfix);
                    break;
                case 7:
                    dayInWeek.append(context.getString(R.string.day_in_week_sat)).append(postfix);
                    break;
                case 8:
                    dayInWeek.append(context.getString(R.string.day_in_week_sun)).append(postfix);
                    break;
            }
        }
        result += dayInWeek.replace(dayInWeek.length() - postfix.length(), dayInWeek.length(), " ").toString();
        try {
            Date startDate = getDateFollowFormat(getDateFormatUTC(schedule.getStartTime()), DATE_TIME_FORMAT);
            Date endDate = getDateFollowFormat(getDateFormatUTC(schedule.getEndTime()), DATE_TIME_FORMAT);

            String startTime = getDateStringFollowFormat(startDate, TIME_FORMAT);
            String endTime = getDateStringFollowFormat(endDate, TIME_FORMAT);

            result += startTime + " - ";
            result += endTime;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

    public static String getDisplayBookmarkTime(Context context, String bookmarkTimeString) {
        StringBuilder result = new StringBuilder();
        Calendar currentCalendar = Calendar.getInstance();
        Calendar bookMarkCalendar = Calendar.getInstance();

        Date bookMarkDate = getDateFollowFormat(bookmarkTimeString, DATE_TIME_FORMAT);
        bookMarkCalendar.setTime(bookMarkDate);

        if (isSameDate(bookMarkDate, currentCalendar.getTime())) {
            result.append(context.getString(R.string.day_today)).append(", ");
        }

        result.append(bookMarkCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US))
                .append(", ");
        result.append(bookMarkCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US))
                .append(" ");
        result.append(bookMarkCalendar.get(Calendar.DAY_OF_MONTH));

        return result.toString().toUpperCase();
    }

    public static boolean isSameDate(Date firstDate, Date secondDate) {
        Date firstDateTime = convertDateToOtherFormat(firstDate, DATE_FORMAT);
        Date secondDateTime = convertDateToOtherFormat(secondDate, DATE_FORMAT);
        return firstDateTime != null && secondDateTime != null && firstDateTime.compareTo(secondDateTime) == 0;
    }

    public static Date getDateFollowFormat(String dateString, String format) {
        if (TextUtils.isEmpty(dateString)) {
            return null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            AppLog.e(e.getMessage());
        }
        return null;
    }

    public static String getDateStringFollowFormat(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
        return dateFormat.format(date);
    }

    public static String convertStringDateToOtherFormat(String dateString, String fromFormat, String toFormat) {
        SimpleDateFormat sdfFromFormat = new SimpleDateFormat(fromFormat, Locale.getDefault());
        SimpleDateFormat sdfToFormat = new SimpleDateFormat(toFormat, Locale.getDefault());
        try {
            Date date = sdfFromFormat.parse(dateString);
            return sdfToFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Date convertDateToOtherFormat(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
        String formatDateString = dateFormat.format(date);

        try {
            return dateFormat.parse(formatDateString);
        } catch (ParseException e) {
            AppLog.e(e.getMessage());
        }

        return null;
    }

    public static String formatHourMinute(int hour, int minutes) {
        StringBuilder result = new StringBuilder();

        if (hour < 10) {
            result.append("0")
                    .append(hour)
                    .append(":");
        } else {
            result.append(hour);
        }

        if (minutes < 10) {
            result.append("0").append(minutes);
        } else {
            result.append(minutes);
        }

        return result.toString();
    }

    public static String getElapsedInterval(Context context, String dateString) {
        if (TextUtils.isEmpty(dateString)) {
            return "";
        }
        Date compareDate = getDateFollowFormat(dateString, DATE_TIME_FORMAT);
        Calendar currentCalendar = Calendar.getInstance();
        Calendar compareCalendar = Calendar.getInstance();

        compareCalendar.setTime(compareDate);
        long difference = currentCalendar.getTimeInMillis() - compareCalendar.getTimeInMillis();
        if (difference <= 0) {
            return "";
        }

        long days = TimeUnit.MILLISECONDS.toDays(difference);
        if (days >= TIME_DAYS_IN_YEAR) {
            long differenceYears = days / TIME_DAYS_IN_YEAR;
            if (differenceYears > 1) {
                return differenceYears + context.getString(R.string.product_years_ago);
            } else {
                return context.getString(R.string.product_a_year_ago);
            }
        } else if (days >= TIME_DAYS_IN_MONTH) {
            long differenceMonths = days / TIME_DAYS_IN_MONTH;
            if (differenceMonths > 1) {
                return differenceMonths + context.getString(R.string.product_months_ago);
            } else {
                return context.getString(R.string.product_a_month_ago);
            }
        } else if (days >= TIME_DAYS_IN_WEEK) {
            long differenceWeeks = days / TIME_DAYS_IN_WEEK;
            if (differenceWeeks > 1) {
                return differenceWeeks + context.getString(R.string.product_weeks_ago);
            } else {
                return context.getString(R.string.product_a_week_ago);
            }
        } else if (days > 1) {
            return days + context.getString(R.string.product_days_ago);
        } else if (days == 1) {
            return context.getString(R.string.product_a_day_ago);
        } else {
            long hours = TimeUnit.MILLISECONDS.toHours(difference);
            if (hours > 1) {
                return hours + context.getString(R.string.product_hours_ago);
            } else if (hours == 1) {
                return context.getString(R.string.product_an_hour_ago);
            } else {
                long minutes = TimeUnit.MILLISECONDS.toMinutes(difference);
                if (minutes > 1) {
                    return minutes + context.getString(R.string.product_minutes_ago);
                } else if (minutes == 1) {
                    return context.getString(R.string.product_a_minute_ago);
                } else {
                    return context.getString(R.string.product_a_moment_ago);
                }
            }
        }
    }

    public static String getElapsedIntervalConversation(Context context, String dateString, Locale locale) {
        if (TextUtils.isEmpty(dateString)) {
            return "";
        }
        Date compareDate = getDateFollowFormat(dateString, DATE_TIME_FORMAT);
        Calendar currentCalendar = Calendar.getInstance();
        Calendar compareCalendar = Calendar.getInstance();

        compareCalendar.setTime(compareDate);
        long difference = currentCalendar.getTimeInMillis() - compareCalendar.getTimeInMillis();
        if (difference <= 0) {
            return "";
        }

        long days = TimeUnit.MILLISECONDS.toDays(difference);
        String timeFormat = "h:mm aa";
        String toDay = context.getString(R.string.day_today);
        String yesterday = context.getString(R.string.day_yesterday);
        if (days < 1) {
            return toDay.substring(0, 1).toUpperCase() + toDay.substring(1).toLowerCase() + ", " + getDateStringFollowFormat(compareDate, timeFormat);
        } else if (days < 2) {
            return yesterday.substring(0, 1).toUpperCase() + yesterday.substring(1).toLowerCase() + ", " + getDateStringFollowFormat(compareDate, timeFormat);
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, " + timeFormat, locale);
            return simpleDateFormat.format(compareDate);
        }
    }

    public static String getDateFormatUTC(String dateUTC) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat(ServiceConstants.DATE_TIME_FORMAT, Locale.getDefault());
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = df.parse(dateUTC);
        df.setTimeZone(TimeZone.getDefault());
        return df.format(date);
    }

    public static String parseDateToYyyyMmDd(String time) {
        String inputPattern = "dd/mm/yyyy";
        String outputPattern = "yyyy-mm-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateToDDMMYY(String time) {
        String inputPattern = "dd/mm/yyyy";
        String outputPattern = "dd/mm/yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static DateRangeLimiter getDateRangeLimiter(@NonNull Calendar calendarStart, @NonNull Calendar calendarEnd) {
        return new DateRangeLimiter() {
            @Override
            public int getMinYear() {
                return calendarStart.get(Calendar.YEAR);
            }

            @Override
            public int getMaxYear() {
                return calendarEnd.get(Calendar.YEAR);
            }

            @NonNull
            @Override
            public Calendar getStartDate() {
                return calendarStart;
            }

            @NonNull
            @Override
            public Calendar getEndDate() {
                return calendarEnd;
            }

            @Override
            public boolean isOutOfRange(int year, int month, int day) {
                return false;
            }

            @NonNull
            @Override
            public Calendar setToNearestDate(@NonNull Calendar day) {
                return day;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {

            }
        };
    }

    public static long getTimeStamp(String dateString) {
        DateFormat formatter;
        Date date = null;
        formatter = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.ENGLISH);
        try {
            date = (Date) formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    /**
     * Display time follow format hh:mm:ss from millisecond
     *
     * @param millisecond millisecond
     * @return Time String with format hh:mm:ss
     */
    public static String getDisplayTimeFromMillis(long millisecond) {
        long min = millisecond / 1000 / 60;
        long hour = min / 60;
        long displayMin = hour >= 1 ? (min - hour * 60) : min;
        long sec = (millisecond / 1000) % 60;
        return (hour < 10 ? "0" + hour : hour) + ":" + (displayMin < 10 ? "0" + displayMin : displayMin) + ":" + (sec < 10 ? "0" + sec : sec);
    }

    /**
     * Display time follow format hh:mm:ss from millisecond
     *
     * @param millisecond millisecond
     * @return Time String with format 00d00
     */
    public static String getDisplayTimeSaleFromMillis(long millisecond) {
        long min = millisecond / 1000 / 60;
        long hour = min / 60;
        long displayMin = hour >= 1 ? (min - hour * 60) : min;
        return (hour < 10 ? "0" + hour : hour) + "h" + (displayMin < 10 ? "0" + displayMin : displayMin);
    }

    /**
     * Get current time follow format
     *
     * @return  Time follow format
     */
    public static String getCurrentTime(String format) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        return sdf.format(cal.getTime());
    }

    public static Date getCurrentDate(boolean isShowTime) {
        Calendar calendar = Calendar.getInstance();
        if (!isShowTime) {
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
        }
        return new Date(calendar.getTimeInMillis());
    }
}
