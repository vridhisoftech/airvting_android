package com.airvtinginc.airvtinglive.tools;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.Behavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class FlingBehavior extends Behavior {
    private static final int TOP_CHILD_FLING_THRESHOLD = 3;
    private boolean isPositive;

    public FlingBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onNestedFling(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, float f, float f2, boolean z) {
        float f3;
        boolean z2;
        if ((f2 <= 0.0f || this.isPositive) && (f2 >= 0.0f || !this.isPositive)) {
            f3 = f2;
        } else {
            f3 = f2 * -1.0f;
        }
        if (!(view instanceof RecyclerView) || f3 >= 0.0f) {
            z2 = z;
        } else {
            boolean z3;
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getChildAdapterPosition(recyclerView.getChildAt(0)) > TOP_CHILD_FLING_THRESHOLD) {
                z3 = true;
            } else {
                z3 = false;
            }
            z2 = z3;
        }
        return super.onNestedFling(coordinatorLayout, appBarLayout, view, f, f3, z2);
    }

    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int[] iArr, int type) {
        super.onNestedPreScroll(coordinatorLayout, appBarLayout, view, i, i2, iArr, type);
        this.isPositive = i2 > 0;
    }
}
