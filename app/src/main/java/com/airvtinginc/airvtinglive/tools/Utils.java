package com.airvtinginc.airvtinglive.tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.app.MainApplication;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.gui.components.ShowKeyboardOverlap;
import com.airvtinginc.airvtinglive.gui.dialog.SearchDialog;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.MessageType;
import com.airvtinginc.airvtinglive.models.MessageContentModel;
import com.airvtinginc.airvtinglive.models.NotificationsConfig;
import com.airvtinginc.airvtinglive.models.Red5ProConfig;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.fonts.CustomTypefaceSpan;
import com.android.billingclient.api.Purchase;
import com.google.gson.Gson;
import com.lightfire.gradienttextcolor.GradientTextView;

import java.io.File;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Utils {
    private static Realm mCartRealm = null;

    private static final Pattern HASHTAG_PATTERN = Pattern.compile(
            "(#(?:[a-zA-Z].*?|\\\\d+[a-zA-Z]+.*?))"
    );

    public static final Pattern HASH_TAG_MATCHER = Pattern.compile("[#]+[A-Za-z0-9-_]+\\b");

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void systemBarLolipop(Activity act) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = act.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(act.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    public static void systemKeyboard(Activity activity) {
        // Solve keyboard overlap textView problem after 4.4
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            ShowKeyboardOverlap.assistActivity(activity);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // ADJUST_RESIZE can solve most of the device
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    public static void systemKeyboard(Dialog dialog) {
        // Solve keyboard overlap textView problem after 4.4
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            ShowKeyboardOverlap.assistDialog(dialog);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // ADJUST_RESIZE can solve most of the device
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    private static float getAPIVerison() {

        Float f = null;
        try {
            StringBuilder strBuild = new StringBuilder();
            strBuild.append(android.os.Build.VERSION.RELEASE.substring(0, 2));
            f = new Float(strBuild.toString());
        } catch (NumberFormatException e) {
            Log.e("", "erro ao recuperar a versão da API" + e.getMessage());
        }

        return f.floatValue();
    }

    public static void setUpRecycleViewWithNoDivider(Context context, RecyclerView recycleView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycleView.setLayoutManager(linearLayoutManager);
        recycleView.setHasFixedSize(true);
        recycleView.setNestedScrollingEnabled(false);
    }

    public static void setUpRecycleView(Context context, RecyclerView recycleView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycleView.setLayoutManager(linearLayoutManager);
        recycleView.setHasFixedSize(true);
        recycleView.setNestedScrollingEnabled(false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.recycleview_line_divider));
        recycleView.addItemDecoration(dividerItemDecoration);
    }

    public static void setUpRecycleViewHorizontal(Context context, RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            //Software Keyboard was shown" , hide it
            View view = activity.getCurrentFocus();
            if (view != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static void hideSoftKeyboard(View view) {
        final InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void showKeyboardOnStart(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    public static File getApplicationFolder() {
        try {
            String folderPath = Constant.AIRVTING_FOLDER;
            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            return folder;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static boolean isRememberLogin(Context context) {
        if (TextUtils.isEmpty(SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_TOKEN_ID, "")) |
                TextUtils.isEmpty(SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, ""))) {
            return false;
        }
        return true;
    }

    public static void saveCurrentUserInfoToPreferences(Context context, String tokenId, int countPostsFollowing, User user) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_TOKEN_ID, tokenId);
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_ID, user.getId());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_USERNAME, user.getUsername());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_EMAIL, user.getEmail());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_FIRST_NAME, user.getFirstName());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_LAST_NAME, user.getLastName());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_DISPLAY_NAME, user.getDisplayName());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_PHONE_NUMBER, user.getPhoneNumber());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_PHOTO_URL, user.getAvatar());
        SharedPreferencesManager.getInstance(context).putInt(Constants.PREF_CURRENT_USER_NUMBER_FOLLOW_POST, countPostsFollowing);
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_HAS_PAYMENT_METHOD, user.isHasPaymentMethod());
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_IS_VERIFIED_EMAIL, user.isVerifiedEmail());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_CREATE_STRIPE_ACCOUNT_URL, user.getUrlCreateStripeAccount());
    }

    public static void removeCurrentUserInfoFromPreferences(Context context) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_TOKEN_ID, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_ID, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_USERNAME, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_EMAIL, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_FIRST_NAME, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_LAST_NAME, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_DISPLAY_NAME, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_PHONE_NUMBER, "");
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_PHOTO_URL, "");
        SharedPreferencesManager.getInstance(context).putInt(Constants.PREF_CURRENT_USER_NUMBER_FOLLOW_POST, 0);
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_HAS_PAYMENT_METHOD, false);
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_PAYMENT_METHOD, "");
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_IS_VERIFIED_EMAIL, false);
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_CREATE_STRIPE_ACCOUNT_URL, "");
    }


    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size.x;
    }

    private static Float getScreenWidthInPixel(Context context) {
        return (float) context.getResources().getDisplayMetrics().widthPixels;
    }

    private static Float getScreenHeightInPixel(Context context) {
        return (float) context.getResources().getDisplayMetrics().heightPixels;
    }

    public static Float getScreenRatio(Context context) {
        float width = getScreenWidthInPixel(context);
        float height = getScreenHeightInPixel(context);
        return width / height;
    }


    @SuppressLint("MissingPermission")
    public static boolean isNetworkConnectionAvailable() {
        ConnectivityManager conMgr = (ConnectivityManager) MainApplication
                .getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo != null) {
            if (netInfo.getType() == ConnectivityManager.TYPE_WIFI
                    || netInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                return netInfo.isConnected();
            }
        }
        return false;
    }

    public static boolean isValidHashTag(CharSequence hashTag) {
        return HASHTAG_PATTERN.matcher(hashTag).matches();
    }

    public static void setDefaultAvatar(Context context, CircleImageView ivDefaultAvatar, GradientTextView tvDefaultAvatar, CircleImageView ivAvatar, User user, int textSize) {
        ivDefaultAvatar.setVisibility(View.VISIBLE);
        tvDefaultAvatar.setVisibility(View.VISIBLE);
        tvDefaultAvatar.setText(getDefaultAvatarText(user));
        tvDefaultAvatar.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(textSize));
        ivAvatar.setVisibility(View.GONE);
    }

    public static void setHideDefaultAvatar(Context context, CircleImageView ivDefaultAvatar, GradientTextView tvDefaultAvatar, CircleImageView ivAvatar) {
        ivDefaultAvatar.setVisibility(View.GONE);
        tvDefaultAvatar.setText("");
        ivAvatar.setVisibility(View.VISIBLE);
        ivAvatar.setBorderGradientStart(context.getResources().getColor(android.R.color.transparent));
        ivAvatar.setBorderGradientEnd(context.getResources().getColor(android.R.color.transparent));
        ivAvatar.setBorderWidth(0);
    }

    public static String getDefaultAvatarText(User user) {
        if (user != null) {
//            String firstName = user.getFirstName();
//            String lastName = user.getLastName();
            StringBuilder stringBuilder = new StringBuilder();
//            if (!TextUtils.isEmpty(firstName)) {
//                stringBuilder.append(firstName.substring(0, 1));
//            }
//            if (!TextUtils.isEmpty(lastName)) {
//                stringBuilder.append(lastName.substring(0, 1));
//            }
//
//            if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(user.getDisplayName())) {
//                stringBuilder.append(user.getDisplayName().substring(0, 1));
//            }

            if (!TextUtils.isEmpty(user.getDisplayName())) {
                stringBuilder.append(user.getDisplayName().substring(0, 1));
            }

            return stringBuilder.toString().toUpperCase();
        }
        return "";
    }

    public static String getUIIDDevice() {
//        try {
//            String UIIDDevice = getMACAddress("wlan0");
//            if (UIIDDevice.equals("")) {
//                UIIDDevice = getMACAddress("eth0");
//            }
//            if (!UIIDDevice.equals("")) {
//                return UIIDDevice;
//            } else {
//                String android_id = Settings.Secure.getString(MainApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//                return android_id;
//            }
//        } catch (Exception ex) {
//            String android_id = Settings.Secure.getString(MainApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//            return android_id;
//        }
        return Settings.Secure.getString(MainApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);

    }

    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
        }
        return "";

    }

    public static void spanAndCheckContentUsername(Context context, String username, String content, boolean isOnline, TextView tvShow, ClickableSpan clickableSpanUsername, int colorHeader) {
        if (username != null) {
            String displayUsername = username;
            if (username.length() > Constants.LIMIT_DISPLAY_NAME_LENGTH) {
                displayUsername = username.substring(0, Constants.LIMIT_DISPLAY_NAME_LENGTH - Constants.STRING_ELLIPS_END.length())
                        .concat(Constants.STRING_ELLIPS_END);
            }

            spanContentUsername(context, displayUsername, content, isOnline, tvShow, clickableSpanUsername, colorHeader);
        }
    }
    public static void spanContentUsername(Context context, String username, String content, boolean isOnline, TextView tvShow, ClickableSpan clickableSpanUsername, int colorHeader) {
        String separator = "    ";
        String itemValue = username + separator + content;
        SpannableStringBuilder SS = new SpannableStringBuilder(itemValue);

        SS.setSpan(clickableSpanUsername, 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SS.setSpan(new ForegroundColorSpan(colorHeader), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SS.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_semibold)), 0, username.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_regular)), username.length(), itemValue.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        if (isOnline) {
            //Add icon tick online
            Drawable dr = context.getResources().getDrawable(R.drawable.ic_verified);
            Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
            Drawable drawable = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, 17, 17, true));
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            ImageSpan span = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
            SS.setSpan(span, username.length() + 1, username.length() + 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        tvShow.setText(SS);
        tvShow.setMovementMethod(LinkMovementMethod.getInstance());

        //Show hashtag
        Linkify.addLinks(tvShow, Utils.HASH_TAG_MATCHER, null);
        tvShow.setLinkTextColor(context.getResources().getColor(R.color.md_blue_500));
        Utils.stripUnderlines(context, tvShow);

        tvShow.post(() -> {
            if (tvShow.getLineCount() > tvShow.getMaxLines()) {
                // this returns _1 past_ the index of the last character shown
                // on the indicated line. the lines are zero indexed, so the last
                // valid line is maxLines -1;
                int lastCharShown = tvShow.getLayout().getLineVisibleEnd(tvShow.getMaxLines() - 1);
                // chop off some characters. this value is arbitrary, i chose 3 just
                // to be conservative.
                int numCharsToChop = 3;
                String truncatedText = tvShow.getText().toString().substring(0, lastCharShown - numCharsToChop) + "...";

                if (truncatedText.length() >= username.length() + separator.length()) {
                    spanContentUsername(context, username, truncatedText.substring(username.length() + separator.length()), isOnline, tvShow, clickableSpanUsername, colorHeader);
                }
            }
        });
    }

    public static void stripUnderlines(Context context, TextView textView) {
        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span : spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            ClickableSpan clickableSpanHashTag = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    // TODO add check if tv.getText() instanceof Spanned
                    Spanned s = (Spanned) ((TextView) textView).getText();
                    int start = s.getSpanStart(this);
                    int end = s.getSpanEnd(this);
                    Activity activity = (Activity) context;
                    if (activity instanceof FragmentActivity) {
                        SearchDialog dialog = new SearchDialog();
                        dialog.setTextSearch(s.subSequence(start, end).toString().substring(1));
                        dialog.show(((FragmentActivity) activity).getSupportFragmentManager(), "Search");
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            s.setSpan(clickableSpanHashTag, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    private static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    public static Realm getCartRealm(Context context) {
        if (mCartRealm == null || mCartRealm.isClosed()) {
            mCartRealm = Realm.getInstance(getCartRealmConfiguration(context));
        }
        return mCartRealm;
    }

    public static RealmConfiguration getCartRealmConfiguration(Context context) {
        return new RealmConfiguration.Builder(context)
                .name(Constants.REALM_MY_CART)
                .deleteRealmIfMigrationNeeded()
                .build();
    }

    public static String getDisplayPrice(float price) {
        return "S$" + Float.valueOf(price).toString().replaceAll("\\.?0*$", "");
    }

    public static String getCurrentUserId(Context context) {
        return SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
    }

    public static String getCurrentUserEmail(Context context) {
        return SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_EMAIL, "");
    }

    public static void saveUserEmailAndName(Context context, User user) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_EMAIL, user.getEmail());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_DISPLAY_NAME, user.getDisplayName());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_FIRST_NAME, user.getFirstName());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_LAST_NAME, user.getLastName());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_USERNAME, user.getUsername());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_PHOTO_URL, user.getAvatar());
    }

    public static void savePaymentMethodId(Context context, String paymentMethodId) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_PAYMENT_METHOD, paymentMethodId);
    }

    public static String getPaymentMethodId(Context context) {
        return SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_PAYMENT_METHOD, "");
    }

    public static void saveHasPaymentMethod(Context context, Boolean hasPaymentMethod) {
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_HAS_PAYMENT_METHOD, hasPaymentMethod);
    }

    public static Boolean hasPaymentMethod(Context context) {
        return SharedPreferencesManager.getInstance(context).getBoolean(Constants.PREF_CURRENT_USER_HAS_PAYMENT_METHOD, false);
    }

    public static String getCreateStripeAccountUrl(Context context) {
        return SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_CREATE_STRIPE_ACCOUNT_URL, "");
    }

    public static boolean hasEmail(Context context) {
        String email = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_EMAIL, "");
        return !TextUtils.isEmpty(email);
    }

    public static void saveEmailStatus(Context context, boolean isVerify) {
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_IS_VERIFIED_EMAIL, isVerify);
    }

    public static boolean isActiveEmail(Context context) {
        return SharedPreferencesManager.getInstance(context).getBoolean(Constants.PREF_CURRENT_USER_IS_VERIFIED_EMAIL, false);
    }

    public static void saveCreateStripeAccountUrl(Context context, String createStripeAccountUrl) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_CREATE_STRIPE_ACCOUNT_URL, createStripeAccountUrl);
    }

    public static void saveRed5ProConfig(Context context, Red5ProConfig red5ProConfig) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_RED5PRO_LICENSE_KEY, red5ProConfig.getLicenseKey());
    }

    public static void savePurchase(Context context, String purchase, String key) {
        SharedPreferencesManager.getInstance(context).putString(key, purchase);
    }

    public static void saveEmailAndPaymentInfo(Context context, User user) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_EMAIL, user.getEmail());
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_IS_VERIFIED_EMAIL, user.isVerifiedEmail());
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_CREATE_STRIPE_ACCOUNT_URL, user.getUrlCreateStripeAccount());
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_HAS_PAYMENT_METHOD, user.isHasPaymentMethod());
    }

    public static void removeEmailAndPaymentInfo(Context context) {
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_EMAIL, "");
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_IS_VERIFIED_EMAIL, false);
        SharedPreferencesManager.getInstance(context).putString(Constants.PREF_CURRENT_USER_CREATE_STRIPE_ACCOUNT_URL, "");
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_CURRENT_USER_HAS_PAYMENT_METHOD, false);
    }

    public static void saveNotificationSetting(Context context, NotificationsConfig notificationsConfig) {
        if (notificationsConfig != null) {
            SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_NOTIFICATION_LIVE_STREAM, notificationsConfig.isNotifyLiveStream());
            SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_NOTIFICATION_MESSAGE, notificationsConfig.isNotifyMessage());
            SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_NOTIFICATION_SYSTEM, notificationsConfig.isNotifySystem());
        }
    }

    public static void resetNotificationSetting(Context context) {
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_NOTIFICATION_LIVE_STREAM, false);
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_NOTIFICATION_MESSAGE, false);
        SharedPreferencesManager.getInstance(context).putBoolean(Constants.PREF_NOTIFICATION_SYSTEM, false);
    }

    public static Purchase getPurchase(Context context, String key) {
        Gson gson = new Gson();
        String json = SharedPreferencesManager.getInstance(context).getString(key, "");
        return gson.fromJson(json, Purchase.class);
    }

    @ColorInt
    public static int getColor(Context context, @ColorRes int colorId) {
        return ContextCompat.getColor(context, colorId);
    }

    public static MessageContentModel parseMessageContent(String content) {
        MessageContentModel messageContentModel;
        try {
            messageContentModel = new Gson().fromJson(content, MessageContentModel.class);
        } catch (Exception ex) {
            messageContentModel = new MessageContentModel(content, MessageType.TEXT.getType());
        }

        return messageContentModel;
    }

    public static int getWidthScreen(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getNewHeightScreenByRatio(int oldWidth, int oldHeight, int newWidth) {
        return newWidth * oldHeight / oldWidth;
    }

    public static void openUserDetail(Context context, String userId) {
        String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (userId.equals(currentUserId)) {
            if (!(context instanceof MainActivity)) {
                Intent intent = new Intent(context, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
            context.sendBroadcast(new Intent(MainActivity.BroadcastActionGoToProfileTab));
        } else {
            openUserDetailOrder(context, userId);
        }
    }

    public static void openUserDetailOrder(Context context, String userId) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, userId);
        intent.putExtras(args);
        // Sender usage
        DetailType.PROFILE.attachTo(intent);
        context.startActivity(intent);
    }
}
