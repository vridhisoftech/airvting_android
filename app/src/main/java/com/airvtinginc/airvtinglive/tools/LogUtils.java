package com.airvtinginc.airvtinglive.tools;

import android.util.Log;

import com.airvtinginc.airvtinglive.BuildConfig;

public class LogUtils {
    public static void debug(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }

    public static void error(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, message);
        }
    }
}
