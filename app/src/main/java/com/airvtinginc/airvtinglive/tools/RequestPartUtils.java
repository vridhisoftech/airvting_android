package com.airvtinginc.airvtinglive.tools;

import android.graphics.Bitmap;
import android.text.TextUtils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RequestPartUtils {
    public static RequestBody createFromObject(Object value) {
        if (value == null) {
            return createEmptyRequest();
        }
        return RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value));
    }

    public static RequestBody createEmptyRequest() {
        return RequestBody.create(null, new byte[0]);
    }


    public static RequestBody createFromBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return createEmptyRequest();
        }
        byte[] byteArrays = ImageUtils.convertBitmapToByteArray(bitmap);
        return RequestBody.create(MediaType.parse("image/*"), byteArrays);
    }

    public static RequestBody createFromFile(File file) {
        if (file == null) {
            return createEmptyRequest();
        }
        return RequestBody.create(MediaType.parse("multipart/form-data"), file);
    }

    public static MultipartBody.Part createImagePart(String partName, Bitmap bitmap, String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            fileName = "default.jpg";
        }
        if (bitmap == null) {
            partName = "";
        }
        // create RequestBody instance from bitmap
        RequestBody requestImage = createFromBitmap(bitmap);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, fileName,
                requestImage);
    }

    public static MultipartBody.Part createImagePartUpdate(String partName, Bitmap bitmap) {
        String fileName = "default.jpg";
        RequestBody requestImage;
        if (bitmap == null) {
            requestImage = createEmptyRequest();
        } else {
            // create RequestBody instance from bitmap
            requestImage = createFromBitmap(bitmap);
        }

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, fileName,
                requestImage);
    }

    public static MultipartBody.Part createEmptyPart(String partName) {
        String fileName = "default.jpg";
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, fileName,
                createEmptyRequest());
    }

    public static MultipartBody.Part createFilePart(String partName, File file, String fileName) {
        // create RequestBody instance from bitmap
        if (file == null) {
            partName = "";
        }
        RequestBody requestFile = createFromFile(file);
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, fileName,
                requestFile);
    }

    public static MultipartBody.Part createFilePart(String partName, String path, String fileName) {
        File file = null;
        if (!TextUtils.isEmpty(path)) {
            file = new File(path);
            if (TextUtils.isEmpty(fileName)) {
                fileName = file.getName();
            }
        }
        return createFilePart(partName, file, fileName);
    }
}
