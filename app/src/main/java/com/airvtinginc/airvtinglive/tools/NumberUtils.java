package com.airvtinginc.airvtinglive.tools;

import android.text.Editable;
import android.widget.EditText;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberUtils {
    public static final String NUMBER_FORMAT = "#,###,###,###";
    public static final String PRICE_FORMAT = "#,###,###,##0.00";

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public static String formatPrice(float price) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
        return "S" + numberFormat.format(price);
    }

    public static String formatPriceByCurrency(float price, String currency) {
        DecimalFormat formatter = new DecimalFormat(PRICE_FORMAT);
        String format = formatter.format(price);
        return currency + " " + format;
    }

    public static void formatPriceEditText(Editable editable, EditText editText, int cursorPosition, boolean isShowCurrency) {
        try {
            String originalString = editable.toString();
            String decimal = "";
            String currency = "S" + '\u0024';
            if (isShowCurrency) {
                originalString = originalString.replace("S", "")
                        .replace("$", "");
            }

            if (originalString.contains(",")) {
                originalString = originalString.replaceAll(",", "");
            }

            if (originalString.contains(".")) {
                decimal = originalString.substring(originalString.indexOf("."));
                originalString = originalString.substring(0, originalString.indexOf("."));
            }

            Long longVal = Long.parseLong(originalString);

            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern(NUMBER_FORMAT);
            String formattedString = formatter.format(longVal);

            //setting text after format to EditText
            String displayPrice = formattedString + decimal;
            if (isShowCurrency) {
                displayPrice = currency + displayPrice;
            }
            editText.setText(displayPrice);

            if (cursorPosition <= currency.length() && isShowCurrency) {
                editText.setSelection(editText.getText().length());
            } else if (cursorPosition <= (currency.length() + formattedString.length()) && isShowCurrency) {
                editText.setSelection(currency.length() + formattedString.length());
            } else {
                editText.setSelection(editText.getText().length());
            }
        } catch (NumberFormatException e) {
            AppLog.e(e.getMessage());
        }
    }

//    public static void formatPriceDiscountAmount(Editable editable, EditText editText, boolean isShowCurrency) {
//        try {
//            String originalString = editable.toString();
//            if (isShowCurrency) {
//                originalString = originalString.replace("S", "")
//                        .replace("$", "");
//            }
//
//            if (originalString.contains(",")) {
//                originalString = originalString.replaceAll(",", "");
//            }
//            Long longVal = Long.parseLong(originalString);
//
//            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
//            formatter.applyPattern(NUMBER_FORMAT);
//            String formattedString = formatter.format(longVal);
//
//            //setting text after format to EditText
//            if (isShowCurrency) {
//                String displayPrice = "S$" + formattedString;
//                editText.setText(displayPrice);
//            } else {
//                editText.setText(formattedString);
//            }
//            editText.setSelection(editText.getText().length());
//        } catch (NumberFormatException e) {
//            AppLog.e(e.getMessage());
//        }
//    }

    public static String formatPriceEditText(float number) {
        DecimalFormat formatter = new DecimalFormat(PRICE_FORMAT);
        String format = formatter.format(number);
        return format;
    }

    public static float getPriceFromString(String price) {
        price = price.replace("S", "")
                .replace("$", "")
                .replace(",", "")
                .replace("\u0024", "");
        return Float.parseFloat(price);
    }
}
