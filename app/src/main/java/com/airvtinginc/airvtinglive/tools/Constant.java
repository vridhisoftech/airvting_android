package com.airvtinginc.airvtinglive.tools;

import android.os.Environment;

/**
 * Created by xuan on 7/11/18.
 */

public class Constant {
    public static final String AIRVTING_FOLDER = Environment.getExternalStorageDirectory() + "/airvting";
}
