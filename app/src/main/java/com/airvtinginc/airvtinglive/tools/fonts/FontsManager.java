package com.airvtinginc.airvtinglive.tools.fonts;

import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;


import com.airvtinginc.airvtinglive.gui.components.CustomFontTextView;

import static android.graphics.Typeface.BOLD;

public class FontsManager {
    public static Typeface defaultHeaderTypeface;
    public static Typeface defaultTypeface;

    static {
        defaultTypeface = null;
        defaultHeaderTypeface = null;
    }

    public static Typeface getDefaultTypeface() {
        return defaultTypeface;
    }

    public static void init(Typeface typeface, Typeface typeFaceBold) {
        if (typeface == null) {
            //TLog.m19e("FontsManagerExceptiontypeface is null");
            throw new IllegalStateException("typeface name");
        }
        defaultTypeface = typeface;
        defaultHeaderTypeface = typeFaceBold;
    }

    public static void changeFonts(View view) {
        if (defaultTypeface == null) {
            //TLog.m19e("FontsManagerExceptionFontsManagerException");
            throw new IllegalStateException("FontsManagerException");
        } else {
            changeFonts(view, defaultTypeface);
        }
    }

    public static void changeFonts(ViewGroup viewGroup, Typeface typeface) {
        int i = 0;
        while (i < viewGroup.getChildCount()) {
            try {
                changeFonts(viewGroup.getChildAt(i), typeface);
                i++;
            } catch (Exception e) {
                return;
            }
        }
    }

    public static void changeFonts(View view, Typeface typeface) {
        try {
            if (view instanceof ViewGroup) {
                changeFonts((ViewGroup) view, typeface);
            } else if (view instanceof CustomFontTextView) {
                return;
            } else if (view instanceof TextView) {
                if (((TextView) view).getTypeface() != null) {
                    if (((TextView) view).getTypeface().isItalic() && defaultHeaderTypeface != null) {
                        ((TextView) view).setTypeface(defaultHeaderTypeface);
                        return;
                    }
                    if (((TextView) view).getTypeface().isBold()) {
                        ((TextView) view).setTypeface(typeface, BOLD);
                        return;
                    }
                }
                ((TextView) view).setTypeface(typeface);
            } else if (view instanceof Button) {
                if (((Button) view).getTypeface() != null) {
                    if (((Button) view).getTypeface().isItalic() && defaultHeaderTypeface != null) {
                        ((Button) view).setTypeface(defaultHeaderTypeface);
                        return;
                    }
                    if (((Button) view).getTypeface().isBold()) {
                        ((Button) view).setTypeface(typeface, BOLD);
                        return;
                    }
                }
                ((Button) view).setTypeface(typeface);
            } else if (view instanceof Switch) {
                if (((Switch) view).getTypeface() != null) {
                    if (((Switch) view).getTypeface().isItalic() && defaultHeaderTypeface != null) {
                        ((Switch) view).setTypeface(defaultHeaderTypeface);
                        return;
                    }
                    if (((Switch) view).getTypeface().isBold()) {
                        ((Switch) view).setTypeface(typeface, BOLD);
                        return;
                    }
                }
                ((Switch) view).setTypeface(typeface);
            } else if (view instanceof EditText) {
                if (((EditText) view).getTypeface() != null) {
                    if (((EditText) view).getTypeface().isItalic() && defaultHeaderTypeface != null) {
                        ((EditText) view).setTypeface(defaultHeaderTypeface);
                        return;
                    }
                    if (((EditText) view).getTypeface().isBold()) {
                        ((EditText) view).setTypeface(typeface, BOLD);
                        return;
                    }
                }
                ((EditText) view).setTypeface(typeface);
            }
        } catch (Exception e) {
        }
    }

    public static void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", defaultTypeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public static void applyFontToActionBar(ActionBar actionBar) {
        SpannableString mNewTitle = new SpannableString(Html.fromHtml("<small>" + actionBar.getTitle() + "</small>"));
        mNewTitle.setSpan(new CustomTypefaceSpan("", defaultTypeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        actionBar.setTitle(mNewTitle);
    }

    public static SpannableString getSpannableString(String text) {
        SpannableString mNewTitle = new SpannableString(text);
        mNewTitle.setSpan(new CustomTypefaceSpan("", defaultTypeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return mNewTitle;
    }

    public static void changeTabsFont(TabLayout tabLayout) {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(defaultTypeface, BOLD);
                }
            }
        }
    }
}
