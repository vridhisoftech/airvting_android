package com.airvtinginc.airvtinglive.tools;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

import com.airvtinginc.airvtinglive.R;

import java.util.regex.Pattern;

public class Validator {


    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static Validator mInstance;

    public synchronized static Validator getInstance() {
        if (mInstance == null) {
            synchronized (Validator.class) {
                if (mInstance == null) {
                    mInstance = new Validator();
                }
            }
        }
        return mInstance;
    }

    public boolean isCheckEmail(Context context, EditText edtEmail) {
        boolean result = true;
        if (TextUtils.isEmpty(edtEmail.getText())) {
            edtEmail.setError(context.getString(R.string.validator_error_email_required));
            result = false;
        } else if (!Validator.getInstance().isEmailValid(edtEmail.getText().toString())) {
            edtEmail.setError(context.getString(R.string.validator_error_invalid_email));
            result = false;
        }
        return result;
    }

    public boolean isCheckPassword(Context context, EditText edtPassword) {
        boolean result = true;
        if (TextUtils.isEmpty(edtPassword.getText())) {
            edtPassword.setError(context.getString(R.string.validator_error_password_required));
            result = false;
        } else if (!Validator.getInstance().isPasswordValid(edtPassword.getText().toString())) {
            edtPassword.setError(context.getString(R.string.validator_error_invalid_password));
            result = false;
        }
        return result;
    }

    public boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 6;
    }

    public boolean isEmailValid(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        return pattern.matcher(email).matches();
    }
}
