package com.airvtinginc.airvtinglive.tools.fonts;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.Log;

@SuppressLint({"DefaultLocale"})
public class ActionBarHelper {

    private static class TypefaceSpan extends MetricAffectingSpan {
        Typeface typeface;

        TypefaceSpan(Typeface typeface) {
            this.typeface = typeface;
        }

        public void updateMeasureState(TextPaint p) {
            p.setTypeface(this.typeface);
            p.setFlags(p.getFlags() | NotificationCompat.PRIORITY_HIGH);
        }

        public void updateDrawState(TextPaint tp) {
            tp.setTypeface(this.typeface);
            tp.setFlags(tp.getFlags() | NotificationCompat.PRIORITY_HIGH);
        }
    }

    public static void changeActionBarFonts(ActionBar actionBar, Typeface typeface) {
        if (typeface == null) {
            Log.e("ActioHelper", "activity");
            return;
        }
        try {
            setTitle(actionBar, typeface, actionBar.getTitle().toString());
        } catch (Exception e) {
            Log.e("ActioHelper", e.toString());
        }
    }

    public static void setTitle(ActionBar actionBar, Typeface typeface, String title) {
        if (typeface == null || actionBar == null) {
            Log.e("ActioHelper", "typefacactionbar");
            return;
        }
        SpannableString sp = new SpannableString(title);
        sp.setSpan(new TypefaceSpan(typeface), 0, sp.length(), 33);
        setTitle(actionBar, sp);
    }

    public static void setTitle(ActionBar actionBar, SpannableString spannableString) {
        if (VERSION.SDK_INT == 16 && Build.MANUFACTURER.toUpperCase().equals("LGE")) {
            actionBar.setTitle(spannableString.toString());
        } else {
            actionBar.setTitle((CharSequence) spannableString);
        }
    }
}
