package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.gui.enums.MediaType;

public class Media {
    private String id;
    private String name;
    private String imageUrl;
    private MediaType type;
    private boolean isLiked;

    public Media(String id, String name, String imageUrl, MediaType type, boolean isLiked) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.type = type;
        this.isLiked = isLiked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public MediaType getType() {
        return type;
    }

    public void setType(MediaType type) {
        this.type = type;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }
}
