package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class CategoryProductRequest {
    @SerializedName("categoryId")
    private String categoryId;

    @SerializedName("paginate")
    private int paginate;

    @SerializedName("perPage")
    private int perPage;

    @SerializedName("maxId")
    private String maxId;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getPaginate() {
        return paginate;
    }

    public void setPaginate(int paginate) {
        this.paginate = paginate;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }
}
