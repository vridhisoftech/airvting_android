package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PaymentDetailResponse implements Serializable {
    @SerializedName("paymentDetail")
    private List<PaymentDetails> paymentDetailList;

    @SerializedName("paymentMethodDetail")
    private PaymentDetails paymentDetails;

    public List<PaymentDetails> getPaymentDetailList() {
        return paymentDetailList;
    }

    public void setPaymentDetailList(List<PaymentDetails> paymentDetailList) {
        this.paymentDetailList = paymentDetailList;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
}
