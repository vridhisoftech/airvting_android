package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class BookmarkPostRequest {
    @SerializedName("postId")
    private String postId;

    public BookmarkPostRequest(String postId) {
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
