package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.models.AirToken;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AirTokenListResponse {
    @SerializedName("airTokenOfUser")
    private int airTokenOfUser;
    @SerializedName("airTokenDetail")
    private List<AirToken> airTokenList;

    public int getAirTokenOfUser() {
        return airTokenOfUser;
    }

    public void setAirTokenOfUser(int airTokenOfUser) {
        this.airTokenOfUser = airTokenOfUser;
    }

    public List<AirToken> getAirTokenList() {
        return airTokenList;
    }

    public void setAirTokenList(List<AirToken> airTokenList) {
        this.airTokenList = airTokenList;
    }
}
