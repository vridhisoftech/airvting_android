package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

public class GiftDetail {
    @SerializedName("giftId")
    private String giftId;
    @SerializedName("title")
    private String title;
    @SerializedName("airToken")
    private int airToken;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("quantity")
    private int quantity; // quantity of gift which own by user - API My Gift
    @SerializedName("countGiftsOfUser")
    private int countGiftsOfUser; // quantity of gift which own by user - API Gift Store
    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("createdAt")
    private String createdAt;

    public String getGiftId() {
        return giftId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAirToken() {
        return airToken;
    }

    public void setAirToken(int airToken) {
        this.airToken = airToken;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCountGiftsOfUser() {
        return countGiftsOfUser;
    }

    public void setCountGiftsOfUser(int countGiftsOfUser) {
        this.countGiftsOfUser = countGiftsOfUser;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
