package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProductCategory extends RealmObject {
    @PrimaryKey
    @SerializedName("categoryId")
    private String categoryId;
    @SerializedName("title")
    private String title;

    public ProductCategory() {

    }

    public ProductCategory(String categoryId, String title) {
        this.categoryId = categoryId;
        this.title = title;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
