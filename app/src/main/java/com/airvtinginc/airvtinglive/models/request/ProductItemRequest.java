package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductItemRequest implements Serializable {
    @SerializedName("productId")
    private String productId;

    @SerializedName("title")
    private String title;

    @SerializedName("price")
    private float price;

    @SerializedName("discount")
    private String discount;

    @SerializedName("featuredImage")
    private String featuredImage;

    @SerializedName("timer")
    private int timer;

    @SerializedName("quantity")
    private int quantity;

    public ProductItemRequest(String productId, String title, float price, String featuredImage) {
        this.productId = productId;
        this.title = title;
        this.price = price;
        this.featuredImage = featuredImage;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
