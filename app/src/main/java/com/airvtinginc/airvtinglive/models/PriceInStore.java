package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

public class PriceInStore {
    @SerializedName("_id")
    private String id;
    @SerializedName("price")
    private float price;
    @SerializedName("discount")
    private String discount;
    @SerializedName("priceSale")
    private float priceSale;
    @SerializedName("startedAt")
    private String startedAt;
    @SerializedName("expiredAt")
    private String expiredAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public float getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(float priceSale) {
        this.priceSale = priceSale;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }
}