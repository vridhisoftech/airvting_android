package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AirTokenExchangeResponse implements Serializable {
    @SerializedName("exchangeDollaToAirToken")
    private int exchangeDollaToAirToken;

    public int getExchangeDollaToAirToken() {
        return exchangeDollaToAirToken;
    }

    public void setExchangeDollaToAirToken(int exchangeDollaToAirToken) {
        this.exchangeDollaToAirToken = exchangeDollaToAirToken;
    }
}
