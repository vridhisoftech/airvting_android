package com.airvtinginc.airvtinglive.models.response;


import com.google.gson.annotations.SerializedName;

public class CommentItem {
    @SerializedName("_id")
    private String id;

    @SerializedName("owner")
    private Sender user;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("comment")
    private String comment;

    public CommentItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Sender getUser() {
        return user;
    }

    public void setUser(Sender user) {
        this.user = user;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
