package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostResponse {
    @SerializedName("postDetail")
    private List<PostDetail> listPostDetail;
    @SerializedName("commentDetail")
    private List<CommentItem> commentDetail;
    @SerializedName("totalComments")
    private int totalComments;
    @SerializedName("categoryId")
    private String categoryId;
    @SerializedName("maxId")
    private String maxId;

    public List<PostDetail> getListPostDetail() {
        return listPostDetail;
    }

    public void setListPostDetail(List<PostDetail> listPostDetail) {
        this.listPostDetail = listPostDetail;
    }

    public List<CommentItem> getCommentDetail() {
        return commentDetail;
    }

    public void setCommentDetail(List<CommentItem> commentDetail) {
        this.commentDetail = commentDetail;
    }

    public int getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(int totalComments) {
        this.totalComments = totalComments;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }

}
