package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionItem {
    @SerializedName("_id")
    private String id;
    @SerializedName("products")
    private List<TransactionItemHistory> products;
    @SerializedName("gift")
    private GiftHistory gift;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("type")
    private String type;
    @SerializedName("totalPrice")
    private float totalPrice;
    @SerializedName("totalToken")
    private int totalToken;
    @SerializedName("totalQuantity")
    private int totalQuantity;
    @SerializedName("currency")
    private String currency;
    @SerializedName("transaction")
    private String transaction;

    public TransactionItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TransactionItemHistory> getProducts() {
        return products;
    }

    public void setProducts(List<TransactionItemHistory> products) {
        this.products = products;
    }

    public GiftHistory getGift() {
        return gift;
    }

    public void setGift(GiftHistory gift) {
        this.gift = gift;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getTotalToken() {
        return totalToken;
    }

    public void setTotalToken(int totalToken) {
        this.totalToken = totalToken;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
}
