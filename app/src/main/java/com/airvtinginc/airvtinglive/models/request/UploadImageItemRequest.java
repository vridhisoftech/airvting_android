package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import okhttp3.MultipartBody;

public class UploadImageItemRequest implements Serializable {

    @SerializedName("image")
    private List<MultipartBody.Part> image;

    public List<MultipartBody.Part> getImage() {
        return image;
    }

    public void setImage(List<MultipartBody.Part> image) {
        this.image = image;
    }
}
