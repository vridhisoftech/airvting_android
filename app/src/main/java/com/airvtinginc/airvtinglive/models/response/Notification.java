package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.models.PostInNotification;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.google.gson.annotations.SerializedName;

public class Notification {
    @SerializedName("_id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("receiverId")
    private String receiverId;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("notifyMessage")
    private String notifyMessage;
    @SerializedName("isRead")
    private boolean isRead;
    @SerializedName("notifier")
    private User notifier;
    @SerializedName("product")
    private ProductInPost product;
    @SerializedName("post")
    private PostInNotification post;
    @SerializedName("message")
    private Message message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getNotifyMessage() {
        return notifyMessage;
    }

    public void setNotifyMessage(String notifyMessage) {
        this.notifyMessage = notifyMessage;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public User getNotifier() {
        return notifier;
    }

    public void setNotifier(User notifier) {
        this.notifier = notifier;
    }

    public ProductInPost getProduct() {
        return product;
    }

    public void setProduct(ProductInPost product) {
        this.product = product;
    }

    public PostInNotification getPost() {
        return post;
    }

    public void setPost(PostInNotification post) {
        this.post = post;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
