package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FeaturedImage extends RealmObject{
    @PrimaryKey
    @SerializedName("featuredImage")
    private String featuredImage;

    public FeaturedImage() {

    }

    public FeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }
}
