package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SearchUserModelRequest implements Serializable {
    @SerializedName("username")
    private String username;

    @SerializedName("paginate")
    private int paginate;

    @SerializedName("maxId")
    private String maxId;

    @SerializedName("perPage")
    private int perPage;

    @SerializedName("type")
    private String type;

    @SerializedName("userId")
    private String userId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPaginate() {
        return paginate;
    }

    public void setPaginate(int paginate) {
        this.paginate = paginate;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
