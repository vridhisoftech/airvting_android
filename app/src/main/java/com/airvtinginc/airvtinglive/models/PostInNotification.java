package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

public class PostInNotification {
    @SerializedName("postId")
    private String postId;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("mediaUrl")
    private String mediaUrl;
    @SerializedName("description")
    private String description;
    @SerializedName("commentId")
    private String commentId;
    @SerializedName("content")
    private String content;
    @SerializedName("type")
    private String type;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
