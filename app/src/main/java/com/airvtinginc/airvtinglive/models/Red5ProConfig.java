package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

public class Red5ProConfig {
    @SerializedName("licenseKey")
    private String licenseKey;

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }
}
