package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.gui.adapter.LiveStreamHorizontalAdapter;

import java.util.List;

public class ExplorePosts {

    private String categoryId;
    private String categoryName;
    private List<PostDetail> postDetailList;
    private boolean isMoreLoading;
    private LiveStreamHorizontalAdapter liveStreamHorizontalAdapter;
    private int currentPage = 1;
    private String maxId;

    public ExplorePosts(String categoryId, String categoryName, String maxId, List<PostDetail> postDetailList) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.maxId = maxId;
        this.postDetailList = postDetailList;
        this.isMoreLoading = false;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<PostDetail> getPostDetailList() {
        return postDetailList;
    }

    public LiveStreamHorizontalAdapter getLiveStreamHorizontalAdapter() {
        return liveStreamHorizontalAdapter;
    }

    public void setLiveStreamHorizontalAdapter(LiveStreamHorizontalAdapter liveStreamHorizontalAdapter) {
        this.liveStreamHorizontalAdapter = liveStreamHorizontalAdapter;
    }

    public boolean isMoreLoading() {
        return isMoreLoading;
    }

    public void setMoreLoading(boolean moreLoading) {
        isMoreLoading = moreLoading;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }
}
