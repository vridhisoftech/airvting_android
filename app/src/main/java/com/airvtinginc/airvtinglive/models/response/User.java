package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {
    @SerializedName("_id")
    private String id;
    @SerializedName("userId")
    private String userId;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("email")
    private String email;
    @SerializedName("birth")
    private String birthday;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("displayName")
    private String displayName;
    @SerializedName("gender")
    private int gender;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("socialType")
    private String socialType;
    @SerializedName("featuredImage")
    private String avatar;
    @SerializedName("coverImage")
    private String cover;
    @SerializedName("description")
    private String description;
    @SerializedName("airToken")
    private int airToken;
    @SerializedName("isVerified")
    private boolean isVerifiedEmail;
    @SerializedName("location")
    private Location location;
    @SerializedName("posts")
    private int numberOfPosts;
    @SerializedName("followers")
    private int numberOfFollowers;
    @SerializedName("following")
    private int numberOfFollowing;
    @SerializedName("isFollow")
    private boolean isFollow;
    @SerializedName("isLive")
    private boolean isLive;
    @SerializedName("isOnline")
    private boolean isOnline;
    @SerializedName("photos")
    private List<String> photos;
    @SerializedName("schedules")
    private Schedule schedule;
    @SerializedName("hasPaymentMethod")
    private boolean hasPaymentMethod;
    @SerializedName("tokenId")
    private String fcmToken;
    @SerializedName("urlCreateStripeAccount")
    private String urlCreateStripeAccount;
    @SerializedName("configs")
    private ConfigSettings configSettings;

    public User(String firstName, String avatar, boolean isLive) {
        this.firstName = firstName;
        this.avatar = avatar;
        this.isLive = isLive;
    }

    public User(String firstName, String avatar, String description, List<String> photos) {
        this.firstName = firstName;
        this.avatar = avatar;
        this.description = description;
        this.photos = photos;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSocialType() {
        return socialType;
    }

    public void setSocialType(String socialType) {
        this.socialType = socialType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAirToken() {
        return airToken;
    }

    public void setAirToken(int airToken) {
        this.airToken = airToken;
    }

    public boolean isVerifiedEmail() {
        return isVerifiedEmail;
    }

    public void setVerifiedEmail(boolean verifiedEmail) {
        isVerifiedEmail = verifiedEmail;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    public int getNumberOfPosts() {
        return numberOfPosts;
    }

    public void setNumberOfPosts(int numberOfPosts) {
        this.numberOfPosts = numberOfPosts;
    }

    public int getNumberOfFollowers() {
        return numberOfFollowers;
    }

    public void setNumberOfFollowers(int numberOfFollowers) {
        this.numberOfFollowers = numberOfFollowers;
    }

    public int getNumberOfFollowing() {
        return numberOfFollowing;
    }

    public void setNumberOfFollowing(int numberOfFollowing) {
        this.numberOfFollowing = numberOfFollowing;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    public boolean isLive() {
        return isLive;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public void setLive(boolean live) {
        isLive = live;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isHasPaymentMethod() {
        return hasPaymentMethod;
    }

    public void setHasPaymentMethod(boolean hasPaymentMethod) {
        this.hasPaymentMethod = hasPaymentMethod;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getUrlCreateStripeAccount() {
        return urlCreateStripeAccount;
    }

    public void setHasPaymentMethod(Boolean hasPaymentMethod) {
        this.hasPaymentMethod = hasPaymentMethod;
    }

    public ConfigSettings getConfigSettings() {
        return configSettings;
    }

    public void setConfigSettings(ConfigSettings configSettings) {
        this.configSettings = configSettings;
    }
}
