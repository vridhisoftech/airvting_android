package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LeftMenuResponse implements Serializable {
    @SerializedName("totalUnReadCountMessages")
    private int totalUnReadCountMessages;
    @SerializedName("totalUnReadCountNotifyMessages")
    private int totalUnReadCountNotifyMessages;
    @SerializedName("totalUnReadCountActivities")
    private int totalUnReadCountActivities;

    public int getTotalUnReadCountMessages() {
        return totalUnReadCountMessages;
    }

    public void setTotalUnReadCountMessages(int totalUnReadCountMessages) {
        this.totalUnReadCountMessages = totalUnReadCountMessages;
    }

    public int getTotalUnReadCountNotifyMessages() {
        return totalUnReadCountNotifyMessages;
    }

    public void setTotalUnReadCountNotifyMessages(int totalUnReadCountNotifyMessages) {
        this.totalUnReadCountNotifyMessages = totalUnReadCountNotifyMessages;
    }

    public int getTotalUnReadCountActivities() {
        return totalUnReadCountActivities;
    }

    public void setTotalUnReadCountActivities(int totalUnReadCountActivities) {
        this.totalUnReadCountActivities = totalUnReadCountActivities;
    }
}
