package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddProductRequest {
    private String productId;

    private Map<String, RequestBody> params;

    @SerializedName("featuredImages")
    private MultipartBody.Part featuredImages0;
    @SerializedName("featuredImages")
    private MultipartBody.Part featuredImages1;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages2;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages3;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages4;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages5;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages6;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages7;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages8;
@SerializedName("featuredImages")
    private MultipartBody.Part featuredImages9;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Map<String, RequestBody> getParams() {
        return params;
    }

    public void setParams(Map<String, RequestBody> params) {
        this.params = params;
    }

    public MultipartBody.Part getFeaturedImages0() {
        return featuredImages0;
    }
    public MultipartBody.Part getFeaturedImages1() { return featuredImages1; }
    public MultipartBody.Part getFeaturedImages2() { return featuredImages2; }
    public MultipartBody.Part getFeaturedImages3() { return featuredImages3; }
    public MultipartBody.Part getFeaturedImages4() { return featuredImages4; }
    public MultipartBody.Part getFeaturedImages5() { return featuredImages5; }
    public MultipartBody.Part getFeaturedImages6() { return featuredImages6; }
    public MultipartBody.Part getFeaturedImages7() { return featuredImages7; }
    public MultipartBody.Part getFeaturedImages8() { return featuredImages8; }
    public MultipartBody.Part getFeaturedImages9() { return featuredImages9; }

    public void setFeaturedImages0(MultipartBody.Part featuredImages0) {
        this.featuredImages0 = featuredImages0;
    }
    public void setFeaturedImages1(MultipartBody.Part featuredImages1) {
        this.featuredImages1 = featuredImages1;
    } public void setFeaturedImages2(MultipartBody.Part featuredImages2) {
        this.featuredImages2 = featuredImages2;
    } public void setFeaturedImages3(MultipartBody.Part featuredImages3) {
        this.featuredImages3 = featuredImages3;
    } public void setFeaturedImages4(MultipartBody.Part featuredImages4) {
        this.featuredImages4 = featuredImages4;
    } public void setFeaturedImages5(MultipartBody.Part featuredImages5) {
        this.featuredImages5 = featuredImages5;
    } public void setFeaturedImages6(MultipartBody.Part featuredImages6) {
        this.featuredImages6 = featuredImages6;
    } public void setFeaturedImages7(MultipartBody.Part featuredImages7) {
        this.featuredImages7 = featuredImages7;
    } public void setFeaturedImages8(MultipartBody.Part featuredImages8) {
        this.featuredImages8 = featuredImages8;
    } public void setFeaturedImages9(MultipartBody.Part featuredImages9) {
        this.featuredImages9 = featuredImages9;
    }
}
