package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class TransactionItemHistory {
    @SerializedName("productId")
    private String productId;
    @SerializedName("title")
    private String title;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("price")
    private float price;
    @SerializedName("currency")
    private String currency;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("displayName")
    private String displayName;

    public TransactionItemHistory() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
