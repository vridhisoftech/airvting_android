package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class SendGiftRequest extends GiftRequest {
    @SerializedName("quantity")
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
