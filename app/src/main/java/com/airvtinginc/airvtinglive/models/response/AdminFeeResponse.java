package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class AdminFeeResponse {
    @SerializedName("feeAdmin")
    private int adminFee;

    public int getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(int adminFee) {
        this.adminFee = adminFee;
    }
}
