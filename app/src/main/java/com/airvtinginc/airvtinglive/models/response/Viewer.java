package com.airvtinginc.airvtinglive.models.response;

import com.google.firebase.firestore.PropertyName;

public class Viewer {

    private String userId;
    private String featuredImage;
    private String displayName;
    private String postId;
    private boolean isView;
    private boolean isLike;

    public Viewer() {
    }

    public Viewer(String userId, String featuredImage, String displayName, String postId, boolean isView, boolean isLike) {
        this.userId = userId;
        this.featuredImage = featuredImage;
        this.displayName = displayName;
        this.postId = postId;
        this.isView = isView;
        this.isLike = isLike;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    @PropertyName(value = "isView")
    public boolean isView() {
        return isView;
    }

    @PropertyName(value = "isView")
    public void setView(boolean isView) {
        this.isView = isView;
    }

    @PropertyName(value = "isLike")
    public boolean isLike() {
        return isLike;
    }

    @PropertyName(value = "isLike")
    public void setLike(boolean isLike) {
        this.isLike = isLike;
    }
}
