package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class LiveStream {
    @SerializedName("")
    private int userId;
    @SerializedName("")
    private String username;
    @SerializedName("")
    private String caption;
    @SerializedName("")
    private String imageUrl;
    @SerializedName("")
    private boolean isLive;
    @SerializedName("")
    private String type;
    @SerializedName("")
    private Product product;
    @SerializedName("")
    private int view;
    @SerializedName("")
    private String schedule;


    public LiveStream(String imageUrl, String username, String caption, boolean isLive, String type) {
        this.imageUrl = imageUrl;
        this.username = username;
        this.caption = caption;
        this.isLive = isLive;
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getCaption() {
        return caption;
    }

    public boolean isLive() {
        return isLive;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Product getProduct() {
        return product;
    }
}
