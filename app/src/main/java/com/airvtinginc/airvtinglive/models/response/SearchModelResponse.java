package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SearchModelResponse implements Serializable {
    @SerializedName("userDetail")
    private List<User> userList;

    @SerializedName("postDetail")
    private List<PostDetail> postDetails;

    @SerializedName("productDetail")
    private List<Product> productDetail;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<PostDetail> getPostDetails() {
        return postDetails;
    }

    public void setPostDetails(List<PostDetail> postDetails) {
        this.postDetails = postDetails;
    }

    public List<Product> getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(List<Product> productDetail) {
        this.productDetail = productDetail;
    }
}
