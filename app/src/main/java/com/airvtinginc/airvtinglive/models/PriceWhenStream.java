package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class PriceWhenStream extends RealmObject {
    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("discount")
    private String discount;
    @SerializedName("priceDiscount")
    private float priceDiscount;
    @SerializedName("startedAt")
    private String startedAt;
    @SerializedName("expiredAt")
    private String expiredAt;

    public PriceWhenStream() {

    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public float getPriceDiscount() {
        return priceDiscount;
    }

    public void setPriceDiscount(float priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }
}
