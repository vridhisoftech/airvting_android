package com.airvtinginc.airvtinglive.models.response;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.airvtinginc.airvtinglive.gui.adapter.PeopleGroupAvatarAdapter;
import com.airvtinginc.airvtinglive.gui.enums.PeopleType;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PeopleGroup extends People {
    @SerializedName("")
    private String groupId;
    @SerializedName("")
    private String groupName;
    @SerializedName("")
    private List<User> users;

    private int currentPage = 1;
    private boolean isMoreLoading;
    private RecyclerView.OnScrollListener recyclerViewOnScrollListener;
    private PeopleGroupAvatarAdapter peopleGroupAvatarAdapter;
    private LinearLayoutManager linearLayoutManager;

    public PeopleGroup(String groupName, List<User> users) {
        super.setPeopleType(PeopleType.PEOPLE_GROUP);
        this.groupName = groupName;
        this.users = users;
        this.isMoreLoading = false;
    }

    public String getGroupId() {
        return groupId;
    }


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isMoreLoading() {
        return isMoreLoading;
    }

    public void setMoreLoading(boolean moreLoading) {
        isMoreLoading = moreLoading;
    }

    public RecyclerView.OnScrollListener getRecyclerViewOnScrollListener() {
        return recyclerViewOnScrollListener;
    }

    public void setRecyclerViewOnScrollListener(RecyclerView.OnScrollListener recyclerViewOnScrollListener) {
        this.recyclerViewOnScrollListener = recyclerViewOnScrollListener;
    }

    public PeopleGroupAvatarAdapter getPeopleGroupAvatarAdapter() {
        return peopleGroupAvatarAdapter;
    }

    public void setPeopleGroupAvatarAdapter(PeopleGroupAvatarAdapter peopleGroupAvatarAdapter) {
        this.peopleGroupAvatarAdapter = peopleGroupAvatarAdapter;
    }

    public LinearLayoutManager getLinearLayoutManager() {
        return linearLayoutManager;
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }
}
