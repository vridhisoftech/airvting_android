package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.gui.enums.PeopleType;

public abstract class People {
    private PeopleType peopleType;

    public PeopleType getPeopleType() {
        return peopleType;
    }

    public void setPeopleType(PeopleType peopleType) {
        this.peopleType = peopleType;
    }
}
