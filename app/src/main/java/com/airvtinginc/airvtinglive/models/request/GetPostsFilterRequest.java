package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class GetPostsFilterRequest {
    @SerializedName("filter")
    private String filter;

    @SerializedName("typePost")
    private String[] typePost;

    @SerializedName("paginate")
    private int paginate;

    @SerializedName("perPage")
    private int perPage;

    @SerializedName("maxId")
    private String maxId;

    @SerializedName("isShowAdver")
    private boolean isShowAdver;

    public GetPostsFilterRequest(String filter, String[] typePost, int paginate, int perPage, String maxId, boolean isShowAdver) {
        this.filter = filter;
        this.typePost = typePost;
        this.paginate = paginate;
        this.perPage = perPage;
        this.maxId = maxId;
        this.isShowAdver = isShowAdver;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String[] getTypePost() {
        return typePost;
    }

    public void setTypePost(String[] typePost) {
        this.typePost = typePost;
    }

    public int getPaginate() {
        return paginate;
    }

    public void setPaginate(int paginate) {
        this.paginate = paginate;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }

    public boolean isShowAdver() {
        return isShowAdver;
    }

    public void setShowAdver(boolean showAdver) {
        isShowAdver = showAdver;
    }
}
