package com.airvtinginc.airvtinglive.models.response;

import java.util.Date;

public class Comment {

    private String userId;
    private String userName;
    private String postId;
    private String comment;
    private String type;
    private Date createdAt;
    private Date updatedAt;

    public Comment() {
    }

    public Comment(String userId, String userName, String postId, String comment, String type, Date createdAt, Date updatedAt) {
        this.userId = userId;
        this.userName = userName;
        this.postId = postId;
        this.comment = comment;
        this.type = type;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
