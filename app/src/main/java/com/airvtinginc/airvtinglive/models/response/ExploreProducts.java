package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.gui.adapter.HorizontalProductsAdapter;

import java.util.List;

public class ExploreProducts {

    private String categoryId;
    private String categoryName;
    private List<Product> productList;
    private boolean isMoreLoading;
    private HorizontalProductsAdapter horizontalProductsAdapter;
    private int currentPage = 1;
    private String maxId;

    public ExploreProducts(String categoryId, String categoryName, String maxId, List<Product> productList) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.maxId = maxId;
        this.productList = productList;
        this.isMoreLoading = false;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public HorizontalProductsAdapter getHorizontalProductsAdapter() {
        return horizontalProductsAdapter;
    }

    public void setHorizontalProductsAdapter(HorizontalProductsAdapter horizontalProductsAdapter) {
        this.horizontalProductsAdapter = horizontalProductsAdapter;
    }

    public boolean isMoreLoading() {
        return isMoreLoading;
    }

    public void setMoreLoading(boolean moreLoading) {
        isMoreLoading = moreLoading;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }
}
