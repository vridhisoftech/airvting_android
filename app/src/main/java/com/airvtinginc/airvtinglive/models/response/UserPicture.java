package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserPicture {

    @SerializedName("data")
    @Expose
    private UserPictureData userPictureData;

    public UserPictureData getUserPictureData() {
        return userPictureData;
    }

    public void setUserPictureData(UserPictureData userPictureData) {
        this.userPictureData = userPictureData;
    }
}
