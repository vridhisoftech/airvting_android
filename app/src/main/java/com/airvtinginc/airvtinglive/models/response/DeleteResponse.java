package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class DeleteResponse {
    @SerializedName("isDeleted")
    private boolean isDeleted;

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
