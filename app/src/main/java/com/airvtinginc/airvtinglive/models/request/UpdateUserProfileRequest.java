package com.airvtinginc.airvtinglive.models.request;

import java.io.Serializable;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateUserProfileRequest implements Serializable {
    private Map<String, RequestBody> params;

    private MultipartBody.Part avatar;

    private MultipartBody.Part cover;

    public Map<String, RequestBody> getParams() {
        return params;
    }

    public void setParams(Map<String, RequestBody> params) {
        this.params = params;
    }

    public MultipartBody.Part getAvatar() {
        return avatar;
    }

    public void setAvatar(MultipartBody.Part avatar) {
        this.avatar = avatar;
    }

    public MultipartBody.Part getCover() {
        return cover;
    }

    public void setCover(MultipartBody.Part cover) {
        this.cover = cover;
    }
}
