package com.airvtinginc.airvtinglive.models.response;

public class Banner {

    private String imageUrl;

    public Banner(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
