package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

public class NotificationsConfig {
    @SerializedName("liveStream")
    private boolean isNotifyLiveStream;
    @SerializedName("message")
    private boolean isNotifyMessage;
    @SerializedName("system")
    private boolean isNotifySystem;

    public boolean isNotifyLiveStream() {
        return isNotifyLiveStream;
    }

    public void setNotifyLiveStream(boolean notifyLiveStream) {
        isNotifyLiveStream = notifyLiveStream;
    }

    public boolean isNotifyMessage() {
        return isNotifyMessage;
    }

    public void setNotifyMessage(boolean notifyMessage) {
        isNotifyMessage = notifyMessage;
    }

    public boolean isNotifySystem() {
        return isNotifySystem;
    }

    public void setNotifySystem(boolean notifySystem) {
        isNotifySystem = notifySystem;
    }
}
