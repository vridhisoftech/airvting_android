package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class GiftRequest {
    @SerializedName("giftId")
    private String giftId;
    @SerializedName("title")
    private String title;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("airToken")
    private int airToken;
    @SerializedName("postId")
    private String postId;
    @SerializedName("receiverId")
    private String receiverId;
    @SerializedName("quantityToSend")
    private int quantityToSend;

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public int getAirToken() {
        return airToken;
    }

    public void setAirToken(int airToken) {
        this.airToken = airToken;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public int getQuantityToSend() {
        return quantityToSend;
    }

    public void setQuantityToSend(int quantityToSend) {
        this.quantityToSend = quantityToSend;
    }
}
