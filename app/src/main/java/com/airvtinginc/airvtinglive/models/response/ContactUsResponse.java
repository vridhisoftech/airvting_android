package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.models.request.ContactUsRequest;
import com.google.gson.annotations.SerializedName;

public class ContactUsResponse {
    @SerializedName("contactDetail")
    private ContactUsRequest contactUs;

    public ContactUsRequest getContactUs() {
        return contactUs;
    }

    public void setContactUs(ContactUsRequest contactUs) {
        this.contactUs = contactUs;
    }
}
