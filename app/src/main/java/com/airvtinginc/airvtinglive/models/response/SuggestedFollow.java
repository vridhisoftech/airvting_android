package com.airvtinginc.airvtinglive.models.response;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.airvtinginc.airvtinglive.gui.adapter.PeopleSuggestedPostAdapter;
import com.airvtinginc.airvtinglive.gui.enums.PeopleType;

public class SuggestedFollow extends People {
    private User user;
    private int currentMediaPage = 1;
    private boolean isMoreLoadingMedia;
    private RecyclerView.OnScrollListener recyclerViewOnScrollListener;
    private PeopleSuggestedPostAdapter peopleGroupAvatarAdapter;
    private LinearLayoutManager linearLayoutManager;

    public SuggestedFollow(User user) {
        super.setPeopleType(PeopleType.SUGGESTED_FOLLOW);
        this.user = user;
        this.isMoreLoadingMedia = false;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getCurrentMediaPage() {
        return currentMediaPage;
    }

    public void setCurrentMediaPage(int currentMediaPage) {
        this.currentMediaPage = currentMediaPage;
    }

    public boolean isMoreLoadingMedia() {
        return isMoreLoadingMedia;
    }

    public void setMoreLoadingMedia(boolean moreLoadingMedia) {
        isMoreLoadingMedia = moreLoadingMedia;
    }

    public RecyclerView.OnScrollListener getRecyclerViewOnScrollListener() {
        return recyclerViewOnScrollListener;
    }

    public void setRecyclerViewOnScrollListener(RecyclerView.OnScrollListener recyclerViewOnScrollListener) {
        this.recyclerViewOnScrollListener = recyclerViewOnScrollListener;
    }

    public PeopleSuggestedPostAdapter getPeopleSuggestedPhotosAdapter() {
        return peopleGroupAvatarAdapter;
    }

    public void setPeopleSuggestedPhotosAdapter(PeopleSuggestedPostAdapter peopleGroupAvatarAdapter) {
        this.peopleGroupAvatarAdapter = peopleGroupAvatarAdapter;
    }

    public LinearLayoutManager getLinearLayoutManager() {
        return linearLayoutManager;
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }
}
