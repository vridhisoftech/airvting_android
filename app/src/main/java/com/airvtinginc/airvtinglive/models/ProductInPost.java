package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductInPost {
    @SerializedName("uniqueProductId")
    private String id;
    @SerializedName("productId")
    private String productId;
    @SerializedName("title")
    private String title;
    @SerializedName("price")
    private float price;
    @SerializedName("discount")
    private String discount;
    @SerializedName("priceDiscount")
    private float priceDiscount;
    @SerializedName("description")
    private String description;
    @SerializedName("timer")
    private int timer;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("featuredImages")
    private List<FeaturedImage> featuredImages;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("endTime")
    private String endTime;
    @SerializedName("expiredAt")
    private String expiredAt;
    @SerializedName("isShow")
    private boolean isShow;
    @SerializedName("priceStore")
    private PriceInStore priceInStore;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public float getPriceDiscount() {
        return priceDiscount;
    }

    public void setPriceDiscount(float priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public List<FeaturedImage> getFeaturedImages() {
        return featuredImages;
    }

    public void setFeaturedImages(List<FeaturedImage> featuredImages) {
        this.featuredImages = featuredImages;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public PriceInStore getPriceInStore() {
        return priceInStore;
    }

    public void setPriceInStore(PriceInStore priceInStore) {
        this.priceInStore = priceInStore;
    }
}
