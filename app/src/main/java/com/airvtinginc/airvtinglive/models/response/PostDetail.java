package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostDetail {
    @SerializedName("_id")
    private String id;
    @SerializedName("owner")
    private User user;
    @SerializedName("type")
    private String type;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("mediaUrl")
    private String mediaUrl;
    @SerializedName("products")
    private List<ProductInPost> products;
    @SerializedName("viewers")
    private int viewers;
    @SerializedName("isLive")
    private boolean isLive;
    @SerializedName("title")
    private String title;
    @SerializedName("isBookmark")
    private boolean isBookmark;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("bookmarkedAt")
    private String bookmarkedAt;
    @SerializedName("isLike")
    private boolean isLike;
    @SerializedName("status")
    private int status;
    @SerializedName("deviceName")
    private String deviceName;

    private String numberViewer;

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public List<ProductInPost> getProducts() {
        return products;
    }

    public void setProducts(List<ProductInPost> products) {
        this.products = products;
    }

    public int getViewers() {
        return viewers;
    }

    public void setViewers(int viewers) {
        this.viewers = viewers;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBookmark() {
        return isBookmark;
    }

    public void setBookmark(boolean bookmark) {
        isBookmark = bookmark;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getBookmarkedAt() {
        return bookmarkedAt;
    }

    public void setBookmarkedAt(String bookmarkedAt) {
        this.bookmarkedAt = bookmarkedAt;
    }

    public boolean getLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getNumberViewer() {
        return numberViewer;
    }

    public void setNumberViewer(String numberViewer) {
        this.numberViewer = numberViewer;
    }
}
