package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class Purchase {
    @SerializedName("receipt")
    private String receipt;
    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }



}
