package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsersResponse {
    @SerializedName("userDetail")
    private List<User> users;
    @SerializedName("type")
    private String type;

    @SerializedName("reportDetail")
    private ReportResponse reportResponse;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ReportResponse getReportResponse() {
        return reportResponse;
    }

    public void setReportResponse(ReportResponse reportResponse) {
        this.reportResponse = reportResponse;
    }
}
