package com.airvtinginc.airvtinglive.models.request;

import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

import okhttp3.RequestBody;

public class PostRequest {
    @SerializedName("postId")
    private String postId;
    @SerializedName("paginate")
    private int paginate;
    @SerializedName("perPage")
    private int perPage;
    @SerializedName("maxId")
    private String maxId;

    private Map<String, RequestBody> params;

    private SaveStreamRequest saveStreamRequest;

    private ProductInPost productInPost;

    public PostRequest(String postId) {
        this.postId = postId;
    }

    public PostRequest(String postId, int paginate, int perPage, String maxId) {
        this.postId = postId;
        this.paginate = paginate;
        this.perPage = perPage;
        this.maxId = maxId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public int getPaginate() {
        return paginate;
    }

    public void setPaginate(int paginate) {
        this.paginate = paginate;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }

    public SaveStreamRequest getSaveStreamRequest() {
        return saveStreamRequest;
    }

    public void setSaveStreamRequest(SaveStreamRequest saveStreamRequest) {
        this.saveStreamRequest = saveStreamRequest;
    }

    public ProductInPost getProductInPost() {
        return productInPost;
    }

    public void setProductInPost(ProductInPost productInPost) {
        this.productInPost = productInPost;
    }

    public Map<String, RequestBody> getParams() {
        return params;
    }

    public void setParams(Map<String, RequestBody> params) {
        this.params = params;
    }
}
