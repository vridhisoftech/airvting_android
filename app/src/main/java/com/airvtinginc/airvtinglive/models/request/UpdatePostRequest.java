package com.airvtinginc.airvtinglive.models.request;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdatePostRequest {
    private String postId;
    private Map<String, RequestBody> params;
    private MultipartBody.Part featureImage;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public Map<String, RequestBody> getParams() {
        return params;
    }

    public void setParams(Map<String, RequestBody> params) {
        this.params = params;
    }

    public MultipartBody.Part getFeatureImage() {
        return featureImage;
    }

    public void setFeatureImage(MultipartBody.Part featureImage) {
        this.featureImage = featureImage;
    }
}
