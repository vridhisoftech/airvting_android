package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class LikeProductResponse {
    @SerializedName("isLike")
    private boolean isLike;

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }
}
