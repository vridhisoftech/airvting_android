package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class GiftList {

    @SerializedName("giftId")
    private String giftId;
    @SerializedName("title")
    private String title;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("airToken")
    private int airToken;
    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("createdAt")
    private String createdAt;

    public GiftList() {
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAirToken() {
        return airToken;
    }

    public void setAirToken(int airToken) {
        this.airToken = airToken;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
