package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class ConfigsResponse {
    @SerializedName("resultConfigs")
    private ConfigSettings configSettings;

    public ConfigSettings getConfigSettings() {
        return configSettings;
    }

    public void setConfigSettings(ConfigSettings configSettings) {
        this.configSettings = configSettings;
    }
}
