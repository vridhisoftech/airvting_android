package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class SendFCMTokenRequest {
    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
