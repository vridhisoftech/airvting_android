package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TagUserListRequest implements Serializable {
    @SerializedName("tagUsers")
    private List<TagUserRequest> userRequestList;

    public List<TagUserRequest> getUserRequestList() {
        return userRequestList;
    }

    public void setUserRequestList(List<TagUserRequest> userRequestList) {
        this.userRequestList = userRequestList;
    }
}
