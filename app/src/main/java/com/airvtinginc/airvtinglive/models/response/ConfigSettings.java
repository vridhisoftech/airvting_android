package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.models.NotificationsConfig;
import com.google.gson.annotations.SerializedName;

public class ConfigSettings {
    @SerializedName("notifications")
    private NotificationsConfig notificationsConfig;

    public NotificationsConfig getNotificationsConfig() {
        return notificationsConfig;
    }

    public void setNotificationsConfig(NotificationsConfig notificationsConfig) {
        this.notificationsConfig = notificationsConfig;
    }
}
