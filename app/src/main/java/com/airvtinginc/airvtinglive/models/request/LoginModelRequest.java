package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginModelRequest implements Serializable {
    @SerializedName("password")
    @Expose
    private String Password;

    @SerializedName("email")
    @Expose private String Email;

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
