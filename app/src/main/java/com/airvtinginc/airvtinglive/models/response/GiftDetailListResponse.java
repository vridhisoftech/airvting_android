package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.models.GiftDetail;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiftDetailListResponse {
    @SerializedName("totalPages")
    private int totalPages;
    @SerializedName("giftDetail")
    private List<GiftDetail> giftDetailList;

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<GiftDetail> getGiftDetailList() {
        return giftDetailList;
    }

    public void setGiftDetailList(List<GiftDetail> giftDetailList) {
        this.giftDetailList = giftDetailList;
    }
}
