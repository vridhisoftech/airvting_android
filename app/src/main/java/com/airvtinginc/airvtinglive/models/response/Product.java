package com.airvtinginc.airvtinglive.models.response;

import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.models.PriceWhenStream;
import com.airvtinginc.airvtinglive.models.ProductCategory;
import com.airvtinginc.airvtinglive.models.ProductComment;
import com.google.gson.annotations.SerializedName;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject {
    @PrimaryKey
    @SerializedName("_id")
    private String productId;
    @SerializedName("title")
    private String title;
    @SerializedName("price")
    private float price;
    @SerializedName("priceSale")
    private float priceSale;
    @SerializedName("condition")
    private String condition;
    @SerializedName("productCategories")
    private RealmList<ProductCategory> productCategories;
    @SerializedName("discount")
    private String discount;
    @SerializedName("description")
    private String description;
    @SerializedName("content")
    private String content;
    @SerializedName("userId")
    private String userId;
    @SerializedName("username")
    private String username;
    @SerializedName("userCaption")
    private String userCaption;
    @SerializedName("isLimited")
    private boolean isLimited;
    @SerializedName("isLike")
    private boolean isLike;
    @SerializedName("totalLike")
    private int totalLike;
    private String displayImage;
    private int quantityToBuy;
    @SerializedName("featuredImages")
    private RealmList<FeaturedImage> featuredImages;
    @SerializedName("productComments")
    private RealmList<ProductComment> productComments;
    @SerializedName("likes")
    private RealmList<Like> likes;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("startedAt")
    private String startedAt;
    @SerializedName("expiredAt")
    private String expiredAt;
    @SerializedName("owner")
    private Owner owner;
    @SerializedName("sellerId")
    private String sellerId;
    @SerializedName("postId")
    private String postId;
    @SerializedName("priceWhenStream")
    private PriceWhenStream priceWhenStream;
    private float paymentPrice;

    public Product() {
    }


    public Product(String title, float price, int totalLike) {
        this.title = title;
        this.price = price;
        this.totalLike = totalLike;
    }

    public Product(String productId, String title, float price, float priceSale, String displayImage, int quantityToBuy) {
        this.productId = productId;
        this.title = title;
        this.price = price;
        this.priceSale = priceSale;
        this.displayImage = displayImage;
        this.quantityToBuy = quantityToBuy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(float priceSale) {
        this.priceSale = priceSale;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public RealmList<ProductCategory> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(RealmList<ProductCategory> productCategories) {
        this.productCategories = productCategories;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserCaption() {
        return userCaption;
    }

    public void setUserCaption(String userCaption) {
        this.userCaption = userCaption;
    }

    public boolean isLimited() {
        return isLimited;
    }

    public void setLimited(boolean limited) {
        isLimited = limited;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public int getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(int totalLike) {
        this.totalLike = totalLike;
    }

    public String getDisplayImage() {
        return displayImage;
    }

    public void setDisplayImage(String displayImage) {
        this.displayImage = displayImage;
    }

    public int getQuantityToBuy() {
        return quantityToBuy;
    }

    public void setQuantityToBuy(int quantityToBuy) {
        this.quantityToBuy = quantityToBuy;
    }

    public RealmList<FeaturedImage> getFeaturedImages() {
        return featuredImages;
    }

    public void setFeaturedImages(RealmList<FeaturedImage> featuredImages) {
        this.featuredImages = featuredImages;
    }

    public RealmList<ProductComment> getProductComments() {
        return productComments;
    }

    public void setProductComments(RealmList<ProductComment> productComments) {
        this.productComments = productComments;
    }

    public RealmList<Like> getLikes() {
        return likes;
    }

    public void setLikes(RealmList<Like> likes) {
        this.likes = likes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public PriceWhenStream getPriceWhenStream() {
        return priceWhenStream;
    }

    public void setPriceWhenStream(PriceWhenStream priceWhenStream) {
        this.priceWhenStream = priceWhenStream;
    }

    public float getPaymentPrice() {
        return paymentPrice;
    }

    public void setPaymentPrice(float paymentPrice) {
        this.paymentPrice = paymentPrice;
    }
}
