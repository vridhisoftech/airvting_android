package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

public class AirToken {
    @SerializedName("_id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("currency")
    private String currency;
    @SerializedName("price")
    private float price;
    @SerializedName("GoogleProductId")
    private String GoogleProductId;

    private String priceInGoogle;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getGoogleProductId() {
        return GoogleProductId;
    }

    public void setGoogleProductId(String googleProductId) {
        GoogleProductId = googleProductId;
    }

    public String getPriceInGoogle() {
        return priceInGoogle;
    }

    public void setPriceInGoogle(String priceInGoogle) {
        this.priceInGoogle = priceInGoogle;
    }
}
