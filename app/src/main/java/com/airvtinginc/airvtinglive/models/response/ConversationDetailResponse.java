package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConversationDetailResponse implements Serializable {
    @SerializedName("conversationsDetail")
    private Conversation conversationsDetail;


    public Conversation getConversationsDetail() {
        return conversationsDetail;
    }

    public void setConversationsDetail(Conversation conversationsDetail) {
        this.conversationsDetail = conversationsDetail;
    }
}
