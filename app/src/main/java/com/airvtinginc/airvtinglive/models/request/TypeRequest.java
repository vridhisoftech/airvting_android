package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class TypeRequest {
    @SerializedName("type")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
