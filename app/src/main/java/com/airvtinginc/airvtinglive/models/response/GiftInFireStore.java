package com.airvtinginc.airvtinglive.models.response;

public class GiftInFireStore {
    private String senderId;
    private String postId;
    private String featuredImage;
    private int quantity;

    public GiftInFireStore() {
    }

    public GiftInFireStore(String senderId, String postId, String featuredImage, int quantity) {
        this.senderId = senderId;
        this.postId = postId;
        this.featuredImage = featuredImage;
        this.quantity = quantity;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
