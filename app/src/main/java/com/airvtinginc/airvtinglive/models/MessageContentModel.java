package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MessageContentModel implements Serializable {

    @SerializedName("message")
    private String message;

    @SerializedName("type")
    private String type;

    @SerializedName("width")
    private int width;

    @SerializedName("height")
    private int height;

    public MessageContentModel(String message, String type) {
        this(message, type, 0, 0);
    }

    public MessageContentModel(String message, String type, int width, int height) {
        this.message = message;
        this.type = type;
        this.width = width;
        this.height = height;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
