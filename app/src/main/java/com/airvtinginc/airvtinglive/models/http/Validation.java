package com.airvtinginc.airvtinglive.models.http;

import java.util.List;

public class Validation{
	private List<String> keys;
	private String source;

	public void setKeys(List<String> keys){
		this.keys = keys;
	}

	public List<String> getKeys(){
		return keys;
	}

	public void setSource(String source){
		this.source = source;
	}

	public String getSource(){
		return source;
	}
}