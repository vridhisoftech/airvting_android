package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class ScheduleDate {
    @SerializedName("date")
    private int date;

    public int getDate() {
        return date;
    }
}
