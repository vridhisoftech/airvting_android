package com.airvtinginc.airvtinglive.models.response;

public class ResponseModel<Model> {
    private boolean success;
    private String message;
    private Model data;
    private int statusCode;

    private String uid_request;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Model getData() {
        return data;
    }

    public void setData(Model data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getUid_request() {
        return uid_request;
    }

    public void setUid_request(String uid_request) {
        this.uid_request = uid_request;
    }
}