package com.airvtinginc.airvtinglive.models.request;

import java.util.List;

public class CartProductsRequest {
    private List<CartProductRequest> products;

    public List<CartProductRequest> getProducts() {
        return products;
    }

    public void setProducts(List<CartProductRequest> products) {
        this.products = products;
    }
}
