package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Like extends RealmObject {
    @PrimaryKey
    @SerializedName("userId")
    private String userId;
    @SerializedName("isLike")
    private boolean isLike;

    public Like() {

    }

    public Like(boolean isLike) {
        this.isLike = isLike;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
