package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BuyAirTokenResponse implements Serializable {
    @SerializedName("airToken")
    private int airToken;

    public int getAirToken() {
        return airToken;
    }

    public void setAirToken(int airToken) {
        this.airToken = airToken;
    }
}
