package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class GiftHistory {
    @SerializedName("_id")
    private String id;
    @SerializedName("sender")
    private Sender sender;
    @SerializedName("receiver")
    private Sender receiver;
    @SerializedName("giftId")
    private String giftId;
    @SerializedName("title")
    private String title;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("airToken")
    private int airToken;
    @SerializedName("postId")
    private String postId;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("fromUser")
    private String fromUser;
    @SerializedName("method")
    private String method;

    public GiftHistory() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Sender getReceiver() {
        return receiver;
    }

    public void setReceiver(Sender receiver) {
        this.receiver = receiver;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public int getAirToken() {
        return airToken;
    }

    public void setAirToken(int airToken) {
        this.airToken = airToken;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
