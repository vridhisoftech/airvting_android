package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class ProductDetailResponse {
    @SerializedName("productDetail")
    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
