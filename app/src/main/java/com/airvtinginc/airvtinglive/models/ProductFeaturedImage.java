package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductFeaturedImage implements Serializable {
    @SerializedName("featuredImage")
    private String featuredImage;

    public ProductFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }
}
