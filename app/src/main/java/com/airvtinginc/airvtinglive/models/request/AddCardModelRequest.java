package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;
import com.stripe.android.model.Token;
import java.io.Serializable;

public class AddCardModelRequest implements Serializable {
    @SerializedName("cardType")
    private String cardType;
    @SerializedName("defaultPaymentMethod")
    private boolean defaultPaymentMethod;
    @SerializedName("stripeObject")
    private Token stripeObject;
    @SerializedName("expiryMonth")
    private String expiryMonth;
    @SerializedName("expiryYear")
    private String expiryYear;
    private String paymentMethodId;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public boolean isDefaultPaymentMethod() {
        return defaultPaymentMethod;
    }

    public void setDefaultPaymentMethod(boolean defaultPaymentMethod) {
        this.defaultPaymentMethod = defaultPaymentMethod;
    }


    public Token getStripeObject() {
        return stripeObject;
    }

    public void setStripeObject(Token stripeObject) {
        this.stripeObject = stripeObject;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }
}
