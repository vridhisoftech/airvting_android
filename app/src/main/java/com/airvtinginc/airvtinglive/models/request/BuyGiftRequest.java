package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class BuyGiftRequest extends GiftRequest {
    @SerializedName("quantityToBuy")
    private int quantityToBuy;
    @SerializedName("isStream")
    private boolean isStream;

    public int getQuantityToBuy() {
        return quantityToBuy;
    }

    public void setQuantityToBuy(int quantityToBuy) {
        this.quantityToBuy = quantityToBuy;
    }

    public boolean isStream() {
        return isStream;
    }

    public void setStream(boolean stream) {
        isStream = stream;
    }
}
