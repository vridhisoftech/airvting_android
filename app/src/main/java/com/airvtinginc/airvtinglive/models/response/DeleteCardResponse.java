package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class DeleteCardResponse {
    @SerializedName("isDeleted")
    private boolean isDeleted;

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
