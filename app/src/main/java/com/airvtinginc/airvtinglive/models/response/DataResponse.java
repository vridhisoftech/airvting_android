package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class DataResponse<T> {
    @SerializedName("statusCode")
    private String statusCode;
    @SerializedName("message")
    private String message;
    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private T data;


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
