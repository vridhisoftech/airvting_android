package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class Location {
    @SerializedName("type")

    private String type;
    @SerializedName("address")
    private String address;
    @SerializedName("coordinates")
    private Double[] coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Double[] coordinates) {
        this.coordinates = coordinates;
    }
}
