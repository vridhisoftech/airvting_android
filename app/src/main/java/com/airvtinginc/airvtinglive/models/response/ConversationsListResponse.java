package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ConversationsListResponse implements Serializable {
    @SerializedName("conversationsDetail")
    private List<Conversation> conversationsList;

    @SerializedName("totalPages")
    private int totalPages;

    public List<Conversation> getConversationsList() {
        return conversationsList;
    }

    public void setConversationsList(List<Conversation> conversationsList) {
        this.conversationsList = conversationsList;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
