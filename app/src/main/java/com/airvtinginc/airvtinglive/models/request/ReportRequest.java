package com.airvtinginc.airvtinglive.models.request;

import java.util.Map;

import okhttp3.RequestBody;

public class ReportRequest {
    private Map<String, RequestBody> params;

    public ReportRequest(Map<String, RequestBody> params) {
        this.params = params;
    }

    public Map<String, RequestBody> getParams() {
        return params;
    }

    public void setParams(Map<String, RequestBody> params) {
        this.params = params;
    }
}
