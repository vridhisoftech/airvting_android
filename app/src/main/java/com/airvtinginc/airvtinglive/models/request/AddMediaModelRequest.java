package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddMediaModelRequest implements Serializable {
    @SerializedName("media")
    private MultipartBody.Part media;

    @SerializedName("mediaFile")
    private RequestBody mediaFile;

    @SerializedName("type")
    private RequestBody type;

    @SerializedName("title")
    private RequestBody title;

    @SerializedName("tagUsers")
    private RequestBody tagUsers;

    private Map<String, RequestBody> params;

    public MultipartBody.Part getMedia() {
        return media;
    }

    public void setMedia(MultipartBody.Part media) {
        this.media = media;
    }

    public RequestBody getType() {
        return type;
    }

    public void setType(RequestBody type) {
        this.type = type;
    }

    public RequestBody getTitle() {
        return title;
    }

    public void setTitle(RequestBody title) {
        this.title = title;
    }

    public RequestBody getTagUsers() {
        return tagUsers;
    }

    public void setTagUsers(RequestBody tagUsers) {
        this.tagUsers = tagUsers;
    }

    public Map<String, RequestBody> getParams() {
        return params;
    }

    public void setParams(Map<String, RequestBody> params) {
        this.params = params;
    }

    public RequestBody getMediaFile() {
        return mediaFile;
    }

    public void setMediaFile(RequestBody mediaFile) {
        this.mediaFile = mediaFile;
    }
}
