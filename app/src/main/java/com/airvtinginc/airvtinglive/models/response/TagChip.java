package com.airvtinginc.airvtinglive.models.response;


import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.pchmn.materialchips.model.ChipInterface;

public class TagChip implements ChipInterface {

    private String id;
    private String name;

    public TagChip(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public Object getId() {
        return id;
    }

    @Override
    public Uri getAvatarUri() {
        return null;
    }

    @Override
    public Drawable getAvatarDrawable() {
        return null;
    }

    @Override
    public String getLabel() {
        return name;
    }

    @Override
    public String getInfo() {
        return null;
    }

    public void setLabel(String name) {
        this.name = name;
    }
}
