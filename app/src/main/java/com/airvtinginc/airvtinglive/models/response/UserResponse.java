package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class UserResponse {
    @SerializedName("tokenId")
    private String tokenId;
    @SerializedName("countPostsFollowing")
    private int countPostsFollowing;
    @SerializedName("code")
    private int verifyCode;
    @SerializedName("message")
    private String message;
    @SerializedName("userDetail")
    private User user;
    @SerializedName("isOnline")
    private boolean isOnline;
    @SerializedName("PaymentMethodIdDefault")
    private String PaymentMethodIdDefault;

    public String getTokenId() {
        return tokenId;
    }

    public int getCountPostsFollowing() {
        return countPostsFollowing;
    }

    public int getVerifyCode() {
        return verifyCode;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }

    public String getPaymentMethodIdDefault() {
        return PaymentMethodIdDefault;
    }

    public void setPaymentMethodIdDefault(String paymentMethodIdDefault) {
        PaymentMethodIdDefault = paymentMethodIdDefault;
    }
}
