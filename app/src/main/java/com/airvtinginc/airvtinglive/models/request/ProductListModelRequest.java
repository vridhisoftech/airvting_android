package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductListModelRequest implements Serializable {
    @SerializedName("userId")
    private String userId;

    @SerializedName("categoryId")
    private String categoryId;

    @SerializedName("paginate")
    private int paginate;

    @SerializedName("perPage")
    private int perPage;

    @SerializedName("maxId")
    private String maxId;

    public ProductListModelRequest(String userId, int paginate, int perPage, String maxId) {
        this(userId, null, paginate, perPage, maxId);
    }

    public ProductListModelRequest(String userId, String categoryId, int paginate, int perPage, String maxId) {
        this.userId = userId;
        this.categoryId = categoryId;
        this.paginate = paginate;
        this.perPage = perPage;
        this.maxId = maxId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getPaginate() {
        return paginate;
    }

    public void setPaginate(int paginate) {
        this.paginate = paginate;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }
}
