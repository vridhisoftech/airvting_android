package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AddConversationsRequest implements Serializable {
    @SerializedName("receivers")
    private List<TagUserRequest> receivers;

    @SerializedName("title")
    private String title;

    @SerializedName("content")
    private String content;

    //Use for reply conversation
    @SerializedName("conversationId")
    private String conversationId;


    public List<TagUserRequest> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<TagUserRequest> receivers) {
        this.receivers = receivers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }
}
