package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionItemResponse {
    @SerializedName("transactionDetail")
    private List<TransactionItem> transactionDetail;
    @SerializedName("maxId")
    private String maxId;

    public TransactionItemResponse() {
    }

    public List<TransactionItem> getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(List<TransactionItem> transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }
}
