package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class FollowUserRequest {
    @SerializedName("userIdToFollow")
    private String userIdToFollow;

    public FollowUserRequest(String userIdToFollow) {
        this.userIdToFollow = userIdToFollow;
    }

    public String getUserIdToFollow() {
        return userIdToFollow;
    }

    public void setUserIdToFollow(String userIdToFollow) {
        this.userIdToFollow = userIdToFollow;
    }
}
