package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class SaveStreamRequest {
    @SerializedName("saveStream")
    private boolean isSaveStream;
    @SerializedName("commentCount")
    private int commentCount;
    @SerializedName("likeCount")
    private int likeCount;
    @SerializedName("giftCount")
    private int giftCount;
    @SerializedName("cameraPosition")
    private int cameraPosition;

    public boolean isSaveStream() {
        return isSaveStream;
    }

    public void setSaveStream(boolean saveStream) {
        isSaveStream = saveStream;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getGiftCount() {
        return giftCount;
    }

    public void setGiftCount(int giftCount) {
        this.giftCount = giftCount;
    }

    public int getCameraPosition() {
        return cameraPosition;
    }

    public void setCameraPosition(int cameraPosition) {
        this.cameraPosition = cameraPosition;
    }
}
