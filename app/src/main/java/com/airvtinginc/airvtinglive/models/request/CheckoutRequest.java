package com.airvtinginc.airvtinglive.models.request;

import com.airvtinginc.airvtinglive.models.ProductInCart;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CheckoutRequest implements Serializable {
    private String userId;

    @SerializedName("paymentMethodId")
    private String paymentMethodId;
    @SerializedName("totalPrice")
    private float totalPrice;
    @SerializedName("totalQuantity")
    private int totalQuantity;
    @SerializedName("products")
    private List<ProductInCart> products;
    @SerializedName("useAirToken")
    private int useAirToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public List<ProductInCart> getProducts() {
        return products;
    }

    public void setProducts(List<ProductInCart> products) {
        this.products = products;
    }

    public int getUseAirToken() {
        return useAirToken;
    }

    public void setUseAirToken(int useAirToken) {
        this.useAirToken = useAirToken;
    }
}
