package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

public class PostDetailResponse {
    @SerializedName("postDetail")
    private PostDetail postDetail;
    @SerializedName("commentDetail")
    private PostDetail commentDetail;
    @SerializedName("maxId")
    private String maxId;

    public PostDetail getPostDetail() {
        return postDetail;
    }

    public void setPostDetail(PostDetail postDetail) {
        this.postDetail = postDetail;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }
}
