package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WalletRespone {
    @SerializedName("transactionDetail")
    private List<TransactionItem> transactionDetail;
    @SerializedName("giftDetail")
    private List<GiftList> giftDetail;
    @SerializedName("giftsHistoryDetail")
    private List<GiftHistory> giftsHistoryDetail;
    @SerializedName("maxId")
    private String maxId;
    @SerializedName("totalPages")
    private int totalPages;
    @SerializedName("totalGift")
    private int totalGift;

    public WalletRespone() {
    }

    public List<TransactionItem> getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(List<TransactionItem> transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public List<GiftList> getGiftDetail() {
        return giftDetail;
    }

    public void setGiftDetail(List<GiftList> giftDetail) {
        this.giftDetail = giftDetail;
    }

    public List<GiftHistory> getGiftsHistoryDetail() {
        return giftsHistoryDetail;
    }

    public void setGiftsHistoryDetail(List<GiftHistory> giftsHistoryDetail) {
        this.giftsHistoryDetail = giftsHistoryDetail;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalGift() {
        return totalGift;
    }

    public void setTotalGift(int totalGift) {
        this.totalGift = totalGift;
    }
}
