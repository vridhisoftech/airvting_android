package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class ConfigRequest {
    @SerializedName("notifications")
    private String notification;

    public ConfigRequest(String notification) {
        this.notification = notification;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
}
