package com.airvtinginc.airvtinglive.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductInCart implements Serializable {
    @SerializedName("productId")
    private String productId;
    @SerializedName("price")
    private float price;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("currency")
    private String currency;
    @SerializedName("sellerId")
    private String sellerId;
    @SerializedName("postId")
    private String postId;
    @SerializedName("title")
    private String title;
    @SerializedName("featuredImage")
    private String featuredImage;
    @SerializedName("description")
    private String description;

    public ProductInCart() {
    }

    public ProductInCart(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
