package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class GetListRequest {
    @SerializedName("userId")
    private String userId;
    @SerializedName("giftId")
    private String giftId;
    @SerializedName("paginate")
    private int paginate;
    @SerializedName("perPage")
    private int perPage;
    @SerializedName("maxId")
    private String maxId;
    @SerializedName("type")
    private String type;
    @SerializedName("typePost")
    private String[] typePost;
    @SerializedName("bookmarks")
    private int bookmarks;
    @SerializedName("isMessage")
    private boolean isGetNotificationInbox;

    public GetListRequest() {
    }

    public GetListRequest(String userId, String giftId, int paginate, int perPage, String maxId, String type, String[] typePost, int bookmarks) {
        this.userId = userId;
        this.giftId = giftId;
        this.paginate = paginate;
        this.perPage = perPage;
        this.maxId = maxId;
        this.type = type;
        this.typePost = typePost;
        this.bookmarks = bookmarks;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public int getPaginate() {
        return paginate;
    }

    public void setPaginate(int paginate) {
        this.paginate = paginate;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getMaxId() {
        return maxId;
    }

    public void setMaxId(String maxId) {
        this.maxId = maxId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getTypePost() {
        return typePost;
    }

    public void setTypePost(String[] typePost) {
        this.typePost = typePost;
    }

    public int getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(int bookmarks) {
        this.bookmarks = bookmarks;
    }

    public boolean isGetNotificationInbox() {
        return isGetNotificationInbox;
    }

    public void setGetNotificationInbox(boolean getNotificationInbox) {
        isGetNotificationInbox = getNotificationInbox;
    }
}
