package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

public class CartProductRequest {
    @SerializedName("productId")
    private String productId;

    public CartProductRequest(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
