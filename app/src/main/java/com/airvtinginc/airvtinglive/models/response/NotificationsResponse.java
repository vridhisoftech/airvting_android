package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsResponse {
    @SerializedName("items")
    private List<Notification> notifications;

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

}
