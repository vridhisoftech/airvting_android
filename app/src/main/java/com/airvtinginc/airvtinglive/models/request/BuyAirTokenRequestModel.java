package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BuyAirTokenRequestModel implements Serializable {
    @SerializedName("quantityToBuy")
    private int quantityToBuy;
    @SerializedName("Purchase")
    private com.airvtinginc.airvtinglive.models.request.Purchase Purchase;
    @SerializedName("airTokenId")
    private String airTokenId;
    @SerializedName("title")
    private String title;
    @SerializedName("price")
    private float price;
    @SerializedName("currency")
    private String currency;

    public int getQuantityToBuy() {
        return quantityToBuy;
    }

    public void setQuantityToBuy(int quantityToBuy) {
        this.quantityToBuy = quantityToBuy;
    }

    public Purchase getPurchase() {
        return Purchase;
    }

    public void setPurchase(Purchase purchase) {
        Purchase = purchase;
    }

    public String getAirTokenId() {
        return airTokenId;
    }

    public void setAirTokenId(String airTokenId) {
        this.airTokenId = airTokenId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}