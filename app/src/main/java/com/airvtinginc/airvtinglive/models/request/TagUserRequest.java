package com.airvtinginc.airvtinglive.models.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TagUserRequest implements Serializable {
    @SerializedName("userId")
    private String userId;

    @SerializedName("username")
    private String username;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
