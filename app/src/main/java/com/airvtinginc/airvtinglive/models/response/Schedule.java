package com.airvtinginc.airvtinglive.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Schedule {
    @SerializedName("dates")
    private List<ScheduleDate> scheduleDates;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("endTime")
    private String endTime;

    public List<ScheduleDate> getScheduleDates() {
        return scheduleDates;
    }

    public void setScheduleDates(List<ScheduleDate> scheduleDates) {
        this.scheduleDates = scheduleDates;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
