package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.PeopleGroupViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.PeopleSuggestedViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.ExplorePeopleType;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.gui.enums.PeopleType;
import com.airvtinginc.airvtinglive.models.request.FollowUserRequest;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.People;
import com.airvtinginc.airvtinglive.models.response.PeopleGroup;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.SuggestedFollow;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

public class PeopleAdapter extends BaseAdapter {

    private static final String TAG = PeopleAdapter.class.getSimpleName();

    private static final int PEOPLE_GROUP_SIZE = 3; // Rising Star, Top Users, New
    private static final int VIEW_TYPE_ITEM_PEOPLE_GROUP = 0;
    private static final int VIEW_TYPE_ITEM_SUGGESTED = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    private int mFollowPosition = -1;

    public PeopleAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(v);
            }
            case VIEW_TYPE_ITEM_PEOPLE_GROUP: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_people_group, parent, false);
                return new PeopleGroupViewHolder(v);
            }
            case VIEW_TYPE_ITEM_SUGGESTED: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_people_suggested, parent, false);
                return new PeopleSuggestedViewHolder(v);
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(v);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder) return;
        if (list.get(position) == null) return;
        if (mHolder instanceof PeopleGroupViewHolder) {
            final PeopleGroupViewHolder holder = (PeopleGroupViewHolder) mHolder;
            PeopleGroup peopleGroup = (PeopleGroup) list.get(position);
            holder.tvGroupName.setText(peopleGroup.getGroupName());

            // use a linear layout manager
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.rvPeopleGroup.setLayoutManager(linearLayoutManager);
//            holder.rvPeopleGroup.setHasFixedSize(true);

            holder.rvPeopleGroup.setAdapter(peopleGroup.getPeopleGroupAvatarAdapter());
            holder.rvPeopleGroup.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    int visibleItemCount, pastVisibleItems, totalItemCount;
                    super.onScrolled(recyclerView, dx, dy);
                    if (dx > 0) {
                        visibleItemCount = linearLayoutManager.getChildCount();
                        totalItemCount = linearLayoutManager.getItemCount();
                        pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                        if (!peopleGroup.isMoreLoading()) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                peopleGroup.getPeopleGroupAvatarAdapter().addItem(null);
                                peopleGroup.setMoreLoading(true);
                                getExplorePeople(getTypeName(peopleGroup.getGroupName()),
                                        peopleGroup.getCurrentPage() + 1);
                            }
                        }
                    }
                }
            });

            if (peopleGroup.getPeopleGroupAvatarAdapter() != null && peopleGroup.getPeopleGroupAvatarAdapter().getItemCount() > 0) {
                holder.rvPeopleGroup.setVisibility(View.VISIBLE);
                holder.emptyView.setVisibility(View.GONE);
            } else {
                holder.rvPeopleGroup.setVisibility(View.GONE);
                holder.emptyView.setVisibility(View.VISIBLE);
            }
        } else if (mHolder instanceof PeopleSuggestedViewHolder) {
            final PeopleSuggestedViewHolder holder = (PeopleSuggestedViewHolder) mHolder;
            SuggestedFollow suggestedFollow = (SuggestedFollow) list.get(position);
            if (suggestedFollow.getUser() == null) return;
            User user = suggestedFollow.getUser();
            getUserMedia(user.getId(), suggestedFollow.getCurrentMediaPage());

            if (position == 3) {
                holder.tvSuggested.setVisibility(View.VISIBLE);
                holder.marginTop.setVisibility(View.VISIBLE);
            } else {
                holder.tvSuggested.setVisibility(View.GONE);
                holder.marginTop.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(user.getAvatar())) {
                holder.pbAvatar.setVisibility(View.VISIBLE);
                holder.ivAvatar.setVisibility(View.VISIBLE);
                Glide.with(context).load(user.getAvatar())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbAvatar.setVisibility(View.GONE);
                                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.people_default_avatar_text_size);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbAvatar.setVisibility(View.GONE);
                                Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                                return false;
                            }
                        })
                        .into(holder.ivAvatar);
            } else {
                holder.pbAvatar.setVisibility(View.GONE);
                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.people_default_avatar_text_size);
            }

            if (!TextUtils.isEmpty(user.getDisplayName())) {
                holder.tvName.setText(user.getDisplayName());
            }

            if (user.isOnline()) {
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_verified_green, 0);
            } else {
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

            if (!TextUtils.isEmpty(user.getDescription())) {
                holder.tvStatus.setText(user.getDescription());
                holder.tvStatus.setVisibility(View.VISIBLE);
            } else {
                holder.tvStatus.setVisibility(View.GONE);
            }

            // use a linear layout manager
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.rvPhotos.setLayoutManager(linearLayoutManager);
            holder.rvPhotos.setAdapter(suggestedFollow.getPeopleSuggestedPhotosAdapter());
            holder.rvPhotos.addOnScrollListener((new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    int visibleItemCount, pastVisibleItems, totalItemCount;
                    super.onScrolled(recyclerView, dx, dy);
                    if (dx > 0) {
                        visibleItemCount = linearLayoutManager.getChildCount();
                        totalItemCount = linearLayoutManager.getItemCount();
                        pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                        if (!suggestedFollow.isMoreLoadingMedia()) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                suggestedFollow.getPeopleSuggestedPhotosAdapter().addItem(null);
                                suggestedFollow.setMoreLoadingMedia(true);
                                getUserMedia(user.getId(), suggestedFollow.getCurrentMediaPage() + 1);
                            }
                        }
                    }
                }
            }));

            holder.llInfo.setOnClickListener(view -> openUserDetail(suggestedFollow.getUser().getId()));

            String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
            if (!currentUserId.equals(user.getId())) {
                if (user.isFollow()) {
                    holder.ivIsFollow.setImageResource(R.drawable.ic_check);
                    holder.tvFollow.setText(context.getString(R.string.my_user_following_capitalize));
                } else {
                    holder.ivIsFollow.setImageResource(R.drawable.ic_add_store);
                    holder.tvFollow.setText(context.getString(R.string.my_user_follow_capitalize));
                }

                holder.rlFollow.setVisibility(View.VISIBLE);
                holder.rlFollow.setOnClickListener(view -> {
                    mFollowPosition = position;
                    FollowUserRequest followUserRequest = new FollowUserRequest(user.getId());
                    requestApi(followUserRequest, true, RequestTarget.FOLLOW_USER, this);
                });
            } else {
                holder.rlFollow.setVisibility(View.GONE);
            }
        }
    }

    private void getExplorePeople(String type, int page) {
        GetListRequest requestModel = new GetListRequest();
        requestModel.setPaginate(page);
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestModel.setType(type);
        requestApi(requestModel, false, RequestTarget.GET_EXPLORE_PEOPLE, this);
    }

    private void getUserMedia(String userId, int page) {
        String[] typePost = {MediaType.IMAGE.getType(), MediaType.VIDEO.getType(), MediaType.STREAM.getType()};

        GetListRequest requestModel = new GetListRequest();
        requestModel.setUserId(userId);
        requestModel.setPaginate(page);
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_PHOTO_IN_SUGGESTED_LOADING);
        requestModel.setTypePost(typePost);
        requestModel.setBookmarks(0);
        requestApi(requestModel, false, RequestTarget.GET_POST_BY_USER_ID, this);
    }

    private String getTypeName(String groupName) {
        if (groupName.equals(context.getString(R.string.explore_people_rising_star))) {
            return ExplorePeopleType.RISING_STAR.getType();
        } else if (groupName.equals(context.getString(R.string.explore_people_top_users))) {
            return ExplorePeopleType.TOP_USERS.getType();
        } else if (groupName.equals(context.getString(R.string.explore_people_new))) {
            return ExplorePeopleType.NEW.getType();
        }
        return "";
    }

    public String getGroupName(String type) {
        if (type.equals(ExplorePeopleType.RISING_STAR.getType())) {
            return context.getString(R.string.explore_people_rising_star);
        } else if (type.equals(ExplorePeopleType.TOP_USERS.getType())) {
            return context.getString(R.string.explore_people_top_users);
        } else if (type.equals(ExplorePeopleType.NEW.getType())) {
            return context.getString(R.string.explore_people_new);
        }
        return "";
    }

    public int getPositionOfType(String type) {
        if (type.equals(ExplorePeopleType.RISING_STAR.getType())) {
            return 0;
        } else if (type.equals(ExplorePeopleType.TOP_USERS.getType())) {
            return 1;
        } else if (type.equals(ExplorePeopleType.NEW.getType())) {
            return 2;
        }
        return -1;
    }

    private void openUserDetail(String userId) {
        String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (!userId.equals(currentUserId)) {
            Utils.openUserDetailOrder(context, userId);
        }
    }

    private void updateFollow(boolean isFollow, int position) {
        SuggestedFollow suggestedFollow = (SuggestedFollow) list.get(position);
        if (suggestedFollow != null) {
            suggestedFollow.getUser().setFollow(isFollow);
            notifyItemChanged(position);
        }
    }

    public <T> void addItem(T item, int position, boolean isReplace) {
        People people = (People) item;
        if (people != null) {
            switch (people.getPeopleType()) {
                case PEOPLE_GROUP:
                    PeopleGroup peopleGroup = (PeopleGroup) people;
                    peopleGroup.setPeopleGroupAvatarAdapter(new PeopleGroupAvatarAdapter(context, peopleGroup.getUsers(), new OnItemClickListener() {
                        @Override
                        public <T> void onItemClicked(T item, int position) {

                        }
                    }));
                    break;
                case SUGGESTED_FOLLOW:
                    SuggestedFollow suggestedFollow = (SuggestedFollow) people;
                    suggestedFollow.setPeopleSuggestedPhotosAdapter(new PeopleSuggestedPostAdapter(context, new ArrayList<>(), new OnItemClickListener() {
                        @Override
                        public <T> void onItemClicked(T item, int position) {

                        }
                    }));
                    break;
            }
        }
        if (isReplace) {
            list.set(position, people);
            notifyItemChanged(position);
        } else {
            list.add(people);
            notifyItemInserted(list.size() - 1);
            notifyItemChanged(list.size() - 1);
        }
    }

    public void setEmptyGroupName(String groupName) {
        for (int i = 0; i < list.size(); i++) {
            PeopleGroup peopleGroup = (PeopleGroup) list.get(i);
            if (peopleGroup.getGroupName().equals(groupName)) {
                peopleGroup.setGroupName("");
                notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? (((People) list.get(position)).getPeopleType() == PeopleType.PEOPLE_GROUP ? VIEW_TYPE_ITEM_PEOPLE_GROUP : VIEW_TYPE_ITEM_SUGGESTED) : VIEW_TYPE_LOAD_MORE;
    }

    private void hideLoadMore(PeopleGroup peopleGroup) {
        PeopleGroupAvatarAdapter adapter = peopleGroup.getPeopleGroupAvatarAdapter();
        if (peopleGroup.isMoreLoading() && adapter.getItemCount() > 0) {
            adapter.removeItem(adapter.getItemCount() - 1);
            peopleGroup.setMoreLoading(false);
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_EXPLORE_PEOPLE:
                    String type = ((UsersResponse) response.getData()).getType();
                    if (TextUtils.isEmpty(type)) {
                        return;
                    }
                    List<User> users = ((UsersResponse) response.getData()).getUsers();

                    for (int i = 0; i < PEOPLE_GROUP_SIZE; i++) {
                        PeopleGroup peopleGroup = (PeopleGroup) list.get(i);
                        hideLoadMore(peopleGroup);
                        if (getTypeName(peopleGroup.getGroupName()).equals(type)) {
                            peopleGroup.getPeopleGroupAvatarAdapter().addAllItems(users);
                            peopleGroup.setCurrentPage(peopleGroup.getCurrentPage() + 1);
                            break;
                        }
                    }
                    break;
                case GET_POST_BY_USER_ID:
                    for (int i = PEOPLE_GROUP_SIZE; i < list.size(); i++) {
                        SuggestedFollow suggestedFollow = (SuggestedFollow) list.get(i);
                        List<PostDetail> postDetailList = ((PostResponse) response.getData()).getListPostDetail();
                        if (suggestedFollow != null) {
                            PeopleSuggestedPostAdapter adapter = suggestedFollow.getPeopleSuggestedPhotosAdapter();
                            if (suggestedFollow.isMoreLoadingMedia() && adapter.getItemCount() > 0) {
                                adapter.removeItem(adapter.getItemCount() - 1);
                                suggestedFollow.setMoreLoadingMedia(false);
                            }
                        }
                        if (postDetailList != null) {
                            if (!postDetailList.isEmpty() && suggestedFollow.getUser().getId().equals(postDetailList.get(0).getUser().getUserId())) {
                                for (PostDetail postDetail : postDetailList) {
                                    suggestedFollow.getPeopleSuggestedPhotosAdapter().addItem(postDetail);
                                }
                                suggestedFollow.setCurrentMediaPage(suggestedFollow.getCurrentMediaPage() + 1);
                                break;
                            }
                        }
                    }
                    break;
                case FOLLOW_USER:
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null && userResponse.getUser() != null && mFollowPosition != -1) {
                        updateFollow(userResponse.getUser().isFollow(), mFollowPosition);
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
