package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.GiftAdapter;
import com.airvtinginc.airvtinglive.gui.dialog.BuyGiftDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.GiftDetail;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.GiftDetailListResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GiftStoreFragment extends BaseFragment {

    private static final String TAG = GiftStoreFragment.class.getName();

    @BindView(R.id.rv_gift_store)
    RecyclerView mRvBottomGiftStore;

    SwipeRefreshLayout mSwipeRefreshGiftStore;
    private TextView mTvCurrentToken;
    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private int mCurrentPage = 1;
    private GridLayoutManager mLayoutManager;
    private GiftAdapter mGiftAdapter;
    private BuyGiftListener mBuyGiftListener;
    private boolean mIsShowMenuGiftStore;
    private String mStreamerId;
    private String mPostId;
    private int mBoughtGiftPosition = -1;

    public static GiftStoreFragment newInstance(boolean isShowMenuGiftStore, String streamerId, String postId) {
        GiftStoreFragment fragment = new GiftStoreFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.EXTRA_SHOW_MENU_GIFT_STORE, isShowMenuGiftStore);
        args.putString(Constants.EXTRA_USER_ID, streamerId);
        args.putString(Constants.EXTRA_POST_ID, postId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mIsShowMenuGiftStore = getArguments().getBoolean(Constants.EXTRA_SHOW_MENU_GIFT_STORE, false);
            mStreamerId = getArguments().getString(Constants.EXTRA_USER_ID);
            mPostId = getArguments().getString(Constants.EXTRA_POST_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mIsShowMenuGiftStore) {
            return inflater.inflate(R.layout.fragment_menu_gift_store, container, false);
        } else {
            return inflater.inflate(R.layout.fragment_bottom_gift_store, container, false);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (mIsShowMenuGiftStore) {
            int currentToken = SharedPreferencesManager.getInstance(getContext()).getInt(Constants.PREF_CURRENT_USER_TOKEN, 0);
            mSwipeRefreshGiftStore = view.findViewById(R.id.srl_gift_store);
            mTvCurrentToken = view.findViewById(R.id.tv_gift_store_current_token);
            mTvCurrentToken.setText(String.valueOf(currentToken));
        }

        setUpRecyclerView();
        getGiftStore(mCurrentPage);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsShowMenuGiftStore) {
            Intent intent = new Intent();

            intent.setAction(DetailActivity.BroadcastActionHideAllToolbarIcons);
            getContext().sendBroadcast(intent);

            intent.setAction(DetailActivity.BroadcastActionUpdateHeaderTitle);
            DetailActivity.headerTitle = getString(R.string.gift_store_title);
            getContext().sendBroadcast(intent);

            intent.setAction(DetailActivity.BroadcastActionChangeToolbarToWhiteColor);
            getContext().sendBroadcast(intent);

            intent.setAction(DetailActivity.BroadcastActionShowIconWalletToken);
            intent.putExtra(Constants.EXTRA_SHOW_WALLET_TOKEN, true);
            getContext().sendBroadcast(intent);
        }
    }

    private void setUpRecyclerView() {
        mLayoutManager = new GridLayoutManager(getContext(), 4);
        mRvBottomGiftStore.setLayoutManager(mLayoutManager);
        mRvBottomGiftStore.setHasFixedSize(true);

        mGiftAdapter = new GiftAdapter(getContext(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                showBuyGiftDialog((GiftDetail) item, position);
            }
        });
        if (mIsShowMenuGiftStore) {
            mGiftAdapter.setTextColor(getResources().getColor(R.color.black));
            mGiftAdapter.setIsShowInGiftStore(true);
        } else {
            mGiftAdapter.setTextColor(getResources().getColor(R.color.white));
        }
        mRvBottomGiftStore.setAdapter(mGiftAdapter);

        mRvBottomGiftStore.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isMoreLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            mGiftAdapter.addItem(null);
                            isMoreLoading = true;
                            getGiftStore(++mCurrentPage);
                        }
                    }
                }
            }
        });

        if (mIsShowMenuGiftStore) {
            mSwipeRefreshGiftStore.setOnRefreshListener(() -> {
                mCurrentPage = 1;
                getGiftStore(mCurrentPage);
            });
        }
    }

    private void getGiftStore(int page) {
        GetListRequest getListRequest = new GetListRequest();
        getListRequest.setPaginate(page);
        int numberPerLoading;
        if (mIsShowMenuGiftStore) {
            numberPerLoading = ServiceConstants.NUMBER_OF_GIFT_STORE_ITEMS_PER_LOADING;
        } else {
            numberPerLoading = ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING;
        }
        getListRequest.setPerPage(numberPerLoading);
        requestApi(getListRequest, false, RequestTarget.GET_GIFT_STORE, this);
    }

    private void hideLoadMore() {
        if (isMoreLoading && mGiftAdapter.getItemCount() > 0) {
            mGiftAdapter.removeItem(mGiftAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    private void showBuyGiftDialog(GiftDetail giftDetail, int position) {
        if (giftDetail == null) {
            return;
        }
        mBoughtGiftPosition = position;
        BuyGiftDialog buyGiftDialog = BuyGiftDialog.newInstance(mIsShowMenuGiftStore, mStreamerId, mPostId);
        buyGiftDialog.setGiftDetail(giftDetail);
        buyGiftDialog.setOnBuyGiftListener(new BuyGiftDialog.OnBuyGiftListener() {
            @Override
            public void onUpdateToken(int spentToken) {
                updateUserToken(spentToken);
            }

            @Override
            public void onUpdateQuantity(int boughtQuantity) {
                updateGift(boughtQuantity);
            }
        });
        buyGiftDialog.show(getFragmentManager(), "Show Buy Gift Dialog");
    }

    public void refreshGiftStore() {
        mCurrentPage = 1;
        mGiftAdapter.clearAllItems();
        getGiftStore(mCurrentPage);
    }

    private void updateUserToken(int spentToken) {
        int savedToken = SharedPreferencesManager.getInstance(getContext()).getInt(Constants.PREF_CURRENT_USER_TOKEN, 0);
        int currentToken = savedToken - spentToken;
        if (mIsShowMenuGiftStore && mTvCurrentToken != null) {
            mTvCurrentToken.setText(String.valueOf(currentToken));
        }
        SharedPreferencesManager.getInstance(getContext()).putInt(Constants.PREF_CURRENT_USER_TOKEN, currentToken);
        if (mBuyGiftListener != null) {
            mBuyGiftListener.onBuyGift(currentToken);
        }
    }

    private void updateGift(int boughtQuantity) {
        GiftDetail boughtGiftDetail = mGiftAdapter.getItemAtPosition(mBoughtGiftPosition);
        boughtGiftDetail.setCountGiftsOfUser(boughtGiftDetail.getCountGiftsOfUser() + boughtQuantity);
        mGiftAdapter.notifyItemChanged(mBoughtGiftPosition);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_GIFT_STORE:
                    if (mSwipeRefreshGiftStore != null && mSwipeRefreshGiftStore.isRefreshing()) {
                        mGiftAdapter.clearAllItems();
                    }
                    GiftDetailListResponse giftDetailListResponse = (GiftDetailListResponse) response.getData();
                    if (giftDetailListResponse != null) {
                        mGiftAdapter.addAllItems(giftDetailListResponse.getGiftDetailList());
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
        hideLoadMore();
        if (mSwipeRefreshGiftStore != null) {
            mSwipeRefreshGiftStore.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_buy_gift_title), failMessage);

        hideLoadMore();
        if (mSwipeRefreshGiftStore != null) {
            mSwipeRefreshGiftStore.setRefreshing(false);
        }
    }

    public interface BuyGiftListener {
        void onBuyGift(int currentToken);
    }

    public void setBuyGiftListener(BuyGiftListener buyGiftListener) {
        mBuyGiftListener = buyGiftListener;
    }
}
