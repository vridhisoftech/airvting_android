package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.ConversationsDetailAdapter;
import com.airvtinginc.airvtinglive.gui.components.hashtag.EditTextHashtag;
import com.airvtinginc.airvtinglive.gui.dialog.AddPhotoDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageType;
import com.airvtinginc.airvtinglive.models.MessageContentModel;
import com.airvtinginc.airvtinglive.models.request.AddConversationsRequest;
import com.airvtinginc.airvtinglive.models.request.UploadImageItemRequest;
import com.airvtinginc.airvtinglive.models.request.SearchModelRequest;
import com.airvtinginc.airvtinglive.models.response.Conversation;
import com.airvtinginc.airvtinglive.models.response.ConversationDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ConversationReplyResponse;
import com.airvtinginc.airvtinglive.models.response.Message;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.Sender;
import com.airvtinginc.airvtinglive.models.response.UploadImageResponse;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.RealPathUtil;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.google.gson.Gson;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ConversationsDetailFragment extends BaseFragment {

    private static final String TAG = ConversationsDetailFragment.class.getSimpleName();

    @BindView(R.id.fm_conversation_detail_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fm_conversation_detail_tv_title)
    TextView tvTitle;
    @BindView(R.id.fm_conversation_detail_rv_conversations)
    RecyclerView rvConversation;
    @BindView(R.id.fm_conversation_detail_et_comments)
    EditTextHashtag etComment;
    @BindView(R.id.fm_conversation_detail_iv_send)
    ImageView ivSend;

    private int perPage = 20, paginate = 1, maxId;
    private ConversationsDetailAdapter adapter;
    private List<Message> messageDetailList;
    private List<String> avatarIndexList;
    private List<String> timeHeaderIndexList;
    private String currentUserId, conversationId;
    private boolean needScrollToBottomMessageList = true;
    private boolean showLoading = true;
    private int widthImageUploaded;
    private int heightImageUploaded;

    public Unregistrar mUnregistrar;

    @OnTouch(R.id.llBackground)
    boolean hideKeyBoard() {
        Utils.hideSoftKeyboard(getActivity());
        return true;
    }

    @OnClick(R.id.fm_conversation_detail_iv_add_image)
    void addImageMessage() {
        AddPhotoDialog addPhotoDialog = AddPhotoDialog.newInstance();
        addPhotoDialog.setReturnImageListener((data) -> {

            UploadImageItemRequest uploadImageItemRequest = new UploadImageItemRequest();


            List<MultipartBody.Part> featureImagesPart = new ArrayList<>();
            String imageRealPath = RealPathUtil.getRealPath(getContext(), data);
            File imageFile = new File(imageRealPath);
            RequestBody requestImage = RequestBody.create(MediaType.parse("image/*"), imageFile);
            MultipartBody.Part imagePartFile = MultipartBody.Part.createFormData(ServiceConstants.FEATURE_IMAGES_MESSAGE, imageFile.getName(), requestImage);
            featureImagesPart.add(imagePartFile);

            uploadImageItemRequest.setImage(featureImagesPart);

            setWidthHeightImage(imageRealPath);

            requestApi(uploadImageItemRequest, true, RequestTarget.UPLOAD_NEW_IMAGE, this);

            addPhotoDialog.dismiss();
        });
        addPhotoDialog.show(getActivity().getSupportFragmentManager(), "Add Photo Dialog");
    }

    public static ConversationsDetailFragment newInstance(Bundle bundle) {
        ConversationsDetailFragment detailFragment = new ConversationsDetailFragment();
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversations_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        messageDetailList = new ArrayList<>();
        avatarIndexList = new ArrayList<>();
        timeHeaderIndexList = new ArrayList<>();

        if (getArguments().getString(Constants.EXTRA_CONVERSATION_ID) != null) {
            conversationId = getArguments().getString(Constants.EXTRA_CONVERSATION_ID);
            getConversationDetail(conversationId);
        }

        ivSend.setOnClickListener(v -> replyConversation());

        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(true);
            needScrollToBottomMessageList = false;
            paginate++;
            showLoading = false;
            getConversationDetail(conversationId);
        });

        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(getActivity(), isOpen -> {
            if (isOpen) {
                if (adapter != null) {
                    rvConversation.scrollToPosition(adapter.getItemCount() - 1);
                }
            } else {
            }
        });
    }

    private void getConversationDetail(String conversationId) {
        SearchModelRequest modelRequest = new SearchModelRequest();
        modelRequest.setConversationId(conversationId);
        modelRequest.setPerPage(perPage);
        modelRequest.setPaginate(paginate);
        modelRequest.setMaxId(maxId);
        requestApi(modelRequest, showLoading, RequestTarget.GET_CONVERSATIONS_DETAIL, this);
    }

    private void displayContent(Conversation conversation) {
        currentUserId = SharedPreferencesManager.getInstance(getActivity()).getString(Constants.PREF_CURRENT_USER_ID, "");
        Sender sender = conversation.getSender();
        Sender receiver = conversation.getReceiver();
        String displayName;
        String title = conversation.getTitle();

        if (currentUserId.equals(sender.getUserId())) {
            displayName = receiver.getDisplayName();
        } else {
            displayName = sender.getDisplayName();
        }

        //Update header title
        DetailActivity.headerTitle = displayName.toUpperCase();
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);

        tvTitle.setText(title);
        Utils.setUpRecycleViewWithNoDivider(getContext(), rvConversation);

        messageDetailList.clear();
        avatarIndexList.clear();
        timeHeaderIndexList.clear();

        messageDetailList = conversation.getMessageList();
        if (messageDetailList != null && messageDetailList.size() > 0) {
            Collections.reverse(messageDetailList);
            int timeInterval = 5 * 60 * 1000; //5 minute

            //Handle show time header
            for (int i = 0; i < messageDetailList.size(); i++) {
                timeHeaderIndexList.add("hideTimeHeader");
                avatarIndexList.add("hideAvatar");
                if (i == 0) {
                    //Always show time send and avatar on first position of message list
                    timeHeaderIndexList.set(i, "showTimeHeader");
                    checkMessageFrom(messageDetailList, i, avatarIndexList);
                }
            }

            //Handle display avatar
            for (int i = 0; i < messageDetailList.size(); i++) {
                if (i < messageDetailList.size() - 1) {
                    Message firstMessage = messageDetailList.get(i);
                    Message nextMessage = messageDetailList.get(i + 1);

                    Long timeFirst = DateTimeUtils.getTimeStamp(firstMessage.getCreatedAt());
                    Long timeNext = DateTimeUtils.getTimeStamp(nextMessage.getCreatedAt());
                    if (timeNext - timeFirst < timeInterval) {
                        String senderIdFirst = firstMessage.getSenderId();
                        String senderIdNext = nextMessage.getSenderId();

                        if (!senderIdFirst.equals(senderIdNext)) {
                            checkMessageFrom(messageDetailList, i + 1, avatarIndexList);
                        }
                    } else {
                        checkMessageFrom(messageDetailList, i + 1, avatarIndexList);
                        timeHeaderIndexList.set(i + 1, "showTimeHeader");
                    }
                }
            }

        }

        //We will reload screen with 20 newest message when have a reply
        if (needScrollToBottomMessageList) {
            adapter = new ConversationsDetailAdapter(getContext(), conversation, this::openImage);
            adapter.addAllItem(messageDetailList, avatarIndexList, timeHeaderIndexList);
            rvConversation.setAdapter(adapter);
            new Handler().postDelayed(() -> rvConversation.scrollToPosition(adapter.getItemCount() - 1), 50);
        } else {
            if (adapter == null) {
                adapter = new ConversationsDetailAdapter(getContext(), conversation, uri -> {
                    openImage(uri);
                });
                adapter.addAllItem(messageDetailList, avatarIndexList, timeHeaderIndexList);
                rvConversation.setAdapter(adapter);
            } else {
                if (messageDetailList.size() > 0) {
                    adapter.addMoreData(messageDetailList, avatarIndexList, timeHeaderIndexList);
                }
            }
        }
    }

    private void openImage(Uri uri) {
        String type = "image/*";
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setDataAndType(uri, type);
        startActivity(intent);
    }

    private void checkMessageFrom(List<Message> messageDetailList, int index, List<String> avatarIndexList) {
        Message message = messageDetailList.get(index);
        String senderId = message.getSenderId();
        if (!currentUserId.equals(senderId)) {
            //Show friend avatar
            avatarIndexList.set((index), "showAvatar");
        }
    }

    private void replyConversation() {
        if (!etComment.getText().toString().equals("")) {
            hideKeyBoard();
            replyConversation(etComment.getText().toString(), MessageType.TEXT.getType());
            etComment.setText("");
        }
    }

    private void replyConversation(String reply, String type) {
        AddConversationsRequest request = new AddConversationsRequest();
        request.setConversationId(conversationId);
        request.setContent(new Gson().toJson(new MessageContentModel(reply, type, widthImageUploaded, heightImageUploaded)));
        requestApi(request, true, RequestTarget.REPLY_CONVERSATION, this);
    }

    private void setWidthHeightImage(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        heightImageUploaded = options.outHeight;
        widthImageUploaded = options.outWidth;
    }

    @Override
    public void onPause() {
        super.onPause();
        mUnregistrar.unregister();
    }

    @Override
    public void onStop() {
        super.onStop();
        mUnregistrar.unregister();
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_CONVERSATIONS_DETAIL:
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    Conversation conversation = ((ConversationDetailResponse) response.getData()).getConversationsDetail();
                    displayContent(conversation);
                    break;

                case REPLY_CONVERSATION:
                    if (((ConversationReplyResponse) response.getData()).getSent()) {
                        needScrollToBottomMessageList = true;
                        paginate = 1;
                        getConversationDetail(conversationId);
                    }
                    break;

                case UPLOAD_NEW_IMAGE:
                    UploadImageResponse uploadImageResponse = ((UploadImageResponse) response.getData());
                    replyConversation(uploadImageResponse.getImageUrl(), MessageType.IMAGE.getType());
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
