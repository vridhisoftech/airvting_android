package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.WelcomeActivity;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessageDialog extends BaseFragmentDialog {
    @BindView(R.id.tv_dialog_message_title)
    TextView mTvDialogTitle;
    @BindView(R.id.tv_dialog_message_msg)
    TextView mTvDialogMessage;
    @BindView(R.id.ll_dialog_message_action_button)
    LinearLayout mLlActionButton;
    @BindView(R.id.mrl_dialog_message_positive_action_button)
    MaterialRippleLayout mMrlPositiveActionButton;
    @BindView(R.id.mrl_dialog_message_negative_action_button)
    MaterialRippleLayout mMrlNegativeActionButton;
    @BindView(R.id.tv_dialog_message_positive_action_button_title)
    TextView mTvPositiveActionButtonTitle;
    @BindView(R.id.tv_dialog_message_negative_action_button_title)
    TextView mTvNegativeActionButtonTitle;

    private MessageDialogType mDialogType;
    private String mDialogTitle;
    private String mDialogMessage;
    private String mDialogPositiveButtonTitle;
    private String mDialogNegativeButtonTitle;
    private boolean mIsShowActionButtons = true;
    private OnActionClickListener mOnActionClickListener;

    @OnClick({R.id.iv_dialog_message_close, R.id.mrl_dialog_message_negative_action_button})
    void closeDialog() {
        dismiss();
        if (mOnActionClickListener != null) {
            mOnActionClickListener.onCloseActionButtonClick();
        } else if (mDialogType == MessageDialogType.UNAUTHORIZED) {
            clearUserLocalData();
            dismiss();
        }
    }

    @OnClick(R.id.mrl_dialog_message_positive_action_button)
    void dialogActionButtonClick() {
        switch (mDialogType) {
            case UPDATE_EMAIL:
                goToProfile();
                dismiss();
                break;
            case VERIFY_EMAIL:
                resendEmail();
                break;
            case CONNECT_WITH_STRIPE:
                connectWithStripe();
                dismiss();
                break;
            case ADD_PAYMENT_METHOD:
                goToAddCard();
                dismiss();
                break;
            case CONNECT_STRIPE_AND_ADD_PAYMENT:
                goToMyCard();
                dismiss();
                break;
            case UNAUTHORIZED:
                clearUserLocalData();
                dismiss();
                break;
            case PERMISSION:
                getActivity().finish();
                goToPermissionSettings();
                dismiss();
                break;
            case REMOVE_CART_ITEM:
            case LOG_OUT:
                if (mOnActionClickListener != null) {
                    mOnActionClickListener.onPositiveActionButtonClick();
                }
                break;
            case EXIT:


            case SENT_CONTACT_SUCCESS:
                if (mOnActionClickListener != null) {
                    mOnActionClickListener.onPositiveActionButtonClick();
                }
                dismiss();
                break;
            case CONFIRM_DEACTIVATE:
                deactivateAccount();
                break;
            case NOT_ENOUGH_TOKEN_BUY_GIFT:
            case DEFAULT:
                if (mOnActionClickListener != null) {
                    mOnActionClickListener.onPositiveActionButtonClick();
                }
                dismiss();
                break;
        }
    }

    public static MessageDialog newInstance() {
        return new MessageDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BORDER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_message, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setMessageAndAction();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    public void setCancel(boolean isCancel) {
        this.setCancelable(isCancel);
    }

    public void setDialogType(MessageDialogType dialogType) {
        mDialogType = dialogType;
    }

    public void setDialogTitle(String dialogTitle) {
        mDialogTitle = dialogTitle;
    }

    public void setDialogMessage(String dialogMessage) {
        mDialogMessage = dialogMessage;
    }

    public void setDialogPositiveButtonTitle(String dialogActionButtonTitle) {
        mDialogPositiveButtonTitle = dialogActionButtonTitle;
    }

    public void setDialogNegativeButtonTitle(String dialogNegativeButtonTitle) {
        mDialogNegativeButtonTitle = dialogNegativeButtonTitle;
    }

    public void setShowActionButtons(boolean isShowActionButtons) {
        mIsShowActionButtons = isShowActionButtons;
    }

    private void setMessageAndAction() {
        if (!TextUtils.isEmpty(mDialogTitle)) {
            mTvDialogTitle.setText(mDialogTitle);
        }

        if (!TextUtils.isEmpty(mDialogMessage)) {
            mTvDialogMessage.setText(mDialogMessage);
        }

        if (!TextUtils.isEmpty(mDialogPositiveButtonTitle)) {
            mTvPositiveActionButtonTitle.setText(mDialogPositiveButtonTitle);
            mMrlPositiveActionButton.setVisibility(View.VISIBLE);
        } else {
            mMrlPositiveActionButton.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(mDialogNegativeButtonTitle)) {
            mTvNegativeActionButtonTitle.setText(mDialogNegativeButtonTitle);
            mMrlNegativeActionButton.setVisibility(View.VISIBLE);
        } else {
            mMrlNegativeActionButton.setVisibility(View.GONE);
        }

        if (mIsShowActionButtons) {
            mLlActionButton.setVisibility(View.VISIBLE);
        } else {
            mLlActionButton.setVisibility(View.GONE);
        }
    }

    private void goToProfile() {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        String currentUserId = Utils.getCurrentUserId(getContext());
        intent.putExtra(Constants.EXTRA_USER_ID, currentUserId);
        DetailType.EDIT_PROFILE.attachTo(intent);
        startActivity(intent);
    }

    private void resendEmail() {
        requestApi(null, true, RequestTarget.RESEND_VERIFY_EMAIL, this);
    }

    private void connectWithStripe() {
        String createStripeAccountUrl = Utils.getCreateStripeAccountUrl(getContext());
        if (!TextUtils.isEmpty(createStripeAccountUrl)) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(createStripeAccountUrl));
            startActivity(browserIntent);
        }
    }

    private void goToAddCard() {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        DetailType.ADD_CARD.attachTo(intent);
        startActivity(intent);
    }

    private void goToMyCard() {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        DetailType.CARD_LIST.attachTo(intent);
        startActivity(intent);
    }

    private void clearUserLocalData() {
        Utils.removeCurrentUserInfoFromPreferences(getContext());
        Intent intent = new Intent(getContext(), WelcomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void goToPermissionSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse("package:" + getContext().getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        getContext().startActivity(intent);
    }

    private void deactivateAccount() {
        requestApi(null, true, RequestTarget.DEACTIVATE_ACCOUNT, this);
    }

    private void exitFromTheApp() {

    }
    private void logout() {
        requestApi(null, true, RequestTarget.LOG_OUT, this);
    }

    private void handleLogout() {
        Utils.removeCurrentUserInfoFromPreferences(getContext());
        Utils.removeEmailAndPaymentInfo(getContext());
        Utils.resetNotificationSetting(getContext());
        Intent intent = new Intent(getContext(), WelcomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        dismiss();
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case RESEND_VERIFY_EMAIL:
                    DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                            getString(R.string.dialog_verify_email_title),
                            getString(R.string.dialog_verify_email_sent));
                    dismiss();
                    break;
                case DEACTIVATE_ACCOUNT:
                    logout();
                    break;
                case LOG_OUT:
                    handleLogout();
                    break;
            }
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        String failDialogTitle = "";
        switch (requestTarget) {
            case RESEND_VERIFY_EMAIL:
                failDialogTitle = getString(R.string.dialog_verify_email_title);
                break;
            case DEACTIVATE_ACCOUNT:
                failDialogTitle = getString(R.string.dialog_confirm_deactivate_title);
                break;
            case LOG_OUT:
                failDialogTitle = getString(R.string.dialog_logout);
                break;
        }


        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), failDialogTitle, failMessage);
    }

    public interface OnActionClickListener {
        void onPositiveActionButtonClick();

        void onCloseActionButtonClick();
    }

    public void setOnActionButtonClickListener(OnActionClickListener onActionClickListener) {
        mOnActionClickListener = onActionClickListener;
    }
}
