package com.airvtinginc.airvtinglive.gui.components.photofilter;

import android.content.Context;
import android.graphics.Bitmap;

import com.zomato.photofilters.SampleFilters;
import com.zomato.photofilters.geometry.Point;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.ColorOverlaySubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.ToneCurveSubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.VignetteSubfilter;

import java.util.List;

/**
 * Created by dev on 8/29/17.
 */

public class MyEffect {

    public List<ThumbnailItem> getThumbList(Context ctx, Bitmap bmThumb){
        ThumbnailItem t1 = new ThumbnailItem();
        ThumbnailItem t2 = new ThumbnailItem();
        ThumbnailItem t3 = new ThumbnailItem();
        ThumbnailItem t4 = new ThumbnailItem();
        ThumbnailItem t5 = new ThumbnailItem();
        ThumbnailItem t6 = new ThumbnailItem();
        ThumbnailItem t7 = new ThumbnailItem();
        ThumbnailItem t8 = new ThumbnailItem();
        ThumbnailItem t9 = new ThumbnailItem();
        ThumbnailItem t10 = new ThumbnailItem();
        ThumbnailItem t11 = new ThumbnailItem();
        ThumbnailItem t12 = new ThumbnailItem();
        ThumbnailItem t13 = new ThumbnailItem();
        ThumbnailItem t14 = new ThumbnailItem();

        t1.image = bmThumb;
        t1.effectName = "Normal";
        t2.image = bmThumb;
        t2.effectName = "StarLit";
        t3.image = bmThumb;
        t3.effectName = "Blue";
        t4.image = bmThumb;
        t4.effectName = "Awe";
        t5.image = bmThumb;
        t5.effectName = "Lime";
        t6.image = bmThumb;
        t6.effectName = "Night";
        t7.image = bmThumb;
        t7.effectName = "Saturation";
        t8.image = bmThumb;
        t8.effectName = "Cross";
        t9.image = bmThumb;
        t9.effectName = "Darker";
        t10.image = bmThumb;
        t10.effectName = "Posterize";
        t11.image = bmThumb;
        t11.effectName = "ColorOverlay";
        t12.image = bmThumb;
        t12.effectName = "Contrast";
        t13.image = bmThumb;
        t13.effectName = "Brightness";
        t14.image = bmThumb;
        t14.effectName = "Vignette";

        ThumbnailsManager.clearThumbs();
        ThumbnailsManager.addThumb(t1); // Original Image

        t2.filter = SampleFilters.getStarLitFilter();
        ThumbnailsManager.addThumb(t2);

        t3.filter = SampleFilters.getBlueMessFilter();
        ThumbnailsManager.addThumb(t3);

        t4.filter = SampleFilters.getAweStruckVibeFilter();
        ThumbnailsManager.addThumb(t4);

        t5.filter = SampleFilters.getLimeStutterFilter();
        ThumbnailsManager.addThumb(t5);

        t6.filter = SampleFilters.getNightWhisperFilter();
        ThumbnailsManager.addThumb(t6);

        t7.filter = getSaturationFilter();
        ThumbnailsManager.addThumb(t7);

        t8.filter = getCrossFilter();
        ThumbnailsManager.addThumb(t8);

        t9.filter = getDarkerFilter();
        ThumbnailsManager.addThumb(t9);

        t10.filter = getposterizeFilter();
        ThumbnailsManager.addThumb(t10);

        t11.filter = getColorOverlayFilter();
        ThumbnailsManager.addThumb(t11);

        t12.filter = getContrastFilter();
        ThumbnailsManager.addThumb(t12);

        t13.filter = getBrightnessFilter();
        ThumbnailsManager.addThumb(t13);

        t14.filter = getVignetteFilter(ctx);
        ThumbnailsManager.addThumb(t14);

        return ThumbnailsManager.processThumbs(ctx);
    }

    private Filter getCrossFilter() {
        Point[] rgbKnots;
        Point[] redKnots;
        Point[] greenKnots;
        Point[] blueKnots;

        rgbKnots = new Point[2];
        rgbKnots[0] = new Point(0, 0);
        rgbKnots[1] = new Point(255, 255);

        redKnots = new Point[5];
        redKnots[0] = new Point(0, 0);
        redKnots[1] = new Point(64, 40);
        redKnots[2] = new Point(128, 125);
        redKnots[3] = new Point(175, 190);
        redKnots[4] = new Point(255, 255);

        greenKnots = new Point[5];
        greenKnots[0] = new Point(0, 0);
        greenKnots[1] = new Point(64, 48);
        greenKnots[2] = new Point(97, 128);
        greenKnots[3] = new Point(190, 208);
        greenKnots[4] = new Point(255, 208);

        blueKnots = new Point[4];
        blueKnots[0] = new Point(0, 0);
        blueKnots[1] = new Point(59, 24);
        blueKnots[2] = new Point(181, 223);
        blueKnots[3] = new Point(255, 255);

        Filter filter = new Filter();
        filter.addSubFilter(new ToneCurveSubfilter(rgbKnots, redKnots, greenKnots, blueKnots));
        return filter;
    }

    private Filter getDarkerFilter() {
        Point[] rgbKnots;
        Point[] redKnots;
        Point[] greenKnots;
        Point[] blueKnots;

        rgbKnots = new Point[3];
        rgbKnots[0] = new Point(0, 0);
        rgbKnots[1] = new Point(130, 101);
        rgbKnots[2] = new Point(255, 255);

        redKnots = new Point[2];
        redKnots[0] = new Point(0, 0);
        redKnots[1] = new Point(255, 255);

        greenKnots = new Point[2];
        greenKnots[0] = new Point(0, 0);
        greenKnots[1] = new Point(255, 255);

        blueKnots = new Point[2];
        blueKnots[0] = new Point(0, 0);
        blueKnots[1] = new Point(255, 255);

        Filter filter = new Filter();
        filter.addSubFilter(new ToneCurveSubfilter(rgbKnots, redKnots, greenKnots, blueKnots));
        return filter;
    }

    private Filter getposterizeFilter() {
        Filter myFilter = new Filter();
        Point[] rgbKnots;
        rgbKnots = new Point[6];
        rgbKnots[0] = new Point(0, 0);
        rgbKnots[1] = new Point(20, 255);
        rgbKnots[2] = new Point(60, 200);
        rgbKnots[3] = new Point(80, 175);
        rgbKnots[4] = new Point(125, 150);
        rgbKnots[5] = new Point(175, 100);

        myFilter.addSubFilter(new ToneCurveSubfilter(rgbKnots, null, null, null));
        return myFilter;
    }

    private Filter getSaturationFilter() {
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new SaturationSubfilter(1.3f));
        return myFilter;
    }

    private Filter getColorOverlayFilter() {
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new ColorOverlaySubfilter(100, .2f, .2f, .0f));
        return myFilter;
    }

    private Filter getContrastFilter() {
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new ContrastSubfilter(1.2f));
        return myFilter;
    }

    private Filter getBrightnessFilter() {
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubfilter(30));
        return myFilter;
    }

    private Filter getVignetteFilter(Context ctx) {
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new VignetteSubfilter(ctx, 100));
        return myFilter;
    }
}
