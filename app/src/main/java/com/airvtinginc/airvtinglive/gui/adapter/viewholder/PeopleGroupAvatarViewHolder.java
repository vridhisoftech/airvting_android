package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleGroupAvatarViewHolder extends BaseViewHolder {
    public View mView;
    @BindView(R.id.ivAvatar)
    public CircleImageView ivAvatar;
    @BindView(R.id.ivDefaultAvatar)
    public CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    public GradientTextView tvDefaultAvatar;
    @BindView(R.id.ivIsLive)
    public ImageView ivIsLive;
    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.pbAvatarLoading)
    public ProgressBar pbAvatarLoading;

    public PeopleGroupAvatarViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}
