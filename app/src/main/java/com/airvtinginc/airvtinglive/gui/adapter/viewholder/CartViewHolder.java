package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.iv_cart_item_product_image)
    RoundedImageView ivProductImage;
    public @BindView(R.id.iv_cart_item_product_image_sale_overlay)
    RoundedImageView ivProductImageSaleOverlay;
    public @BindView(R.id.tv_cart_item_overlay_price)
    TextView tvProductSaleOverlay;
    public @BindView(R.id.pb_cart_item_product_image_loading)
    ProgressBar pbProductImageLoading;
    public @BindView(R.id.tv_cart_item_product_name)
    TextView tvProductName;
    public @BindView(R.id.tv_cart_item_price)
    TextView tvPrice;
    public @BindView(R.id.iv_cart_item_decrease)
    ImageView ivDecrease;
    public @BindView(R.id.iv_cart_item_increase)
    ImageView ivIncrease;
    public @BindView(R.id.tv_cart_item_quantity)
    TextView tvQuantity;

    public CartViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}