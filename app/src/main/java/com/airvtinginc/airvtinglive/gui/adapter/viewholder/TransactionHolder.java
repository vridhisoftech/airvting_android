package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivProduct)
    RoundedImageView ivProduct;
    public @BindView(R.id.pbProduct)
    ProgressBar pbProduct;
    public @BindView(R.id.tvPrice)
    TextView tvPrice;
    public @BindView(R.id.tvUserName)
    TextView tvUserName;
    public @BindView(R.id.tvDate)
    TextView tvDate;
    public @BindView(R.id.tvTitle)
    TextView tvTitle;
    public @BindView(R.id.tvPriceAirToken)
    TextView tvPriceAirToken;
    public @BindView(R.id.tvDateFull)
    TextView tvDateFull;
    public @BindView(R.id.tvStatus)
    TextView tvStatus;
    public @BindView(R.id.tvQuantity)
    TextView tvQuantity;

    public TransactionHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}