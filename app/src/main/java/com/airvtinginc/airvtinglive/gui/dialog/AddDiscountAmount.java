package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddDiscountAmount extends DialogFragment implements TextWatcher {
    private static String TAG = AddDiscountAmount.class.getName();

    private final int MAXIMUM_PERCENTAGE = 100;
    @BindView(R.id.edAmount)
    EditText edAmount;

    private float price = 0;

    private String oldText = "";

    @OnClick({R.id.llBackground, R.id.btnClose})
    void close() {
        dismiss();
    }

    @OnClick(R.id.btnOk)
    void okClick() {
        String amount = edAmount.getText().toString();
        if (!TextUtils.isEmpty(amount)) {
            String amountPercentString = amount.replace(",", "");
            if (amount.contains("%")) {
                amountPercentString = amountPercentString.substring(0, amountPercentString.indexOf("%"));
                if (TextUtils.isEmpty(amountPercentString)) {
                    DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_discount_amount_tile).toUpperCase(),
                            getString(R.string.dialog_add_product_error_add_sale_wrong_format));
                    return;
                }
            }

            float amountPercent = Float.parseFloat(amountPercentString);
            Log.d(TAG, "amountPercent: " + amountPercent);

            boolean percentInWrongPosition = amount.indexOf("%") != amount.length() - 1;
            if (amount.contains("%") && percentInWrongPosition) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_discount_amount_tile).toUpperCase(),
                        getString(R.string.dialog_add_product_error_add_sale_wrong_format));
                return;
            } else if (amount.contains("%") && amountPercent > MAXIMUM_PERCENTAGE) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_discount_amount_tile).toUpperCase(),
                        getString(R.string.dialog_add_product_error_add_sale_over_percent));
                return;
            }

            float discount = NumberUtils.getPriceFromString(amount.replace("%", ""));
            Log.v("discount ",discount+" Price"+price);

//            if (price == 0 || (!amount.contains("%") && discount > price)) {
//                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
//                        getString(R.string.add_product_error_discount_greater_than_price));
//                return;
//            }

            if (onOkListener != null) {
                onOkListener.onOkClick(amount);
            }
        }
    }

    private OnOkListener onOkListener;

    public OnOkListener getOnOkListener() {
        return onOkListener;
    }

    public void setOnOkListener(OnOkListener onOkListener) {
        this.onOkListener = onOkListener;
    }

    public static AddDiscountAmount newInstance() {
        return new AddDiscountAmount();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_discount_amount, container, false);
        ButterKnife.bind(this, view);
        Utils.systemKeyboard(getDialog());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        setUpEditTextInput();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void setUpEditTextInput() {
//        edAmount.setFilters(new InputFilter[]{new CustomRangeInputFilter(0, MAX_VALUE_AMOUNT)});
        edAmount.addTextChangedListener(this);
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        edAmount.removeTextChangedListener(this);
        String str = editable.toString();
        if (str.length() > 0) {
            // Not allow input multi "." or multi "%"
            if (countCharacter(str, '.') > 1 || countCharacter(str, '%') > 1) {
                edAmount.setText(oldText);
            } else {
                NumberUtils.formatPriceEditText(editable, edAmount, edAmount.getSelectionStart(), false);
            }
        }
        edAmount.addTextChangedListener(this);
        oldText = edAmount.getText().toString();
        edAmount.setSelection(oldText.length());
    }

    private int countCharacter(String str, Character character) {
        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == character) {
                counter++;
            }
        }
        return counter;
    }

    public interface OnOkListener {
        void onOkClick(String amount);
    }
}
