package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.adapter.ChoseTextAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.gui.dialog.ListDialog;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.FollowType;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.request.FollowUserRequest;
import com.airvtinginc.airvtinglive.models.request.ReportRequest;
import com.airvtinginc.airvtinglive.models.response.ReportResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lightfire.gradienttextcolor.GradientTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ProfileFragment extends BaseFragment {

    private static final String TAG = ProfileFragment.class.getName();

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.mViewPager)
    ViewPager mViewPager;
    @BindView(R.id.rlCover)
    RelativeLayout mRlCover;
    @BindView(R.id.ivCover)
    ImageView mIvCover;
    @BindView(R.id.ivAvatar)
    CircleImageView mIvAvatar;
    @BindView(R.id.ivAvatarRefresh)
    CircleImageView ivAvatarRefresh;
    @BindView(R.id.ivIsLive)
    ImageView mIvIsLive;
    @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    GradientTextView mTvDefaultAvatar;
    @BindView(R.id.tvUsername)
    TextView mTvUsername;
    @BindView(R.id.ivEdit)
    ImageView mIvEdit;
    @BindView(R.id.ivMessage)
    ImageView mIvMessage;
    @BindView(R.id.rlFollow)
    RelativeLayout mRlFollow;
    @BindView(R.id.ivIsFollow)
    ImageView mIvIsFollow;
    @BindView(R.id.tvFollow)
    TextView mTvFollow;
    @BindView(R.id.iBtnReport)
    ImageButton iBtnReport;
    @BindView(R.id.tvIntroduction)
    TextView mTvIntroduction;
    @BindView(R.id.tvNumberOfPosts)
    TextView mTvNumberOfPosts;
    @BindView(R.id.tvNumberOfFollowers)
    TextView mTvNumberOfFollowers;
    @BindView(R.id.tvNumberOfFollowing)
    TextView mTvNumberOfFollowing;
    @BindView(R.id.tvLocation)
    TextView mTvLocation;

    @BindView(R.id.pbAvatarLoading)
    ProgressBar mPbAvatar;

    private String mUserId;
    private String mUsername;
    private boolean mIsCurrentUser;

    @OnClick(R.id.iBtnReport)
    void report() {
        ListDialog report = ListDialog.newInstance();
        report.setShowDialogType(ShowDialogType.REPORT);
        report.setTitle(getActivity(), R.string.dialog_report_title);
        report.setBackgroundColorRes(getActivity(), R.color.white);
        report.setTitleColorRes(getActivity(), R.color.black);
        ChoseTextAdapter choseTextAdapter = new ChoseTextAdapter(getActivity(), new ArrayList<>(), null);
        choseTextAdapter.setTextColorRes(getActivity(), R.color.black);
        choseTextAdapter.setBackgroundColorRes(getActivity(), android.R.color.white);
        report.setAdapter(choseTextAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        report.setOnSendListener(content -> {
            if (!TextUtils.isEmpty(mUserId)) {
                Map<String, RequestBody> bodyMap = new HashMap<>();
                bodyMap.put("userId", RequestBody.create(MediaType.parse("application/json"), mUserId));
                bodyMap.put("content", RequestBody.create(MediaType.parse("application/json"), content));
                ReportRequest reportRequest = new ReportRequest(bodyMap);
                requestApi(reportRequest, true, RequestTarget.REPORT, this);
            }
        });
        report.setOnDismissListener(() -> {
            report.dismiss();
        });
        report.show(getFragmentManager(), "Add Report Dialog");
    }

    @OnClick(R.id.ivMessage)
    void goToMessage() {
        if (!TextUtils.isEmpty(mUserId)) {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            intent.putExtra(Constants.EXTRA_USER_ID, mUserId);
            intent.putExtra(Constants.EXTRA_USERNAME, mUsername);
            DetailType.CONVERSATIONS_ADD_NEW.attachTo(intent);
            startActivity(intent);
        }
    }

    @OnClick(R.id.llFollowers)
    void showFollowers() {
        goToFollows(FollowType.FOLLOWERS.getType());
    }

    @OnClick(R.id.llFollowing)
    void showFollowing() {
        goToFollows(FollowType.FOLLOWING.getType());
    }

    private void goToFollows(String followType) {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra(Constants.EXTRA_FOLLOW_TYPE, followType);
        intent.putExtra(Constants.EXTRA_USER_ID, mUserId);
        DetailType.FOLLOWS.attachTo(intent);
        startActivity(intent);
    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String userId, boolean isCurrentUser) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, userId);
        args.putBoolean(Constants.EXTRA_USER_IS_CURRENT_USER, isCurrentUser);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mUserId = getArguments().getString(Constants.EXTRA_USER_ID);
            this.mIsCurrentUser = getArguments().getBoolean(Constants.EXTRA_USER_IS_CURRENT_USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        prepareViewPager();
        prepareTabLayout();
        enableProfileIcons();
        ivAvatarRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProfile();
            }
        });
    }

    private void prepareViewPager() {
        PageFragmentAdapter mAdapter = new PageFragmentAdapter(getChildFragmentManager());
        PreviousLiveStreamsFragment previousLiveStreamsFragment;
        if (!mIsCurrentUser) {
            mAdapter.addFragment(GalleryFragment.newInstance(mUserId, false), null);
            previousLiveStreamsFragment = PreviousLiveStreamsFragment.newInstance(mUserId, false);
            previousLiveStreamsFragment.setGetProfileCallBack(this::getProfile);
            mAdapter.addFragment(previousLiveStreamsFragment, null);
            mAdapter.addFragment(ProfileStoreFragment.newInstance(mUserId), null);
        } else {
            mAdapter.addFragment(GalleryFragment.newInstance("", true), null);
            previousLiveStreamsFragment = PreviousLiveStreamsFragment.newInstance("", true);
            previousLiveStreamsFragment.setGetProfileCallBack(this::getProfile);
            mAdapter.addFragment(previousLiveStreamsFragment, null);
//            mAdapter.addFragment(BookmarkFragment.newInstance(), null);
        }
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(2);
    }

    private void enableProfileIcons() {
        mIvMessage.setEnabled(!mIsCurrentUser);
        mRlFollow.setEnabled(!mIsCurrentUser);
        iBtnReport.setEnabled(!mIsCurrentUser);
        mIvEdit.setEnabled(mIsCurrentUser);

        if (mIsCurrentUser) {
            mIvMessage.setVisibility(View.INVISIBLE);
            mRlFollow.setVisibility(View.GONE);
            iBtnReport.setVisibility(View.GONE);
        } else {
            mIvEdit.setVisibility(View.INVISIBLE);
        }
    }

    private void prepareTabLayout() {
        int[] tabIcons;
        if (!mIsCurrentUser) {
//            viewLine.setVisibility(View.GONE);
            Intent intent = new Intent(DetailActivity.BroadcastActionChangeToolbarToTransparent);
            getActivity().sendBroadcast(intent);
            tabIcons = new int[]{R.drawable.ic_gallery_black, R.drawable.ic_previous_live_stream_black, R.drawable.ic_store_black};
        } else {
            mIvEdit.setVisibility(View.VISIBLE);
            tabIcons = new int[]{R.drawable.ic_gallery_black, R.drawable.ic_previous_live_stream_black};
        }

        for (int i = 0; i < tabIcons.length; i++) {
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
//            tabLayout.addTab(tabLayout.newTab().setIcon(tabIcons[i]));
        }

        final int tabIconColor = ContextCompat.getColor(getContext(), R.color.transparent_70_black);
        for (int i = 0; i < tabIcons.length; i++) {
            tabLayout.getTabAt(i).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(getContext(), R.color.black);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(getContext(), R.color.transparent_70_black);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.getTabAt(1).select();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mIsCurrentUser) {
            getActivity().sendBroadcast(new Intent(DetailActivity.BroadcastActionChangeToolbarToTransparent));
        }
        getProfile();
    }

    public void getProfile() {
        if (TextUtils.isEmpty(mUserId) && mIsCurrentUser) {
            mUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        }

        requestApi(mUserId, true, RequestTarget.GET_USER_PROFILE, this);
    }

    private void showUserInfo(User user) {

        mUserId = user.getId();
        mUsername = user.getUsername();
        if (mIsCurrentUser) {
            SharedPreferencesManager.getInstance(getActivity()).putString(Constants.PREF_CURRENT_USER_PHOTO_URL, user.getAvatar());
        }

        if (!TextUtils.isEmpty(user.getAvatar())) {
            mPbAvatar.setVisibility(View.VISIBLE);
            mIvAvatar.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(user.getAvatar())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            mPbAvatar.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(getActivity(), ivDefaultAvatar, mTvDefaultAvatar, mIvAvatar, user, R.dimen.profile_default_avatar_text_size);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            mPbAvatar.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(getActivity(), ivDefaultAvatar, mTvDefaultAvatar, mIvAvatar);
                            return false;
                        }
                    })
                    .into(mIvAvatar);
        } else {
            mPbAvatar.setVisibility(View.GONE);
            Utils.setDefaultAvatar(getActivity(), ivDefaultAvatar, mTvDefaultAvatar, mIvAvatar, user, R.dimen.profile_default_avatar_text_size);
        }

        if (!TextUtils.isEmpty(user.getCover())) {
            Glide.with(getActivity()).load(user.getCover()).into(mIvCover);
        }

        if (!mIsCurrentUser && user.isLive()) {
            mIvIsLive.setVisibility(View.VISIBLE);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500); //You can manage the blinking time with this parameter
            anim.setStartOffset(0);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            mIvIsLive.startAnimation(anim);
        } else {
            mIvIsLive.setAnimation(null);
            mIvIsLive.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getDisplayName())) {
//            mTvName.setText(user.getDisplayName());
            if (mIsCurrentUser) {
                Intent intent = new Intent(MainActivity.BroadcastActionUpdateMainBarTitle);
                intent.putExtra(Constants.EXTRA_TITLE, user.getDisplayName());
                getActivity().sendBroadcast(intent);
            } else {
                Intent intent = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
                DetailActivity.headerTitle = user.getDisplayName();
                getActivity().sendBroadcast(intent);
            }
        }

        if (!TextUtils.isEmpty(user.getUsername())) {
            String displayUserName = "@" + user.getUsername();
            mTvUsername.setText(displayUserName);
        }

        if (!TextUtils.isEmpty(user.getDescription())) {
            mTvIntroduction.setText(user.getDescription());
        }

        if (mIsCurrentUser && user.getLocation() != null && !TextUtils.isEmpty(user.getLocation().getAddress())) {
            mTvLocation.setText(user.getLocation().getAddress());
            mTvLocation.setVisibility(View.VISIBLE);
        } else {
            mTvLocation.setVisibility(View.GONE);
        }

        mTvNumberOfPosts.setText(String.valueOf(user.getNumberOfPosts()));
        mTvNumberOfFollowers.setText(String.valueOf(user.getNumberOfFollowers()));
        mTvNumberOfFollowing.setText(String.valueOf(user.getNumberOfFollowing()));

        SharedPreferencesManager.getInstance(getContext()).putInt(Constants.PREF_CURRENT_USER_TOKEN, user.getAirToken());

        updateFollow(user.isFollow());

        mIvEdit.setOnClickListener(view -> goToEditProfile());
        mRlFollow.setOnClickListener(view -> {
            FollowUserRequest followUserRequest = new FollowUserRequest(user.getId());
            requestApi(followUserRequest, true, RequestTarget.FOLLOW_USER, this);
        });

        checkForDisplaying(user);
    }

    private void checkForDisplaying(User user) {
        int headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height);
        String description = user.getDescription();
        String address = user.getLocation().getAddress();

        boolean isNotEmptyDesAndAddress = !TextUtils.isEmpty(description) && !TextUtils.isEmpty(address);
        boolean isEmptyDesAndAddress = TextUtils.isEmpty(description) && TextUtils.isEmpty(address);
        boolean isEmptyDescriptionOnly = TextUtils.isEmpty(description) && !TextUtils.isEmpty(address);
        boolean isEmptyAddressOnly = !TextUtils.isEmpty(description) && TextUtils.isEmpty(address);

        if (mIsCurrentUser) {
            if (isNotEmptyDesAndAddress) {
                // Display no follow region + Description + address
                if (mTvIntroduction.getLineCount() == 1) {
                    headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller) - 30;
                } else {
                    headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller);
                }
            } else if (isEmptyDescriptionOnly) {
                // Display no follow region + Only address
                headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller) - 20;
            } else if (isEmptyAddressOnly) {
                // Display no follow region + Only description
                if (mTvIntroduction.getLineCount() == 1) {
                    headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller) - 80;
                } else {
                    headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller) - 40;
                }
            } else if (isEmptyDesAndAddress) {
                // Display no follow region + No description or address
                headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_no_follow_no_description);
            }
        } else {
            if (isEmptyDescriptionOnly) {
                // Display follow region + Only address
                headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller);
            } else if (isEmptyAddressOnly) {
                // Display follow region + Only description
                if (mTvIntroduction.getLineCount() == 1) {
                    headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller) - 20;
                } else {
                    headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller) + 5;
                }
            } else if (isEmptyDesAndAddress) {
                // Display follow region + No description or address
                headerHeight = (int) getResources().getDimension(R.dimen.profile_cover_height_smaller) - 20;
            }
        }

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, headerHeight);
        mRlCover.setLayoutParams(params);
    }

    private void goToEditProfile() {
        if (!TextUtils.isEmpty(mUserId)) {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            intent.putExtra(Constants.EXTRA_USER_ID, mUserId);
            DetailType.EDIT_PROFILE.attachTo(intent);
            startActivity(intent);
        }
    }

    private void updateFollow(boolean isFollow) {
        if (isFollow) {
            mIvIsFollow.setImageResource(R.drawable.ic_check);
            mTvFollow.setText(getString(R.string.my_user_following_capitalize));
        } else {
            mIvIsFollow.setImageResource(R.drawable.ic_add_store);
            mTvFollow.setText(getString(R.string.my_user_follow_capitalize));
        }
    }

    private void showUserNotFoundDialog(String message) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setDialogTitle(getString(R.string.profile_title));
        messageDialog.setDialogMessage(message);
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                getActivity().finish();
            }

            @Override
            public void onCloseActionButtonClick() {
                getActivity().finish();
            }
        });
        messageDialog.setCancelable(false);
        messageDialog.show(getFragmentManager(), "Show User Not Found Dialog");
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            ivAvatarRefresh.setVisibility(View.GONE);
            switch (requestTarget) {
                case GET_USER_PROFILE: {
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null) {
                        Utils.saveEmailAndPaymentInfo(getContext(), userResponse.getUser());
                        Utils.saveNotificationSetting(getContext(), userResponse.getUser().getConfigSettings().getNotificationsConfig());
                        showUserInfo(userResponse.getUser());
                    }
                    break;
                }
                case FOLLOW_USER: {
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null && userResponse.getUser() != null) {
                        updateFollow(userResponse.getUser().isFollow());
                    }
                    break;
                }
                case REPORT:
                    ReportResponse reportResponse = ((UsersResponse) response.getData()).getReportResponse();
                    DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_report_title), getString(R.string.dialog_thanks_report));
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        switch (requestTarget) {
            case REPORT:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_report_title), failMessage);
                break;
            case GET_USER_PROFILE:
                if (!mIsCurrentUser)
                    showUserNotFoundDialog(failMessage);
                break;
        }
        if (statusCode==EnumManager.StatusCode.ERR_NO_INTERNET_CONNECTION)
        {
            ivAvatarRefresh.setVisibility(View.VISIBLE);
        }
    }

}
