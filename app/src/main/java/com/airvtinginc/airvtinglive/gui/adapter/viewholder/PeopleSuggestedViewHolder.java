package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleSuggestedViewHolder extends BaseViewHolder {
    public View mView;
    @BindView(R.id.tvSuggested)
    public TextView tvSuggested;
    @BindView(R.id.tvViewMore)
    public TextView tvViewMore;
    @BindView(R.id.rvPhotos)
    public RecyclerView rvPhotos;
    @BindView(R.id.ivAvatar)
    public CircleImageView ivAvatar;
    @BindView(R.id.ivDefaultAvatar)
    public CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    public GradientTextView tvDefaultAvatar;
    @BindView(R.id.pbAvatarLoading)
    public ProgressBar pbAvatar;
    @BindView(R.id.llInfo)
    public LinearLayout llInfo;
    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.tvStatus)
    public TextView tvStatus;
    public @BindView(R.id.rlFollow)
    RelativeLayout rlFollow;
    public @BindView(R.id.ivIsFollow)
    ImageView ivIsFollow;
    public @BindView(R.id.tvFollow)
    TextView tvFollow;
    @BindView(R.id.marginTop)
    public View marginTop;

    public PeopleSuggestedViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}
