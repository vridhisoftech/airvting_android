package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.NotificationActivityViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.gui.enums.NotiActivityType;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.models.PostInNotification;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.request.FollowUserRequest;
import com.airvtinginc.airvtinglive.models.response.Notification;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivityAdapter extends BaseAdapter {
    private static final String TAG = NotificationActivityAdapter.class.getSimpleName();
    private static final int VIEW_TYPE_ACTIVITY = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;
    private User mFollowUser;

    public NotificationActivityAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE:
                View loadMoreView = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(loadMoreView);
            default:
                View offerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_noti_activity, parent, false);
                return new NotificationActivityViewHolder(offerView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder || list.get(position) == null) {
            return;
        }

        final NotificationActivityViewHolder holder = (NotificationActivityViewHolder) mHolder;
        Notification notification = (Notification) list.get(position);
        User notifier = notification.getNotifier();
        ProductInPost product = notification.getProduct();
        PostInNotification post = notification.getPost();

        if (notifier != null) {
            if (!TextUtils.isEmpty(notifier.getAvatar())) {
                holder.pbAvatarLoading.setVisibility(View.VISIBLE);
                holder.ivAvatar.setVisibility(View.VISIBLE);
                Glide.with(context).load(notifier.getAvatar())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, notifier, R.dimen.text_size_comment_default_avatar_text);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                                return false;
                            }
                        })
                        .into(holder.ivAvatar);
            } else {
                holder.pbAvatarLoading.setVisibility(View.GONE);
                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, notifier, R.dimen.text_size_comment_default_avatar_text);
            }
            holder.ivAvatar.setOnClickListener(view -> openUserDetail(notifier));

            String displayName;
            if (!TextUtils.isEmpty(notifier.getUsername())) {
                displayName = "@" + notifier.getUsername();
            } else {
                displayName = "@" + notifier.getDisplayName();
            }
            holder.tvUsername.setText(displayName);

            if (notifier.isFollow()) {
                holder.rlFollow.setVisibility(View.GONE);
            } else {
                holder.rlFollow.setVisibility(View.VISIBLE);
                holder.rlFollow.setOnClickListener(view -> followUser(notifier));
            }
        }

        holder.tvStatus.setText(notification.getNotifyMessage());

        if (!TextUtils.isEmpty(notification.getCreatedAt())) {
            try {
                String displayTime = DateTimeUtils.getElapsedInterval(context, DateTimeUtils.getDateFormatUTC(notification.getCreatedAt()));
                holder.tvTime.setText(displayTime.toLowerCase());
            } catch (Exception e) {
                AppLog.e(e.getMessage());
            }
        }

        if (notification.isRead()) {
            holder.cvBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cvBackground.setBackgroundColor(context.getResources().getColor(R.color.color_cell_day));
        }


        if (notification.getType().equals(NotiActivityType.FOLLOW.getType())) {
            holder.llMessage.setVisibility(View.GONE);
            holder.rvLikedProductImage.setVisibility(View.GONE);

            holder.mView.setOnClickListener(view -> {
                readNotification(notification.getId());
                openUserDetail(notifier);
            });
        } else if (notification.getType().equals(NotiActivityType.OFFER.getType())) {
            holder.llMessage.setVisibility(View.VISIBLE);
            holder.rvLikedProductImage.setVisibility(View.GONE);
            holder.ivOfferImage.setVisibility(View.VISIBLE);

            if (product != null) {
                if (!TextUtils.isEmpty(product.getFeaturedImage())) {
                    holder.pbOfferImageLoading.setVisibility(View.VISIBLE);
                    Glide.with(context).load(product.getFeaturedImage())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    holder.pbOfferImageLoading.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    holder.pbOfferImageLoading.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(holder.ivOfferImage);

                    holder.ivOfferImage.setOnClickListener(view -> {
                        readNotification(notification.getId());
                        openProductDetail(product);
                    });
                }

                if (!TextUtils.isEmpty(product.getTitle())) {
                    holder.tvMessage.setText(product.getTitle());

                    if (notification.isRead()) {
                        holder.tvMessage.setTypeface(holder.tvMessage.getTypeface(), Typeface.NORMAL);
                    } else {
                        holder.tvMessage.setTypeface(holder.tvMessage.getTypeface(), Typeface.BOLD);
                    }
                }

                holder.mView.setOnClickListener(view -> {
                    readNotification(notification.getId());
                    openTransactionScreen();
                });
            }

        } else if (notification.getType().equals(NotiActivityType.LIKE.getType())) {
            holder.llMessage.setVisibility(View.GONE);

            if (product != null) {
                // Case: Like product
                List<FeaturedImage> featuredImages = product.getFeaturedImages();
                if (featuredImages != null && !featuredImages.isEmpty()) {
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    LikeProductPhotoAdapter adapter = new LikeProductPhotoAdapter(context, new OnItemClickListener() {
                        @Override
                        public <T> void onItemClicked(T item, int position) {
                            openProductDetail(product);
                        }
                    });
                    adapter.addAllItems(featuredImages);
                    holder.rvLikedProductImage.setLayoutManager(linearLayoutManager);
                    holder.rvLikedProductImage.setAdapter(adapter);
                    holder.rvLikedProductImage.setVisibility(View.VISIBLE);
                }
            } else {
                // Case: Like stream
                holder.rvLikedProductImage.setVisibility(View.GONE);
            }

            holder.mView.setOnClickListener(view -> {
                readNotification(notification.getId());
                openProductDetail(product);
            });
        } else if (notification.getType().equals(NotiActivityType.LIVE_STREAM.getType())) {
            holder.llMessage.setVisibility(View.GONE);
            holder.ivOfferImage.setVisibility(View.GONE);
            holder.rvLikedProductImage.setVisibility(View.GONE);

            holder.mView.setOnClickListener(view -> {
                readNotification(notification.getId());
                openStream(post, notifier.getUserId());
            });
        } else if (notification.getType().equals(NotiActivityType.COMMENT.getType())) {
            holder.llMessage.setVisibility(View.GONE);
            holder.ivOfferImage.setVisibility(View.GONE);
            holder.rvLikedProductImage.setVisibility(View.GONE);

            holder.mView.setOnClickListener(view -> {
                readNotification(notification.getId());
                openMedia(post);
            });
        } else if (notification.getType().equals(NotiActivityType.TAGGED.getType())) {
            holder.llMessage.setVisibility(View.GONE);
            holder.rvLikedProductImage.setVisibility(View.GONE);

            holder.mView.setOnClickListener(view -> {
                readNotification(notification.getId());
                openPostDetail(post, notifier);
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ACTIVITY : VIEW_TYPE_LOAD_MORE;
    }

    private void readNotification(String notificationId) {
        Notification notification = new Notification();
        notification.setId(notificationId);
        requestApi(notification, false, RequestTarget.READ_NOTIFICATION, this);
    }

    private void openUserDetail(User notifier) {
        if (notifier != null && !TextUtils.isEmpty(notifier.getUserId())) {
            Utils.openUserDetail(context, notifier.getUserId());
        }
    }

    private void openMedia(PostInNotification post) {
        if (post != null && !TextUtils.isEmpty(post.getPostId())) {
            Intent intent = new Intent(context, DetailActivity.class);
            Bundle args = new Bundle();
            args.putString(Constants.EXTRA_GALLERY_ID, post.getPostId());
            intent.putExtras(args);
            // Sender usage
            DetailType.GALLERY.attachTo(intent);
            context.startActivity(intent);
        }
    }

    private void openProductDetail(ProductInPost product) {
        if (product != null && !TextUtils.isEmpty(product.getProductId())) {
            Intent intent = new Intent(context, DetailActivity.class);
            Bundle args = new Bundle();
            args.putString(Constants.EXTRA_PRODUCT_ID, product.getProductId());
            intent.putExtras(args);
            // Sender usage
            DetailType.PRODUCT.attachTo(intent);
            context.startActivity(intent);
        }
    }

    private void openPostDetail(PostInNotification post, User notifier) {
        Intent intent = null;
        Bundle args = new Bundle();
        if (post.getType().equals(MediaType.STREAM.getType())) {
            if (!TextUtils.isEmpty(post.getPostId()) && !TextUtils.isEmpty(notifier.getUserId())) {
                intent = new Intent(context, LiveStreamActivity.class);
                args.putString(Constants.EXTRA_USER_ID, notifier.getUserId());
                args.putString(Constants.EXTRA_POST_ID, post.getPostId());
                args.putString(Constants.EXTRA_MEDIA_URL, post.getMediaUrl());
                intent.putExtras(args);
            }
        } else {
            intent = new Intent(context, DetailActivity.class);
            args.putString(Constants.EXTRA_GALLERY_ID, post.getPostId());
            intent.putExtras(args);
            // Sender usage
            DetailType.GALLERY.attachTo(intent);
        }

        if (intent != null) {
            context.startActivity(intent);
        }
    }

    private void openStream(PostInNotification post, String streamerId) {
        if (TextUtils.isEmpty(post.getPostId()) || TextUtils.isEmpty(streamerId)) {
            return;
        }
        Intent intent = new Intent(context, LiveStreamActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, streamerId);
        args.putString(Constants.EXTRA_POST_ID, post.getPostId());
        args.putString(Constants.EXTRA_MEDIA_URL, post.getMediaUrl());
        intent.putExtras(args);
        context.startActivity(intent);
    }

    private void openTransactionScreen() {
        Intent intent = new Intent(context, DetailActivity.class);
        DetailType.WALLET.attachTo(intent);
        context.startActivity(intent);
    }

    private void followUser(User user) {
        mFollowUser = user;
        FollowUserRequest followUserRequest = new FollowUserRequest(mFollowUser.getUserId());
        requestApi(followUserRequest, true, RequestTarget.FOLLOW_USER, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case FOLLOW_USER:
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null && userResponse.getUser() != null && mFollowUser != null) {
                        for (int i = 0; i < list.size(); i++) {
                            Notification notification = (Notification) list.get(i);
                            if (notification.getNotifier() != null
                                    && notification.getNotifier().getUserId().equals(mFollowUser.getUserId())) {
                                notification.getNotifier().setFollow(userResponse.getUser().isFollow());
                            }
                        }
                        notifyDataSetChanged();
                        if (!TextUtils.isEmpty(mFollowUser.getDisplayName())) {
                            Toast.makeText(context, String.format(context.getString(R.string.activity_followed), mFollowUser.getDisplayName()), Toast.LENGTH_SHORT).show();
                        }

                    }
                    break;
                case READ_NOTIFICATION:
                    Log.d(TAG, "onResponseSuccess: Read notification activity");
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
        DialogUtils.hideLoadingProgress();
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.hideLoadingProgress();
        switch (requestTarget) {
            case READ_NOTIFICATION:
                Log.d(TAG, "onResponseFail: Read notification activity - failMessage: " + failMessage);
                break;
        }
    }
}
