package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.GiftHistoryHolder;
import com.airvtinginc.airvtinglive.gui.enums.GiftType;
import com.airvtinginc.airvtinglive.models.response.GiftHistory;
import com.airvtinginc.airvtinglive.models.response.Sender;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class GiftHistoryAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    public GiftHistoryAdapter(Context context, List<GiftHistory> giftHistories, OnItemClickListener onItemClickListener) {
        super(context, giftHistories, onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gift_history, parent, false);
                GiftHistoryHolder vh = new GiftHistoryHolder(v);
                return vh;
            }
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null) return;
        GiftHistoryHolder holder = (GiftHistoryHolder) mHolder;
        GiftHistory giftHistory = (GiftHistory) list.get(position);
        try {
            holder.tvDateFull.setText(DateTimeUtils.convertStringDateToOtherFormat(DateTimeUtils.getDateFormatUTC(giftHistory.getCreatedAt()), DateTimeUtils.DATE_TIME_FORMAT, DateTimeUtils.DATE_FORMAT));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        holder.tvQuantity.setText(String.valueOf(giftHistory.getQuantity()));
        holder.tvMethod.setText(giftHistory.getMethod() + " ");
        Sender sender;
        String username;
        String featuredImage;

        if (giftHistory.getMethod().toLowerCase().equals(GiftType.RECEIVED.getType().toLowerCase())) {
            sender = giftHistory.getSender();
            username = giftHistory.getSender().getDisplayName();
            featuredImage = giftHistory.getSender().getFeaturedImage();
        } else {
            sender = giftHistory.getReceiver();
            username = giftHistory.getReceiver().getDisplayName();
            featuredImage = giftHistory.getReceiver().getFeaturedImage();
        }
        holder.tvNameUser.setText(username);

        if (!TextUtils.isEmpty(featuredImage)) {
            holder.pbAvatar.setVisibility(View.VISIBLE);
            holder.ivAvatar.setVisibility(View.VISIBLE);
            Glide.with(context).load(featuredImage)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            User user = new User();
                            user.setFirstName(sender.getFirstName());
                            user.setLastName(sender.getLastName());
                            user.setDisplayName(sender.getDisplayName());
                            holder.pbAvatar.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.people_default_avatar_text_size);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbAvatar.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                            return false;
                        }
                    })
                    .into(holder.ivAvatar);
        } else {
            User user = new User();
            user.setFirstName(sender.getFirstName());
            user.setLastName(sender.getLastName());
            user.setDisplayName(sender.getDisplayName());
            holder.pbAvatar.setVisibility(View.GONE);
            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.people_default_avatar_text_size);
        }
        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(giftHistory, position);
        });
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }
}
