package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.FormatType;
import com.airvtinginc.airvtinglive.components.enums.RequestMethod;
import com.airvtinginc.airvtinglive.components.interfaces.ResultListener;
import com.airvtinginc.airvtinglive.components.services.HttpRequest;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.davidmiguel.numberkeyboard.NumberKeyboard;
import com.davidmiguel.numberkeyboard.NumberKeyboardListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyCodeFragment extends Fragment implements NumberKeyboardListener {
    private static final String TAG = VerifyCodeFragment.class.getName();
    private static final int TIME_COUNT_DOWN = 1 * 60 * 1000;

    @BindView(R.id.mrlVerify)
    MaterialRippleLayout mrlVerify;
    @BindView(R.id.numberKeyboard)
    NumberKeyboard numberKeyboard;
    @BindView(R.id.pinEntryEditText)
    PinEntryEditText pinEntryEditText;
    @BindView(R.id.tvTimeResendCode)
    TextView tvTimeResendCode;
    @BindView(R.id.tvResendCode)
    TextView tvResendCode;

    private String mToken;
    private int mVerifyCode;

    @OnClick(R.id.mrlVerify)
    void onClickVerifyCode() {
        String verifyCode = String.valueOf(mVerifyCode);
        if (pinEntryEditText.getText().toString().equals(verifyCode)) {
            verifyCode(mVerifyCode);
        } else {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.verify_verify),
                    getString(R.string.validator_error_invalid_verify_code));
            pinEntryEditText.setText(null);
        }
    }

    @OnClick(R.id.tvResendCode)
    void resendCode() {
        Toast.makeText(getActivity(), "RESEND", Toast.LENGTH_SHORT).show();
        //Test Debug
        startCountDownTimer();
    }

    public CountDownTimer timer;

    private OnVerifyCodeClickListener onVerifyCodeClickListener;

    public VerifyCodeFragment() {
        // Required empty public constructor
    }

    public static VerifyCodeFragment newInstance() {
        VerifyCodeFragment fragment = new VerifyCodeFragment();
        return fragment;
    }

    public void setOnItemClickListener(OnVerifyCodeClickListener onVerifyCodeClickListener) {
        this.onVerifyCodeClickListener = onVerifyCodeClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verify_code, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        numberKeyboard.setListener(this);

        if (pinEntryEditText != null) {
            pinEntryEditText.setAnimateText(true);
//            pinEntryEditText.setOnPinEnteredListener(str -> {
//                if (str.toString().equals("1234")) {
//                    Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT).show();
//                } else {
//                    pinEntryEditText.setError(true);
//                    Toast.makeText(getActivity(), "FAIL", Toast.LENGTH_SHORT).show();
//                    pinEntryEditText.postDelayed(() -> pinEntryEditText.setText(null), 1000);
//                }
//            });
        }
        //Test  DEBUG
        startCountDownTimer();
    }

    @Override
    public void onNumberClicked(int number) {
        showAmount(number + "");
    }

    @Override
    public void onLeftAuxButtonClicked() {
        // Nothing to do
    }

    @Override
    public void onRightAuxButtonClicked() {
        int length = pinEntryEditText.getText().length();
        if (length > 0) {
            pinEntryEditText.getText().delete(length - 1, length);
        }
    }

    private void showAmount(String number) {
        pinEntryEditText.requestFocus();
        pinEntryEditText.append(number);
    }

    public interface OnVerifyCodeClickListener {
        void onVerifyCodeClicked(String tokenId, User user);
    }

    private void verifyCode(int code) {
        startCountDownTimer();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ServiceConstants.VERIFY_CODE, code);
        } catch (JSONException e) {
            AppLog.e(e.getMessage());
        }

        HashMap<String, String> header = new HashMap<>();
        header.put(ServiceConstants.HEADER_AUTHORIZATION, ServiceConstants.HEADER_AUTHORIZATION_PREFIX + mToken);

        ResultListener<UserResponse> resultListener = new ResultListener<UserResponse>() {
            @Override
            public void onSucceed(UserResponse result, String msg) {
                onVerifyCodeClickListener.onVerifyCodeClicked(result.getTokenId(), result.getUser());
            }

            @Override
            public void onError(int code, String msg) {
                Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                pinEntryEditText.setText(null);
            }
        };

        HttpRequest.request(ServiceConstants.URL_VERIFY_CODE, FormatType.FormBody,
                RequestMethod.POST, header, jsonObject, resultListener, UserResponse.class, true);
    }

    private void startCountDownTimer() {
        tvResendCode.setEnabled(false);
        timer = new CountDownTimer(TIME_COUNT_DOWN, 1000) {
            public void onTick(long millisUntilFinished) {
                long min = (millisUntilFinished / 1000) / 60;
                long sec = (millisUntilFinished / 1000) % 60;
                tvTimeResendCode.setText((min < 10 ? "0" + min : min) + ":" + (sec < 10 ? "0" + sec : sec));
            }

            public void onFinish() {
                tvTimeResendCode.setText("00:00");
                tvTimeResendCode.setAnimation(null);
                tvResendCode.setEnabled(true);
            }
        }.start();
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(0);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        tvTimeResendCode.startAnimation(anim);
    }

    public void setToken(String token) {
        mToken = token;
    }

    public void setVerifyCode(int verifyCode) {
        mVerifyCode = verifyCode;
    }
}
