package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ProductsAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.ProductListModelRequest;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileStoreFragment extends BaseFragment {
    private static final String TAG = ProfileStoreFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private int mCurrentPage = 1;
    private GridLayoutManager mLayoutManager;

    private ProductsAdapter mAdapter;
    private String mUserId;

    public ProfileStoreFragment() {
        // Required empty public constructor
    }

    public static ProfileStoreFragment newInstance(String userId) {
        ProfileStoreFragment fragment = new ProfileStoreFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(Constants.EXTRA_USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_store, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setUpRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mCurrentPage = 1;
        getProducts(mCurrentPage, false);
    }

    private void setUpRecyclerView() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) emptyView.getLayoutParams();
        layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        emptyView.setPadding(0, getResources().getDimensionPixelSize(R.dimen.profile_padding_top_no_data), 0, 0);
        emptyView.setLayoutParams(layoutParams);

        mRecyclerView.setEmptyView(emptyView);
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return 1;
            }
        });
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new ProductsAdapter(getActivity(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        });
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isMoreLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            mAdapter.addItem(null);
                            isMoreLoading = true;
                            getProducts(++mCurrentPage, true);
                        }
                    }
                }
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            getProducts(mCurrentPage, false);
        });
    }

    private void getProducts(int page, boolean isShowLoading) {
        if (TextUtils.isEmpty(mUserId)) {
            return;
        }
        ProductListModelRequest modelRequest = new ProductListModelRequest(mUserId, page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, null);
        requestApi(modelRequest, isShowLoading, RequestTarget.GET_LIST_PRODUCT, this);
    }

    private void hideLoadMore() {
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_LIST_PRODUCT:
                    hideLoadMore();
                    if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                        mAdapter.clearAllItems();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    List<Product> products = ((ProductsResponse) response.getData()).getProducts();
                    Log.e(TAG, "onResponseSuccess: product size : " + products.size());
                    if (mAdapter != null) {
                        mAdapter.addAllItems(products);
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
