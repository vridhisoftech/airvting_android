package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.NonSwipeableViewPager;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.template.viewpager.transformers.DefaultTransformer;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VerifyFragment extends Fragment implements VerifyMoreFragment.OnVerifyMoreClickListener,
        VerifyCodeFragment.OnVerifyCodeClickListener {

    @BindView(R.id.mIndicator)
    CirclePageIndicator mIndicator;
    @BindView(R.id.mViewPager)
    NonSwipeableViewPager mViewPager;

    private FragmentManager fragmentManager;
    private boolean mIsLogout;
    private VerifyMoreFragment mVerifyMoreFragment;
    private VerifyCodeFragment mVerifyCodeFragment;

    public VerifyFragment() {
        // Required empty public constructor
    }

    public static VerifyFragment newInstance() {
        VerifyFragment fragment = new VerifyFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verify, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        fragmentManager = getChildFragmentManager();
        prepareViewPager();
    }

    private void prepareViewPager() {
        PageFragmentAdapter mAdapter = new PageFragmentAdapter(fragmentManager);
        mVerifyMoreFragment = VerifyMoreFragment.newInstance();
        mVerifyMoreFragment.setOnItemClickListener(this);

        mVerifyCodeFragment = VerifyCodeFragment.newInstance();
        mVerifyCodeFragment.setOnItemClickListener(this);

        mAdapter.addFragment(mVerifyMoreFragment, null);
        mAdapter.addFragment(mVerifyCodeFragment, null);
        mViewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mViewPager);
        mViewPager.setCurrentItem(0, true);
        mViewPager.setPageTransformer(true, new DefaultTransformer());
    }

    public ViewPager getVerifyViewPager() {
        return mViewPager;
    }

    public boolean isLogout() {
        return mIsLogout;
    }

    public void setIdToken(String token) {
        mVerifyMoreFragment.setToken(token);
    }

    public void setUser(User user) {
        mVerifyMoreFragment.setUser(user);
    }

    @Override
    public void onVerifyCodeClicked(String tokenId, User user) {
        Utils.saveCurrentUserInfoToPreferences(getContext(), tokenId, 0, user);
        goToHome();
    }

    @Override
    public void onVerifyMoreClicked(String token, int verifyCode) {
        mVerifyCodeFragment.setToken(token);
        mVerifyCodeFragment.setVerifyCode(verifyCode);
        mViewPager.setCurrentItem(1, true);
    }

    private void goToHome() {
        Intent in = new Intent(getContext(), MainActivity.class);
        startActivity(in);
        getActivity().finish();
    }
}
