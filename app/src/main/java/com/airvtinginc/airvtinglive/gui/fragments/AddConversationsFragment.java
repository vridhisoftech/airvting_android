package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TagAdapter;
import com.airvtinginc.airvtinglive.gui.components.hashtag.EditTextHashtag;
import com.airvtinginc.airvtinglive.gui.dialog.ListDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageType;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.MessageContentModel;
import com.airvtinginc.airvtinglive.models.request.AddConversationsRequest;
import com.airvtinginc.airvtinglive.models.request.TagUserRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.TagChip;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.google.gson.Gson;
import com.pchmn.materialchips.ChipsInput;
import com.pchmn.materialchips.model.ChipInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class AddConversationsFragment extends BaseFragment {

    private static final String TAG = AddConversationsFragment.class.getSimpleName();
    @BindView(R.id.fm_add_conversation_rl_to)
    RelativeLayout rlTo;
    @BindView(R.id.fm_add_conversation_chipsInput)
    ChipsInput chipsInput;
    @BindView(R.id.fm_add_conversation_et_title)
    EditText etTitle;
    @BindView(R.id.et_description)
    EditTextHashtag etDescriptions;

    @OnTouch(R.id.nsvBackground)
    boolean touchHideKeyboard() {
        Utils.hideSoftKeyboard(getActivity());
        return true;
    }

    @OnClick(R.id.rlDescription)
    void clickDescriptionArea() {
        etDescriptions.requestFocus();
        Utils.showSoftKeyboard(getActivity(), etDescriptions);
    }

    public static AddConversationsFragment newInstance(String userId, String username) {
        AddConversationsFragment fragment = new AddConversationsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, userId);
        args.putString(Constants.EXTRA_USERNAME, username);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_conversation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setupChipsInput();

        DetailActivity.headerTitle = getString(R.string.title_new_message);
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);
    }

    private void setupChipsInput() {
        chipsInput.setOnClickEditText(true);
        chipsInput.addChipsListener(new ChipsInput.ChipsListener() {
            @Override
            public void onChipAdded(ChipInterface chip, int newSize) {
            }

            @Override
            public void onChipRemoved(ChipInterface chip, int newSize) {
            }

            @Override
            public void onTextChanged(CharSequence text) {
            }

            @Override
            public void onEditTextClick() {
                addTagUser();
            }
        });

        if (getArguments() != null) {
            String userId = getArguments().getString(Constants.EXTRA_USER_ID);
            String username = getArguments().getString(Constants.EXTRA_USERNAME);

            if (TextUtils.isEmpty(userId) || TextUtils.isEmpty(username)) {
                return;
            }
            TagChip tagChip = new TagChip(userId, username);
            if (tagChip.getLabel().length() > Constants.LIMIT_DISPLAY_NAME_LENGTH) {
                String name = tagChip.getLabel()
                        .substring(0, Constants.LIMIT_DISPLAY_NAME_LENGTH - Constants.STRING_ELLIPS_END.length())
                        .concat(Constants.STRING_ELLIPS_END);
                tagChip.setLabel(name);
            }
            chipsInput.addChip(tagChip);
        }
    }

    @OnClick(R.id.fm_add_conversation_rl_to)
    void addTagUser() {
        ListDialog addTag = ListDialog.newInstance();
        addTag.setShowDialogType(ShowDialogType.TAG);
        addTag.setTitle(getActivity(), R.string.dialog_tag_title);
        addTag.setBackgroundColorRes(getActivity(), R.color.white);
        addTag.setTitleColorRes(getActivity(), R.color.black);
        TagAdapter tagAdapter = new TagAdapter(getActivity(), new ArrayList<>(), chipsInput.getSelectedChipList(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(chipsInput.getWindowToken(), 0);

                TagChip tagChip = (TagChip) item;
                if (tagChip.getLabel().length() > Constants.LIMIT_DISPLAY_NAME_LENGTH) {
                    String name = tagChip.getLabel()
                            .substring(0, Constants.LIMIT_DISPLAY_NAME_LENGTH - Constants.STRING_ELLIPS_END.length())
                            .concat(Constants.STRING_ELLIPS_END);
                    tagChip.setLabel(name);
                }
                chipsInput.addChip(tagChip);
                addTag.getOnDismissListener().onDismiss();
            }
        });
        tagAdapter.setTextColorRes(getActivity(), R.color.black);
        tagAdapter.setBackgroundColorRes(getActivity(), R.color.white);
        addTag.setAdapter(tagAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        addTag.setOnDismissListener(() -> {
            addTag.dismiss();
        });
        addTag.show(getFragmentManager(), "Add Tag Dialog");
    }

    private boolean checkValidateData() {
        if (chipsInput.getSelectedChipList().size() == 0) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getResources().getString(R.string.title_new_message),
                    getResources().getString(R.string.null_receivers));
            return false;
        }

        if (etTitle.getText().toString().equals("")) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getResources().getString(R.string.title_new_message),
                    getResources().getString(R.string.err_title_null));
            return false;
        }
        if (etDescriptions.getText().toString().equals("")) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getResources().getString(R.string.title_new_message),
                    getResources().getString(R.string.err_description_null));
            return false;
        }

        return true;
    }

    @OnClick(R.id.fm_add_conversation_mrl_send)
    void addNewConversation() {
        if (checkValidateData()) {
            Utils.hideSoftKeyboard(getActivity());
            AddConversationsRequest conversationsRequest = new AddConversationsRequest();
            conversationsRequest.setTitle(etTitle.getText().toString());
            conversationsRequest.setContent(new Gson().toJson(new MessageContentModel(etDescriptions.getText().toString(), MessageType.TEXT.getType())));
            if (chipsInput.getSelectedChipList().size() > 0) {
                List<TagUserRequest> userRequestList = new ArrayList<>();
                for (ChipInterface selectedChip : chipsInput.getSelectedChipList()) {
                    TagUserRequest userRequest = new TagUserRequest();
                    userRequest.setUserId(selectedChip.getId().toString());
                    userRequestList.add(userRequest);
                }
                conversationsRequest.setReceivers(userRequestList);
                requestApi(conversationsRequest, true, RequestTarget.ADD_CONVERSATION, this);
            }
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
//            getActivity().getSupportFragmentManager().popBackStack();
            getActivity().finish();
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
