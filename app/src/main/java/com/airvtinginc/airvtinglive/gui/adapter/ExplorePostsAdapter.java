package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.CategoriesViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.models.request.CategoryPostsRequest;
import com.airvtinginc.airvtinglive.models.response.ExplorePosts;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;

import java.util.ArrayList;
import java.util.List;

public class ExplorePostsAdapter extends BaseAdapter {

    private static final String TAG = ExplorePostsAdapter.class.getSimpleName();

    public ExplorePostsAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_explore_category, parent, false);
        CategoriesViewHolder vh = new CategoriesViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        final CategoriesViewHolder holder = (CategoriesViewHolder) mHolder;
        ExplorePosts explorePosts = (ExplorePosts) list.get(position);
        holder.tvCategoryName.setText(explorePosts.getCategoryName());

        // use a linear layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.mRecyclerView.setLayoutManager(linearLayoutManager);
        holder.mRecyclerView.setAdapter(explorePosts.getLiveStreamHorizontalAdapter());
        holder.mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount, pastVisibleItems, totalItemCount;
                super.onScrolled(recyclerView, dx, dy);
                if (dx > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!explorePosts.isMoreLoading()) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            explorePosts.getLiveStreamHorizontalAdapter().addItem(null);
                            explorePosts.setMoreLoading(true);
                            getCategoryPosts(explorePosts);
                        }
                    }
                }
            }
        });

        holder.emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        holder.tvShowEmpty.setText(context.getString(R.string.recycle_view_empty_post));

        if (explorePosts.getLiveStreamHorizontalAdapter().getItemCount() > 0) {
            holder.mRecyclerView.setVisibility(View.VISIBLE);
            holder.emptyView.setVisibility(View.GONE);
        } else {
            holder.mRecyclerView.setVisibility(View.GONE);
            holder.emptyView.setVisibility(View.VISIBLE);
        }
    }

    public <T> void addItem(T item, int position, boolean isReplace) {
        ExplorePosts explorePosts = (ExplorePosts) item;
        explorePosts.setLiveStreamHorizontalAdapter(new LiveStreamHorizontalAdapter(context, explorePosts.getPostDetailList(), new OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        }));

        if (isReplace && list.get(position) != null) {
            list.set(position, explorePosts);
            notifyItemChanged(position);
        } else {
            list.add(explorePosts);
            notifyItemInserted(list.size() - 1);
            notifyItemChanged(list.size() - 1);
        }
    }

    private void getCategoryPosts(ExplorePosts explorePosts) {
        if (explorePosts.getCurrentPage() > 1 && TextUtils.isEmpty(explorePosts.getMaxId())) {
            hideLoadMore(explorePosts);
            return;
        }
        String[] typePost = {MediaType.STREAM.getType(), MediaType.VIDEO.getType()};
        CategoryPostsRequest requestModel = new CategoryPostsRequest();

        requestModel.setCategoryId(explorePosts.getCategoryId());
        requestModel.setTypePost(typePost);
        requestModel.setPaginate(explorePosts.getCurrentPage() + 1);
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestModel.setMaxId(explorePosts.getMaxId());

        requestApi(requestModel, false, RequestTarget.GET_CATEGORY_POSTS, this);
    }

    private void hideLoadMore(ExplorePosts explorePosts) {
        if (explorePosts.isMoreLoading() && explorePosts.getLiveStreamHorizontalAdapter().getItemCount() > 0) {
            explorePosts.getLiveStreamHorizontalAdapter().removeItem(explorePosts.getLiveStreamHorizontalAdapter().getItemCount() - 1);
            explorePosts.setMoreLoading(false);
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_CATEGORY_POSTS:
                    List<PostDetail> posts = ((PostResponse) response.getData()).getListPostDetail();
                    String categoryId = ((PostResponse) response.getData()).getCategoryId();
                    String maxId = ((PostResponse) response.getData()).getMaxId();

                    for (int i = 0; i < list.size(); i++) {
                        ExplorePosts explorePosts = (ExplorePosts) list.get(i);
                        if (categoryId.equals(explorePosts.getCategoryId())) {
                            hideLoadMore(explorePosts);
                            if (posts != null && !posts.isEmpty()) {
                                explorePosts.getLiveStreamHorizontalAdapter().addAllItems(posts);
                                explorePosts.setCurrentPage(explorePosts.getCurrentPage() + 1);
                                explorePosts.setMaxId(maxId);
                            }
                        }
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
