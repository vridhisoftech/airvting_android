package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ProductsAddLiveAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.request.ProductListModelRequest;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class BottomStoreDialog extends BaseFragmentDialog implements ProductsAddLiveAdapter.OnUpdateCartItemsListener {
    private static final String TAG = BottomStoreDialog.class.getSimpleName();

    @BindView(R.id.rv_bottom_store)
    EmptyRecyclerView mRvBottomStore;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.tv_bottom_store_cart_items)
    TextView mTvCartItems;

    private LinearLayoutManager mLinearLayoutManager;
    private ProductsAddLiveAdapter mAdapter;
    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private int mCurrentPage = 1;
    private String mUserId;
    private String mPostId;
    private String mCurrentSaleProductId;

    @OnClick(R.id.ll_bottom_store_empty)
    void dismissDialog() {
        dismiss();
    }

    public static BottomStoreDialog newInstance(String userId, String postId, ProductInPost currentSaleProduct) {
        BottomStoreDialog bottomStoreDialog = new BottomStoreDialog();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, userId);
        args.putString(Constants.EXTRA_POST_ID, postId);
        if (currentSaleProduct != null) {
            args.putString(Constants.EXTRA_PRODUCT_ID, currentSaleProduct.getProductId());
        }
        bottomStoreDialog.setArguments(args);
        return bottomStoreDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BOTTOM);
        if (getArguments() != null) {
            mUserId = getArguments().getString(Constants.EXTRA_USER_ID);
            mPostId = getArguments().getString(Constants.EXTRA_POST_ID);
            mCurrentSaleProductId = getArguments().getString(Constants.EXTRA_PRODUCT_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_bottom_store, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);

        setCartItems();
        setUpRecyclerView();
        getProducts(mCurrentPage);
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void setCartItems() {
        Realm realm = Utils.getCartRealm(getContext());
        realm.beginTransaction();
        RealmResults<Product> realmProducts = realm.where(Product.class).findAll();
        if (realmProducts != null) {
            int quantity = 0;
            for (Product countProduct : realmProducts) {
                quantity += countProduct.getQuantityToBuy();
            }
            mTvCartItems.setText(String.valueOf(quantity));
        }
        realm.commitTransaction();
    }

    private void setUpRecyclerView() {
        mRvBottomStore.setEmptyView(emptyView);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new ProductsAddLiveAdapter(getContext(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        }, true, true);
        mAdapter.setOnUpdateCartItemsListener(this);
        mAdapter.setUserId(mUserId);
        mAdapter.setPostId(mPostId);
        mRvBottomStore.setLayoutManager(mLinearLayoutManager);
        mRvBottomStore.setAdapter(mAdapter);

        mRvBottomStore.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                               @Override
                                               public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                   super.onScrolled(recyclerView, dx, dy);
                                                   if (dy > 0) {
                                                       visibleItemCount = mLinearLayoutManager.getChildCount();
                                                       totalItemCount = mLinearLayoutManager.getItemCount();
                                                       pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                                                       if (!isMoreLoading) {
                                                           if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                               mAdapter.addItem(null);
                                                               isMoreLoading = true;
                                                               getProducts(++mCurrentPage);
                                                           }
                                                       }
                                                   }
                                               }
                                           }
        );
    }

    private void getProducts(int page) {
        if (!TextUtils.isEmpty(mUserId)) {
            ProductListModelRequest modelRequest = new ProductListModelRequest(mUserId, page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, null);
            requestApi(modelRequest, false, RequestTarget.GET_LIST_PRODUCT, this);
        }
    }

    private void hideLoadMore() {
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_LIST_PRODUCT:
                    hideLoadMore();
                    List<Product> products = ((ProductsResponse) response.getData()).getProducts();
                    for (Product product : products) {
                        if (!product.getProductId().equals(mCurrentSaleProductId)) {
                            mAdapter.addItem(product);
                        }
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
    }

    @Override
    public void onUpdateCartItemsQuantity(int quantity) {
        mTvCartItems.setText(String.valueOf(quantity));
    }
}
