package com.airvtinginc.airvtinglive.gui.components;

public interface PlayPauseListener {
    void onPlay();

    void onPause();
}