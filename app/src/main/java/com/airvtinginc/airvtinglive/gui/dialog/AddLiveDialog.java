package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ProductsAddLiveAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TagAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TextRecycleViewAdapter;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.gui.enums.AddDiscountTimerDialogType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.request.AddMediaModelRequest;
import com.airvtinginc.airvtinglive.models.request.CategoryItemRequest;
import com.airvtinginc.airvtinglive.models.request.ProductItemRequest;
import com.airvtinginc.airvtinglive.models.request.TagUserRequest;
import com.airvtinginc.airvtinglive.models.request.UpdatePostRequest;
import com.airvtinginc.airvtinglive.models.response.CategoryItem;
import com.airvtinginc.airvtinglive.models.response.MediaModelResponse;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostDetailResponse;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.TagChip;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.airvtinginc.airvtinglive.tools.RequestPartUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.lightfire.gradienttextcolor.GradientTextView;
import com.pchmn.materialchips.ChipsInput;
import com.pchmn.materialchips.model.ChipInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AddLiveDialog extends BaseFragmentDialog {

    private static final String TAG = AddLiveDialog.class.getSimpleName();

    @BindView(R.id.btnAddProduct)
    ImageButton btnAddProduct;
    @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;
    @BindView(R.id.pbAvatarLoading)
    ProgressBar pbAvatarLoading;
    @BindView(R.id.etTitle)
    EditText etTitle;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.tvDiscountTimer)
    TextView tvDiscountTimer;
    @BindView(R.id.tvDiscountAmount)
    TextView tvDiscountAmount;
    @BindView(R.id.tvStockAvailability)
    TextView tvStockAvailability;
    @BindView(R.id.ivProduct)
    ImageView ivProduct;
    @BindView(R.id.pbProductLoading)
    ProgressBar pbProductLoading;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.tvNameProduct)
    TextView tvNameProduct;
    @BindView(R.id.rlProduct)
    RelativeLayout rlProduct;
    @BindView(R.id.rlAddLiveDialog)
    RelativeLayout rlAddLiveDialog;
    @BindView(R.id.chipsInput)
    ChipsInput mChipsInput;

    @BindView(R.id.llTextTitle)
    LinearLayout llTextTitle;
    @BindView(R.id.llAddLive)
    LinearLayout llAddLive;
    @BindView(R.id.rlCategory)
    RelativeLayout rlCategory;
    @BindView(R.id.btnStartLive)
    Button btnStartLive;
    @BindView(R.id.rlSaveChange)
    RelativeLayout rlSaveChange;

    @BindView(R.id.rlCameraPosition)
    RelativeLayout rlCameraPosition;
    @BindView(R.id.ivCameraFront)
    ImageView ivCameraFront;
    @BindView(R.id.ivCameraBack)
    ImageView ivCameraBack;

    private OnAddLiveListener onAddLiveListener;
    private CategoryItemRequest categoryItemRequest;
    private int minSale = 0;
    private String discountAmount;
    private int saleQuantity = 0;

    private ProductItemRequest productItemRequest;

    private boolean isChangeInfo = false;
    private ProductInPost changedProduct;
    private String changedPostId;
    private int cameraPosition;

    @OnClick(R.id.btnClose)
    void close() {
        if (onAddLiveListener != null) {
            onAddLiveListener.onDismiss();
        } else {
            dismiss();
        }
    }

    @OnClick(R.id.llTag)
    void addTag() {
        ListDialog addTag = ListDialog.newInstance();
        addTag.setShowDialogType(ShowDialogType.TAG);
        addTag.setTitle(getActivity(), R.string.dialog_tag_title);
        addTag.setBackgroundColorRes(getActivity(), R.color.transparent_70_black);
        addTag.setTitleColorRes(getActivity(), R.color.white);
        TagAdapter tagAdapter = new TagAdapter(getActivity(), new ArrayList<>(), mChipsInput.getSelectedChipList(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                TagChip tagChip = (TagChip) item;
                if (tagChip.getLabel().length() > Constants.LIMIT_DISPLAY_NAME_LENGTH) {
                    String name = tagChip.getLabel()
                            .substring(0, Constants.LIMIT_DISPLAY_NAME_LENGTH - Constants.STRING_ELLIPS_END.length())
                            .concat(Constants.STRING_ELLIPS_END);
                    tagChip.setLabel(name);
                }
                mChipsInput.addChip(tagChip);
                addTag.getOnDismissListener().onDismiss();
            }
        });
        tagAdapter.setTextColorRes(getActivity(), R.color.white);
        tagAdapter.setBackgroundColorRes(getActivity(), android.R.color.transparent);
        addTag.setAdapter(tagAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rlAddLiveDialog.setVisibility(View.GONE);
        addTag.setOnDismissListener(() -> {
            addTag.dismiss();
            rlAddLiveDialog.setVisibility(View.VISIBLE);
        });
        addTag.show(getFragmentManager(), "Add Tag Dialog");
    }

    @OnClick({R.id.btnAddProduct, R.id.rlProduct})
    void addProduct() {
        ListDialog listProductDialog = ListDialog.newInstance();
        listProductDialog.setShowDialogType(ShowDialogType.STORE);
        listProductDialog.setTitle(getActivity(), R.string.dialog_store);
        listProductDialog.setBackgroundColorRes(getActivity(), R.color.transparent_70_black);
        listProductDialog.setTitleColorRes(getActivity(), R.color.white);
        ProductsAddLiveAdapter productsAddLiveAdapter = new ProductsAddLiveAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                Product product = (Product) item;
                showProductInfo(product);
                List<FeaturedImage> featuredImages = product.getFeaturedImages();
                productItemRequest = new ProductItemRequest(product.getProductId(), product.getTitle(),
                        product.getPrice(), featuredImages.get(0).getFeaturedImage());

                listProductDialog.getOnDismissListener().onDismiss();
            }
        }, false, false);

        listProductDialog.setAdapter(productsAddLiveAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rlAddLiveDialog.setVisibility(View.GONE);
        listProductDialog.setOnDismissListener(() -> {
            listProductDialog.dismiss();
            rlAddLiveDialog.setVisibility(View.VISIBLE);
        });
        listProductDialog.show(getFragmentManager(), "Show Add Store Dialog");
    }

    @OnClick(R.id.rlCategory)
    void addCategory() {
        ListDialog listCategoryDialog = ListDialog.newInstance();
        listCategoryDialog.setShowDialogType(ShowDialogType.CATEGORY);
        listCategoryDialog.setPost(true);
        listCategoryDialog.setTitle(getActivity(), R.string.dialog_category_title);
        listCategoryDialog.setBackgroundColorRes(getActivity(), R.color.transparent_70_black);
        listCategoryDialog.setTitleColorRes(getActivity(), R.color.white);
        TextRecycleViewAdapter textRecycleViewAdapter = new TextRecycleViewAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                CategoryItem category = (CategoryItem) item;
                categoryItemRequest = new CategoryItemRequest();
                categoryItemRequest.setCategoryId(category.getId());
                categoryItemRequest.setTitle(category.getTitle());

                tvCategory.setText(category.getTitle());
                listCategoryDialog.getOnDismissListener().onDismiss();
            }
        });
        textRecycleViewAdapter.setTextColorRes(getActivity(), R.color.white);
        textRecycleViewAdapter.setBackgroundColorRes(getActivity(), android.R.color.transparent);
        listCategoryDialog.setAdapter(textRecycleViewAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rlAddLiveDialog.setVisibility(View.GONE);
        listCategoryDialog.setOnDismissListener(() -> {
            listCategoryDialog.dismiss();
            rlAddLiveDialog.setVisibility(View.VISIBLE);
        });
        listCategoryDialog.show(getFragmentManager(), "Show All ExplorePosts Dialog");
    }

    @OnClick(R.id.rlDiscountTimer)
    void addDiscountTimer() {
        AddDiscountTimer addDiscountTimer = AddDiscountTimer.newInstance();
        addDiscountTimer.setAddDiscountTimerDialogType(AddDiscountTimerDialogType.LIVE);
        addDiscountTimer.setOnOkListener(new AddDiscountTimer.OnOkListener() {
            @Override
            public void onOkClick(int hour, int minutes) {
                minSale = hour * 60 + minutes;
                tvDiscountTimer.setText(DateTimeUtils.formatHourMinute(hour, minutes));
                addDiscountTimer.dismiss();
            }

            @Override
            public void onOkClick(String from, String to) {

            }
        });
        addDiscountTimer.show(getFragmentManager(), "Add Discount Timer Dialog");
    }

    @OnClick(R.id.rlDiscountAmount)
    void addDiscountAmount() {
        AddDiscountAmount addDiscountAmount = AddDiscountAmount.newInstance();
        addDiscountAmount.setOnOkListener((amount) -> {
            discountAmount = amount;
            tvDiscountAmount.setText(discountAmount);

            if (productItemRequest != null) {
                float displayPrice;
                if (discountAmount.contains("%")) {
                    float discountPercent = Float.parseFloat(discountAmount.substring(0, discountAmount.indexOf("%")));
                    displayPrice = productItemRequest.getPrice() - productItemRequest.getPrice() * discountPercent / 100;
                } else {
                    displayPrice = productItemRequest.getPrice() - Float.parseFloat(discountAmount.replace(",", ""));
                }

                String displayPriceSale = NumberUtils.formatPrice(displayPrice);
                // Because format will return 'S-$...' if price sale is below 0 so need to replacing
                displayPriceSale = displayPriceSale.replace("S-", "-S");

                tvPrice.setText(displayPriceSale);
                tvPrice.setTextColor(getResources().getColor(R.color.color_product_price_sale));
            }

            addDiscountAmount.dismiss();
        });
        addDiscountAmount.show(getFragmentManager(), "Add Discount Amount Dialog");
    }

    @OnClick(R.id.rlStockAvailability)
    void addStockAvailability() {
        AddStockAvailabilityDialog addStockAvailabilityDialog = AddStockAvailabilityDialog.newInstance();
        addStockAvailabilityDialog.setOnOkListener((amount) -> {
            saleQuantity = Integer.valueOf(amount);
            tvStockAvailability.setText(amount);
            addStockAvailabilityDialog.dismiss();
        });
        addStockAvailabilityDialog.show(getFragmentManager(), "Add Stock Availability Dialog");
    }

    @OnClick(R.id.btnStartLive)
    void startLive() {
        addNewPost();
    }

    @OnClick(R.id.mrlSaveChange)
    void startChange() {
        if (!checkValidateData()) {
            return;
        }
        UpdatePostRequest postModel = new UpdatePostRequest();

        Map<String, RequestBody> bodyMap = new HashMap<>();

        productItemRequest.setDiscount(discountAmount);
        productItemRequest.setTimer(minSale);
        productItemRequest.setQuantity(saleQuantity);
        RequestBody bodyProduct = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(productItemRequest));
        bodyMap.put(ServiceConstants.PRODUCT, bodyProduct);
        bodyMap.put(ServiceConstants.TYPE, RequestPartUtils.createFromObject(com.airvtinginc.airvtinglive.gui.enums.MediaType.STREAM.getType()));

        postModel.setParams(bodyMap);
        postModel.setPostId(changedPostId);
        requestApi(postModel, true, RequestTarget.UPDATE_POST, this);
    }

    @OnClick({R.id.llCameraFront})
    void setCameraFont() {
        cameraPosition = Camera.CameraInfo.CAMERA_FACING_FRONT;
        setCameraPosition();
    }

    @OnClick({R.id.llCameraBack})
    void setCameraBack() {
        cameraPosition = Camera.CameraInfo.CAMERA_FACING_BACK;
        setCameraPosition();
    }

    public static AddLiveDialog newInstance() {
        return new AddLiveDialog();
    }


    public void setOnAddLiveListener(OnAddLiveListener onAddLiveListener) {
        this.onAddLiveListener = onAddLiveListener;
    }

    public OnAddLiveListener getOnAddLiveListener() {
        return onAddLiveListener;
    }


    public boolean isChangeInfo() {
        return isChangeInfo;
    }

    public void setChangeInfo(boolean changeInfo, ProductInPost productInPost, String postId) {
        isChangeInfo = changeInfo;
        changedProduct = productInPost;
        changedPostId = postId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_live, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        cameraPosition = onAddLiveListener.getCameraPosition();
        setCameraPosition();
        if (isChangeInfo()) {
            llTextTitle.setVisibility(View.VISIBLE);
            rlSaveChange.setVisibility(View.VISIBLE);
            llAddLive.setVisibility(View.GONE);
            rlCategory.setVisibility(View.GONE);
            rlCameraPosition.setVisibility(View.GONE);
            btnStartLive.setVisibility(View.GONE);

            getChangedProductDetail();
        } else {
            setupChipsInput();
            setupAvatar();
            Utils.hideSoftKeyboard(mChipsInput);

            llTextTitle.setVisibility(View.GONE);
            rlSaveChange.setVisibility(View.GONE);
            llAddLive.setVisibility(View.VISIBLE);
            rlCategory.setVisibility(View.VISIBLE);
            rlCameraPosition.setVisibility(View.VISIBLE);
            btnStartLive.setVisibility(View.VISIBLE);
        }
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void setupChipsInput() {
        mChipsInput.setOnClickEditText(true);
        mChipsInput.setBackgroundColor(Utils.getColor(getActivity(), android.R.color.transparent));
        // chips listener
        mChipsInput.addChipsListener(new ChipsInput.ChipsListener() {
            @Override
            public void onChipAdded(ChipInterface chip, int newSize) {
            }

            @Override
            public void onChipRemoved(ChipInterface chip, int newSize) {
            }

            @Override
            public void onTextChanged(CharSequence text) {
            }

            @Override
            public void onEditTextClick() {
                addTag();
            }
        });
    }

    private void setCameraPosition() {
        if (cameraPosition == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            Glide.with(getActivity()).load(R.drawable.box_check_while).into(ivCameraFront);
            Glide.with(getActivity()).load(R.drawable.box_un_check).into(ivCameraBack);
        } else {
            Glide.with(getActivity()).load(R.drawable.box_un_check).into(ivCameraFront);
            Glide.with(getActivity()).load(R.drawable.box_check_while).into(ivCameraBack);
        }
        onAddLiveListener.onCameraChange(cameraPosition);
    }

    private void setupAvatar() {
        String avatarUrl = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_PHOTO_URL, "");
        String displayName = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_DISPLAY_NAME, "");
        String firstName = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_FIRST_NAME, "");
        String lastName = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_LAST_NAME, "");

        if (!TextUtils.isEmpty(avatarUrl)) {
            pbAvatarLoading.setVisibility(View.VISIBLE);
            ivAvatar.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(avatarUrl)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            User user = new User();
                            user.setDisplayName(displayName);
                            user.setFirstName(firstName);
                            user.setLastName(lastName);

                            pbAvatarLoading.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(getActivity(), ivDefaultAvatar, tvDefaultAvatar, ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbAvatarLoading.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(getActivity(), ivDefaultAvatar, tvDefaultAvatar, ivAvatar);
                            return false;
                        }
                    })
                    .into(ivAvatar);
        } else {
            User user = new User();
            user.setDisplayName(displayName);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            pbAvatarLoading.setVisibility(View.GONE);
            Utils.setDefaultAvatar(getActivity(), ivDefaultAvatar, tvDefaultAvatar, ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
        }
    }

    private void addNewPost() {
        if (checkValidateData()) {
            Utils.hideSoftKeyboard(getActivity());

            AddMediaModelRequest postModel = new AddMediaModelRequest();
            Map<String, RequestBody> bodyMap = new HashMap<>();

            RequestBody bodyType = RequestBody.create(MediaType.parse("application/json"), "stream");
            RequestBody bodyTitle = RequestBody.create(MediaType.parse("application/json"), etTitle.getText().toString());

            bodyMap.put("type", bodyType);
            bodyMap.put("title", bodyTitle);

            if (mChipsInput.getSelectedChipList().size() > 0) {
                List<TagUserRequest> userRequestList = new ArrayList<>();
                for (ChipInterface selectedChip : mChipsInput.getSelectedChipList()) {
                    TagUserRequest userRequest = new TagUserRequest();
                    userRequest.setUserId(selectedChip.getId().toString());
                    userRequest.setUsername(selectedChip.getLabel().toLowerCase());
                    userRequestList.add(userRequest);
                }
                RequestBody bodyTagUser = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(userRequestList));
                bodyMap.put("tagUsers", bodyTagUser);
            }
            List<CategoryItemRequest> categoryItemRequestList = new ArrayList<>();
            categoryItemRequestList.add(categoryItemRequest);
            RequestBody bodyCategory = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(categoryItemRequestList));
            bodyMap.put("postCategories", bodyCategory);

            if (!TextUtils.isEmpty(discountAmount) && !discountAmount.contains("%")) {
                discountAmount = discountAmount.replace(",", "");
            }

            if (productItemRequest != null) {
                productItemRequest.setDiscount(discountAmount);
                productItemRequest.setTimer(minSale);
                productItemRequest.setQuantity(saleQuantity);
                RequestBody bodyProduct = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(productItemRequest));
                bodyMap.put("product", bodyProduct);
            }

            postModel.setParams(bodyMap);
            requestApi(postModel, true, RequestTarget.ADD_POST, this);
        }
    }

    private boolean checkValidateData() {
        if (etTitle.getText().toString().equals("") && !isChangeInfo()) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_live_title),
                    getString(R.string.err_title_null));
            return false;
        }

        if (categoryItemRequest == null && !isChangeInfo()) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_live_title),
                    getString(R.string.err_category_null));
            return false;
        }

        boolean isEmptyTimer = TextUtils.isEmpty(tvDiscountTimer.getText());
        boolean isEmptyDiscount = TextUtils.isEmpty(tvDiscountAmount.getText());
        boolean isEmptySaleQuantity = TextUtils.isEmpty(tvStockAvailability.getText());
        if (productItemRequest != null) {
            if (isEmptyTimer && (!isEmptyDiscount || !isEmptySaleQuantity)) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_live_title),
                        getString(R.string.add_product_error_empty_timer));
                return false;
            } else if (!isEmptyTimer && isEmptyDiscount) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_live_title),
                        getString(R.string.add_product_error_empty_discount));
                return false;
            } else if (isEmptySaleQuantity && !isEmptyTimer) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_live_title),
                        getString(R.string.add_product_error_empty_sale_quantity));
                return false;
            }

            String discountString = tvDiscountAmount.getText().toString();
            if (!isEmptyDiscount && !discountString.contains("%")) {
                float discount = NumberUtils.getPriceFromString(discountString);
                float price = productItemRequest.getPrice();
                Log.v("discount ",discount+" Price"+price);
//                if (discount > price) {
//                    DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_live_title),
//                            getString(R.string.add_product_error_discount_greater_than_price));
//                    return true;
//                }
            }

        }
//        else {
//////            if (!isEmptyTimer || !isEmptyDiscount || !isEmptySaleQuantity) {
//                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_live_title),
//                        getString(R.string.add_product_error_empty_product));
//                return false;
//            }
//        }

        return true;
    }

    private void getChangedProductDetail() {
        if (!TextUtils.isEmpty(changedProduct.getProductId())) {
            DialogUtils.showLoadingProgress(getContext(), false);
            requestApi(changedProduct.getProductId(), true, RequestTarget.GET_PRODUCT_DETAIL, this);
        }
    }

    private void showProductInfo(Product product) {
        if (product == null) {
            return;
        }

        rlProduct.setVisibility(View.VISIBLE);
        btnAddProduct.setVisibility(View.GONE);

        tvNameProduct.setText(product.getTitle());
        List<FeaturedImage> featuredImages = product.getFeaturedImages();

        if (featuredImages != null && !featuredImages.isEmpty()) {
            pbProductLoading.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(featuredImages.get(0).getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbProductLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbProductLoading.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(ivProduct);
        }

        float displayPrice = product.getPrice();
        int priceColor = getResources().getColor(R.color.white);

        if (isChangeInfo() && changedProduct != null) {
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DateTimeUtils.DATE_TIME_FORMAT, Locale.getDefault());
            Calendar startCalendar = Calendar.getInstance();
            Calendar expiredCalendar = Calendar.getInstance();

            try {
                startCalendar.setTimeInMillis(dateTimeFormat.parse(DateTimeUtils.getDateFormatUTC(changedProduct.getStartTime())).getTime());
                expiredCalendar.setTimeInMillis(dateTimeFormat.parse(DateTimeUtils.getDateFormatUTC(changedProduct.getExpiredAt())).getTime());
            } catch (Exception e) {
                AppLog.e(e.getMessage());
            }

            long difference = expiredCalendar.getTimeInMillis() - startCalendar.getTimeInMillis();
            int hour = (int) (difference / 1000 / 60 / 60);
            int differentMinute = (int) (difference / 1000 / 60);
            int minutes = differentMinute >= 60 ? (differentMinute - 60) : differentMinute;

            minSale = hour * 60 + minutes;
//            discountAmount = product.getDiscount();

//            minSale = changedProduct.getTimer();
//            int hour = minSale / 60;
//            int minutes = minSale >= 60 ? (minSale - 60) : minSale;

            productItemRequest = new ProductItemRequest(product.getProductId(), product.getTitle(),
                    product.getPrice(), featuredImages.get(0).getFeaturedImage());


            if (TextUtils.isEmpty(tvDiscountAmount.getText())) {
                String discount = changedProduct.getDiscount();
//              displayPrice = changedProduct.getPriceDiscount();
                if (discount.equals("0%")) {
                    discountAmount = "";
                } else {
                    discountAmount = discount;
                    priceColor = getResources().getColor(R.color.color_product_price_sale);
                }
                tvDiscountAmount.setText(discountAmount);
            }

            saleQuantity = changedProduct.getQuantity();
            tvDiscountTimer.setText(DateTimeUtils.formatHourMinute(hour, minutes));
            tvStockAvailability.setText(String.valueOf(saleQuantity));
        }

        if (!TextUtils.isEmpty(discountAmount)) {
            if (discountAmount.contains("%")) {
                float discountPercent = Float.valueOf(discountAmount.substring(0, discountAmount.indexOf("%")));
                displayPrice = product.getPrice() - (product.getPrice() * discountPercent / 100);
            } else {
                displayPrice = product.getPrice() - Float.valueOf(discountAmount.replace(",", ""));
            }
            priceColor = getResources().getColor(R.color.color_product_price_sale);
        }

        tvPrice.setText(NumberUtils.formatPrice(displayPrice));
        tvPrice.setTextColor(priceColor);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case ADD_POST:
                    MediaModelResponse mediaModelResponse = (MediaModelResponse) response.getData();
                    onAddLiveListener.onGoLive(mediaModelResponse.getPostDetail());
                    dismiss();
                    Log.e(TAG, "add post ok: " + response.getMessage());

                    break;

                case GET_PRODUCT_DETAIL:
                    ProductDetailResponse productDetailResponse = (ProductDetailResponse) response.getData();
                    if (productDetailResponse != null) {
                        showProductInfo(productDetailResponse.getProduct());
                    }
                    DialogUtils.hideLoadingProgress();
                    break;
                case UPDATE_POST:
                    PostDetailResponse postDetailResponse = (PostDetailResponse) response.getData();
                    if (postDetailResponse != null && onAddLiveListener != null) {
                        onAddLiveListener.onChange(postDetailResponse.getPostDetail());
                        onAddLiveListener.onDismiss();
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.hideLoadingProgress();
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        onAddLiveListener.onDismiss();
    }

    public interface OnAddLiveListener {
        void onDismiss();

        void onGoLive(PostDetail postDetail);

        void onChange(PostDetail postDetail);

        void onCameraChange(int cameraPosition);

        int getCameraPosition();
    }
}
