package com.airvtinginc.airvtinglive.gui.dialog;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.PriceInStore;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class AddToCartDialog extends BaseFragmentDialog {
    private static final String TAG = AddToCartDialog.class.getSimpleName();

    @BindView(R.id.ll_add_to_cart_sale_price)
    LinearLayout mLlSalePrice;
    @BindView(R.id.tv_add_to_cart_base_price)
    TextView mTvBasePrice;
    @BindView(R.id.tv_add_to_cart_sale_price)
    TextView mTvSalePrice;
    @BindView(R.id.tv_add_to_cart_price)
    TextView mTvPrice;
    @BindView(R.id.tv_add_to_cart_discount)
    TextView mTvDiscount;

    private ProductInPost productInPost;
    private String sellerId;
    private String postId;
    private boolean mIsShowSalePrice;

    @OnClick(R.id.iv_add_to_cart_close)
    void closeDialog() {
        dismiss();
    }

    @OnClick(R.id.mrl_add_to_cart_ok)
    void addToCart() {
        if (productInPost != null && !TextUtils.isEmpty(sellerId) && !TextUtils.isEmpty(postId)) {
            Realm realm = Utils.getCartRealm(getContext());
            realm.beginTransaction();
            Product realmProduct = realm.where(Product.class).equalTo(Constants.REALM_PRODUCT_ID_PRIMARY_KEY, productInPost.getProductId()).findFirst();
            Product cartProduct;
            if (realmProduct != null) {
                cartProduct = new Product();
                cartProduct.setQuantityToBuy(realmProduct.getQuantityToBuy() + 1);
            } else {
                cartProduct = realm.createObject(Product.class);
                cartProduct.setQuantityToBuy(1);
            }

            cartProduct.setProductId(productInPost.getProductId());
            cartProduct.setDisplayImage(productInPost.getFeaturedImage());
            cartProduct.setTitle(productInPost.getTitle());
            cartProduct.setSellerId(sellerId);
            cartProduct.setPostId(postId);

            if (realmProduct != null) {
                realm.copyToRealmOrUpdate(cartProduct);
            }

            realm.commitTransaction();
            Toast.makeText(getContext(), String.format(getString(R.string.my_cart_add_product), productInPost.getTitle()), Toast.LENGTH_SHORT).show();
        }
        dismiss();
    }

    public static AddToCartDialog newInstance() {
        return new AddToCartDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BORDER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_to_cart, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        showProductInfo();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void showProductInfo() {
        if (productInPost == null) {
            return;
        }

        float productPrice = productInPost.getPrice();
        if (mIsShowSalePrice) {
            String discount = productInPost.getDiscount();
            float priceSale;

            if (discount.contains("%")) {
                float discountPercent = Float.valueOf(discount.substring(0, discount.indexOf("%")));
                priceSale = productPrice - (productPrice * discountPercent / 100);
            } else {
                priceSale = productPrice - Float.valueOf(discount.replace(",", ""));
            }

            mTvBasePrice.setText(NumberUtils.formatPrice(productPrice));
            mTvBasePrice.setPaintFlags(mTvBasePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            mTvSalePrice.setText(NumberUtils.formatPrice(priceSale));
            mLlSalePrice.setVisibility(View.VISIBLE);
            mTvPrice.setVisibility(View.GONE);

            if (discount.contains("%")) {
                mTvDiscount.setText(String.format(getString(R.string.dialog_add_to_cart_discount), discount));
            } else {
                mTvDiscount.setText(String.format(getString(R.string.dialog_add_to_cart_discount), "S$" + discount));
            }
        } else {
            if (productInPost.getPriceInStore() != null) {
                checkAndShowStorePrice(productInPost.getPriceInStore());
            } else {
                showBasePrice(productPrice);
            }
        }
    }

    private void checkAndShowStorePrice(PriceInStore priceInStore) {
        String discountStartTimeUTC = priceInStore.getStartedAt();
        String discountExpiredTimeUTC = priceInStore.getExpiredAt();
        if (!TextUtils.isEmpty(discountStartTimeUTC) && !TextUtils.isEmpty(discountExpiredTimeUTC)) {
            try {
                String discountStartTime = DateTimeUtils.getDateFormatUTC(discountStartTimeUTC);
                String discountExpiredTime = DateTimeUtils.getDateFormatUTC(discountExpiredTimeUTC);
                Date discountStartDate = DateTimeUtils.getDateFollowFormat(discountStartTime, DateTimeUtils.DATE_TIME_FORMAT);
                Date discountExpiredDate = DateTimeUtils.getDateFollowFormat(discountExpiredTime, DateTimeUtils.DATE_TIME_FORMAT);
                Date currentDate = DateTimeUtils.getCurrentDate(true);

                if (currentDate.compareTo(discountStartDate) >= 0 && currentDate.compareTo(discountExpiredDate) <= 0) {
                    if (!TextUtils.isEmpty(priceInStore.getDiscount()) && !priceInStore.getDiscount().equals("0%")) {
                        showPriceSale(priceInStore);
                    } else {
                        showBasePrice(priceInStore.getPrice());
                    }
                } else {
                    showBasePrice(priceInStore.getPrice());
                }
            } catch (Exception e) {
                AppLog.e(e.getMessage());
            }
        } else {
            showBasePrice(priceInStore.getPrice());
        }
    }

    private void showPriceSale(PriceInStore priceInStore) {
        mTvBasePrice.setText(NumberUtils.formatPrice(priceInStore.getPrice()));
        mTvBasePrice.setPaintFlags(mTvBasePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        mTvSalePrice.setText(NumberUtils.formatPrice(priceInStore.getPriceSale()));
        mLlSalePrice.setVisibility(View.VISIBLE);
        mTvPrice.setVisibility(View.GONE);

        if (priceInStore.getDiscount().contains("%")) {
            mTvDiscount.setText(String.format(getString(R.string.dialog_add_to_cart_discount), priceInStore.getDiscount()));
        } else {
            mTvDiscount.setText(String.format(getString(R.string.dialog_add_to_cart_discount), "S$" + priceInStore.getDiscount()));
        }
    }

    private void showBasePrice(float productPrice) {
        mTvPrice.setText(NumberUtils.formatPrice(productPrice));
        mTvPrice.setVisibility(View.VISIBLE);
        mLlSalePrice.setVisibility(View.GONE);
        mTvDiscount.setText(String.format(getString(R.string.dialog_add_to_cart_discount), "S$0"));
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {

            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }

    public void setProductInPost(ProductInPost productInPost) {
        this.productInPost = productInPost;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public void setIsShowSalePrice(boolean isShowSalePrice) {
        mIsShowSalePrice = isShowSalePrice;
    }
}
