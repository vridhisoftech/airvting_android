package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.FollowsAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.FollowType;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowsFragment extends BaseFragment {
    private static final String TAG = FollowsFragment.class.getName();

    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshFollows;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRvFollows;

    private LinearLayoutManager mLayoutManager;
    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private int mCurrentPage = 1;
    private FollowsAdapter mFollowsAdapter;
    private String mFollowType = FollowType.FOLLOWERS.getType();
    private String mUserId;
    private String mTitle;

    public static FollowsFragment newInstance(String type, String userId) {
        FollowsFragment fragment = new FollowsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_FOLLOW_TYPE, type);
        args.putString(Constants.EXTRA_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFollowType = getArguments().getString(Constants.EXTRA_FOLLOW_TYPE);
            mUserId = getArguments().getString(Constants.EXTRA_USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_follows, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setUpRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpActionBar();
        refreshFollows();
    }

    private void refreshFollows() {
        mSwipeRefreshFollows.setRefreshing(true);
        mCurrentPage = 1;
        getFollows(mCurrentPage, true);
    }

    private void setUpActionBar() {
        Intent intent = new Intent();
        mTitle = getString(R.string.follow_followers_title);

        if (!mFollowType.equals(FollowType.FOLLOWERS.getType())) {
            mTitle = getString(R.string.follow_following_title);
        }
        intent.setAction(DetailActivity.BroadcastActionHideAllToolbarIcons);
        getContext().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = mTitle;
        getContext().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionChangeToolbarToWhiteColor);
        getContext().sendBroadcast(intent);
    }

    private void setUpRecyclerView() {
        mRvFollows.setEmptyView(emptyView);

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRvFollows.setLayoutManager(mLayoutManager);
        mRvFollows.setHasFixedSize(true);

        mFollowsAdapter = new FollowsAdapter(getContext(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        });

        mRvFollows.setAdapter(mFollowsAdapter);

        mRvFollows.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isMoreLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            mFollowsAdapter.addItem(null);
                            isMoreLoading = true;
                            getFollows(++mCurrentPage, false);
                        }
                    }
                }
            }
        });

        mSwipeRefreshFollows.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            getFollows(mCurrentPage, false);
        });
    }

    private void getFollows(int page, boolean isLoading) {
        GetListRequest getListRequest = new GetListRequest();
        getListRequest.setUserId(mUserId);
        getListRequest.setPaginate(page);
        getListRequest.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING * 2);

        RequestTarget requestTarget = RequestTarget.GET_FOLLOWERS;
        if (!mFollowType.equals(FollowType.FOLLOWERS.getType())) {
            requestTarget = RequestTarget.GET_FOLLOWING;
        }
        requestApi(getListRequest, isLoading, requestTarget, this);
    }

    private void hideLoadMore() {
        if (isMoreLoading && mFollowsAdapter.getItemCount() > 0) {
            mFollowsAdapter.removeItem(mFollowsAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_FOLLOWERS:
                case GET_FOLLOWING:
                    hideLoadMore();
                    UsersResponse usersResponse = (UsersResponse) response.getData();
                    if (usersResponse != null && usersResponse.getUsers() != null) {
                        if (mSwipeRefreshFollows != null && mSwipeRefreshFollows.isRefreshing()) {
                            mFollowsAdapter.clearAllItems();
                            mSwipeRefreshFollows.setRefreshing(false);
                        }
                        mFollowsAdapter.addAllItems(usersResponse.getUsers());
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            if (mSwipeRefreshFollows != null) {
                mSwipeRefreshFollows.setRefreshing(false);
            }
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), mTitle, failMessage);

        hideLoadMore();
        if (mSwipeRefreshFollows != null) {
            mSwipeRefreshFollows.setRefreshing(false);
        }
    }
}
