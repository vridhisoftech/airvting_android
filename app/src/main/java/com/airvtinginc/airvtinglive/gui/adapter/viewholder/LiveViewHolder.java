package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.google.firebase.firestore.ListenerRegistration;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.item_post_ll_bound)
    LinearLayout llBound;
    public @BindView(R.id.tvDate)
    TextView tvDate;
    public @BindView(R.id.ivLive)
    ImageView ivLive;
    public @BindView(R.id.ivLiveRec)
    ImageView ivLiveRec;
    public @BindView(R.id.tvDefaultAvatar)
    TextView tvDefaultAvatar;
    public @BindView(R.id.ivPostVideo)
    ImageView ivPostVideo;
    public @BindView(R.id.ivBookmark)
    ImageView ivBookmark;
    public @BindView(R.id.lnBookmark)
    LinearLayout lnBookmark;
    public @BindView(R.id.ivDeletePost)
    ImageView ivDeletePost;
    public @BindView(R.id.ivPriceTag)
    ImageView ivPriceTag;
    public @BindView(R.id.tvUserName)
    TextView tvUserName;
    public @BindView(R.id.tvDescription)
    TextView tvDescription;
    public @BindView(R.id.tvCreatePost)
    TextView tvCreatePost;
    public @BindView(R.id.tvViewerCount)
    TextView tvCurrentViewers;
    public @BindView(R.id.rlDeal)
    RelativeLayout rlDeal;
    public @BindView(R.id.ivProductCountDown)
    RoundedImageView ivProductCountDown;
    public @BindView(R.id.ivProductNonSale)
    RoundedImageView ivProductNonSale;
    public @BindView(R.id.pbFeedImage)
    ProgressBar pbFeedImage;
    public @BindView(R.id.pbProductImage)
    ProgressBar pbProductImage;
    public @BindView(R.id.tvCountDownTimer)
    TextView tvCountDownTimer;
    public @BindView(R.id.tvSaleQuantity)
    TextView tvSaleQuantity;
    public @BindView(R.id.llCountDownAndQuantity)
    LinearLayout llCountDownAndQuantity;
    public @BindView(R.id.tvSchedule)
    TextView tvSchedule;
    public @BindView(R.id.viewMarginLeft)
    View viewMarginLeft;
    public @BindView(R.id.viewMarginRight)
    View viewMarginRight;
    public @BindView(R.id.separator_line)
    View separatorLine;
    public CountDownTimer timer;
    public ListenerRegistration listenerViewers;

    public LiveViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}