package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GiftHistoryHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    public @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    public @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;
    public @BindView(R.id.tvNameUser)
    TextView tvNameUser;
    public @BindView(R.id.tvMethod)
    TextView tvMethod;
    public @BindView(R.id.tvQuantity)
    TextView tvQuantity;
    public @BindView(R.id.tvDateFull)
    TextView tvDateFull;
    public @BindView(R.id.pbLoadAvatar)
    ProgressBar pbAvatar;

    public GiftHistoryHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}