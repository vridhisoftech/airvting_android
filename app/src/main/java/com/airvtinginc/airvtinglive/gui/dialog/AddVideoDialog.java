package com.airvtinginc.airvtinglive.gui.dialog;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddVideoDialog extends DialogFragment {
    private static final int REQUEST_TAKE_VIDEO = 111;
    private static final int REQUEST_CHOOSE_FROM_GALLERY = 222;

    @BindView(R.id.ivTakeVideo)
    ImageView ivTakeVideo;
    @BindView(R.id.dialog_add_photo_iv_gallery)
    ImageView ivGallery;
    @BindView(R.id.dialog_add_video_ll_background)
    LinearLayout llBackground;

    @OnClick(R.id.dialog_add_video_ll_background)
    void close() {
        dismiss();
    }

    @OnClick(R.id.dialog_add_photo_iv_gallery)
    void openGallery() {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        DetailType.ADD_NEW_VIDEO.attachTo(intent);
        intent.putExtra("chooseVideoType", EnumManager.ChooseVideoType.Video.ordinal());
        startActivity(intent);
        dismiss();
    }

    @OnClick(R.id.ivTakeVideo)
    void openTakeVideo() {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        DetailType.ADD_NEW_VIDEO.attachTo(intent);
        // Sender usage
        intent.putExtra("chooseVideoType", EnumManager.ChooseVideoType.Camera.ordinal());
        startActivity(intent);
        //todo Danh test
//            Intent intent1 = new Intent(getActivity(), VideoCapture.class);
//            startActivity(intent1);
        dismiss();
    }

    private ReturnEditImageListener mReturnEditImageListener;

    public static AddVideoDialog newInstance() {
        return new AddVideoDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_video, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);

        llBackground.setOnClickListener(v ->
                dismiss()
        );

        Intent intent = new Intent(getActivity(), DetailActivity.class);
        DetailType.ADD_NEW_VIDEO.attachTo(intent);
        ivTakeVideo.setOnClickListener(v -> {
            // Sender usage
            intent.putExtra("chooseVideoType", EnumManager.ChooseVideoType.Camera.ordinal());
            startActivity(intent);
//            dismiss();
        });
        ivGallery.setOnClickListener(v -> {
            intent.putExtra("chooseVideoType", EnumManager.ChooseVideoType.Video.ordinal());
            startActivity(intent);
//            dismiss();
        });
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case REQUEST_TAKE_VIDEO:
            case REQUEST_CHOOSE_FROM_GALLERY:
                if (resultCode == Activity.RESULT_OK) {
                    if (mReturnEditImageListener != null) {
                        mReturnEditImageListener.onReturnEditImage(imageReturnedIntent.getData());
                    }
                }

                break;
        }
    }

    public interface ReturnEditImageListener {
        void onReturnEditImage(Uri uri);
    }

    public void setReturnEditImageListener(ReturnEditImageListener returnEditImageListener) {
        mReturnEditImageListener = returnEditImageListener;
    }
}
