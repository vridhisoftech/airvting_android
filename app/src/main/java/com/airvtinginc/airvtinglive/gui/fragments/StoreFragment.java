package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ProductsAdapter;

import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.ProductListModelRequest;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StoreFragment extends BaseFragment {
    private static final String TAG = StoreFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private int mCurrentPage = 1;
    private GridLayoutManager mLayoutManager;
    private String mUserId;
    private ProductsAdapter mAdapter;

    public StoreFragment() {
        // Required empty public constructor
    }

    public static StoreFragment newInstance() {
        return new StoreFragment();
    }


    @OnClick(R.id.ivAddProduct)
    void addProduct() {
        requestApi(Utils.getCurrentUserId(getContext()), true, RequestTarget.GET_USER_PROFILE, this);
    }

    @OnClick(R.id.mrlWallet)
    void goToWallet() {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        // Sender usage
        DetailType.WALLET.attachTo(intent);
        getActivity().startActivity(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");

        setupRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.clearAllItems();
        mCurrentPage = 1;
        getProducts(mCurrentPage);
    }

    private void setupRecyclerView() {
        mRecyclerView.setEmptyView(emptyView);
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new ProductsAdapter(getActivity(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        });
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isMoreLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            mAdapter.addItem(null);
                            isMoreLoading = true;
                            getProducts(++mCurrentPage);
                        }
                    }
                }
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            refresh();
        });
    }

    public void refresh() {
        mCurrentPage = 1;
        getProducts(mCurrentPage);
    }

    private void getProducts(int page) {
        String currentUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (!TextUtils.isEmpty(currentUserId)) {
            ProductListModelRequest modelRequest = new ProductListModelRequest(currentUserId, page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, null);
            requestApi(modelRequest, false, RequestTarget.GET_LIST_PRODUCT, this);
        }
    }

    private void checkEmailAndPayment() {
        boolean hasConnectWithStripe = TextUtils.isEmpty(Utils.getCreateStripeAccountUrl(getContext()));
        boolean hasPaymentMethod = Utils.hasPaymentMethod(getContext());
//        if (!Utils.hasEmail(getContext())) {
//            DialogUtils.showUpdateEmailDialog(getContext(), getFragmentManager());
//        } else if (!Utils.isActiveEmail(getContext())) {
//            DialogUtils.showVerifyEmailDialog(getContext(), getFragmentManager());
//        } else if (!hasConnectWithStripe && !hasPaymentMethod) {
//            DialogUtils.showConnectStripeAndAddPaymentDialog(getContext(), getFragmentManager());
//        } else if (!hasConnectWithStripe) {
//            DialogUtils.showConnectStripeDialog(getContext(), getFragmentManager());
//        } else if (!hasPaymentMethod) {
//            DialogUtils.showAddPaymentMethodDialog(getContext(), getFragmentManager());
//        } else {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            // Sender usage
            DetailType.ADD_NEW_PRODUCT.attachTo(intent);
            getActivity().startActivity(intent);
//        }
    }

    private void hideLoadMore() {
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_LIST_PRODUCT:
                    hideLoadMore();
                    if (mSwipeRefreshLayout.isRefreshing()) {
                        mAdapter.clearAllItems();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    List<Product> products = ((ProductsResponse) response.getData()).getProducts();
                    if (mAdapter != null) {
                        mAdapter.addAllItems(products);
                    }
                    break;
                case GET_USER_PROFILE: {
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null) {
                        Utils.saveEmailAndPaymentInfo(getContext(), userResponse.getUser());
                        checkEmailAndPayment();
                    }
                    break;
                }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
