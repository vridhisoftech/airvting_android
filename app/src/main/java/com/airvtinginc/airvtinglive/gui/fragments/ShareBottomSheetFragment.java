package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.activities.ShareActivity;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.facebook.CallbackManager;
import com.facebook.share.widget.ShareDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShareBottomSheetFragment extends BottomSheetDialogFragment {

    @BindView(R.id.add_video_switch_share_facebook)
    Switch add_video_switch_share_facebook;
    @BindView(R.id.add_video_switch_share_Insta)
    Switch add_video_switch_share_Insta;
    @BindView(R.id.add_video_mrl_submit)
    MaterialRippleLayout mrlSubmit;
    private boolean shareFacebook = false;
    private boolean shareInstagram = false;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    Uri videoFileUri;
    String shareTo="share";
    public ShareBottomSheetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_share_bottom_sheet, container, false);
        ButterKnife.bind(this, view);

        add_video_switch_share_facebook.setOnCheckedChangeListener((compoundButton, isChecked) -> shareFacebook = isChecked);
        add_video_switch_share_Insta.setOnCheckedChangeListener((compoundButton, isChecked) -> shareInstagram = isChecked);

        mrlSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!shareFacebook && !shareInstagram) {
                    Toast.makeText(getApplicationContext(), "Select At least one to Share", Toast.LENGTH_LONG).show();
                } else {

                    Intent intent = new Intent(getApplicationContext(), ShareActivity.class);

                    if (shareFacebook && !shareInstagram) {
                        intent.putExtra(shareTo, "facebook");
                    } else if (shareInstagram && !shareFacebook) {
                        intent.putExtra(shareTo, "instagram");

                    } else if (shareFacebook && shareInstagram) {
                        intent.putExtra(shareTo, "both");
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }

            }
        });
    return view;
    }
}
