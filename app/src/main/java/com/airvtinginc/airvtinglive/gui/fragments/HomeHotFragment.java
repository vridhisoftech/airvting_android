package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.adapter.PostAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.HomeFilterType;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.models.request.GetPostsFilterRequest;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeHotFragment extends BaseFragment implements PostAdapter.OnItemClickListener {
    private static final String TAG = HomeHotFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.tvShowEmpty)
    TextView tvShowEmpty;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private GridLayoutManager mLayoutManager;
    private PostAdapter mAdapter;
    private int mCurrentPage = 1;
    private String mMaxId;
    private boolean isMoreLoading;
    private int visibleItemCount, pastVisibleItems, totalItemCount;

    public static HomeHotFragment newInstance() {
        return new HomeHotFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycleview, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupRecyclerView();
    }

    private void setupRecyclerView() {
        emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        tvShowEmpty.setText(getString(R.string.recycle_view_empty_post));
        mRecyclerView.setEmptyView(emptyView);
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mAdapter = new PostAdapter(getActivity(), this, false, false, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = mLayoutManager.getChildCount();
                                                      totalItemCount = mLayoutManager.getItemCount();
                                                      pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getHotPosts(++mCurrentPage);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            refresh();
        });
    }

    private void refresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mCurrentPage = 1;
        mMaxId = null;
        getHotPosts(mCurrentPage);
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        //modify
    }

    private void getHotPosts(int page) {
        String filter = HomeFilterType.HOT.getType();
        String[] typePost = {MediaType.STREAM.getType(), MediaType.VIDEO.getType()};

        GetPostsFilterRequest modelRequest = new GetPostsFilterRequest(filter, typePost, page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, mMaxId, false);
        requestApi(modelRequest, false, RequestTarget.GET_POSTS_BY_FILTER, this);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_POSTS_BY_FILTER:
                    hideLoadMore();
                    PostResponse result = ((PostResponse) response.getData());
                    if (result != null && mAdapter != null) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mAdapter.addAllItems(result.getListPostDetail());
                        mMaxId = result.getMaxId();
                    }
                    if (result.getListPostDetail().size() == 0) {
                        tvShowEmpty.setText(getString(R.string.recycle_view_empty_post));
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.clearAllItems();
        emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        tvShowEmpty.setText("No Network Available, Please Connect with the Internet.");
        mRecyclerView.setEmptyView(emptyView);
    }
}
