package com.airvtinginc.airvtinglive.gui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.splash)
    ImageView splash;

    private boolean isRunning = false;
    private final long TIME_DELAY_SPLASH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            // Get data from background notification
            String notificationData = getIntent().getStringExtra(ServiceConstants.NOTIFICATION);
            if (!TextUtils.isEmpty(notificationData)) {
                SharedPreferencesManager.getInstance(this).putString(Constants.PREF_NOTIFICATION, notificationData);
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return;
            }
        }

        bindLogo();
        isRunning = true;
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                doFinish();
            }
        };
        // Show splash screen for 2 seconds
        new Timer().schedule(task, TIME_DELAY_SPLASH);

        // for system bar in lollipop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Utils.systemBarLolipop(this);
        }
    }

    /**
     * If the app is still running than this method will start the MainActivity
     * and finish the Splash.
     */
    public synchronized void doFinish() {
        if (isRunning) {
            isRunning = false;
            if (Utils.isRememberLogin(this)) {
                Intent in = new Intent(this, MainActivity.class);
                startActivity(in);
                finish();
            } else {
                Intent i = new Intent(this, WelcomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //if(!Utilities.getBooleanPreference(this, Constants.PREF_FIRST_LAUNCH)){
            isRunning = false;
            finish();
            //}
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void bindLogo() {
        // Start animating the image
        final AlphaAnimation animation1 = new AlphaAnimation(0.2f, 1.0f);
        animation1.setDuration(700);
        final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.2f);
        animation2.setDuration(700);
        //animation1 AnimationListener
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                splash.startAnimation(animation2);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                splash.startAnimation(animation1);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        splash.startAnimation(animation1);
    }
}
