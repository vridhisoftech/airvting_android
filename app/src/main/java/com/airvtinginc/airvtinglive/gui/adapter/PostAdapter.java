package com.airvtinginc.airvtinglive.gui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.LiveViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ProductViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.HomeFeedType;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.request.BookmarkPostRequest;
import com.airvtinginc.airvtinglive.models.response.PostDetailResponse;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.Schedule;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.Viewer;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PostAdapter extends BaseAdapter {

    private static final String TAG = PostAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_EMPTY = -1;
    private static final int VIEW_TYPE_LIVE = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;
    private static final int VIEW_TYPE_PRODUCT = 2;
    private static final int VIEW_TYPE_VIDEO = 3;
    private static final int VIEW_TYPE_IMAGE = 4;

    private int mBookmarkPosition = -1;
    private boolean isShowMarginLeftRight;
    private boolean isNotShowSeparator;

    private CollectionReference viewersRef;
    private Activity activity;

    private boolean isBookmark;

    private boolean isCurrentUser;

    private boolean isDeletePost;

    private ActionOnPostItem actionOnPostItem;

    public void setShowMarginLeftRight(boolean showMarginLeftRight) {
        isShowMarginLeftRight = showMarginLeftRight;
    }

    public void setNotShowSeparator(boolean notShowSeparator) {
        isNotShowSeparator = notShowSeparator;
    }

    public PostAdapter(Context context, OnItemClickListener onItemClickListener, boolean isBookmark, boolean isCurrentUser, boolean isDeletePost) {
        super(context, onItemClickListener);
        this.activity = (Activity) context;
        viewersRef = FirebaseFirestore.getInstance().collection(Constants.COLLECTION_VIEWERS);
        this.isBookmark = isBookmark;
        this.isCurrentUser = isCurrentUser;
        this.isDeletePost = isDeletePost;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_VIDEO:
            case VIEW_TYPE_IMAGE:
            case VIEW_TYPE_LIVE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_livestream, parent, false);
                LiveViewHolder vh = new LiveViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_PRODUCT: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_home, parent, false);
                ProductViewHolder vh = new ProductViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof EmptyViewHolder || list.get(position) == null) {
            return;
        }
        PostDetail postDetail = (PostDetail) list.get(position);
        User user = postDetail.getUser();
        List<ProductInPost> productInPosts = postDetail.getProducts();
        ProductInPost product = null;

        if (productInPosts != null && !productInPosts.isEmpty()) {
            for (ProductInPost productInPost : productInPosts) {
                if (productInPost.isShow()) {
                    product = productInPost;
                }
            }
        }

        if (mHolder instanceof LiveViewHolder) {

            final LiveViewHolder holder = (LiveViewHolder) mHolder;
            int screenWidth = Utils.getScreenWidth(context);
//            holder.llBound.getLayoutParams().width = (screenWidth / 2);
            if (isShowMarginLeftRight) {
                holder.viewMarginLeft.setVisibility(View.VISIBLE);
                holder.viewMarginRight.setVisibility(View.VISIBLE);
            } else {
                holder.viewMarginLeft.setVisibility(View.GONE);
                holder.viewMarginRight.setVisibility(View.GONE);
            }

            if (isNotShowSeparator) {
                holder.separatorLine.setVisibility(View.GONE);
            } else {
                holder.separatorLine.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(postDetail.getFeaturedImage())) {
                holder.pbFeedImage.setVisibility(View.VISIBLE);
                Glide.with(context).load(postDetail.getFeaturedImage())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbFeedImage.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbFeedImage.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.ivLive);

                holder.ivLive.setVisibility(View.VISIBLE);
                holder.tvDefaultAvatar.setText("");
                holder.tvDefaultAvatar.setVisibility(View.GONE);
            } else {
                holder.ivLive.setVisibility(View.INVISIBLE);
                holder.tvDefaultAvatar.setText(Utils.getDefaultAvatarText(user));
                holder.tvDefaultAvatar.setVisibility(View.VISIBLE);
                holder.ivLive.setImageResource(R.drawable.ic_white);
            }

            if (isBookmark) {
                String currentBookmarkDateString = postDetail.getBookmarkedAt();
                try {
                    holder.tvDate.setText(DateTimeUtils.getDisplayBookmarkTime(context, DateTimeUtils.getDateFormatUTC(currentBookmarkDateString)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (position > 0) {
                    String previousBookmarkDateString = ((PostDetail) list.get(position - 1)).getBookmarkedAt();
                    Date currentBookMarkDate = DateTimeUtils.getDateFollowFormat(currentBookmarkDateString, DateTimeUtils.DATE_TIME_FORMAT);
                    Date previousBookMarkDate = DateTimeUtils.getDateFollowFormat(previousBookmarkDateString, DateTimeUtils.DATE_TIME_FORMAT);

                    if (DateTimeUtils.isSameDate(currentBookMarkDate, previousBookMarkDate)) {
                        holder.tvDate.setVisibility(View.GONE);
                    } else {
                        holder.tvDate.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.tvDate.setVisibility(View.VISIBLE);
                }
            } else {
                holder.tvDate.setVisibility(View.GONE);
            }

            if (postDetail.isBookmark()) {
                holder.ivBookmark.setImageResource(R.drawable.ic_bookmark_fill);
            } else {
                holder.ivBookmark.setImageResource(R.drawable.ic_bookmark_empty);
            }

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    openUserDetail(position);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
if(user.getUsername()!=null) {
    Utils.spanAndCheckContentUsername(context, user.getUsername(), "", user.isOnline(), holder.tvUserName, clickableSpan, context.getResources().getColor(R.color.colorHeader));
}
            holder.tvDescription.setText(postDetail.getTitle());
            try {
                holder.tvCreatePost.setText((DateTimeUtils.getElapsedInterval(context, DateTimeUtils.getDateFormatUTC(postDetail.getCreatedAt()))).toLowerCase());
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.lnBookmark.setOnClickListener(view -> {
                BookmarkPostRequest requestModel = new BookmarkPostRequest(postDetail.getId());
                requestApi(requestModel, true, RequestTarget.BOOKMARK_POST, this);
                mBookmarkPosition = position;
            });

            holder.mView.setOnClickListener(v -> {
                if (postDetail.getType().equals(HomeFeedType.LIVE.getType())) {
                    openStream(postDetail, position);
                } else {
                    openMedia(postDetail, position);
                }
            });

            ProductInPost productInPost = product;
            holder.rlDeal.setOnClickListener(view -> {
                if (productInPost != null) {
                    openProductDetail(productInPost.getProductId());
                }
            });

            if (postDetail.getType().equals(HomeFeedType.VIDEO.getType())
                    || postDetail.getType().equals(HomeFeedType.IMAGE.getType())) {
                if (postDetail.getType().equals(HomeFeedType.VIDEO.getType())) {
                    holder.ivPostVideo.setVisibility(View.VISIBLE);
                } else {
                    holder.ivPostVideo.setVisibility(View.GONE);
                }
                holder.ivLiveRec.setVisibility(View.GONE);
                holder.tvCurrentViewers.setVisibility(View.GONE);
                holder.rlDeal.setVisibility(View.GONE);
                holder.tvSchedule.setVisibility(View.GONE);

                return;
            } else if (postDetail.getType().equals(HomeFeedType.LIVE.getType())) {
                if (postDetail.isLive()) {
                    holder.ivLiveRec.setImageResource(R.drawable.ic_live);
                } else {
                    holder.ivLiveRec.setImageResource(R.drawable.ic_rec);
                    Glide.with(context).load(R.drawable.ic_price_tag_sold).into(holder.ivPriceTag);
                }
                holder.ivLiveRec.setVisibility(View.VISIBLE);
                holder.ivPostVideo.setVisibility(View.GONE);
                holder.tvCurrentViewers.setVisibility(View.VISIBLE);
                holder.tvCurrentViewers.setText(postDetail.getNumberViewer());

                if (holder.listenerViewers != null) {
                    holder.listenerViewers.remove();
                }
                EventListener<QuerySnapshot> listener = (queryDocumentSnapshots, err) -> {
                    if (err != null) {
                        Log.e(TAG, "listenNewMessages failed " + err.getMessage(), err);
                        return;
                    }
                    if (queryDocumentSnapshots == null) {
                        Log.e(TAG, "queryDocumentSnapshots null");
                        return;
                    }
                    List<Viewer> listViewer = new ArrayList<>();
                    for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                        DocumentSnapshot documentSnapshot = dc.getDocument();
                        Viewer viewer = null;
                        try {
                            viewer = documentSnapshot.toObject(Viewer.class);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (viewer == null) {
                            Log.e(TAG, "viewer null");
                            postDetail.setNumberViewer("0");
                            continue;
                        }

                        String id = documentSnapshot.getId();
                        int oldIndex = dc.getOldIndex();
                        int newIndex = dc.getNewIndex();

                        switch (dc.getType()) {
                            case ADDED:
                                Log.d(TAG, "Added: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                Log.d(TAG, "ID: " + id + "; User Id: " + viewer.getUserId());
                                if (postDetail.isLive()) {
                                    if (viewer.isView()) {
                                        listViewer.add(viewer);
                                    }
                                } else {
                                    listViewer.add(viewer);
                                }
                                break;
                            case MODIFIED:
                                Log.d(TAG, "Modified: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                break;
                            case REMOVED:
                                Log.d(TAG, "Removed: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                break;
                        }
                    }
                    Log.e(TAG, "listenerViewers End");
                    try {
                        if (!postDetail.getNumberViewer().equals(String.valueOf(listViewer.size()))) {
                            postDetail.setNumberViewer(String.valueOf(listViewer.size()));
                            notifyDataSetChanged();
                        }
                    } catch (Exception ex) {
                        postDetail.setNumberViewer(String.valueOf(listViewer.size()));
                        notifyDataSetChanged();
                    }
                };
                if (postDetail.isLive()) {
                    holder.listenerViewers = viewersRef
                            .whereEqualTo(Constants.FIELD_POST_ID, postDetail.getId())
                            .whereEqualTo(Constants.FIELD_IS_VIEW, true)
                            .addSnapshotListener(activity, listener);
                } else {
                    holder.listenerViewers = viewersRef
                            .whereEqualTo(Constants.FIELD_POST_ID, postDetail.getId())
                            .addSnapshotListener(activity, listener);
                }
                holder.rlDeal.setVisibility(View.VISIBLE);
                showSchedule(holder.tvSchedule, user.getSchedule());
            }

            if (product != null) {
                if (!TextUtils.isEmpty(product.getFeaturedImage())) {
                    holder.pbProductImage.setVisibility(View.VISIBLE);
                    Glide.with(context).load(product.getFeaturedImage())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    holder.pbProductImage.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    holder.pbProductImage.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(holder.ivProductCountDown);

                    Glide.with(context).load(product.getFeaturedImage())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    holder.pbProductImage.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    holder.pbProductImage.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(holder.ivProductNonSale);
                }

                holder.tvSaleQuantity.setText(String.valueOf(product.getQuantity()));
                String dealTime = product.getExpiredAt();
                if (!TextUtils.isEmpty(dealTime) && postDetail.isLive()) {
                    holder.ivProductCountDown.setVisibility(View.VISIBLE);
                    holder.llCountDownAndQuantity.setVisibility(View.VISIBLE);
                    holder.ivProductNonSale.setVisibility(View.GONE);
                    SimpleDateFormat dateTimeFormat = new SimpleDateFormat(ServiceConstants.DATE_TIME_FORMAT, Locale.getDefault());
                    Calendar currentCalendar = Calendar.getInstance();
                    Calendar expiredCalendar = Calendar.getInstance();

                    try {
                        expiredCalendar.setTimeInMillis(dateTimeFormat.parse(DateTimeUtils.getDateFormatUTC(dealTime)).getTime());
                    } catch (Exception e) {
                        AppLog.e(e.getMessage());
                    }

                    long difference = expiredCalendar.getTimeInMillis() - currentCalendar.getTimeInMillis();

                    if (holder.timer != null) {
                        holder.timer.cancel();
                    }

                    holder.timer = new CountDownTimer(difference, 1000) {
                        public void onTick(long millisUntilFinished) {
                            try {
                                holder.tvCountDownTimer.setText(DateTimeUtils.getDisplayTimeSaleFromMillis(millisUntilFinished));
                                holder.ivPriceTag.setImageResource(R.drawable.ic_price_tag);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                holder.timer.cancel();
                            }
                        }

                        public void onFinish() {
                            holder.tvCountDownTimer.setAnimation(null);
                            holder.tvCountDownTimer.setText(context.getString(R.string.discount_timer_end));
                            try {
                                holder.ivPriceTag.setImageResource(R.drawable.ic_price_tag_sold);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                holder.timer.cancel();
                            }
                            holder.ivProductCountDown.setVisibility(View.GONE);
                            holder.llCountDownAndQuantity.setVisibility(View.GONE);
                            holder.ivProductNonSale.setVisibility(View.VISIBLE);
                        }
                    }.start();
                } else {
                    holder.ivProductCountDown.setVisibility(View.GONE);
                    holder.llCountDownAndQuantity.setVisibility(View.GONE);
                    holder.ivProductNonSale.setVisibility(View.VISIBLE);
                }
            } else {
                holder.rlDeal.setVisibility(View.GONE);
            }

            if (!holder.tvCountDownTimer.getText().toString().equalsIgnoreCase(context.getString(R.string.discount_timer_end))) {
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500); //You can manage the blinking time with this parameter
                anim.setStartOffset(0);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                holder.tvCountDownTimer.startAnimation(anim);
            }

            if (isCurrentUser && isDeletePost) {
                holder.lnBookmark.setVisibility(View.GONE);
                holder.ivDeletePost.setVisibility(View.VISIBLE);
                holder.ivDeletePost.setOnClickListener(v -> {
                    actionOnPostItem.onDelete(postDetail);
                });
            } else {
                holder.lnBookmark.setVisibility(View.VISIBLE);
                holder.ivDeletePost.setVisibility(View.GONE);
            }
        }
    }

    private void showSchedule(TextView tvSchedule, Schedule schedule) {
        if (schedule != null && schedule.getScheduleDates() != null && !schedule.getScheduleDates().isEmpty()) {
            tvSchedule.setText(DateTimeUtils.getDisplaySchedule(context, schedule));
            tvSchedule.setVisibility(View.VISIBLE);
        } else {
            tvSchedule.setVisibility(View.GONE);
        }
    }

    private void openUserDetail(int position) {
        PostDetail postDetail = (PostDetail) list.get(position);
        String userId = postDetail.getUser().getUserId();
        Utils.openUserDetail(context, userId);
    }

    private void openProductDetail(String productId) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_PRODUCT_ID, productId);
        intent.putExtras(args);
        // Sender usage
        DetailType.PRODUCT.attachTo(intent);
        context.startActivity(intent);
    }

    private void openStream(PostDetail postDetail, int position) {
        Intent intent = new Intent(context, LiveStreamActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, postDetail.getUser().getUserId());
        args.putString(Constants.EXTRA_POST_ID, postDetail.getId());
        args.putString(Constants.EXTRA_MEDIA_URL, postDetail.getMediaUrl());
        intent.putExtras(args);
        context.startActivity(intent);
    }

    private void openMedia(PostDetail media, int position) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_GALLERY_ID, media.getId());
        intent.putExtras(args);
        // Sender usage
        DetailType.GALLERY.attachTo(intent);
        context.startActivity(intent);
    }

    public interface ActionOnPostItem {
        void onDelete(PostDetail postDetail);
    }

    public void setActionOnPostItem(ActionOnPostItem actionOnPostItem) {
        this.actionOnPostItem = actionOnPostItem;
    }

    @Override
    public int getItemViewType(int position) {
        PostDetail postDetail = (PostDetail) list.get(position);
        if (postDetail == null) return VIEW_TYPE_LOAD_MORE;
        else if (postDetail.getType().equalsIgnoreCase(HomeFeedType.LIVE.getType()))
            return VIEW_TYPE_LIVE;
        else if (postDetail.getType().equalsIgnoreCase(HomeFeedType.VIDEO.getType()))
            return VIEW_TYPE_VIDEO;
        else if (postDetail.getType().equalsIgnoreCase(HomeFeedType.IMAGE.getType()))
            return VIEW_TYPE_IMAGE;
        else return VIEW_TYPE_EMPTY;
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case BOOKMARK_POST:
                    if (isBookmark && mBookmarkPosition != -1) {
                        removeItem(mBookmarkPosition);
                    } else {
                        boolean isBookmark = ((PostDetailResponse) response.getData()).getPostDetail().isBookmark();
                        ((PostDetail) list.get(mBookmarkPosition)).setBookmark(isBookmark);
                        notifyItemChanged(mBookmarkPosition);
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        mBookmarkPosition = -1;
    }
}
