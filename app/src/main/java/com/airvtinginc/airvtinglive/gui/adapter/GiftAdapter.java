package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.GiftViewHolder;
import com.airvtinginc.airvtinglive.models.GiftDetail;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class GiftAdapter extends BaseAdapter {
    private static final int VIEW_TYPE_ITEM_GIFT = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;
    private int mTextColor;
    private boolean mIsShowInGiftStore;
    private boolean mIsShowInMyGift;

    public GiftAdapter(Context context, List<GiftDetail> giftDetailList, OnItemClickListener onItemClickListener) {
        super(context, giftDetailList, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM_GIFT: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gift, parent, false);
                return new GiftViewHolder(view);
            }
            case VIEW_TYPE_LOAD_MORE: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(view);
            }
            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(view);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null || mHolder instanceof EmptyViewHolder) {
            return;
        }

        if (mHolder instanceof GiftViewHolder) {
            final GiftViewHolder holder = (GiftViewHolder) mHolder;
            GiftDetail giftDetail = (GiftDetail) list.get(position);

            if (giftDetail == null) {
                return;
            }

            if (!TextUtils.isEmpty(giftDetail.getFeaturedImage())) {
                holder.pbItemGift.setVisibility(View.VISIBLE);
                ImageView ivGift = holder.ivGiftIcon;
                if (mIsShowInGiftStore) {
                    ivGift = holder.ivGiftIconWithBorder;
                }

                Glide.with(context).load(giftDetail.getFeaturedImage())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbItemGift.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbItemGift.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivGift);
            }

            if (!TextUtils.isEmpty(giftDetail.getTitle())) {
                holder.tvGiftTitle.setText(giftDetail.getTitle());
            }

            if (mIsShowInMyGift) {
                holder.tvGiftToken.setText(String.valueOf(giftDetail.getQuantity()));
                holder.ivGiftToken.setVisibility(View.GONE);
                holder.rlGiftBackground.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            } else {
                holder.tvGiftToken.setText(String.valueOf(giftDetail.getAirToken()));
            }

            if (mIsShowInGiftStore) {
                if (giftDetail.getCountGiftsOfUser() > 0) {
                    String displayQuantity;
                    int giftQuantity = giftDetail.getCountGiftsOfUser();
                    if (giftQuantity < 100) {
                        displayQuantity = String.valueOf(giftQuantity);
                    } else {
                        displayQuantity = "99+";
                        holder.tvGiftCurrentOwnByUser.setTextSize(TypedValue.COMPLEX_UNIT_SP, 7f);
                    }
                    holder.tvGiftCurrentOwnByUser.setText(displayQuantity);
                    holder.tvGiftCurrentOwnByUser.setGravity(Gravity.CENTER);
                    holder.rlGiftCurrentOwnByUser.setVisibility(View.VISIBLE);
                    holder.ivGiftIconWithBorder.setBorderGradientStart(context.getResources().getColor(R.color.color_gift_border_orange_start));
                    holder.ivGiftIconWithBorder.setBorderGradientEnd(context.getResources().getColor(R.color.color_gift_border_orange_end));
                } else {
                    holder.tvGiftCurrentOwnByUser.setText("");
                    holder.rlGiftCurrentOwnByUser.setVisibility(View.GONE);
                    holder.ivGiftIconWithBorder.setBorderGradientStart(context.getResources().getColor(R.color.color_divider));
                    holder.ivGiftIconWithBorder.setBorderGradientEnd(context.getResources().getColor(R.color.color_divider));
                }
                holder.ivGiftIconWithBorder.setVisibility(View.VISIBLE);
                holder.ivGiftIcon.setVisibility(View.GONE);
                holder.rlGiftBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
            } else {
                holder.tvGiftCurrentOwnByUser.setText("");
                holder.rlGiftCurrentOwnByUser.setVisibility(View.GONE);
                holder.ivGiftIconWithBorder.setBorderGradientStart(context.getResources().getColor(android.R.color.transparent));
                holder.ivGiftIconWithBorder.setBorderGradientEnd(context.getResources().getColor(android.R.color.transparent));
                holder.ivGiftIconWithBorder.setVisibility(View.GONE);
                holder.ivGiftIcon.setVisibility(View.VISIBLE);
            }

            holder.tvGiftTitle.setTextColor(mTextColor);
            holder.tvGiftToken.setTextColor(mTextColor);

            holder.mView.setOnClickListener(view -> onItemClickListener.onItemClicked(giftDetail, position));
        }
    }

    public void setTextColor(int color) {
        mTextColor = color;
    }

    public void setIsShowInGiftStore(boolean showInGiftStore) {
        mIsShowInGiftStore = showInGiftStore;
    }

    public void setIsShowInMyGift(boolean showInMyGift) {
        mIsShowInMyGift = showInMyGift;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM_GIFT : VIEW_TYPE_LOAD_MORE;
    }

    public GiftDetail getItemAtPosition(int position) {
        if (list != null && !list.isEmpty()) {
            return (GiftDetail) list.get(position);
        }
        return null;
    }
}
