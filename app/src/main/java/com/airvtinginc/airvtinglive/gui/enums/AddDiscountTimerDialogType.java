package com.airvtinginc.airvtinglive.gui.enums;

import android.content.Intent;

public enum AddDiscountTimerDialogType {
    LIVE,
    PRODUCT;

    private static final String name = AddDiscountTimerDialogType.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static AddDiscountTimerDialogType detachFrom(Intent intent) {
        if (!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }
}
