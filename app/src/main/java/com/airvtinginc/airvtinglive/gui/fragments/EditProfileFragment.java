package com.airvtinginc.airvtinglive.gui.fragments;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.gui.dialog.AddPhotoDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.UpdateUserProfileRequest;
import com.airvtinginc.airvtinglive.models.response.Location;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.RealPathUtil;
import com.airvtinginc.airvtinglive.tools.RequestPartUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;
import com.lightfire.gradienttextcolor.GradientTextView;
import com.rilixtech.CountryCodePicker;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

public class EditProfileFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener,
        AddPhotoDialog.ReturnImageListener {
    private static final String TAG = EditProfileFragment.class.getName();

    @BindView(R.id.edit_profile_sv_bound)
    ScrollView svBound;
    @BindView(R.id.ivCover)
    ImageView mIvCover;
    @BindView(R.id.ivAvatar)
    ImageView mIvAvatar;
    @BindView(R.id.rlAvatar)
    RelativeLayout mRlAvatar;
    @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    GradientTextView mTvDefaultAvatar;
    @BindView(R.id.edtFirstName)
    EditText mEdtFirstName;
    @BindView(R.id.edtLastName)
    EditText mEdtLastName;
    @BindView(R.id.edtUsername)
    EditText mEdtUsername;
    @BindView(R.id.btnMale)
    RadioButton mBtnMale;
    @BindView(R.id.btnFemale)
    RadioButton mBtnFemale;
    @BindView(R.id.edtEmail)
    EditText mEdtEmail;
    @BindView(R.id.edtBirthday)
    EditText mEdtBirthday;
    @BindView(R.id.ccpPhonePicker)
    CountryCodePicker mCcpPhonePicker;
    @BindView(R.id.edtPhoneNumber)
    EditText mEdtPhoneNumber;
    @BindView(R.id.edtAddress)
    EditText mEdtAddress;
    @BindView(R.id.edtDescription)
    EditText mEdtDescription;

    @BindView(R.id.pbCoverLoading)
    ProgressBar mPbCoverLoading;
    @BindView(R.id.pbAvatarLoading)
    ProgressBar mPbAvatarLoading;

    private String mUserId;
    private User mUser;
    private BroadcastReceiver mReceiver;

    private enum EditTypeEnum {
        AVATAR, COVER
    }

    private EditTypeEnum mEditType;
    private Calendar mBirthdayCalendar;
    private AddPhotoDialog mEditPhotoDialog;
    private Bitmap mBmAvatar;
    private Bitmap mBmCover;
    private File selectedAvatarFile, selectedCoverFile;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private Location userLocation;

    @OnTouch(R.id.rlBackground)
    boolean touchHideKeyboard() {
        Utils.hideSoftKeyboard(getActivity());
        return true;
    }

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        DetailActivity.headerTitle = getString(R.string.my_user_edit_profile);
        Intent intent = new Intent();

        // Show Edit Profile title
        intent.setAction(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intent);

        // Show save button
        intent.setAction(DetailActivity.BroadcastActionShowIconSave);
        getActivity().sendBroadcast(intent);

        registerReceiver();

        mUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (!TextUtils.isEmpty(mUserId)) {
            getProfile();
        }

    }

    private void registerReceiver() {
        // create new receiver
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (Objects.requireNonNull(intent.getAction())) {
                        case DetailActivity.BroadcastActionSave:
                            saveProfile();
                            break;
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(DetailActivity.BroadcastActionSave);
            getActivity().registerReceiver(mReceiver, filter);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
                mReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProfile() {
        if (TextUtils.isEmpty(mUserId)) {
            return;
        }
        requestApi(mUserId, true, RequestTarget.GET_USER_PROFILE, this);
    }

    private void showUserInfo(User user) {
        if (user.getCover() != null) {
            mPbCoverLoading.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(user.getCover())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            mPbCoverLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            mPbCoverLoading.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(mIvCover);
        }

        if (!TextUtils.isEmpty(user.getAvatar())) {
            mPbAvatarLoading.setVisibility(View.VISIBLE);
            mIvAvatar.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(user.getAvatar())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            mPbAvatarLoading.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(getActivity(), ivDefaultAvatar, mTvDefaultAvatar, (CircleImageView) mIvAvatar, user, R.dimen.profile_default_avatar_text_size);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            mPbAvatarLoading.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(getActivity(), ivDefaultAvatar, mTvDefaultAvatar, (CircleImageView) mIvAvatar);
                            return false;
                        }
                    })
                    .into(mIvAvatar);
        } else {
            mPbAvatarLoading.setVisibility(View.GONE);
            Utils.setDefaultAvatar(getActivity(), ivDefaultAvatar, mTvDefaultAvatar, (CircleImageView) mIvAvatar, user, R.dimen.profile_default_avatar_text_size);
        }

        if (!TextUtils.isEmpty(user.getFirstName())) {
            mEdtFirstName.setText(user.getFirstName());
        }

        if (!TextUtils.isEmpty(user.getLastName())) {
            mEdtLastName.setText(user.getLastName());
        }

        if (!TextUtils.isEmpty(user.getUsername())) {
            String displayUsername = "@" + user.getUsername();
            mEdtUsername.setText(displayUsername);
        }

        if (user.getGender() == ServiceConstants.GENDER_MALE) {
            mBtnMale.setChecked(true);
        } else {
            mBtnFemale.setChecked(true);
        }

        if (TextUtils.isEmpty(user.getEmail())) {
            mEdtEmail.setEnabled(true);
        } else {
            mEdtEmail.setText(user.getEmail());
            mEdtEmail.setEnabled(false);
        }
        if (!TextUtils.isEmpty(user.getBirthday())) {
//            String birthday = DateTimeUtils.convertStringDateToOtherFormat(user.getBirthday(),
//                    DateTimeUtils.DATE_TIME_FORMAT, DateTimeUtils.DATE_FORMAT);
            String birthday = user.getBirthday();
            String[] separated = birthday.split("T");
            mEdtBirthday.setText(separated[0]);
        }

        if (!TextUtils.isEmpty(user.getPhoneNumber())) {
            String[] phoneNumberArray = user.getPhoneNumber().split(" ");
            String phoneNumber;
            if (phoneNumberArray.length > 1) {
                String prefix = phoneNumberArray[0];
                try {
                    mCcpPhonePicker.setCountryForPhoneCode(Integer.valueOf(prefix));
                } catch (NumberFormatException e) {
                    AppLog.e(e.getMessage());
                }

                phoneNumber = phoneNumberArray[1];
            } else {
                phoneNumber = phoneNumberArray[0];
            }

            mEdtPhoneNumber.setText(phoneNumber);
        }

        if (user.getLocation() != null && !TextUtils.isEmpty(user.getLocation().getAddress())) {
            mEdtAddress.setText(user.getLocation().getAddress());
        }

        if (!TextUtils.isEmpty(user.getDescription())) {
            mEdtDescription.setText(user.getDescription());
        }

        mUser = user;
        setOnViewClicks();
    }

    private void setOnViewClicks() {
        mEdtUsername.setEnabled(false);
        mEdtBirthday.setFocusable(false);
        mEdtBirthday.setOnLongClickListener(null);
        mEdtBirthday.setOnClickListener(view -> showDatePickerDialog());

        mEdtAddress.setOnClickListener(v -> searchPlace());
        mIvCover.setOnClickListener(view -> {
            mEditType = EditTypeEnum.COVER;
            showChangePhotoDialog();
        });

        mRlAvatar.setOnClickListener(view -> {
            mEditType = EditTypeEnum.AVATAR;
            showChangePhotoDialog();
        });
    }

    private void searchPlace() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void showDatePickerDialog() {
        mBirthdayCalendar = Calendar.getInstance();
        if (!TextUtils.isEmpty(mUser.getBirthday())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(ServiceConstants.BIRTHDAY_FORMAT, Locale.getDefault());
            try {
                Date date = dateFormat.parse(mUser.getBirthday());
                mBirthdayCalendar.setTime(date);
            } catch (ParseException e) {
                AppLog.e(e.getMessage());
            }
        }
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                mBirthdayCalendar.get(Calendar.YEAR),
                mBirthdayCalendar.get(Calendar.MONTH),
                mBirthdayCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getActivity().getFragmentManager(), TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String day = String.valueOf(dayOfMonth);
        String month = String.valueOf(monthOfYear + 1);

        if (dayOfMonth < 10) {
            day = "0" + String.valueOf(dayOfMonth);
        }

        if (monthOfYear + 1 < 10) {
            month = "0" + String.valueOf(monthOfYear + 1);
        }

        String birthday = day + "/" + month + "/" + year;
        mEdtBirthday.setText(birthday);
        mBirthdayCalendar.set(year, monthOfYear, dayOfMonth);
        mUser.setBirthday(birthday);
    }

    private void showChangePhotoDialog() {
        mEditPhotoDialog = AddPhotoDialog.newInstance();

        //We just change image filter for avatar
        mEditPhotoDialog.setHaveFilter(true);

        mEditPhotoDialog.setReturnImageListener(this);
        mEditPhotoDialog.show(getActivity().getSupportFragmentManager(), "Edit Photo Dialog");
    }

    @Override
    public void onReturnImage(Uri data) {
        String imageRealPath = RealPathUtil.getRealPath(getContext(), data);
        File imageFile = new File(imageRealPath);
        switch (mEditType) {
            case COVER:
                try {
                    Glide.with(getActivity()).load(data)
                            .into(mIvCover);
                    selectedCoverFile = imageFile;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case AVATAR:
                try {
                    mPbAvatarLoading.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(data)
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    mPbAvatarLoading.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    mPbAvatarLoading.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(mIvAvatar);
                    selectedAvatarFile = imageFile;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
        }

        mEditPhotoDialog.dismiss();
    }

    private Map<String, RequestBody> mapUserToPartMap(User user) {
        Map<String, RequestBody> bodyMap = new HashMap<>();
        bodyMap.put(ServiceConstants.FIRST_NAME, RequestPartUtils.createFromObject(user.getFirstName()));
        bodyMap.put(ServiceConstants.LAST_NAME, RequestPartUtils.createFromObject(user.getLastName()));
        bodyMap.put(ServiceConstants.GENDER, RequestPartUtils.createFromObject(user.getGender()));
        bodyMap.put(ServiceConstants.BIRTHDAY, RequestPartUtils.createFromObject(user.getBirthday()));
        bodyMap.put(ServiceConstants.PHONE_NUMBER, RequestPartUtils.createFromObject(user.getPhoneNumber()));
        bodyMap.put(ServiceConstants.DESCRIPTION, RequestPartUtils.createFromObject(user.getDescription()));
        bodyMap.put(ServiceConstants.EMAIL, RequestPartUtils.createFromObject(user.getEmail()));
        List<Location> userRequestList = new ArrayList<>();

        userRequestList.add(user.getLocation());
        RequestBody location = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(userRequestList));
        bodyMap.put(ServiceConstants.LOCATION, location);
        return bodyMap;
    }

    private void saveProfile() {
        if (mUser == null) {
            return;
        }
        int gender = mBtnMale.isChecked() ? ServiceConstants.GENDER_MALE : ServiceConstants.GENDER_FEMALE;
        String phoneNumber = mCcpPhonePicker.getSelectedCountryCodeWithPlus() + " " + mEdtPhoneNumber.getText().toString();
        mUser.setFirstName(mEdtFirstName.getText().toString());
        mUser.setLastName(mEdtLastName.getText().toString());
        mUser.setGender(gender);
        mUser.setBirthday(mEdtBirthday.getText().toString());
        mUser.setPhoneNumber(phoneNumber);
        mUser.setDescription(mEdtDescription.getText().toString());
        mUser.setEmail(mEdtEmail.getText().toString());
        if (userLocation != null) {
            mUser.setLocation(userLocation);
        }
        UpdateUserProfileRequest requestModel = new UpdateUserProfileRequest();
        Map<String, RequestBody> userMap = mapUserToPartMap(mUser);
        requestModel.setParams(userMap);
        if (selectedAvatarFile != null) {
            RequestBody requestImage = RequestBody.create(MediaType.parse("image/*"), selectedAvatarFile);
            MultipartBody.Part avatarPartFile = MultipartBody.Part.createFormData("featuredImage", selectedAvatarFile.getName(), requestImage);
            requestModel.setAvatar(avatarPartFile);
        }


        if (selectedCoverFile != null) {
            RequestBody requestImage = RequestBody.create(MediaType.parse("image/*"), selectedCoverFile);
            MultipartBody.Part coverPartFile = MultipartBody.Part.createFormData("coverImage", selectedCoverFile.getName(), requestImage);
            requestModel.setCover(coverPartFile);
        }

        requestApi(requestModel, true, RequestTarget.UPDATE_USER_PROFILE, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                Log.e(TAG, "Place: " + place.getAddress());
                userLocation = new Location();
                userLocation.setAddress(place.getAddress().toString());
                Double[] coordinates = new Double[]{place.getLatLng().latitude, place.getLatLng().longitude};
                userLocation.setCoordinates(coordinates);
                mEdtAddress.setText(place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                Log.e(TAG, status.getStatusMessage());

            }
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case UPDATE_USER_PROFILE:
                    //Delete image file after upload
                    if (selectedAvatarFile != null && selectedAvatarFile.exists()) {
                        selectedAvatarFile.delete();
                    }
                    if (selectedCoverFile != null && selectedCoverFile.exists()) {
                        selectedCoverFile.delete();
                    }
                    UserResponse userResponse = (UserResponse) response.getData();

                    Utils.saveUserEmailAndName(getActivity(), userResponse.getUser());
                    Toast.makeText(getContext(), getString(R.string.profile_update_successfully), Toast.LENGTH_SHORT).show();
                    getActivity().finish();

                    break;
                case GET_USER_PROFILE:
                    showUserInfo(((UserResponse) response.getData()).getUser());
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
