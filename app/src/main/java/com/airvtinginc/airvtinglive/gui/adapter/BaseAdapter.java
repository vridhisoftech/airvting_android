package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.interfaces.BaseInterface;
import com.airvtinginc.airvtinglive.gui.activities.BaseActivity;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements BaseInterface, APIResponseListener {

    private static final String TAG = BaseAdapter.class.getSimpleName();

    public Context context;
    public List list;
    public OnItemClickListener onItemClickListener;

    public <T> BaseAdapter(Context context, OnItemClickListener onItemClickListener) {
        this(context, new ArrayList<>(), onItemClickListener);
    }

    public <T> BaseAdapter(Context context, List<T> list, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        <T> void onItemClicked(T item, int position);
    }


    @Override
    public <T> void requestApi(T model, boolean isLoading, RequestTarget requestTarget, APIResponseListener listener) {
        ((BaseActivity) context).requestApi(model, isLoading, requestTarget, listener);
    }

    public void cancelRequestApi() {
        ((BaseActivity) context).cancelRequestApi();
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + failMessage);
        FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        switch (statusCode) {
            case ERR_NO_INTERNET_CONNECTION:
                DialogUtils.showMessageDialog(context, fragmentManager,
                        context.getString(R.string.error_network_title), failMessage);
                break;
            case ERR_UNAUTHORIZED:
                DialogUtils.showUnauthorizedDialog(context, fragmentManager, failMessage);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (this.list != null) {
            return this.list.size();
        }
        return 0;
    }

    public <T> void addItem(T item) {
        this.list.add(item);
        notifyItemInserted(this.list.size() - 1);
        notifyItemChanged(this.list.size() - 1);
    }

    public <T> void addAllItems(List<T> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public <T> void addMoreData(List<T> list) {
        this.list.addAll(this.list.size(), list);
        notifyItemRangeChanged(this.list.size(), list.size());
    }

    public void removeItem(int position) {
        if (position > -1) {
            list.remove(position);
            notifyItemRemoved(position);
            notifyDataSetChanged();
        }
    }

    public <T> void removeItem(T item) {
        removeItem(list.indexOf(item));
    }

    public <T> void setList(List<T> list) {
        this.list.clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearAllItems() {
        this.list.clear();
        notifyDataSetChanged();
    }

    public <T> boolean isExistInList(T item) {
        if (list.contains(item)) {
            Log.d(TAG, "item exist");
            return true;
        } else {
            Log.d(TAG, "item not found");
            return false;
        }
    }
}
