package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.FollowsViewHolder;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

public class FollowsAdapter extends BaseAdapter {
    private static final int VIEW_TYPE_ITEM_FOLLOW = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;

    public FollowsAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM_FOLLOW: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_follow, parent, false);
                return new FollowsViewHolder(view);
            }
            case VIEW_TYPE_LOAD_MORE: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(view);
            }
            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(view);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (list == null || list.get(position) == null) {
            return;
        }
        if (mHolder instanceof FollowsViewHolder) {
            FollowsViewHolder holder = (FollowsViewHolder) mHolder;
            User user = (User) list.get(position);

            if (!TextUtils.isEmpty(user.getAvatar())) {
                holder.pbAvatarLoading.setVisibility(View.VISIBLE);
                holder.ivAvatar.setVisibility(View.VISIBLE);
                Glide.with(context).load(user.getAvatar())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                                return false;
                            }
                        })
                        .into(holder.ivAvatar);
            } else {
                holder.pbAvatarLoading.setVisibility(View.GONE);
                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
            }

            holder.itemView.setOnClickListener(view -> openUserDetail(user.getId()));
            holder.tvName.setText(user.getDisplayName());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM_FOLLOW : VIEW_TYPE_LOAD_MORE;
    }

    private void openUserDetail(String userId) {
        String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (!userId.equals(currentUserId)) {
            Utils.openUserDetailOrder(context, userId);
        }
    }
}
