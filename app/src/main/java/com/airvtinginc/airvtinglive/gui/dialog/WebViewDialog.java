package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewDialog extends DialogFragment {

    @BindView(R.id.wvMain)
    WebView mWebView;
    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.textDetails)
    TextView textDetails;
    @BindView(R.id.pb_loading)
    ProgressBar mPbLoading;

    private String mUrl;
    private String mTitle;

    public static WebViewDialog newInstance(String url, String title) {
        WebViewDialog dialog = new WebViewDialog();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_WEB_VIEW_URL, url);
        args.putString(Constants.EXTRA_TITLE, title);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
        if (getArguments() != null) {
            mUrl = getArguments().getString(Constants.EXTRA_WEB_VIEW_URL);
            mTitle = getArguments().getString(Constants.EXTRA_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_web_view, container, false);
        ButterKnife.bind(this, view);
       // mPbLoading.setVisibility(View.VISIBLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWebView();
    }

    private void initWebView() {
//        mWebView.setWebViewClient(new MyWebViewClient());
//        mWebView.clearCache(true);
//        mWebView.clearHistory();
//        mWebView.getSettings().setJavaScriptEnabled(true);
//        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        if (!TextUtils.isEmpty(mUrl)) {
            textDetails.setText(mUrl);
            textDetails.setMovementMethod(new ScrollingMovementMethod());
        }

        if (!TextUtils.isEmpty(mTitle)) {
            mTvTitle.setText(mTitle);
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mPbLoading.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.ivBack)
    void dismissDialog() {
        dismiss();
    }
}
