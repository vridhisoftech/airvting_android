package com.airvtinginc.airvtinglive.gui.enums;

public enum PeopleGroup {
    RISING_STAR("Rising Star"),
    TOP_USERS("Top Users"),
    NEW("New"),
    SUGGESTED_FOLLOWS("Suggested Follows");

    private String groupName;

    PeopleGroup(String groupName){
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }
}
