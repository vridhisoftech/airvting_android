package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.adapter.AddPhotoAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TextRecycleViewAdapter;
import com.airvtinginc.airvtinglive.gui.components.hashtag.EditTextHashtag;
import com.airvtinginc.airvtinglive.gui.dialog.AddDiscountAmount;
import com.airvtinginc.airvtinglive.gui.dialog.AddDiscountTimer;
import com.airvtinginc.airvtinglive.gui.dialog.AddPhotoDialog;
import com.airvtinginc.airvtinglive.gui.dialog.ListDialog;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.AddDiscountTimerDialogType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.models.PriceWhenStream;
import com.airvtinginc.airvtinglive.models.ProductFeaturedImage;
import com.airvtinginc.airvtinglive.models.response.AdminFeeResponse;
import com.airvtinginc.airvtinglive.models.response.CategoryItem;
import com.airvtinginc.airvtinglive.models.request.AddProductRequest;
import com.airvtinginc.airvtinglive.models.request.CategoryItemRequest;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.airvtinginc.airvtinglive.tools.RealPathUtil;
import com.airvtinginc.airvtinglive.tools.RequestPartUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DetailAddProductFragment extends BaseFragment implements TextWatcher {

    private static final String TAG = DetailAddProductFragment.class.getSimpleName();

    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.edTitle)
    EditText edtTitle;
    @BindView(R.id.edPrice)
    EditText edtPrice;
    @BindView(R.id.tvAdminFees)
    TextView tvAdminFees;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.tvDiscountTimer)
    TextView tvDiscountTimer;
    @BindView(R.id.tvSale)
    TextView tvSale;
    @BindView(R.id.et_description)
    EditTextHashtag edtDescription;
    @BindView(R.id.rlDeleteItem)
    RelativeLayout rlDeleteItem;

    private CategoryItemRequest categoryItemRequest;
    private AddPhotoAdapter addPhotoAdapter;
    private String startDay = "";
    private String endDay = "";
    private String editProductId;
    private float mCurrentProductPrice;
    private Timer mTimer = new Timer();
    private static final int DELAY = 1000;

    private enum Mode {
        ADD_NEW, EDIT
    }

    private Mode currentMode = Mode.ADD_NEW;

    public DetailAddProductFragment() {
        // Required empty public constructor
    }

    @OnTouch(R.id.llBackground)
    boolean touchHideKeyboard() {
        Utils.hideSoftKeyboard(getActivity());
        return true;
    }

    @OnClick(R.id.mrlSubmit)
    void submit() {
        addNewProduct();
    }

    @OnClick(R.id.rlDeleteItem)
    void deleteItem() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogTitle(getString(R.string.dialog_delete_item_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_delete_item_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                requestApi(editProductId, true, RequestTarget.DELETE_PRODUCT, DetailAddProductFragment.this);
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {
                messageDialog.dismiss();
            }
        });
        messageDialog.show(getActivity().getSupportFragmentManager(), "Delete Dialog");
    }

    @OnClick(R.id.rlCategory)
    void addCategoryDialog() {
        ListDialog listCategoryDialog = ListDialog.newInstance();
        listCategoryDialog.setShowDialogType(ShowDialogType.CATEGORY);
        listCategoryDialog.setPost(false);
        listCategoryDialog.setTitle(getActivity(), R.string.dialog_category_title);
        listCategoryDialog.setBackgroundColorRes(getActivity(), R.color.white);
        listCategoryDialog.setTitleColorRes(getActivity(), R.color.black);
        TextRecycleViewAdapter textRecycleViewAdapter = new TextRecycleViewAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                CategoryItem category = (CategoryItem) item;
                tvCategory.setText(category.getTitle());
                categoryItemRequest = new CategoryItemRequest();
                categoryItemRequest.setCategoryId(category.getId());
                categoryItemRequest.setTitle(category.getTitle());
                listCategoryDialog.dismiss();
            }
        });
        listCategoryDialog.setAdapter(textRecycleViewAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        listCategoryDialog.show(getFragmentManager(), "Show All ExplorePosts Dialog");
    }

    @OnClick(R.id.rlTimeLeft)
    void addTimeLeftDialog() {
        AddDiscountTimer addDiscountTimer = AddDiscountTimer.newInstance();
        addDiscountTimer.setAddDiscountTimerDialogType(AddDiscountTimerDialogType.PRODUCT);
        addDiscountTimer.setOnOkListener(new AddDiscountTimer.OnOkListener() {
            @Override
            public void onOkClick(int hour, int minutes) {
            }

            @Override
            public void onOkClick(String from, String to) {
                startDay = DateTimeUtils.parseDateToYyyyMmDd(from);
                endDay = DateTimeUtils.parseDateToYyyyMmDd(to);
                tvDiscountTimer.setText(DateTimeUtils.parseDateToDDMMYY(from) + " - " + DateTimeUtils.parseDateToDDMMYY(to));
                addDiscountTimer.dismiss();
            }
        });
        addDiscountTimer.show(getFragmentManager(), "Add Discount Timer Dialog");
    }

    @OnClick(R.id.rlSale)
    void addSaleDialog() {

        if (TextUtils.isEmpty(edtPrice.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                    getString(R.string.add_product_error_empty_price));
            return;
        } else {
            try {
                float price = NumberUtils.getPriceFromString(edtPrice.getText().toString());
                AddDiscountAmount addDiscountAmount = AddDiscountAmount.newInstance();
                addDiscountAmount.setPrice(price);
                addDiscountAmount.setPrice(NumberUtils.getPriceFromString(edtPrice.getText().toString()));
                addDiscountAmount.setOnOkListener((amount) -> {
                    tvSale.setText(amount);
                    addDiscountAmount.dismiss();
                });
                addDiscountAmount.show(getFragmentManager(), "Add Discount Amount Dialog");
                Log.d(TAG, "isValidFields: price - " + price);
            } catch (NumberFormatException e) {
                AppLog.e(e.getMessage());
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                        getString(R.string.add_product_error_price_is_number));
                return;
            }
        }
    }

    @OnClick(R.id.rlDescription)
    void focusDescription() {
        edtDescription.requestFocus();
        Utils.showSoftKeyboard(getActivity(), edtDescription);
    }

    public static DetailAddProductFragment newInstance(String productId) {
        DetailAddProductFragment fragment = new DetailAddProductFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_PRODUCT_ID, productId);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetailAddProductFragment newInstance() {
        return new DetailAddProductFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            editProductId = getArguments().getString(Constants.EXTRA_PRODUCT_ID);
            if (!TextUtils.isEmpty(editProductId)) {
                currentMode = Mode.EDIT;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_add_product, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        configToolbar();
        setupRecycleView();

        edtPrice.addTextChangedListener(this);
        if (TextUtils.isEmpty(editProductId)) {
            return;
        }

        requestApi(editProductId, true, RequestTarget.GET_PRODUCT_DETAIL, this);
    }

    private void configToolbar() {
        //Update header title
        if (currentMode == Mode.ADD_NEW) {
            DetailActivity.headerTitle = getString(R.string.product_add_product_title);
            rlDeleteItem.setVisibility(View.GONE);
        } else {
            DetailActivity.headerTitle = getString(R.string.product_edit_product_title);
            rlDeleteItem.setVisibility(View.VISIBLE);
        }
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);
    }

    private void setupRecycleView() {
        addPhotoAdapter = new AddPhotoAdapter(getActivity(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        }, new AddPhotoAdapter.OnActionClickListener() {
            @Override
            public void onAddClicked() {
                showAddPhotoDialog();
            }

            @Override
            public void onRemovePhoto(int position) {

            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(addPhotoAdapter);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        edtPrice.removeTextChangedListener(this);
        NumberUtils.formatPriceEditText(edtPrice.getText(), edtPrice, edtPrice.getSelectionStart(), true);
        edtPrice.addTextChangedListener(this);
        tvSale.setText("");
    }

    @Override
    public void afterTextChanged(Editable editable) {
        mTimer.cancel();
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                // Check to Get & show admin fee
                if (edtPrice == null) {
                    return;
                }
                String price = edtPrice.getText().toString();
                String currentProductPrice = price.replace("S", "")
                        .replace("$", "")
                        .replace(",", "");
                if (!TextUtils.isEmpty(currentProductPrice)) {
                    mCurrentProductPrice = Float.valueOf(currentProductPrice);
                    getAdminFee();
                }
            }
        }, DELAY);
    }

    private void getAdminFee() {
        requestApi(null, true, RequestTarget.GET_PRODUCT_ADMIN_FEE, this);
    }

    private void showAdminFee(int adminFeePercent) {
        if (adminFeePercent > 0) {
            float adminFee = mCurrentProductPrice * adminFeePercent / 100;
            String displayAdminFee;
            if (adminFee < ServiceConstants.MINIMUM_ADMIN_FEE) {
                displayAdminFee = getString(R.string.product_admin_fee_default);
            } else {
                displayAdminFee = String.format(getString(R.string.product_admin_fees), NumberUtils.formatPrice(adminFee));
            }

            tvAdminFees.setText(displayAdminFee);
        }
    }

    private void showAddPhotoDialog() {
        AddPhotoDialog addPhotoDialog = AddPhotoDialog.newInstance();
        addPhotoDialog.setReturnImageListener((data) -> {
            addPhotoAdapter.addItem(data);
            addPhotoDialog.dismiss();
        });
        addPhotoDialog.show(getActivity().getSupportFragmentManager(), "Add Photo Dialog");
    }

    private boolean isValidFields() {
        boolean isEmptyAddedImages = addPhotoAdapter.getUriItems().isEmpty();
        boolean isEmptyEditImages = addPhotoAdapter.getURLItems().isEmpty();
        if (isEmptyAddedImages && isEmptyEditImages) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                    getString(R.string.add_product_error_empty_image));
            return false;
        }

        if (TextUtils.isEmpty(edtTitle.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                    getString(R.string.add_product_error_empty_title));
            return false;
        }

        if (TextUtils.isEmpty(edtPrice.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                    getString(R.string.add_product_error_empty_price));
            return false;
        } else {
            try {
                float price = NumberUtils.getPriceFromString(edtPrice.getText().toString());
                Log.d(TAG, "isValidFields: price - " + price);
            } catch (NumberFormatException e) {
                AppLog.e(e.getMessage());
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                        getString(R.string.add_product_error_price_is_number));
                return false;
            }
        }

        if (currentMode == Mode.ADD_NEW && categoryItemRequest == null
                || (currentMode == Mode.EDIT && categoryItemRequest == null)) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                    getString(R.string.add_product_error_empty_category));
            return false;
        }

        boolean isNotInputTimeDiscount = TextUtils.isEmpty(tvDiscountTimer.getText())
                && !TextUtils.isEmpty(tvSale.getText());
        boolean isNotInputDiscountAmount = !TextUtils.isEmpty(tvDiscountTimer.getText())
                && TextUtils.isEmpty(tvSale.getText());
        boolean isInputDiscountAmount = !TextUtils.isEmpty(tvDiscountTimer.getText())
                && !TextUtils.isEmpty(tvSale.getText());

        if (isNotInputTimeDiscount) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                    getString(R.string.add_product_error_empty_timer));
            return false;
        }

        if (isNotInputDiscountAmount) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                    getString(R.string.add_product_error_empty_discount));
            return false;
        }

        if (isInputDiscountAmount) {
            String discountString = tvSale.getText().toString().replace("%", "");
            float discount = NumberUtils.getPriceFromString(discountString);
            float price = NumberUtils.getPriceFromString(edtPrice.getText().toString());
            if (!tvSale.getText().toString().contains("%") && discount > price) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
                        getString(R.string.add_product_error_discount_greater_than_price));
                return false;
            }
        }

//        if (TextUtils.isEmpty(edtDescription.getText())) {
//            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_product).toUpperCase(),
//                    getString(R.string.add_product_error_empty_description));
//            return false;
//        }

        return true;
    }

    private void addNewProduct() {
        if (!isValidFields()) {
            return;
        }

        AddProductRequest modelRequest = new AddProductRequest();
        Map<String, RequestBody> productMap = mapProductToRequest();
//        modelRequest.setFeaturedImages(getFeatureImagesPart());

        List<Uri> uriList = addPhotoAdapter.getUriItems();
        for (int i = 0; i < uriList.size(); i++) {
            String imageRealPath = RealPathUtil.getRealPath(getContext(), uriList.get(i));
            File imageFile0 = new File(imageRealPath);
            RequestBody requestImage0 = RequestBody.create(MediaType.parse("image/*"), imageFile0);

            if (i == 0) {
                MultipartBody.Part filePart0 = MultipartBody.Part.createFormData("featuredImages" + 0 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages0(filePart0);
            }
            if (i == 1) {
                MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("featuredImages" + 1 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages1(filePart1);
            }
            if (i == 2) {
                MultipartBody.Part filePart2 = MultipartBody.Part.createFormData("featuredImages" + 2 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages2(filePart2);
            }
            if (i == 3) {
                MultipartBody.Part filePart3 = MultipartBody.Part.createFormData("featuredImages" + 3 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages3(filePart3);
            }
            if (i == 4) {
                MultipartBody.Part filePart4 = MultipartBody.Part.createFormData("featuredImages" + 4 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages4(filePart4);
            }
            if (i == 5) {
                MultipartBody.Part filePart5 = MultipartBody.Part.createFormData("featuredImages" + 5 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages5(filePart5);
            }
            if (i == 6) {
                MultipartBody.Part filePart6 = MultipartBody.Part.createFormData("featuredImages" + 6 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages6(filePart6);
            }
            if (i == 7) {
                MultipartBody.Part filePart7 = MultipartBody.Part.createFormData("featuredImages" + 7 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages7(filePart7);
            }
            if (i == 8) {
                MultipartBody.Part filePart8 = MultipartBody.Part.createFormData("featuredImages" + 8 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages8(filePart8);
            }
            if (i == 9) {
                MultipartBody.Part filePart9 = MultipartBody.Part.createFormData("featuredImages" + 9 + "", imageFile0.getName(), requestImage0);
                modelRequest.setFeaturedImages9(filePart9);
            }
//            featureImagesPart.add(imagePartFile);
        }
        modelRequest.setParams(productMap);

        if (currentMode == Mode.ADD_NEW) {
            requestApi(modelRequest, true, RequestTarget.ADD_NEW_PRODUCT, this);
        } else {
            modelRequest.setProductId(editProductId);
            requestApi(modelRequest, true, RequestTarget.EDIT_PRODUCT, this);
        }
    }

    private Map<String, RequestBody> mapProductToRequest() {
        Map<String, RequestBody> bodyMap = new HashMap<>();
        bodyMap.put(ServiceConstants.TITLE, RequestPartUtils.createFromObject(edtTitle.getText().toString()));
        bodyMap.put(ServiceConstants.PRICE, RequestPartUtils.createFromObject(NumberUtils.getPriceFromString(edtPrice.getText().toString())));
        bodyMap.put(ServiceConstants.DESCRIPTION, RequestPartUtils.createFromObject(edtDescription.getText().toString()));
        bodyMap.put(ServiceConstants.CONDITION, RequestPartUtils.createFromObject(ServiceConstants.NEW));
        bodyMap.put(ServiceConstants.STARTED_AT, RequestPartUtils.createFromObject(startDay));
        bodyMap.put(ServiceConstants.EXPIRE_AT, RequestPartUtils.createFromObject(endDay));

        String discount = tvSale.getText().toString();
        if (!TextUtils.isEmpty(discount) && !discount.contains("%")) {
            discount = discount.replace(",", "");
        }

        bodyMap.put(ServiceConstants.DISCOUNT, RequestPartUtils.createFromObject(discount));

        List<String> urlList = addPhotoAdapter.getURLItems();
        if (currentMode == Mode.EDIT && !urlList.isEmpty()) {
            List<ProductFeaturedImage> productFeaturedImages = new ArrayList<>();
            for (String url : urlList) {
                productFeaturedImages.add(new ProductFeaturedImage(url));
            }

            RequestBody bodyFeatureImage = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(productFeaturedImages));
            bodyMap.put(ServiceConstants.FEATURE_IMAGES_OLD, bodyFeatureImage);
        }

        List<CategoryItemRequest> categoryItemRequestList = new ArrayList<>();
        categoryItemRequestList.add(categoryItemRequest);
        RequestBody bodyCategory = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(categoryItemRequestList));
        bodyMap.put(ServiceConstants.PRODUCT_CATEGORIES, bodyCategory);

        return bodyMap;
    }

    private List<MultipartBody.Part> getFeatureImagesPart() {
        List<Uri> uriList = addPhotoAdapter.getUriItems();
        List<MultipartBody.Part> featureImagesPart = new ArrayList<>();
        for (Uri uri : uriList) {
            String imageRealPath = RealPathUtil.getRealPath(getContext(), uri);
            File imageFile = new File(imageRealPath);
            RequestBody requestImage = RequestBody.create(MediaType.parse("image/*"), imageFile);
            MultipartBody.Part imagePartFile = MultipartBody.Part.createFormData(ServiceConstants.FEATURE_IMAGES, imageFile.getName(), requestImage);
            featureImagesPart.add(imagePartFile);
        }

        return featureImagesPart;
    }

    private void showProductDetail(Product product) {
        edtTitle.setText(product.getTitle());
        edtPrice.setText(NumberUtils.formatPrice(product.getPrice()));

        String displaySale = "";
        PriceWhenStream priceWhenStream = product.getPriceWhenStream();
        if (priceWhenStream != null && priceWhenStream.isActive()) {
            if (!TextUtils.isEmpty(priceWhenStream.getDiscount())) {
                displaySale = priceWhenStream.getDiscount();
            }
        } else if (!TextUtils.isEmpty(product.getDiscount())) {
            displaySale = product.getDiscount();
        }

        tvSale.setText(displaySale);

        if (product.getProductCategories() != null & !product.getProductCategories().isEmpty()) {
            tvCategory.setText(product.getProductCategories().get(0).getTitle());
            categoryItemRequest = new CategoryItemRequest();
            categoryItemRequest.setCategoryId(product.getProductCategories().get(0).getCategoryId());
            categoryItemRequest.setTitle(product.getProductCategories().get(0).getTitle());
        }

        edtDescription.setText(product.getDescription());

        try {
            startDay = DateTimeUtils.convertStringDateToOtherFormat(product.getStartedAt(), DateTimeUtils.DATE_TIME_FORMAT, DateTimeUtils.DATE_YYYY_MM_DD_FORMAT);
            endDay = DateTimeUtils.convertStringDateToOtherFormat(product.getExpiredAt(), DateTimeUtils.DATE_TIME_FORMAT, DateTimeUtils.DATE_YYYY_MM_DD_FORMAT);
            String displayTimer = DateTimeUtils.convertStringDateToOtherFormat(product.getStartedAt(), DateTimeUtils.DATE_TIME_FORMAT, DateTimeUtils.DATE_DD_MM_YY_FORMAT)
                    + " - " + DateTimeUtils.convertStringDateToOtherFormat(product.getExpiredAt(), DateTimeUtils.DATE_TIME_FORMAT, DateTimeUtils.DATE_DD_MM_YY_FORMAT);
            tvDiscountTimer.setText(displayTimer);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (FeaturedImage featuredImage : product.getFeaturedImages()) {
            addPhotoAdapter.addItem(featuredImage.getFeaturedImage());
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case ADD_NEW_PRODUCT:
                    getActivity().finish();
                    break;
                case GET_PRODUCT_DETAIL:
                    showProductDetail(((ProductDetailResponse) response.getData()).getProduct());
                    break;
                case EDIT_PRODUCT:
                    getActivity().finish();
                    break;
                case GET_PRODUCT_ADMIN_FEE:
                    AdminFeeResponse adminFeeResponse = (AdminFeeResponse) response.getData();
                    if (isAdded() && adminFeeResponse != null) { //isAdded() return true if the fragment is currently added to its activity.
                        showAdminFee(adminFeeResponse.getAdminFee());
                    }
                    break;
                case DELETE_PRODUCT: {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().startActivity(intent);
                    getActivity().sendBroadcast(new Intent(MainActivity.BroadcastActionGoToStoreTab));
                    break;
                }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        switch (requestTarget) {
            case DELETE_PRODUCT:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_delete_title), failMessage);
                break;
        }
    }
}
