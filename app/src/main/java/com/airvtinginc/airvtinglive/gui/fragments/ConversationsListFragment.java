package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ConversationListAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.components.SwipeHelper;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.gui.enums.SearchType;
import com.airvtinginc.airvtinglive.models.request.SearchModelRequest;
import com.airvtinginc.airvtinglive.models.response.Conversation;
import com.airvtinginc.airvtinglive.models.response.ConversationsListResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.airvtinginc.airvtinglive.gui.activities.DetailActivity.BroadcastActionSearch;

public class ConversationsListFragment extends BaseFragment implements BaseAdapter.OnItemClickListener {
    private static final String TAG = ConversationsListFragment.class.getName();

    @BindView(R.id.fm_conversation_list_swipe_refresh_conversation)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fm_conversation_list_rv_conversation)
    EmptyRecyclerView rvConversations;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.fm_conversation_list_iv_add)
    ImageView ivAdd;
    @BindView(R.id.fm_conversation_list_et_search)
    EditText etSearch;
    @BindView(R.id.fm_conversation_list_btn_cancel)
    Button ibCancel;
    @BindView(R.id.fm_conversation_frame_search)
    FrameLayout frameLayoutSearch;

    private BroadcastReceiver mReceiver;
    private int paginate = 1, perPage = 10;
    private ConversationListAdapter adapter;
    private int pastVisibleItems;
    private int totalItemCount;
    private int visibleThreshold = 10;
    private boolean canLoadMore = false;
    private String searchKeyword = "";
    private boolean reloadScreen = true;
    private int positionDelete;

    public static ConversationsListFragment newInstance() {
        return new ConversationsListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversation_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        registerReceiver();
        setUpRecyclerView();
        setUpClickListeners();

        frameLayoutSearch.setVisibility(View.GONE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void setUpRecyclerView() {
        rvConversations.setEmptyView(emptyView);
        Utils.setUpRecycleView(getContext(), rvConversations);
        adapter = new ConversationListAdapter(getActivity(), this);
        rvConversations.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            reloadScreen = true;
            paginate = 1;
            getConversationList(false);
        });

        rvConversations.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findLastVisibleItemPosition();
                    if (totalItemCount >= perPage && totalItemCount <= (pastVisibleItems + visibleThreshold)) {
                        if (canLoadMore) {
                            paginate++;
                            reloadScreen = false;
                            getConversationList(true);
                            canLoadMore = false;
                        }
                    }
                }
            }
        });

        new SwipeHelper(getContext(), rvConversations) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new UnderlayButton(getContext(), "", R.drawable.ic_trash, getResources().getColor(R.color.md_red_600), position -> {
                    showConfirmRemove(position);
                }));
            }
        };
    }

    private void showConfirmRemove(int position) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogTitle(getString(R.string.dialog_delete_conversation_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_delete_conversation_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                positionDelete = position;
                requestApi(adapter.getConversationIdByPosition(position), true, RequestTarget.DELETE_CONVERSATION, ConversationsListFragment.this);
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {

            }
        });
        messageDialog.show(getActivity().getSupportFragmentManager(), "Show Remove Cart Item Dialog");
    }

    private void setUpClickListeners() {
        ibCancel.setOnClickListener(v -> {
            Utils.hideSoftKeyboard(etSearch);
            paginate = 1;
            searchKeyword = "";
            etSearch.setText("");
            ivAdd.setVisibility(View.VISIBLE);
            reloadScreen = true;
            getConversationList(false);
            showToolbar();
        });

        etSearch.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                paginate = 1;
                searchKeyword = etSearch.getText().toString();
                ivAdd.setVisibility(View.GONE);
                reloadScreen = true;
                getConversationList(true);
                return true;
            }
            return false;
        });

        ivAdd.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra(Constants.EXTRA_USER_ID, "");
            intent.putExtra(Constants.EXTRA_USERNAME, "");
            DetailType.CONVERSATIONS_ADD_NEW.attachTo(intent);
            startActivity(intent);
        });
    }

    private void configToolbar() {
        //Update header title
        Intent intent = new Intent();

        intent.setAction(DetailActivity.BroadcastActionHideAllToolbarIcons);
        getActivity().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getString(R.string.title_message);
        getActivity().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionChangeToolbarToWhiteColor);
        getActivity().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionShowIconSearch);
        getActivity().sendBroadcast(intent);
    }

    private void showToolbar() {
        Intent intentShowIconSearch = new Intent(DetailActivity.BroadcastActionShowToolbar);
        getActivity().sendBroadcast(intentShowIconSearch);
        frameLayoutSearch.setVisibility(View.GONE);
    }

    private void hideToolbar() {
        Intent intentShowIconSearch = new Intent(DetailActivity.BroadcastActionHideToolbar);
        getActivity().sendBroadcast(intentShowIconSearch);
    }

    private void getConversationList(boolean showLoading) {
        if (reloadScreen) {
            adapter.clearAllItems();
        }
        SearchModelRequest modelRequest = new SearchModelRequest();
        modelRequest.setType(SearchType.Posts.getType());
        modelRequest.setKeyword(searchKeyword);
        modelRequest.setPaginate(paginate);
        modelRequest.setPerPage(perPage);
        requestApi(modelRequest, showLoading, RequestTarget.SEARCH_CONVERSATION, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case SEARCH_CONVERSATION:
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    if (searchKeyword.equals("")) {
                        ivAdd.setVisibility(View.VISIBLE);
                    } else {
                        ivAdd.setVisibility(View.GONE);
                    }
                    ConversationsListResponse conversationsListResponse = (ConversationsListResponse) response.getData();
                    if (conversationsListResponse != null) {
                        List<Conversation> conversationList = conversationsListResponse.getConversationsList();
                        if (conversationList != null && !conversationList.isEmpty()) {
                            adapter.addAllItems(conversationsListResponse.getConversationsList());
                        }

                        int totalPage = conversationsListResponse.getTotalPages();
                        canLoadMore = paginate < totalPage;
                    }
                    break;

                case DELETE_CONVERSATION: {
                    adapter.removeItem(positionDelete);
                    break;
                }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        switch (requestTarget) {
            case DELETE_CONVERSATION:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_delete_title), failMessage);
                break;
        }
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        Conversation conversation = (Conversation) item;
        String conversationId = conversation.getId();
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra(Constants.EXTRA_CONVERSATION_ID, conversationId);
        DetailType.CONVERSATIONS_DETAIL.attachTo(intent);
        startActivity(intent);
    }

    private void registerReceiver() {
        // create new receiver
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (Objects.requireNonNull(intent.getAction())) {
                        case BroadcastActionSearch:
                            hideToolbar();
                            frameLayoutSearch.setVisibility(View.VISIBLE);
                            etSearch.setText("");
                            etSearch.requestFocus();
                            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            break;
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(BroadcastActionSearch);
            getActivity().registerReceiver(mReceiver, filter);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        configToolbar();
        if (searchKeyword.equals("")) {
            ivAdd.setVisibility(View.VISIBLE);
            reloadScreen = true;
            paginate = 1;
            getConversationList(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
                mReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
