package com.airvtinginc.airvtinglive.gui.components.hashtag;

public class MyCursor {
    private int startIndex;
    private int EndIndex;

    int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    int getEndIndex() {
        return EndIndex;
    }

    public void setEndIndex(int endIndex) {
        EndIndex = endIndex;
    }
}
