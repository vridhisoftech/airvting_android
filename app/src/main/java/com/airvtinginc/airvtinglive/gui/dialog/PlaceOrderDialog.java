package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlaceOrderDialog extends BaseFragmentDialog implements ConfirmPaymentDialog.OnConfirmPaymentListener {
    private static final String TAG = PlaceOrderDialog.class.getSimpleName();
    @BindView(R.id.tv_place_order_total_price)
    TextView mTvTotalPrice;
    @BindView(R.id.tv_place_order_your_token)
    TextView mTvYourToken;
    @BindView(R.id.tv_place_order_token_price)
    TextView mTvTokenPrice;
    @BindView(R.id.tv_place_order_total_amount)
    TextView mTvTotalAmount;
    @BindView(R.id.cb_place_order_use_token)
    CheckBox mCbUseToken;

    private List<Product> mProducts;
    private float mTotalPrice;
    private float mTotalAmount;
    private int exchange, airTokenQuantityUse = 0;
    private OnPlaceOrderListener mOnPlaceOrderListener;

    @OnClick(R.id.iv_place_order_close)
    void closeDialog() {
        dismiss();
    }

    @OnClick(R.id.mrl_place_order_payment)
    void payment() {
        dismiss();
        showConfirmPaymentDialog();
    }

    public static PlaceOrderDialog newInstance() {
        return new PlaceOrderDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BORDER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_place_order, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        showOrder();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void showOrder() {
        if (mProducts == null || mProducts.isEmpty()) {
            return;
        }
        mTotalPrice = getTotalPrice();
        mTvTotalPrice.setText(NumberUtils.formatPrice(mTotalPrice));

        int currentToken = SharedPreferencesManager.getInstance(getContext()).getInt(Constants.PREF_CURRENT_USER_TOKEN, 0);
        mTvYourToken.setText(String.valueOf(currentToken));

        float currentCashOfUser = currentToken / exchange;
        Log.e("PlaceOrder", "currentToken: " + currentToken + " currentCashOfUser: " + currentCashOfUser);
        mTvTokenPrice.setText(NumberUtils.formatPrice(currentCashOfUser));

        mTotalAmount = mTotalPrice;
        mTvTotalAmount.setText(NumberUtils.formatPrice(mTotalAmount));

        mCbUseToken.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                if (currentCashOfUser >= mTotalPrice) {
                    mTotalAmount = 0;
                    airTokenQuantityUse = (int) mTotalPrice * exchange;
                } else {
                    mTotalAmount = mTotalPrice - currentCashOfUser;
                    airTokenQuantityUse = currentToken;
                }
            } else {
                mTotalAmount = mTotalPrice;
                airTokenQuantityUse = 0;
            }
            mTvTotalAmount.setText(NumberUtils.formatPrice(mTotalAmount));
        });
    }

    private float getTotalPrice() {
        float totalPrice = 0;
        for (Product product : mProducts) {
            totalPrice += product.getPaymentPrice() * product.getQuantityToBuy();
        }

        return totalPrice;
    }

    public void setProducts(List<Product> products) {
        mProducts = products;
    }

    public void setExchange(int exchange) {
        this.exchange = exchange;
    }

    @Override
    public void onConfirmPaymentSuccessful() {
        if (mOnPlaceOrderListener != null) {
            mOnPlaceOrderListener.onPlaceOrderSuccessful();
        }
        dismiss();
    }

    public interface OnPlaceOrderListener {
        void onPlaceOrderSuccessful();
    }

    public void setOnPlaceOrderListener(OnPlaceOrderListener onPlaceOrderListener) {
        mOnPlaceOrderListener = onPlaceOrderListener;
    }
    private void showConfirmPaymentDialog() {
        ConfirmPaymentDialog confirmPaymentDialog = ConfirmPaymentDialog.newInstance();
        confirmPaymentDialog.setTotalAmount(mTotalAmount);
        confirmPaymentDialog.setTotalAirToken(airTokenQuantityUse);
        confirmPaymentDialog.setProducts(mProducts);
        confirmPaymentDialog.setOnConfirmPaymentListener(this);
        confirmPaymentDialog.show(getFragmentManager(), "Show Confirm Payment Dialog");
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
//                case PRE_CHECKOUT:
//                    showConfirmPaymentDialog();
//                    dismiss();
//                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.dialog_my_cart_place_order), response.getMessage());
            dismiss();
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.dialog_my_cart_place_order), failMessage);
        dismiss();
    }
}
