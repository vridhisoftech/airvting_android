package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.models.response.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchUserAdapter extends BaseAdapter {

    public SearchUserAdapter(Context context, List<User> userList, OnItemClickListener onItemClickListener) {
        super(context, userList, onItemClickListener);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_text, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        ViewHolder holder = (ViewHolder) mHolder;
        if (list.size() > 0) {
            User user = (User) list.get(position);
            holder.tvUserName.setText(user.getUsername());
            holder.llBound.setOnClickListener(v -> onItemClickListener.onItemClicked(user, position));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.row_text_ll_bound)
        LinearLayout llBound;
        @BindView(R.id.row_text_tv_text)
        TextView tvUserName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
