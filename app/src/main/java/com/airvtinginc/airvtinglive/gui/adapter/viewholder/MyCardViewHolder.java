package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyCardViewHolder extends BaseViewHolder {
    @BindView(R.id.item_cards_ll_bound)
    public LinearLayout llBound;
    @BindView(R.id.item_cards_iv_card_type)
    public ImageView ivCardType;
    @BindView(R.id.item_cards_iv_check)
    public ImageView ivCheck;
    @BindView(R.id.item_cards_tv_card_name)
    public TextView tvCardName;
    @BindView(R.id.item_cards_tv_card_number)
    public TextView tvCardNumber;

    public MyCardViewHolder(View paramView) {
        super(paramView);
        ButterKnife.bind(this, paramView);
    }
}
