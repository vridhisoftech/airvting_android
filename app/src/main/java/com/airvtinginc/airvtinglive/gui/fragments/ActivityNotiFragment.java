package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.NotificationsActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.NotificationActivityAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.NotificationsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityNotiFragment extends BaseFragment implements BaseAdapter.OnItemClickListener {
    private static final String TAG = ActivityNotiFragment.class.getSimpleName();

    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;

    private NotificationActivityAdapter mNotificationActivityAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private int mCurrentPage = 1;
    private String mMaxId;
    private boolean isMoreLoading;
    private int visibleItemCount, pastVisibleItems, totalItemCount;

    public ActivityNotiFragment() {
        // Required empty public constructor
    }

    public static ActivityNotiFragment newInstance() {
        return new ActivityNotiFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycleview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.white));
        mRecyclerView.setEmptyView(emptyView);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mNotificationActivityAdapter = new NotificationActivityAdapter(getContext(), this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mNotificationActivityAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = mLinearLayoutManager.getChildCount();
                                                      totalItemCount = mLinearLayoutManager.getItemCount();
                                                      pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mNotificationActivityAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getActivityNotification(++mCurrentPage, true);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            mMaxId = null;
            Intent intent = new Intent(NotificationsActivity.BroadcastActionRefreshUnred);
            getActivity().sendBroadcast(intent);

            getActivityNotification(mCurrentPage, false);
        });
    }

    @Override
    public <T> void onItemClicked(T item, int position) {

    }

    private void getActivityNotification(int page, boolean isShowLoading) {
        String userId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        GetListRequest requestModel = new GetListRequest();
        requestModel.setUserId(userId);
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestModel.setPaginate(page);
        requestModel.setGetNotificationInbox(false);

        requestApi(requestModel, isShowLoading, RequestTarget.GET_NOTIFICATIONS, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshActivityNotification();
    }

    public void refreshActivityNotification() {
        mSwipeRefreshLayout.setRefreshing(true);
        mCurrentPage = 1;
        getActivityNotification(mCurrentPage, false);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mNotificationActivityAdapter.getItemCount() > 0) {
            mNotificationActivityAdapter.removeItem(mNotificationActivityAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        hideLoadMore();
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_NOTIFICATIONS:
                    NotificationsResponse notificationActivities = (NotificationsResponse) response.getData();
                    if (notificationActivities != null && notificationActivities.getNotifications() != null) {
                        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                            mNotificationActivityAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mNotificationActivityAdapter.addAllItems(notificationActivities.getNotifications());
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}
