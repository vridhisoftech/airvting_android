package com.airvtinginc.airvtinglive.gui.enums;

public enum HomeFeedType {
    LIVE("stream"),
    VIDEO("video"),
    IMAGE("image");

    private String type;

    HomeFeedType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
