package com.airvtinginc.airvtinglive.gui.dialog;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.FireStoreManager;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.GiftDetail;
import com.airvtinginc.airvtinglive.models.request.SendGiftRequest;
import com.airvtinginc.airvtinglive.models.response.GiftInFireStore;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendGiftDialog extends BaseFragmentDialog {
    private static final String TAG = SendGiftDialog.class.getSimpleName();

    @BindView(R.id.pb_send_gift_image_loading)
    ProgressBar mPbGiftImageLoading;
    @BindView(R.id.tv_buy_gift_currently_gifts)
    TextView mTvCurrentGifts;
    @BindView(R.id.iv_send_gift_image)
    ImageView mIvGiftImage;
    @BindView(R.id.tv_send_gift_decrease)
    ImageView mIvDecrease;
    @BindView(R.id.tv_send_gift_increase)
    ImageView mIvIncrease;
    @BindView(R.id.tv_send_gift_title)
    TextView mTvGiftTitle;
    @BindView(R.id.tv_send_gift_quantity)
    TextView mTvQuantity;
    @BindView(R.id.tv_send_gift_total_token)
    TextView mTvTotalToken;

    private GiftDetail mGiftDetail;
    private OnSendGiftListener mOnSendGiftListener;
    private String mStreamerId;
    private String mPostId;
    private SendGiftRequest mSendGiftRequest;

    @OnClick(R.id.iv_send_gift_close)
    void closeDialog() {
        dismiss();
    }

    @OnClick(R.id.mrl_send_gift_send_gift)
    void sendGiftToUser() {
        sendGift(mGiftDetail, Integer.valueOf(mTvQuantity.getText().toString()));
    }

    public static SendGiftDialog newInstance(String streamerId, String postId) {
        SendGiftDialog buyGiftDialog = new SendGiftDialog();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, streamerId);
        args.putString(Constants.EXTRA_POST_ID, postId);
        buyGiftDialog.setArguments(args);
        return buyGiftDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BORDER);
        if (getArguments() != null) {
            mStreamerId = getArguments().getString(Constants.EXTRA_USER_ID);
            mPostId = getArguments().getString(Constants.EXTRA_POST_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_send_gift, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        showBuyInfo();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    public void setGiftDetail(GiftDetail giftDetail) {
        mGiftDetail = giftDetail;
    }

    private void showBuyInfo() {
        if (mGiftDetail == null) {
            return;
        }
        int currentGifts = mGiftDetail.getQuantity();
        String displayCurrentlyGift;
        if (currentGifts > 1) {
            displayCurrentlyGift = String.format(getContext().getString(R.string.dialog_currently_gifts), currentGifts);
        } else {
            displayCurrentlyGift = String.format(getContext().getString(R.string.dialog_currently_gift), currentGifts);
        }
        mTvCurrentGifts.setText(displayCurrentlyGift);
        mTvGiftTitle.setText(mGiftDetail.getTitle());
        mTvTotalToken.setText(String.valueOf(mGiftDetail.getAirToken()));

        if (!TextUtils.isEmpty(mGiftDetail.getFeaturedImage())) {
            mPbGiftImageLoading.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(mGiftDetail.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            mPbGiftImageLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            mPbGiftImageLoading.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(mIvGiftImage);
        }

        mIvDecrease.setOnClickListener(view -> {
            int quantity = Integer.valueOf(mTvQuantity.getText().toString());
            if (quantity > 1) {
                quantity--;
                int totalToken = mGiftDetail.getAirToken() * quantity;
                mTvTotalToken.setText(String.valueOf(totalToken));
                mTvQuantity.setText(String.valueOf(quantity));
            }
        });

        mIvIncrease.setOnClickListener(view -> {
            int quantity = Integer.valueOf(mTvQuantity.getText().toString());
            if (quantity < Constants.MAX_CART_QUANTITY) {
                quantity++;
                int totalToken = mGiftDetail.getAirToken() * quantity;
                mTvTotalToken.setText(String.valueOf(totalToken));
                mTvQuantity.setText(String.valueOf(quantity));
            }
        });
    }

    private void sendGift(GiftDetail giftDetail, int quantityToSend) {
        String receiverId = mStreamerId;
        if (TextUtils.isEmpty(receiverId)) {
            // If streamer ID is empty -> current user is streaming -> send gift to themselves
            receiverId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        }

        mSendGiftRequest = new SendGiftRequest();
        mSendGiftRequest.setReceiverId(receiverId);
        mSendGiftRequest.setGiftId(giftDetail.getGiftId());
        mSendGiftRequest.setPostId(mPostId);
        mSendGiftRequest.setTitle(giftDetail.getTitle());
        mSendGiftRequest.setFeaturedImage(giftDetail.getFeaturedImage());
        mSendGiftRequest.setAirToken(giftDetail.getAirToken());
        mSendGiftRequest.setQuantity(giftDetail.getQuantity());
        mSendGiftRequest.setQuantityToSend(quantityToSend);

        requestApi(mSendGiftRequest, true, RequestTarget.SEND_GIFT, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case SEND_GIFT:
                    if (mOnSendGiftListener != null) {
                        mOnSendGiftListener.refreshMyGifts();
                        if (mSendGiftRequest != null) {
                            FireStoreManager.getInstance().addGift(getActivity(), new GiftInFireStore(mSendGiftRequest.getReceiverId(),
                                    mSendGiftRequest.getPostId(), mSendGiftRequest.getFeaturedImage(), 1));
                        }
                        dismiss();
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        switch (requestTarget) {
            case SEND_GIFT:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_send_gift_title).toUpperCase(), failMessage);
                break;
        }
    }

    public interface OnSendGiftListener {
        void refreshMyGifts();
    }

    public void setOnSendGiftListener(OnSendGiftListener onSendGiftListener) {
        mOnSendGiftListener = onSendGiftListener;
    }
}
