package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationInboxViewHolder extends BaseViewHolder {
    public View mView;

    @BindView(R.id.cv_notification_inbox_background)
    public CardView cvBackground;
    @BindView(R.id.ivAvatar)
    public CircleImageView ivAvatar;
    @BindView(R.id.ivDefaultAvatar)
    public CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    public GradientTextView tvDefaultAvatar;
    @BindView(R.id.pbLoadAvatar)
    public ProgressBar pbAvatarLoading;
    @BindView(R.id.tv_notification_inbox_display_name)
    public TextView tvDisplayName;
    @BindView(R.id.tv_notification_inbox_content)
    public TextView tvContent;
    @BindView(R.id.tv_notification_inbox_create_at)
    public TextView tvCreateAt;

    public NotificationInboxViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}
