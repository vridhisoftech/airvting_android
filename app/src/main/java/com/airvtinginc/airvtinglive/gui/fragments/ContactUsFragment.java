package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.components.hashtag.EditTextHashtag;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.models.request.ContactUsRequest;
import com.airvtinginc.airvtinglive.models.response.ContactUsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.airvtinginc.airvtinglive.tools.Validator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUsFragment extends BaseFragment {
    @BindView(R.id.edt_contact_us_name)
    EditText mEdtName;
    @BindView(R.id.edt_contact_us_email)
    EditText mEdtEmail;
    @BindView(R.id.edt_contact_us_message)
    EditTextHashtag mEdtMessage;

    @OnClick(R.id.rl_contact_us_message)
    void focusMessage() {
        mEdtMessage.requestFocus();
        Utils.showSoftKeyboard(getActivity(), mEdtMessage);
    }

    @OnClick(R.id.mrl_contact_us_send_contact)
    void sendContact() {
        if (isValidFields()) {
            requestApi(getContactUsRequest(), true, RequestTarget.SEND_CONTACT, this);
        }
    }

    public static ContactUsFragment newInstance() {
        return new ContactUsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Intent intent = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getString(R.string.setting_contact_title);
        getContext().sendBroadcast(intent);
    }

    private boolean isValidFields() {
        if (TextUtils.isEmpty(mEdtName.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.setting_contact_us).toUpperCase(),
                    getString(R.string.dialog_contact_us_empty_name));
            return false;
        }

        if (TextUtils.isEmpty(mEdtEmail.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.setting_contact_us).toUpperCase(),
                    getString(R.string.dialog_contact_us_empty_email));
            return false;
        }

        if (!Validator.getInstance().isEmailValid(mEdtEmail.getText().toString())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.setting_contact_us).toUpperCase(),
                    getString(R.string.validator_error_invalid_email));
            return false;
        }

        if (TextUtils.isEmpty(mEdtMessage.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.setting_contact_us).toUpperCase(),
                    getString(R.string.dialog_contact_us_empty_message));
            return false;
        }

        return true;
    }

    private ContactUsRequest getContactUsRequest() {
        ContactUsRequest contactUsRequest = new ContactUsRequest();
        contactUsRequest.setName(mEdtName.getText().toString());
        contactUsRequest.setEmail(mEdtEmail.getText().toString());
        contactUsRequest.setMessage(mEdtMessage.getText().toString());

        return contactUsRequest;
    }

    private void showSuccessDialog() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.SENT_CONTACT_SUCCESS);
        messageDialog.setDialogTitle(getString(R.string.setting_contact_us).toUpperCase());
        messageDialog.setDialogMessage(getString(R.string.dialog_contact_us_send_contact_successfully));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }

            @Override
            public void onCloseActionButtonClick() {
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        });
        messageDialog.show(getFragmentManager(), "Show Confirm Logout Dialog");
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case SEND_CONTACT:
                    ContactUsResponse contactUsResponse = (ContactUsResponse) response.getData();
                    if (contactUsResponse != null) {
                        showSuccessDialog();
                    }
                    break;
            }
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                getString(R.string.setting_contact_us).toUpperCase(), failMessage);
    }
}
