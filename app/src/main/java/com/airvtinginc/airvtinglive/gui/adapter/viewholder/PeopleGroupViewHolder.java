package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleGroupViewHolder extends BaseViewHolder {
    public View mView;
    @BindView(R.id.tvGroupName)
    public TextView tvGroupName;
    @BindView(R.id.tvViewMore)
    public TextView tvViewMore;
    @BindView(R.id.rvPeopleGroup)
    public RecyclerView rvPeopleGroup;
    @BindView(R.id.emptyView)
    public LinearLayout emptyView;

    public PeopleGroupViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}
