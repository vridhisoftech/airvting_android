package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyCardStripeViewHolder extends BaseViewHolder {
    @BindView(R.id.tv_info)
    public TextView tvInfo;
    @BindView(R.id.tv_link)
    public TextView tvLink;

    public MyCardStripeViewHolder(View paramView) {
        super(paramView);
        ButterKnife.bind(this, paramView);
    }
}
