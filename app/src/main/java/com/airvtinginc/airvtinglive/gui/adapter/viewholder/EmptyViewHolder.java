package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;

import butterknife.ButterKnife;

public class EmptyViewHolder extends BaseViewHolder {

    public View mView;

    public EmptyViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}