package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.google.firebase.firestore.ListenerRegistration;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveStreamHorizontalViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivLive)
    RoundedImageView ivLive;
    public @BindView(R.id.ivLiveRec)
    ImageView ivLiveRec;
    public @BindView(R.id.ivPostVideo)
    ImageView ivPostVideo;
    public @BindView(R.id.tvDefaultAvatar)
    TextView tvDefaultAvatar;
    public @BindView(R.id.tvContent)
    TextView tvContent;
    public @BindView(R.id.tvViewerCount)
    TextView tvCurrentViewers;
    public @BindView(R.id.pbLoadingThumbnail)
    ProgressBar pbLoadingThumbnail;
    public @BindView(R.id.rlDeal)
    RelativeLayout rlDeal;
    public @BindView(R.id.ivProductCountDown)
    RoundedImageView ivProductCountDown;
    public @BindView(R.id.ivProductNonSale)
    RoundedImageView ivProductNonSale;
    public @BindView(R.id.pbProductImage)
    ProgressBar pbProductImage;
    public @BindView(R.id.tvCountDownTimer)
    TextView tvCountDownTimer;
    public @BindView(R.id.tvSaleQuantity)
    TextView tvSaleQuantity;
    public @BindView(R.id.llCountDownAndQuantity)
    LinearLayout llCountDownAndQuantity;
    public @BindView(R.id.ivPriceTag)
    ImageView ivPriceTag;

    public CountDownTimer timer;
    public ListenerRegistration listenerViewers;

    public LiveStreamHorizontalViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}