package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.dialog.ForgotPasswordDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.LoginModelRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.airvtinginc.airvtinglive.tools.Validator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInFragment extends BaseFragment {
    private static final String TAG = SignInFragment.class.getName();

    @BindView(R.id.mrlSignIn)
    MaterialRippleLayout mrlSignIn;
    @BindView(R.id.mEdtEmail)
    EditText mEdtEmail;
    @BindView(R.id.mEdtPassword)
    EditText mEdtPassword;

    @OnClick(R.id.tvForgotPassword)
    void forgotPassword() {
        ForgotPasswordDialog forgotPasswordDialog = ForgotPasswordDialog.newInstance();
        forgotPasswordDialog.show(getFragmentManager(), "Show Forgot Password Dialog");
    }

    @OnClick(R.id.mrlSignIn)
    void signInClick() {
        signIn();
//        goToHome();
    }

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    private void signIn() {
        if (isValidFields()) {
            String email = mEdtEmail.getText().toString();
            String password = mEdtPassword.getText().toString();
            LoginModelRequest modelRequest = new LoginModelRequest();
            modelRequest.setEmail(email);
            modelRequest.setPassword(password);
            requestApi(modelRequest, true, RequestTarget.SIGN_IN, this);
        }
    }

    private boolean isValidFields() {
        if (TextUtils.isEmpty(mEdtEmail.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_sign_in),
                    getString(R.string.validator_error_email_required));
            return false;
        }

        if (!Validator.getInstance().isEmailValid(mEdtEmail.getText().toString())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_sign_in),
                    getString(R.string.validator_error_invalid_email));
            return false;
        }

        if (TextUtils.isEmpty(mEdtPassword.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_sign_in),
                    getString(R.string.validator_error_password_required));
            return false;
        }
        return true;
    }

    private void goToHome() {
        Intent in = new Intent(getContext(), MainActivity.class);
        startActivity(in);
        getActivity().finish();
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            if (requestTarget == RequestTarget.SIGN_IN) {
                UserResponse userResponse = (UserResponse) response.getData();
                Utils.saveCurrentUserInfoToPreferences(getContext(), userResponse.getTokenId(), userResponse.getCountPostsFollowing(), userResponse.getUser());
             Log.v("methodpayment",""+userResponse.getUser().isHasPaymentMethod());
                Utils.savePaymentMethodId(getContext(), userResponse.getPaymentMethodIdDefault());
                goToHome();
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_sign_in), failMessage);
    }
}
