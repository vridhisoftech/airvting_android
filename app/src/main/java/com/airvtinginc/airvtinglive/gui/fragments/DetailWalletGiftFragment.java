package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.GiftHistoryAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.WalletGiftAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.dialog.GiftListHistoryDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.GiftList;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.WalletRespone;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailWalletGiftFragment extends BaseFragment {
    private static final String TAG = DetailWalletGiftFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.tvTotalGift)
    TextView tvTotalGift;

    private LinearLayoutManager mLinearLayoutManager;
    private WalletGiftAdapter mAdapter;
    private int mCurrentPage = 1;
    private String mMaxId;
    private boolean isMoreLoading;
    private int visibleItemCount, pastVisibleItems, totalItemCount;

    public DetailWalletGiftFragment() {
        // Required empty public constructor
    }

    public static DetailWalletGiftFragment newInstance() {
        return new DetailWalletGiftFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_wallet_gift, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurrentPage = 1;
        mMaxId = null;
        getGifts(mCurrentPage);
    }

    private void setupRecyclerView() {
        mRecyclerView.setEmptyView(emptyView);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new WalletGiftAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                GiftList giftList = (GiftList) item;
                GiftListHistoryDialog giftListHistoryDialog = GiftListHistoryDialog.newInstance();
                giftListHistoryDialog.setGiftList(giftList);
                GiftHistoryAdapter giftHistoryAdapter = new GiftHistoryAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
                    @Override
                    public <T> void onItemClicked(T item, int position) {

                    }
                });
                giftListHistoryDialog.setAdapter(giftHistoryAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                giftListHistoryDialog.setOnDismisskListener(() -> {
                    giftListHistoryDialog.dismiss();
                });
                giftListHistoryDialog.show(getFragmentManager(), "Show Gift History Dialog");
            }
        });
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.white));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = mLinearLayoutManager.getChildCount();
                                                      totalItemCount = mLinearLayoutManager.getItemCount();
                                                      pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getGifts(++mCurrentPage);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            mMaxId = null;
            getGifts(mCurrentPage);
        });
    }

    private void getGifts(int page) {
        GetListRequest modelRequest = new GetListRequest(SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, ""), null,
                page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, mMaxId, null, null, -1);
        requestApi(modelRequest, false, RequestTarget.GET_GIFTS, this);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_GIFTS:
                    hideLoadMore();
                    WalletRespone result = ((WalletRespone) response.getData());
                    if (result != null && mAdapter != null) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mAdapter.addAllItems(result.getGiftDetail());
                        mMaxId = result.getMaxId();
                        tvTotalGift.setText("(" + result.getTotalGift() + ")");
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
