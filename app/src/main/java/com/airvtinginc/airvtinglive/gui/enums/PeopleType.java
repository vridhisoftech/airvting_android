package com.airvtinginc.airvtinglive.gui.enums;

import android.content.Intent;

public enum PeopleType {
    PEOPLE_GROUP,
    SUGGESTED_FOLLOW;

    private static final String name = PeopleType.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static PeopleType detachFrom(Intent intent) {
        if (!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }
}
