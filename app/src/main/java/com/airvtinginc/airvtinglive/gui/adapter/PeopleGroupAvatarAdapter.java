package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.PeopleGroupAvatarViewHolder;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class PeopleGroupAvatarAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;

    public PeopleGroupAvatarAdapter(Context context, List<User> users, OnItemClickListener onItemClickListener) {
        super(context, users, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder vh;
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_horizontal_layout, parent, false);
                vh = new EmptyViewHolder(view);
                break;
            }
            case VIEW_TYPE_ITEM: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_people_group, parent, false);
                vh = new PeopleGroupAvatarViewHolder(view);
                break;
            }
            default: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                vh = new EmptyViewHolder(view);
                break;
            }
        }
        int marginStartEnd = (int) parent.getContext().getResources().getDimension(R.dimen.people_margin);
        view.getLayoutParams().width = (Utils.getScreenWidth(parent.getContext()) - 2 * marginStartEnd) / Constants.NUMBER_OF_USERS_DISPLAY_IN_PEOPLE_GROUP;
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder) return;
        if (list == null || list.isEmpty() || list.get(position) == null) return;
        PeopleGroupAvatarViewHolder holder = (PeopleGroupAvatarViewHolder) mHolder;
        User user = (User) list.get(position);

        if (user.isLive()) {
            holder.ivIsLive.setVisibility(View.VISIBLE);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500); //You can manage the blinking time with this parameter
            anim.setStartOffset(0);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            holder.ivIsLive.startAnimation(anim);

            holder.ivAvatar.setBorderStartColor(context.getResources().getColor(R.color.avatar_border_live_violet));
            holder.ivAvatar.setBorderEndColor(context.getResources().getColor(R.color.avatar_border_live_blue));
        } else {
            holder.ivIsLive.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(user.getAvatar())) {
                holder.ivAvatar.setBorderStartColor(context.getResources().getColor(android.R.color.transparent));
                holder.ivAvatar.setBorderEndColor(context.getResources().getColor(android.R.color.transparent));
            } else {
                holder.ivAvatar.setBorderStartColor(context.getResources().getColor(R.color.md_grey_400));
                holder.ivAvatar.setBorderEndColor(context.getResources().getColor(R.color.md_grey_400));
            }
        }

        if (!TextUtils.isEmpty(user.getAvatar())) {
            holder.ivAvatar.setImageResource(android.R.color.transparent);
            holder.pbAvatarLoading.setVisibility(View.VISIBLE);
            holder.ivAvatar.setVisibility(View.VISIBLE);
            Glide.with(context).load(user.getAvatar())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbAvatarLoading.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.profile_default_avatar_text_size);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbAvatarLoading.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                            return false;
                        }
                    })
                    .into(holder.ivAvatar);
        } else {
            holder.pbAvatarLoading.setVisibility(View.GONE);
            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.profile_default_avatar_text_size);
        }

        holder.ivAvatar.setOnClickListener(view -> openUserDetail(user.getId()));

        holder.mView.setOnClickListener(v -> {
            onItemClickListener.onItemClicked(user, position);
        });

        if (!TextUtils.isEmpty(user.getDisplayName())) {
            holder.tvName.setText(user.getUsername());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }

    private void openUserDetail(String userId) {
        String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (!userId.equals(currentUserId)) {
            Utils.openUserDetailOrder(context, userId);
        }
    }
}
