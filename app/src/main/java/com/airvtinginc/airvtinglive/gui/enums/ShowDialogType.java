package com.airvtinginc.airvtinglive.gui.enums;

import android.content.Intent;

public enum ShowDialogType {
    CATEGORY,
    STORE,
    TAG,
    REPORT;

    private static final String name = ShowDialogType.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static ShowDialogType detachFrom(Intent intent) {
        if (!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }
}
