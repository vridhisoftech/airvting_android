package com.airvtinginc.airvtinglive.gui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TagAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TextRecycleViewAdapter;
import com.airvtinginc.airvtinglive.gui.components.CustomVideoView;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.components.PlayPauseListener;
import com.airvtinginc.airvtinglive.gui.components.hashtag.EditTextHashtag;
import com.airvtinginc.airvtinglive.gui.dialog.ListDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.request.AddMediaModelRequest;
import com.airvtinginc.airvtinglive.models.request.CategoryItemRequest;
import com.airvtinginc.airvtinglive.models.request.TagUserRequest;
import com.airvtinginc.airvtinglive.models.response.CategoryItem;
import com.airvtinginc.airvtinglive.models.response.MediaModelResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.TagChip;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.RealPathUtil;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.iceteck.silicompressorr.SiliCompressor;
import com.pchmn.materialchips.ChipsInput;
import com.pchmn.materialchips.model.ChipInterface;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DetailAddVideoFragment extends BaseFragment {

    private static final String TAG = DetailAddVideoFragment.class.getSimpleName();
    @BindView(R.id.detail_add_video_iv_play)
    ImageView ivPlay;
    @BindView(R.id.detail_add_video_video_view)
    CustomVideoView videoView;
    @BindView(R.id.tvShowAllCategory)
    TextView tvShowAllCategory;
    @BindView(R.id.chipsInput)
    ChipsInput mChipsInput;
    @BindView(R.id.et_description)
    EditTextHashtag etDescription;
    @BindView(R.id.add_video_mrl_submit)
    MaterialRippleLayout mrlSubmit;
    @BindView(R.id.add_video_switch_share_facebook)
    Switch switchShareFacebook;
    @BindView(R.id.add_video_rl_description)
    RelativeLayout rlDescription;

    private MediaController mediaController;
    private Context context;
    private Uri selectedFileUri = null;
    private String selectedFilePath = null;
    private CallbackManager callbackManager;
    private boolean shareFacebook = false;
    private CategoryItemRequest categoryItemRequest;
    private ProgressDialog mDialog;

    @OnTouch(R.id.llBackground)
    boolean touchHideKeyboard() {
        Utils.hideSoftKeyboard(getActivity());
        return true;
    }

    @OnClick(R.id.rlTag)
    void addTag() {
        ListDialog addTag = ListDialog.newInstance();
        addTag.setShowDialogType(ShowDialogType.TAG);
        addTag.setTitle(getActivity(), R.string.dialog_tag_title);
        addTag.setBackgroundColorRes(getActivity(), R.color.white);
        addTag.setTitleColorRes(getActivity(), R.color.black);
        TagAdapter tagAdapter = new TagAdapter(getActivity(), new ArrayList<>(), mChipsInput.getSelectedChipList(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                TagChip tagChip = (TagChip) item;
                if (tagChip.getLabel().length() > Constants.LIMIT_DISPLAY_NAME_LENGTH) {
                    String name = tagChip.getLabel()
                            .substring(0, Constants.LIMIT_DISPLAY_NAME_LENGTH - Constants.STRING_ELLIPS_END.length())
                            .concat(Constants.STRING_ELLIPS_END);
                    tagChip.setLabel(name);
                }
                mChipsInput.addChip(tagChip);
                addTag.getOnDismissListener().onDismiss();
            }
        });
        tagAdapter.setTextColorRes(getActivity(), R.color.black);
        tagAdapter.setBackgroundColorRes(getActivity(), R.color.white);
        addTag.setAdapter(tagAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        addTag.setOnDismissListener(() -> {
            addTag.dismiss();
        });
        addTag.show(getFragmentManager(), "Add Tag Dialog");
    }

    @OnClick(R.id.rlCategory)
    void showCategory() {
        ListDialog listCategoryDialog = ListDialog.newInstance();
        listCategoryDialog.setShowDialogType(ShowDialogType.CATEGORY);
        listCategoryDialog.setPost(true);
        listCategoryDialog.setTitle(context, R.string.dialog_category_title);
        listCategoryDialog.setBackgroundColorRes(getActivity(), R.color.white);
        listCategoryDialog.setTitleColorRes(getActivity(), R.color.black);
        TextRecycleViewAdapter textRecycleViewAdapter = new TextRecycleViewAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                CategoryItem category = (CategoryItem) item;
                categoryItemRequest = new CategoryItemRequest();
                categoryItemRequest.setCategoryId(category.getId());
                categoryItemRequest.setTitle(category.getTitle());

                tvShowAllCategory.setText(category.getTitle());
                listCategoryDialog.dismiss();
            }
        });
        listCategoryDialog.setAdapter(textRecycleViewAdapter, new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        listCategoryDialog.show(getFragmentManager(), "Show All ExplorePosts Dialog");
    }

    @OnClick(R.id.rlDescription)
    void focusDescription() {
        etDescription.requestFocus();
        Utils.showSoftKeyboard(getActivity(), etDescription);
    }

    public DetailAddVideoFragment() {
        // Required empty public constructor
    }

    public static DetailAddVideoFragment newInstance() {
        return new DetailAddVideoFragment();
    }

    public static DetailAddVideoFragment newInstance(Bundle bundle) {
        DetailAddVideoFragment detailAddVideoFragment = new DetailAddVideoFragment();
        detailAddVideoFragment.setArguments(bundle);
        return detailAddVideoFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_add_video, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        context = getActivity();
        if (mDialog == null) {
            mDialog = new ProgressDialog(context);
        }

        Intent intent = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getString(R.string.dialog_new_post);
        getContext().sendBroadcast(intent);
        float layoutWith = Utils.getScreenWidth(context);
        float ratio = Utils.getScreenRatio(context);

        if (getArguments() != null) {
            selectedFileUri = Uri.parse(getArguments().getString("selectedVideoUri"));
            String videoPath = getArguments().getString("videoFilePath");
            Log.e(TAG, "videoPath: " + videoPath);

            selectedFilePath = videoPath;

            ViewGroup.LayoutParams params = videoView.getLayoutParams();
            params.height = (int) (layoutWith / ratio);
            params.width = (int) layoutWith;
            videoView.setVideoURI(selectedFileUri);

            videoView.setLayoutParams(params);
            videoView.seekTo(100);
            if (mediaController == null) {
                mediaController = new MediaController(getActivity());
            }
            mediaController.setAnchorView(videoView);
            videoView.setMediaController(mediaController);

            ivPlay.setOnClickListener(v -> {
                videoView.setVisibility(View.VISIBLE);
                ivPlay.setVisibility(View.GONE);
                videoView.start();
            });

            videoView.setOnPreparedListener(mediaPlayer -> ivPlay.setVisibility(View.VISIBLE));

            videoView.setOnCompletionListener(mediaPlayer -> {
                ivPlay.setVisibility(View.VISIBLE);
                videoView.seekTo(100);
            });

            videoView.setPlayPauseListener(new PlayPauseListener() {
                @Override
                public void onPlay() {
                    ivPlay.setVisibility(View.GONE);
                }

                @Override
                public void onPause() {
                    ivPlay.setVisibility(View.VISIBLE);
                }
            });

            rlDescription.setOnClickListener(v -> {
                etDescription.requestFocus();
                Utils.showSoftKeyboard(getActivity(), etDescription);
            });
            switchShareFacebook.setOnCheckedChangeListener((compoundButton, isChecked) -> shareFacebook = isChecked);
        }

        setupChipsInput();
        mrlSubmit.setOnClickListener(v -> addNewPost());
    }

    private void setupChipsInput() {
        mChipsInput.setOnClickEditText(true);
        // chips listener
        mChipsInput.addChipsListener(new ChipsInput.ChipsListener() {
            @Override
            public void onChipAdded(ChipInterface chip, int newSize) {
            }

            @Override
            public void onChipRemoved(ChipInterface chip, int newSize) {
            }

            @Override
            public void onTextChanged(CharSequence text) {
            }

            @Override
            public void onEditTextClick() {
                addTag();
            }
        });
    }

    private void addNewPost() {
        if (checkValidateData()) {
            Utils.hideSoftKeyboard(getActivity());
            if (!TextUtils.isEmpty(selectedFilePath)) {
                videoView.stopPlayback();
                videoView.seekTo(0);
                videoView.setVideoURI(null);
                File fileDirSave = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES) + "/airvting/videos");
                if (fileDirSave.mkdirs() || fileDirSave.isDirectory()) {
                    new VideoCompressAsyncTask(getActivity()).execute(selectedFilePath, fileDirSave.getPath());
                }

            }

        }
    }

    private void saveFile(String videoPath, String outPath) {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(videoPath));
            bos = new BufferedOutputStream(new FileOutputStream(outPath, false));
            byte[] buf = new byte[1024];
            bis.read(buf);
            do {
                bos.write(buf);
            } while (bis.read(buf) != -1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void callApi(String compressedFilePath) {
        AddMediaModelRequest postModel = new AddMediaModelRequest();
        Map<String, RequestBody> bodyMap = new HashMap<>();
        Bitmap bitmap=ThumbnailUtils.createVideoThumbnail(compressedFilePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
////        String imageRealPath = RealPathUtil.getRealPath(getContext(), uriList.get(i));
//
//        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
//        byte [] b=baos.toByteArray();
//        String temp= Base64.encodeToString(b, Base64.DEFAULT);
//        File imageFile0 = new File(temp);
//            RequestBody requestImage0 = RequestBody.create(MediaType.parse("image/*"), imageFile0);
//            MultipartBody.Part mediaThumb = MultipartBody.Part.createFormData("featuredImage", imageFile0.getName(), requestImage0);
//
//            postModel.setMediaThumb(mediaThumb);
        if (compressedFilePath != null) {
            File selectedFile = new File(compressedFilePath);
            RequestBody requestImage = RequestBody.create(MediaType.parse("video/*"), selectedFile);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("mediaUrl", selectedFile.getName(), requestImage);

            postModel.setMedia(filePart);
  //          postModel.setThumbnail(filePart2);
        }

        RequestBody bodyType = RequestBody.create(MediaType.parse("text/plain"), "video");
        RequestBody bodyTitle = RequestBody.create(MediaType.parse("text/plain"), etDescription.getText().toString());

        bodyMap.put("type", bodyType);
        bodyMap.put("title", bodyTitle);

        if (mChipsInput.getSelectedChipList().size() > 0) {
            List<TagUserRequest> userRequestList = new ArrayList<>();
            for (ChipInterface selectedChip : mChipsInput.getSelectedChipList()) {
                TagUserRequest userRequest = new TagUserRequest();
                userRequest.setUserId(selectedChip.getId().toString());
                userRequest.setUsername(selectedChip.getLabel().toLowerCase());
                userRequestList.add(userRequest);
            }
            RequestBody bodyTagUser = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(userRequestList));
            bodyMap.put("tagUsers", bodyTagUser);
        }

        List<CategoryItemRequest> categoryItemRequestList = new ArrayList<>();
        categoryItemRequestList.add(categoryItemRequest);
        RequestBody bodyCategory = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(categoryItemRequestList));
        bodyMap.put("postCategories", bodyCategory);

        postModel.setParams(bodyMap);

        dialogUpload(getActivity().getString(R.string.dialog_uploading), getActivity().getString(R.string.dialog_please_wait));

        requestApi(postModel, false, RequestTarget.ADD_POST, this);
    }

    private void dialogUpload(String title, String message) {
        mDialog.setCancelable(false);
        mDialog.setTitle(title);
        mDialog.setMessage(message);
        mDialog.show();
    }

    private void dismissDialogUpload() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    private boolean checkValidateData() {
        if (etDescription.getText().toString().equals("")) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getResources().getString(R.string.dialog_add_video_title),
                    getResources().getString(R.string.err_description_null));
            return false;
        }
        if (categoryItemRequest == null) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getResources().getString(R.string.dialog_add_video_title),
                    getResources().getString(R.string.err_category_null));
            return false;
        }
        return true;
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case ADD_POST:
                    try {
                        mDialog.dismiss();
                        String videoLink = ((MediaModelResponse) response.getData()).getPostDetail().getMediaUrl();
                        String title = ((MediaModelResponse) response.getData()).getPostDetail().getTitle();
                        callbackManager = CallbackManager.Factory.create();
                        ShareDialog shareDialog = new ShareDialog(this);
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                gotoHomeScreen();
                            }

                            @Override
                            public void onCancel() {
                                gotoHomeScreen();
                            }

                            @Override
                            public void onError(FacebookException error) {

                            }
                        });

                        if (ShareDialog.canShow(ShareLinkContent.class)) {
                            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse(videoLink))
                                    .setQuote(title)
                                    .build();
                            if (shareFacebook) {
                                shareDialog.show(linkContent);
                            } else {
                                gotoHomeScreen();
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        mDialog.dismiss();
    }

    private void gotoHomeScreen() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getActivity().startActivity(intent);

        Intent intentBroadcastHideDialog = new Intent(MainActivity.BroadcastActionHideDialog);
        getActivity().sendBroadcast(intentBroadcastHideDialog);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private class VideoCompressAsyncTask extends AsyncTask<String, String, String> {
        private Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
            dialogUpload(mContext.getString(R.string.dialog_compressing_video), mContext.getString(R.string.dialog_please_wait));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... paths) {
            try {
                String filePath = SiliCompressor.with(mContext).compressVideo(paths[0], paths[1]);
                return filePath;
            } catch (Exception e) {
                dismissDialogUpload();
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            Log.i(TAG, "Path: " + compressedFilePath);
            dismissDialogUpload();
            if (!TextUtils.isEmpty(compressedFilePath)) {
                callApi(compressedFilePath);
            } else {
                videoView.setVideoURI(selectedFileUri);
                videoView.seekTo(100);
            }
        }
    }
}
