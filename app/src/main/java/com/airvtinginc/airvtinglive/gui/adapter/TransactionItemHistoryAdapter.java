package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.TransactionItemHistoryHolder;
import com.airvtinginc.airvtinglive.models.response.TransactionItemHistory;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class TransactionItemHistoryAdapter extends BaseAdapter {


    public TransactionItemHistoryAdapter(Context context, List<TransactionItemHistory> transactionItemHistories, OnItemClickListener onItemClickListener) {
        super(context, transactionItemHistories, onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_transaction_item_history, parent, false);
        return new TransactionItemHistoryHolder(view);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null) return;
        TransactionItemHistoryHolder holder = (TransactionItemHistoryHolder) mHolder;
        TransactionItemHistory transactionItemHistory = (TransactionItemHistory) list.get(position);
        holder.tvPrice.setText(NumberUtils.formatPrice((transactionItemHistory.getPrice())));
        holder.tvQuantity.setText(String.valueOf(transactionItemHistory.getQuantity()));
        holder.tvUserName.setText(transactionItemHistory.getDisplayName());
        holder.tvTitle.setText(transactionItemHistory.getTitle());

        if (!TextUtils.isEmpty(transactionItemHistory.getFeaturedImage())) {
            holder.pbProduct.setVisibility(View.VISIBLE);
            Glide.with(context).load(transactionItemHistory.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbProduct.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbProduct.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivProduct);
        } else {
        }
        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(transactionItemHistory, position);
        });
    }
}
