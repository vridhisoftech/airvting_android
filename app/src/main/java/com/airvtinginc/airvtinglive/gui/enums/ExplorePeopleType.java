package com.airvtinginc.airvtinglive.gui.enums;

public enum ExplorePeopleType {
    RISING_STAR("risingStar"),
    TOP_USERS("topUsers"),
    NEW("new"),
    SUGGESTED_FOLLOWS("suggestedFollow");

    private String type;

    ExplorePeopleType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
