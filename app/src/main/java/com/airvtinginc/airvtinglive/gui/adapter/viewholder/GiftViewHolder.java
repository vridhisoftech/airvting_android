package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GiftViewHolder extends BaseViewHolder {
    public View mView;
    public @BindView(R.id.rl_item_gift_background)
    RelativeLayout rlGiftBackground;
    public @BindView(R.id.iv_item_gift_icon_border)
    CircleImageView ivGiftIconWithBorder;
    public @BindView(R.id.iv_item_gift_icon)
    ImageView ivGiftIcon;
    public @BindView(R.id.pb_item_gift)
    ProgressBar pbItemGift;
    public @BindView(R.id.rl_item_gift_current_own_by_user)
    RelativeLayout rlGiftCurrentOwnByUser;
    public @BindView(R.id.tv_item_gift_current_own_by_user)
    TextView tvGiftCurrentOwnByUser;
    public @BindView(R.id.tv_item_gift_title)
    TextView tvGiftTitle;
    public @BindView(R.id.iv_item_gift_token)
    ImageView ivGiftToken;
    public @BindView(R.id.tv_item_gift_token)
    TextView tvGiftToken;

    public GiftViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}