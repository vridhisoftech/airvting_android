package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.LiveStreamAdapter;
import com.airvtinginc.airvtinglive.gui.enums.HomeFeedType;
import com.airvtinginc.airvtinglive.models.response.LiveStream;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowsFragment extends Fragment implements BaseAdapter.OnItemClickListener {

    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private LinearLayoutManager llm;

    private LiveStreamAdapter mAdapter;

    public ShowsFragment() {
        // Required empty public constructor
    }

    public static ShowsFragment newInstance() {
        ShowsFragment fragment = new ShowsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shows, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        // use a linear layout manager
        llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new LiveStreamAdapter(getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);

        setDummyLiveStreams();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = llm.getChildCount();
                                                      totalItemCount = llm.getItemCount();
                                                      pastVisibleItems = llm.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              setDummyLiveStreams();
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );

        mAdapter.setOnItemClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            setDummyLiveStreams();
        });
    }

    final String[] dummyNames = {"mongabong", "lifespamsg", "josey490", "stevelopez", "gomeslues"};
    final String[] dummyCaptions = {"Any amount will be greatly appreciated.", "There were white out conditions in town", "I was very proud of my nickname", "She wrote him a long letter", "We have never been to Africa"};

    private void setDummyLiveStreams() {
        new Handler().postDelayed(() -> {
            if (mSwipeRefreshLayout.isRefreshing()) {
                mAdapter.clearAllItems();
                mSwipeRefreshLayout.setRefreshing(false);
            }
            if (isMoreLoading && mAdapter.getItemCount() > 0) {
                mAdapter.removeItem(mAdapter.getItemCount() - 1);
                isMoreLoading = false;
            }
            for (int i = 1; i <= 7; i++) {
                mAdapter.addItem(new LiveStream("sample" + i, dummyNames[new Random().nextInt(5)], dummyCaptions[new Random().nextInt(5)], true, HomeFeedType.LIVE.getType()));
            }
        }, 1500);
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        //modify
    }
}
