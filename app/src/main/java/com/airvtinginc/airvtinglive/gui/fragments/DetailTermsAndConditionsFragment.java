package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.dialog.WebViewDialog;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class DetailTermsAndConditionsFragment extends BaseFragment {

    private static final String TAG = DetailTermsAndConditionsFragment.class.getSimpleName();

    @OnClick({R.id.rlPrivacyPolicy, R.id.icPrivacyPolicy, R.id.tvPrivacyPolicy})
    void clickPrivacyPolicy() {
        WebViewDialog dialog = WebViewDialog.newInstance(getString(R.string.privacy_policy),
                getString(R.string.setting_term_and_conditions_privacy_policy));
        dialog.show(getFragmentManager(), getString(R.string.setting_term_and_conditions_privacy_policy));
    }

    @OnClick({R.id.rlOurCompany, R.id.icOurCompany, R.id.tvOurCompany})
    void clickOurCompany() {
        WebViewDialog dialog = WebViewDialog.newInstance(getString(R.string.about_company),
                getString(R.string.setting_term_and_conditions_our_company));
        dialog.show(getFragmentManager(), getString(R.string.setting_term_and_conditions_our_company));
    }

    @OnClick({R.id.rlUserAgreement, R.id.icUserAgreement, R.id.tvUserAgreement})
    void clickUserAgreement() {
        WebViewDialog dialog = WebViewDialog.newInstance(getString(R.string.company_agreement),
                getString(R.string.setting_term_and_conditions_user_agreement));
        dialog.show(getFragmentManager(), getString(R.string.setting_term_and_conditions_user_agreement));
    }

    public DetailTermsAndConditionsFragment() {
        // Required empty public constructor
    }

    public static DetailTermsAndConditionsFragment newInstance() {
        DetailTermsAndConditionsFragment fragment = new DetailTermsAndConditionsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_terms_conditions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        configToolbar();
    }

    private void configToolbar() {
        //Update header title
        DetailActivity.headerTitle = getString(R.string.setting_term_and_conditions_title);
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);
    }
}
