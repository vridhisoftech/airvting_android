package com.airvtinginc.airvtinglive.gui.enums;

public enum TransactionItemType {
    PRODUCT("product"),
    GIFT("gift"),
    AIR_TOKEN("airToken");

    private String type;

    TransactionItemType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
