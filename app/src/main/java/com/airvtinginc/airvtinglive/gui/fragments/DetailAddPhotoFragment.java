package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TagAdapter;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.components.hashtag.EditTextHashtag;
import com.airvtinginc.airvtinglive.gui.dialog.ListDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.request.AddMediaModelRequest;
import com.airvtinginc.airvtinglive.models.request.TagUserRequest;
import com.airvtinginc.airvtinglive.models.response.MediaModelResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.TagChip;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.pchmn.materialchips.ChipsInput;
import com.pchmn.materialchips.model.ChipInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DetailAddPhotoFragment extends BaseFragment {

    private static final String TAG = DetailAddPhotoFragment.class.getSimpleName();

    @BindView(R.id.add_photo_iv_photo_display)
    ImageView ivPhoto;
    @BindView(R.id.chipsInput)
    ChipsInput mChipsInput;
    @BindView(R.id.et_description)
    EditTextHashtag etDescription;
    @BindView(R.id.add_photo_rl_submit)
    RelativeLayout rlSubmit;
    @BindView(R.id.add_photo_mrl_submit)
    MaterialRippleLayout mrlSubmit;
    @BindView(R.id.add_photo_switch_share_facebook)
    Switch switchShareFacebook;
    @BindView(R.id.add_photo_rl_description)
    RelativeLayout rlDescription;

    private BroadcastReceiver mReceiver;
    private String contentSavedUri;
    private File selectedFile = null;
    private CallbackManager callbackManager;
    private boolean shareFacebook = false;


    @OnTouch(R.id.llBackground)
    boolean touchHideKeyboard() {
        Utils.hideSoftKeyboard(getActivity());
        return true;
    }

    @OnClick(R.id.rlTag)
    void addTag() {
        ListDialog addTag = ListDialog.newInstance();
        addTag.setShowDialogType(ShowDialogType.TAG);
        addTag.setTitle(getActivity(), R.string.dialog_tag_title);
        addTag.setBackgroundColorRes(getActivity(), R.color.white);
        addTag.setTitleColorRes(getActivity(), R.color.black);
        TagAdapter tagAdapter = new TagAdapter(getActivity(), new ArrayList<>(), mChipsInput.getSelectedChipList(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                TagChip tagChip = (TagChip) item;
                if (tagChip.getLabel().length() > Constants.LIMIT_DISPLAY_NAME_LENGTH) {
                    String name = tagChip.getLabel()
                            .substring(0, Constants.LIMIT_DISPLAY_NAME_LENGTH - Constants.STRING_ELLIPS_END.length())
                            .concat(Constants.STRING_ELLIPS_END);
                    tagChip.setLabel(name);
                }
                mChipsInput.addChip(tagChip);
                addTag.getOnDismissListener().onDismiss();
            }
        });
        tagAdapter.setTextColorRes(getActivity(), R.color.black);
        tagAdapter.setBackgroundColorRes(getActivity(), R.color.white);
        addTag.setAdapter(tagAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        addTag.setOnDismissListener(() -> {
            addTag.dismiss();
        });
        addTag.show(getFragmentManager(), "Add Tag Dialog");
    }

    @OnClick(R.id.rlDescription)
    void focusDescription() {
        etDescription.requestFocus();
        Utils.showSoftKeyboard(getActivity(), etDescription);
    }

    public DetailAddPhotoFragment() {
        // Required empty public constructor
    }

    public static DetailAddPhotoFragment newInstance(String galleryId) {
        DetailAddPhotoFragment fragment = new DetailAddPhotoFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_IMAGE_URI, galleryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.contentSavedUri = getArguments().getString(Constants.EXTRA_IMAGE_URI);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_add_photo, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Intent intent = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getString(R.string.dialog_new_post);
        getContext().sendBroadcast(intent);

        float layoutWith = Utils.getScreenWidth(getActivity());
        float ratio = Utils.getScreenRatio(getActivity());
        RequestOptions requestOptions = new RequestOptions().fitCenter().override((int) layoutWith, (int) (layoutWith / ratio));
        Glide.with(getActivity()).load(Uri.parse(contentSavedUri)).apply(requestOptions).into(ivPhoto);
        selectedFile = new File(Uri.parse(contentSavedUri).getPath());
        setupChipsInput();
        rlDescription.setOnClickListener(v -> {
            etDescription.requestFocus();
            Utils.showSoftKeyboard(getActivity(), etDescription);
        });
        switchShareFacebook.setOnCheckedChangeListener((compoundButton, isChecked) -> shareFacebook = isChecked);
        mrlSubmit.setOnClickListener(v -> addNewPost());
    }

    private void setupChipsInput() {
        mChipsInput.setOnClickEditText(true);
        // chips listener
        mChipsInput.addChipsListener(new ChipsInput.ChipsListener() {
            @Override
            public void onChipAdded(ChipInterface chip, int newSize) {
            }

            @Override
            public void onChipRemoved(ChipInterface chip, int newSize) {
            }

            @Override
            public void onTextChanged(CharSequence text) {
            }

            @Override
            public void onEditTextClick() {
                addTag();
            }
        });
    }

    private void addNewPost() {
        if (checkValidateData()) {
            Utils.hideSoftKeyboard(getActivity());

            AddMediaModelRequest postModel = new AddMediaModelRequest();
            Map<String, RequestBody> bodyMap = new HashMap<>();

            if (selectedFile != null) {
                RequestBody requestImage = RequestBody.create(MediaType.parse("image/*"), selectedFile);
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("featuredImage", selectedFile.getName(), requestImage);
                postModel.setMedia(filePart);
            }

            RequestBody bodyType = RequestBody.create(MediaType.parse("application/json"), "image");
            RequestBody bodyTitle = RequestBody.create(MediaType.parse("application/json"), etDescription.getText().toString());

            bodyMap.put("type", bodyType);
            bodyMap.put("title", bodyTitle);

            if (mChipsInput.getSelectedChipList().size() > 0) {
                List<TagUserRequest> userRequestList = new ArrayList<>();
                for (ChipInterface selectedChip : mChipsInput.getSelectedChipList()) {
                    TagUserRequest userRequest = new TagUserRequest();
                    userRequest.setUserId(selectedChip.getId().toString());
                    userRequest.setUsername(selectedChip.getLabel().toLowerCase());
                    userRequestList.add(userRequest);
                }
                RequestBody bodyTagUser = RequestBody.create(MediaType.parse("application/json"), new Gson().toJson(userRequestList));
                bodyMap.put("tagUsers", bodyTagUser);
            }

            postModel.setParams(bodyMap);
            requestApi(postModel, true, RequestTarget.ADD_POST, this);
        }
    }

    private boolean checkValidateData() {
        if (etDescription.getText().toString().equals("")) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getResources().getString(R.string.dialog_add_photo_title),
                    getResources().getString(R.string.err_description_null));
            return false;
        }
        return true;
    }

    private void deleteImage() {
        if (selectedFile != null && selectedFile.exists()) {
            selectedFile.delete();
            scanSdCard();
        }
    }

    private void scanSdCard() {
        String folderPath = Utils.getApplicationFolder().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.parse(contentSavedUri));
            getActivity().sendBroadcast(mediaScanIntent);
        } else {
            getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + folderPath)));
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case ADD_POST:
                    deleteImage();
                    String imageLink = ((MediaModelResponse) response.getData()).getPostDetail().getFeaturedImage();
                    String title = ((MediaModelResponse) response.getData()).getPostDetail().getTitle();
                    callbackManager = CallbackManager.Factory.create();
                    ShareDialog shareDialog = new ShareDialog(this);
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            gotoHomeScreen();
                        }

                        @Override
                        public void onCancel() {
                            gotoHomeScreen();
                        }

                        @Override
                        public void onError(FacebookException error) {

                        }
                    });

                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(imageLink))
                                .setQuote(title)
                                .build();
                        if (shareFacebook) {
                            shareDialog.show(linkContent);
                        } else {
                            gotoHomeScreen();
                        }
                    }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
                mReceiver = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoHomeScreen() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getActivity().startActivity(intent);

        Intent intentBroadcastHideDialog = new Intent(MainActivity.BroadcastActionHideDialog);
        getActivity().sendBroadcast(intentBroadcastHideDialog);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
