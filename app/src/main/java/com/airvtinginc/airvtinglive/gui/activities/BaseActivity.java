package com.airvtinginc.airvtinglive.gui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIManager;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.interfaces.BaseInterface;
import com.airvtinginc.airvtinglive.components.services.MyAPISubscribe;
import com.airvtinginc.airvtinglive.components.services.RetrofitGenerator;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.AddCardModelRequest;
import com.airvtinginc.airvtinglive.models.request.AddConversationsRequest;
import com.airvtinginc.airvtinglive.models.request.AddMediaModelRequest;
import com.airvtinginc.airvtinglive.models.request.AddProductRequest;
import com.airvtinginc.airvtinglive.models.request.BookmarkPostRequest;
import com.airvtinginc.airvtinglive.models.request.BuyAirTokenRequestModel;
import com.airvtinginc.airvtinglive.models.request.BuyGiftRequest;
import com.airvtinginc.airvtinglive.models.request.CartProductsRequest;
import com.airvtinginc.airvtinglive.models.request.CategoryPostsRequest;
import com.airvtinginc.airvtinglive.models.request.CategoryProductRequest;
import com.airvtinginc.airvtinglive.models.request.CheckoutRequest;
import com.airvtinginc.airvtinglive.models.request.ConfigRequest;
import com.airvtinginc.airvtinglive.models.request.ContactUsRequest;
import com.airvtinginc.airvtinglive.models.request.FollowUserRequest;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.request.GetPostsFilterRequest;
import com.airvtinginc.airvtinglive.models.request.PostRequest;
import com.airvtinginc.airvtinglive.models.request.ProductListModelRequest;
import com.airvtinginc.airvtinglive.models.request.ReportRequest;
import com.airvtinginc.airvtinglive.models.request.SendFCMTokenRequest;
import com.airvtinginc.airvtinglive.models.request.SendGiftRequest;
import com.airvtinginc.airvtinglive.models.request.SignUpModelRequest;
import com.airvtinginc.airvtinglive.models.request.SocicalSignInModelRequest;
import com.airvtinginc.airvtinglive.models.request.SearchModelRequest;
import com.airvtinginc.airvtinglive.models.request.TypeRequest;
import com.airvtinginc.airvtinglive.models.request.LoginModelRequest;
import com.airvtinginc.airvtinglive.models.request.SearchUserModelRequest;
import com.airvtinginc.airvtinglive.models.request.UpdatePostRequest;
import com.airvtinginc.airvtinglive.models.request.UpdateUserProfileRequest;
import com.airvtinginc.airvtinglive.models.request.UploadImageItemRequest;
import com.airvtinginc.airvtinglive.models.response.AdminFeeResponse;
import com.airvtinginc.airvtinglive.models.response.AirTokenExchangeResponse;
import com.airvtinginc.airvtinglive.models.response.AirTokenListResponse;
import com.airvtinginc.airvtinglive.models.response.BuyAirTokenResponse;
import com.airvtinginc.airvtinglive.models.response.CategoryListModelResponse;
import com.airvtinginc.airvtinglive.models.response.CheckoutResponse;
import com.airvtinginc.airvtinglive.models.response.ConfigsResponse;
import com.airvtinginc.airvtinglive.models.response.ContactUsResponse;
import com.airvtinginc.airvtinglive.models.response.ConversationDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ConversationReplyResponse;
import com.airvtinginc.airvtinglive.models.response.ConversationsListResponse;
import com.airvtinginc.airvtinglive.models.response.DeleteCardResponse;
import com.airvtinginc.airvtinglive.models.response.DeleteResponse;
import com.airvtinginc.airvtinglive.models.response.GiftDetailListResponse;
import com.airvtinginc.airvtinglive.models.response.LeftMenuResponse;
import com.airvtinginc.airvtinglive.models.response.LikeProductResponse;
import com.airvtinginc.airvtinglive.models.response.MediaModelResponse;
import com.airvtinginc.airvtinglive.models.response.Notification;
import com.airvtinginc.airvtinglive.models.response.NotificationsResponse;
import com.airvtinginc.airvtinglive.models.response.PostDetailResponse;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ProductDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;

import com.airvtinginc.airvtinglive.models.response.SearchModelResponse;
import com.airvtinginc.airvtinglive.models.response.SearchUserModelResponse;
import com.airvtinginc.airvtinglive.models.response.UploadImageResponse;
import com.airvtinginc.airvtinglive.models.response.WalletRespone;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;
import com.airvtinginc.airvtinglive.models.response.PaymentDetailResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import com.airvtinginc.airvtinglive.models.response.UserResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.airvtinginc.airvtinglive.app.MainApplication.getContext;

public abstract class BaseActivity extends AppCompatActivity implements BaseInterface {
    private static final String TAG = BaseActivity.class.getSimpleName();
    private APIManager apiManager = null;
    private MyAPISubscribe myAPISubscribe;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeApiManager();
    }

    private void initializeApiManager() {
        apiManager = RetrofitGenerator.createService(APIManager.class);
    }

    @Override
    public <T> void requestApi(T model, boolean isLoading, RequestTarget requestTarget, APIResponseListener listener) {
        if (!Utils.isNetworkConnectionAvailable()) {
            DialogUtils.hideLoadingProgress();
            if (listener != null)
                listener.onResponseFail(getString(R.string.error_no_internet_connection), EnumManager.StatusCode.ERR_NO_INTERNET_CONNECTION, requestTarget);
            return;
        }

        if (requestTarget == null) {
            return;
        }

        if (isLoading) {
            DialogUtils.showLoadingProgress(this, false);
        }

        processRequestApi(model, requestTarget, listener);
    }

    private <T> void processRequestApi(T model, RequestTarget requestTarget, APIResponseListener listener) {
        String currentUserId = SharedPreferencesManager.getInstance(this).getString(Constants.PREF_CURRENT_USER_ID, "");
        switch (requestTarget) {
            case SIGN_IN:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.login(requestTarget.toString(), (LoginModelRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case SIGN_IN_SOCIAL:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.socialSignIn(requestTarget.toString(), (SocicalSignInModelRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case SIGN_UP:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.signUp(requestTarget.toString(), (SignUpModelRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case LOG_OUT:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UsersResponse>>(listener, requestTarget);
                apiManager.logout(requestTarget.toString(), getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case ADD_POST:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<MediaModelResponse>>(listener, requestTarget);
                AddMediaModelRequest addMediaModelRequest = ((AddMediaModelRequest) model);
                if (addMediaModelRequest.getMedia() != null) {
                    apiManager.postMedia(requestTarget.toString(),getAuthorization(),
                            addMediaModelRequest.getParams(),
                            addMediaModelRequest.getMedia())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(myAPISubscribe);
                }
                else {
                    apiManager.postLive(requestTarget.toString(), getAuthorization(), addMediaModelRequest.getParams())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(myAPISubscribe);
                }
                break;

            case SEARCH_USER:
                SearchUserModelRequest searchUserModelRequest = ((SearchUserModelRequest) model);
                String searchUserUrl = String.format(requestTarget.toString(),
                        searchUserModelRequest.getUsername(),
                        searchUserModelRequest.getPaginate(),
                        searchUserModelRequest.getPerPage(),
                        searchUserModelRequest.getMaxId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<SearchUserModelResponse>>(listener, requestTarget);
                apiManager.searchUser(searchUserUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case UPDATE_USER_PROFILE:
                UpdateUserProfileRequest updateUserProfileRequest = ((UpdateUserProfileRequest) model);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.updateUserProfile(requestTarget.toString(), getAuthorization(),
                        updateUserProfileRequest.getParams(),
                        updateUserProfileRequest.getAvatar(),
                        updateUserProfileRequest.getCover())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;

            case GET_CATEGORY:
                String getCategoryUrl = String.format(requestTarget.toString(), ((TypeRequest) model).getType());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<CategoryListModelResponse>>(listener, requestTarget);
                apiManager.getCategory(getCategoryUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_LIST_PRODUCT:
                ProductListModelRequest productListModelRequest = ((ProductListModelRequest) model);
                String getListStoreUrl = String.format(requestTarget.toString(),
                        productListModelRequest.getUserId(),
                        productListModelRequest.getPaginate(),
                        productListModelRequest.getPerPage(),
                        productListModelRequest.getMaxId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ProductsResponse>>(listener, requestTarget);
                apiManager.getListMyStore(getListStoreUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_CATEGORY_POSTS: {
                CategoryPostsRequest categoryPostsRequest = ((CategoryPostsRequest) model);
                String getCategoryPostUrl = String.format(requestTarget.toString(), categoryPostsRequest.getCategoryId(),
                        categoryPostsRequest.getTypePost()[0], categoryPostsRequest.getTypePost()[1],
                        categoryPostsRequest.getPaginate(), categoryPostsRequest.getPerPage(),
                        categoryPostsRequest.getMaxId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostResponse>>(listener, requestTarget);
                apiManager.getPost(getCategoryPostUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_CATEGORY_PRODUCT: {
                CategoryProductRequest categoryProductRequest = ((CategoryProductRequest) model);
                String getCategoryProductUrl = String.format(requestTarget.toString(), categoryProductRequest.getCategoryId(),
                        categoryProductRequest.getPaginate(), categoryProductRequest.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostResponse>>(listener, requestTarget);
                apiManager.getProduct(getCategoryProductUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case BOOKMARK_POST:
                String bookmarkPostUrl = String.format(requestTarget.toString(), ((BookmarkPostRequest) model).getPostId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.bookmarkPost(bookmarkPostUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case ADD_NEW_PRODUCT:
                AddProductRequest addProductRequest = ((AddProductRequest) model);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.addNewProduct(requestTarget.toString(), getAuthorization(),
                        addProductRequest.getParams(),
                        addProductRequest.getFeaturedImages0(),
                        addProductRequest.getFeaturedImages1(),
                        addProductRequest.getFeaturedImages2(),
                        addProductRequest.getFeaturedImages3(),
                        addProductRequest.getFeaturedImages4(),
                        addProductRequest.getFeaturedImages5(),
                        addProductRequest.getFeaturedImages6(),
                        addProductRequest.getFeaturedImages7(),
                        addProductRequest.getFeaturedImages8(),
                        addProductRequest.getFeaturedImages9())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case EDIT_PRODUCT:
                AddProductRequest editProductRequest = ((AddProductRequest) model);
                String editProductUrl = String.format(requestTarget.toString(), editProductRequest.getProductId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.editProduct(editProductUrl, getAuthorization(),
                        editProductRequest.getParams(),
                        editProductRequest.getFeaturedImages0(),
                        editProductRequest.getFeaturedImages1(),
                        editProductRequest.getFeaturedImages2(),
                        editProductRequest.getFeaturedImages3(),
                        editProductRequest.getFeaturedImages4(),
                        editProductRequest.getFeaturedImages5(),
                        editProductRequest.getFeaturedImages6(),
                        editProductRequest.getFeaturedImages7(),
                        editProductRequest.getFeaturedImages8(),
                        editProductRequest.getFeaturedImages9())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_EXPLORE_PEOPLE: {
                GetListRequest getListRequest = ((GetListRequest) model);
                String getListUrl = String.format(requestTarget.toString(), getListRequest.getPaginate(),
                        getListRequest.getPerPage(), getListRequest.getType());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UsersResponse>>(listener, requestTarget);
                apiManager.getExplorePeople(getListUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_POSTS_BY_FILTER:
                GetPostsFilterRequest getPostsFilterRequest = ((GetPostsFilterRequest) model);
                String getPostByFilter = String.format(requestTarget.toString(), getPostsFilterRequest.getFilter(),
                        getPostsFilterRequest.getPaginate(), getPostsFilterRequest.getPerPage(), getPostsFilterRequest.getMaxId(),
                        getPostsFilterRequest.getTypePost()[0], getPostsFilterRequest.getTypePost()[1],
                        getPostsFilterRequest.isShowAdver());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostResponse>>(listener, requestTarget);
                apiManager.getPost(getPostByFilter, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;

            case SEARCH:
                SearchModelRequest searchModelRequest = ((SearchModelRequest) model);
                String searchUrl = String.format(requestTarget.toString(),
                        searchModelRequest.getType(),
                        searchModelRequest.getKeyword(),
                        searchModelRequest.getPaginate(),
                        searchModelRequest.getPerPage(),
                        searchModelRequest.getMaxId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<SearchModelResponse>>(listener, requestTarget);
                apiManager.search(searchUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;

            case GET_POST_BY_USER_ID: {
                GetListRequest getListRequest = ((GetListRequest) model);
                String getUserGalleryUrl = String.format(requestTarget.toString(), getListRequest.getUserId(),
                        getListRequest.getPaginate(), getListRequest.getPerPage(),
                        (getListRequest.getTypePost().length > 0 ? getListRequest.getTypePost()[0] : ""),
                        (getListRequest.getTypePost().length > 1 ? getListRequest.getTypePost()[1] : ""),
                        (getListRequest.getTypePost().length > 2 ? getListRequest.getTypePost()[2] : ""),
                        getListRequest.getBookmarks());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostResponse>>(listener, requestTarget);
                apiManager.getPost(getUserGalleryUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_BOOKMARK_BY_USER_ID: {
                GetListRequest getListRequest = ((GetListRequest) model);
                String getUserGalleryUrl = String.format(requestTarget.toString(), getListRequest.getUserId(),
                        getListRequest.getPaginate(), getListRequest.getPerPage(),
                        (getListRequest.getTypePost().length > 0 ? getListRequest.getTypePost()[0] : ""),
                        (getListRequest.getTypePost().length > 1 ? getListRequest.getTypePost()[1] : ""),
                        (getListRequest.getTypePost().length > 2 ? getListRequest.getTypePost()[2] : ""),
                        getListRequest.getBookmarks());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostResponse>>(listener, requestTarget);
                apiManager.getPost(getUserGalleryUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_USER_PROFILE: {
                String userId = ((String) model);
                String getUserProfileUrl = String.format(requestTarget.toString(), userId);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.getUserProfile(getUserProfileUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_PRODUCT_DETAIL: {
                String productId = ((String) model);
                String getProductProfileUrl = String.format(requestTarget.toString(), productId);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ProductDetailResponse>>(listener, requestTarget);
                apiManager.getProductDetail(getProductProfileUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case LIKE_PRODUCT: {
                String productId = ((String) model);
                String likeProductUrl = String.format(requestTarget.toString(), productId);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<LikeProductResponse>>(listener, requestTarget);
                apiManager.likeProduct(likeProductUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }

            case SEARCH_CONVERSATION:
                SearchModelRequest searchRequest = (SearchModelRequest) model;
                StringBuilder searchConversationUrl = new StringBuilder();
                searchConversationUrl.append(String.format(requestTarget.toString(),
                        searchRequest.getPaginate(),
                        searchRequest.getPerPage(),
                        searchRequest.getMaxId()));
                if (!searchRequest.getKeyword().equals("")) {
                    searchConversationUrl.append("&keyword=" + searchRequest.getKeyword());
                }

                myAPISubscribe = new MyAPISubscribe<ResponseModel<ConversationsListResponse>>(listener, requestTarget);
                apiManager.searchConversations(searchConversationUrl.toString(), getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;

            case ADD_CONVERSATION:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ConversationDetailResponse>>(listener, requestTarget);
                apiManager.addConversations(requestTarget.toString(), getAuthorization(), (AddConversationsRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;

            case GET_CONVERSATIONS_DETAIL:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ConversationDetailResponse>>(listener, requestTarget);
                SearchModelRequest conversationDetailRequest = (SearchModelRequest) model;
                String conversationDetailUrl = String.format(requestTarget.toString(),
                        conversationDetailRequest.getConversationId(),
                        conversationDetailRequest.getPaginate(),
                        conversationDetailRequest.getPerPage(),
                        conversationDetailRequest.getMaxId());
                apiManager.getConversationDetail(conversationDetailUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);

                break;
            case REPLY_CONVERSATION:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ConversationReplyResponse>>(listener, requestTarget);
                String urlReplyConversation = String.format(requestTarget.toString(), ((AddConversationsRequest) model).getConversationId());
                String content = ((AddConversationsRequest) model).getContent();
                AddConversationsRequest conversationsRequest = new AddConversationsRequest();
                conversationsRequest.setContent(content);
                apiManager.replyConversations(urlReplyConversation, getAuthorization(), conversationsRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_POST_DETAIL:
                String getPostDetailUrl = String.format(requestTarget.toString(), ((PostRequest) model).getPostId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.getPostDetail(getPostDetailUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case UPDATE_POST:
                UpdatePostRequest updatePostRequest = (UpdatePostRequest) model;
                String updatePostUrl = String.format(requestTarget.toString(), updatePostRequest.getPostId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.updatePost(updatePostUrl, getAuthorization(),
                        updatePostRequest.getParams(), updatePostRequest.getFeatureImage())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_GIFT_STORE:
                GetListRequest getGiftStoreRequest = (GetListRequest) model;
                String getGiftStoreUrl = String.format(requestTarget.toString(),
                        getGiftStoreRequest.getPaginate(), getGiftStoreRequest.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<GiftDetailListResponse>>(listener, requestTarget);
                apiManager.getGiftStore(getGiftStoreUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_USER_GIFTS:
                GetListRequest getUserGiftsRequest = (GetListRequest) model;
                String getUserGiftsUrl = String.format(requestTarget.toString(), getUserGiftsRequest.getUserId(),
                        getUserGiftsRequest.getPaginate(), getUserGiftsRequest.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<GiftDetailListResponse>>(listener, requestTarget);
                apiManager.getUserGifts(getUserGiftsUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case BUY_GIFT:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<GiftDetailListResponse>>(listener, requestTarget);
                apiManager.buyGift(requestTarget.toString(), getAuthorization(), (BuyGiftRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case SEND_GIFT:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<GiftDetailListResponse>>(listener, requestTarget);
                apiManager.sendGift(requestTarget.toString(), getAuthorization(), (SendGiftRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case LIKE_POST:
                String postId = ((String) model);
                String likePostUrl = String.format(requestTarget.toString(), postId);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.likePost(likePostUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_LEFT_MENU_INFO:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<LeftMenuResponse>>(listener, requestTarget);
                apiManager.getLeftMenuInfo(requestTarget.toString(), getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case END_STREAM: {
                PostRequest postRequest = (PostRequest) model;
                String endStreamUrl = String.format(requestTarget.toString(), postRequest.getPostId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.endStream(endStreamUrl, getAuthorization(), postRequest.getSaveStreamRequest())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case FOLLOW_USER:
                String followUserUrl = String.format(requestTarget.toString(), ((FollowUserRequest) model).getUserIdToFollow());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.followUser(followUserUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_USER_LIST_CARDS:
                SearchUserModelRequest request = (SearchUserModelRequest) model;
                String urlUserListCards = String.format(requestTarget.toString(), request.getUserId(), request.getPaginate(), request.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PaymentDetailResponse>>(listener, requestTarget);
                apiManager.getListMyCards(urlUserListCards, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case ADD_CARDS:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PaymentDetailResponse>>(listener, requestTarget);
                apiManager.addCard(requestTarget.toString(), getAuthorization(), (AddCardModelRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case UPDATE_CARDS:
                String urlUpdateCard = String.format(requestTarget.toString(), ((AddCardModelRequest) model).getPaymentMethodId());
                ((AddCardModelRequest) model).setPaymentMethodId(null);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PaymentDetailResponse>>(listener, requestTarget);
                apiManager.updateCard(urlUpdateCard, getAuthorization(), (AddCardModelRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case DELETE_CARDS:
                String paymentMethodId = ((String) model);
                String urlDeleteCard = String.format(requestTarget.toString(), paymentMethodId);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<DeleteCardResponse>>(listener, requestTarget);
                apiManager.deleteCard(urlDeleteCard, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_TRANSACTIONS: {
                GetListRequest getListRequest = ((GetListRequest) model);
                String getListRequestUrl = String.format(requestTarget.toString(), getListRequest.getUserId(),
                        getListRequest.getPaginate(), getListRequest.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<WalletRespone>>(listener, requestTarget);
                apiManager.getWallets(getListRequestUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_AIR_TOKENS: {
                GetListRequest getAirTokenRequest = ((GetListRequest) model);
                String getAirTokensUrl = String.format(requestTarget.toString(), getAirTokenRequest.getPaginate(), getAirTokenRequest.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<AirTokenListResponse>>(listener, requestTarget);
                apiManager.getAirTokens(getAirTokensUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_GIFTS: {
                GetListRequest getListRequest = ((GetListRequest) model);
                String getListRequestUrl = String.format(requestTarget.toString(), getListRequest.getUserId(),
                        getListRequest.getPaginate(), getListRequest.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<WalletRespone>>(listener, requestTarget);
                apiManager.getWallets(getListRequestUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_GIFT_HISTORY: {
                GetListRequest getListRequest = ((GetListRequest) model);
                String getListRequestUrl = String.format(requestTarget.toString(), getListRequest.getUserId(), getListRequest.getGiftId(),
                        getListRequest.getPaginate(), getListRequest.getPerPage());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<WalletRespone>>(listener, requestTarget);
                apiManager.getWallets(getListRequestUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_COMMENTS: {
                PostRequest postRequest = ((PostRequest) model);
                String getListRequestUrl = String.format(requestTarget.toString(), postRequest.getPostId(),
                        postRequest.getPaginate(), postRequest.getPerPage(), postRequest.getMaxId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostResponse>>(listener, requestTarget);
                apiManager.getPost(getListRequestUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case ADD_COMMENT: {
                PostRequest postRequest = ((PostRequest) model);
                String commentUrl = String.format(requestTarget.toString(), postRequest.getPostId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.postComment(commentUrl, getAuthorization(), postRequest.getParams())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case BUY_AIR_TOKEN:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<BuyAirTokenResponse>>(listener, requestTarget);
                apiManager.buyAirToken(requestTarget.toString(), getAuthorization(), (BuyAirTokenRequestModel) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);

                break;
            case PRE_CHECKOUT:
                String urlPreCheckout = String.format(requestTarget.toString(), ((CheckoutRequest) model).getUserId());
                ((CheckoutRequest) model).setUserId(null);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<CheckoutResponse>>(listener, requestTarget);
                apiManager.cartCheckout(urlPreCheckout, getAuthorization(), (CheckoutRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case CHECKOUT:
                String urlCartCheckout = String.format(requestTarget.toString(), ((CheckoutRequest) model).getUserId());
                ((CheckoutRequest) model).setUserId(null);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<CheckoutResponse>>(listener, requestTarget);
                apiManager.cartCheckout(urlCartCheckout, getAuthorization(), (CheckoutRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case EXCHANGE_AIR_TOKEN:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<AirTokenExchangeResponse>>(listener, requestTarget);
                apiManager.exchangeAirToken(requestTarget.toString(), getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_NOTIFICATIONS:
                GetListRequest getNotificationsRequest = (GetListRequest) model;
                String getActivityNotificationUrl = String.format(requestTarget.toString(),
                        getNotificationsRequest.getUserId(), getNotificationsRequest.getPaginate(),
                        getNotificationsRequest.getPerPage(), getNotificationsRequest.isGetNotificationInbox());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<NotificationsResponse>>(listener, requestTarget);
                apiManager.getNotifications(getActivityNotificationUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case EXPIRE_POST_PRODUCT:
                PostRequest postRequest = ((PostRequest) model);
                String getRequestUrl = String.format(requestTarget.toString(), postRequest.getPostId(),
                        postRequest.getProductInPost().getId(),
                        postRequest.getProductInPost().getProductId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.getExpireProductInPost(getRequestUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case REPORT: {
                ReportRequest reportRequest = ((ReportRequest) model);
                String reportUrl = String.format(requestTarget.toString(), currentUserId);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UsersResponse>>(listener, requestTarget);
                apiManager.postReport(reportUrl, getAuthorization(), reportRequest.getParams())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case SEND_FCM_TOKEN:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.sendFCMToken(requestTarget.toString(), getAuthorization(), (SendFCMTokenRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case GET_CART_PRODUCTS:
                String getCartProductUrl = String.format(requestTarget.toString(), currentUserId);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ProductsResponse>>(listener, requestTarget);
                apiManager.getCartProducts(getCartProductUrl, getAuthorization(), (CartProductsRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case RESEND_VERIFY_EMAIL:
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.resendVerifyEmail(requestTarget.toString(), getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case FORGOT_PASSWORD: {
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.forgotPassword(requestTarget.toString(), (LoginModelRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case DEACTIVATE_ACCOUNT: {
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.deactivateAccount(requestTarget.toString(), getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case SEND_CONTACT: {
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ContactUsResponse>>(listener, requestTarget);
                apiManager.sendContact(requestTarget.toString(), getAuthorization(), (ContactUsRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case CONFIG_SETTINGS: {
                myAPISubscribe = new MyAPISubscribe<ResponseModel<ConfigsResponse>>(listener, requestTarget);
                apiManager.configSettings(requestTarget.toString(), getAuthorization(), (ConfigRequest) model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case READ_NOTIFICATION: {
                String readNotificationUrl = String.format(requestTarget.toString(), ((Notification) model).getId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<NotificationsResponse>>(listener, requestTarget);
                apiManager.readNotification(readNotificationUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_PRODUCT_ADMIN_FEE: {
                myAPISubscribe = new MyAPISubscribe<ResponseModel<AdminFeeResponse>>(listener, requestTarget);
                apiManager.getAdminFee(requestTarget.toString(), getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_FOLLOWERS: {
                GetListRequest getFollowersRequest = ((GetListRequest) model);
                String getFollowersUrl = String.format(requestTarget.toString(),
                        getFollowersRequest.getPaginate(), getFollowersRequest.getPerPage(), getFollowersRequest.getUserId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UsersResponse>>(listener, requestTarget);
                apiManager.getFollowers(getFollowersUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case GET_FOLLOWING: {
                GetListRequest getFollowersRequest = ((GetListRequest) model);
                String getFollowersUrl = String.format(requestTarget.toString(),
                        getFollowersRequest.getPaginate(), getFollowersRequest.getPerPage(), getFollowersRequest.getUserId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UsersResponse>>(listener, requestTarget);
                apiManager.getFollowing(getFollowersUrl, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }

            case UPLOAD_NEW_IMAGE: {
                UploadImageItemRequest uploadImageItemRequest = ((UploadImageItemRequest) model);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UploadImageResponse>>(listener, requestTarget);
                apiManager.uploadImage(requestTarget.toString(), getAuthorization(),
                        uploadImageItemRequest.getImage())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            case DELETE_POST:
            case DELETE_PRODUCT:
            case DELETE_CONVERSATION:
            case DELETE_COMMENT:
                deleteById(requestTarget, listener, ((String) model));
                break;

            case DELETE_NOTIFICATION: {
                GetListRequest requestList = (GetListRequest) model;
                String url = String.format(requestTarget.toString(), requestList.getUserId(), requestList.isGetNotificationInbox());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<DeleteResponse>>(listener, requestTarget);
                apiManager.delete(url, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            }
            default:
                DialogUtils.hideLoadingProgress();
                break;
        }
    }

    private void deleteById(RequestTarget requestTarget, APIResponseListener listener, String id) {
        String urlDelete = String.format(requestTarget.toString(), id);
        myAPISubscribe = new MyAPISubscribe<ResponseModel<DeleteResponse>>(listener, requestTarget);
        apiManager.delete(urlDelete, getAuthorization())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(myAPISubscribe);
    }

    private String getAuthorization() {
        String tokenId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_TOKEN_ID, "");
        System.out.println(ServiceConstants.HEADER_AUTHORIZATION_PREFIX + tokenId);
        return ServiceConstants.HEADER_AUTHORIZATION_PREFIX + tokenId;
    }

    public void cancelRequestApi() {
        if (myAPISubscribe != null) {
//            myAPISubscribe.(AndroidSchedulers.mainThread());
        }
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }
}
