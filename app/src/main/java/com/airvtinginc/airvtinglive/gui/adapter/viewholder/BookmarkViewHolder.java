package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookmarkViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivLive)
    ImageView ivLive;
    public @BindView(R.id.ivLiveRec)
    ImageView ivLiveRec;
    public @BindView(R.id.ivPriceTag)
    ImageView ivPriceTag;
    public @BindView(R.id.tvDate)
    TextView tvDate;
    public @BindView(R.id.tvViewerCount)
    TextView tvViewerCount;
    public @BindView(R.id.tvSchedule)
    TextView tvSchedule;
    public @BindView(R.id.tvCountDownTimer)
    TextView tvCountDownTimer;
    public @BindView(R.id.ivProductCountDown)
    ImageView ivProductCountDown;
    public @BindView(R.id.pbFeedImage)
    ProgressBar pbFeedImage;
    public @BindView(R.id.pbProductImage)
    ProgressBar pbProductImage;
    public @BindView(R.id.tvContent)
    TextView tvContent;
    public @BindView(R.id.rlDeal)
    RelativeLayout rlDeal;
    public @BindView(R.id.ivBookmark)
    ImageView ivBookmark;
    public @BindView(R.id.lnBookmark)
    LinearLayout lnBookmark;
    public CountDownTimer timer;

    public BookmarkViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}