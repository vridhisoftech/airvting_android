package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConversationsFromMeViewHolder extends BaseViewHolder {
    @BindView(R.id.item_conversations_detail_me_tv_date)
    public TextView tvDate;
    @BindView(R.id.item_conversations_detail_me_tv_content)
    public TextView tvContent;
    @BindView(R.id.item_conversations_detail_me_iv_content)
    public ImageView ivContent;
    @BindView(R.id.pb_image_content)
    public ProgressBar pbImageContent;
    @BindView(R.id.marginTopMe)
    public View marginTopMe;
    @BindView(R.id.marginTopFriend)
    public View marginTopFriend;

    public ConversationsFromMeViewHolder(View paramView) {
        super(paramView);
        ButterKnife.bind(this, paramView);
    }
}
