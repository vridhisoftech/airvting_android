package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConversationsViewHolder extends BaseViewHolder {
    @BindView(R.id.row_conversations_cv_bound)
    public CardView cvBound;
    @BindView(R.id.ivAvatar)
    public CircleImageView ivAvatar;
    @BindView(R.id.pbLoadAvatar)
    public ProgressBar pbAvatar;
    @BindView(R.id.row_conversations_rl_badge)
    public RelativeLayout rlBadge;
    @BindView(R.id.row_conversations_tv_badge_single)
    public TextView tvBadgeSingle;
    @BindView(R.id.row_conversations_tv_badge_multi)
    public TextView tvBadgeMulti;
    @BindView(R.id.row_conversations_tv_content)
    public TextView tvContent;
    @BindView(R.id.row_conversations_tv_create_at)
    public TextView tvCreateAt;
    @BindView(R.id.row_conversations_tv_display_name)
    public TextView tvDisplayName;
    @BindView(R.id.row_conversations_tv_title)
    public TextView tvTitle;
    @BindView(R.id.ivDefaultAvatar)
    public CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    public GradientTextView tvDefaultAvatar;

    public ConversationsViewHolder(View paramView) {
        super(paramView);
        ButterKnife.bind(this, paramView);
    }
}
