package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HorizontalProductsViewHolder extends BaseViewHolder {
    public View mView;
    public @BindView(R.id.ivThumbnail)
    ImageView ivThumbnail;
    public @BindView(R.id.tvProductName)
    TextView tvProductName;
    public @BindView(R.id.tvDate)
    TextView tvDate;
    public @BindView(R.id.ivPriceSale)
    ImageView ivPriceSale;
    public @BindView(R.id.tvPrice)
    TextView tvPrice;
    public @BindView(R.id.tvLikeCount)
    TextView tvLikeCount;
    public @BindView(R.id.ivLike)
    ImageView ivLike;
    public @BindView(R.id.pbLoadingThumbnail)
    ProgressBar pbLoadingThumbnail;
    public @BindView(R.id.item_product_ll_bound)
    LinearLayout llBound;

    public HorizontalProductsViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}