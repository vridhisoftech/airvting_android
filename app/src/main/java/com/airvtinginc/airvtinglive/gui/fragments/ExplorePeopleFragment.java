package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.PeopleAdapter;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.ExplorePeopleType;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.PeopleGroup;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.SuggestedFollow;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExplorePeopleFragment extends BaseFragment {
    private static final String TAG = ExplorePeopleFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private LinearLayoutManager mLinearLayoutManager;
    private PeopleAdapter mAdapter;
    private boolean isMoreLoading;
    private int currentSuggestedPage = 1;

    public ExplorePeopleFragment() {
        // Required empty public constructor
    }

    public static ExplorePeopleFragment newInstance() {
        ExplorePeopleFragment fragment = new ExplorePeopleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_explore_people, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        // use a linear layout manager
        setupRecyclerView();
        getExplore();
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapter = new PeopleAdapter(getActivity(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        });
        setInitAdapterData();

        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  int visibleItemCount, pastVisibleItems, totalItemCount;
                                                  if (dy > 0) {
                                                      visibleItemCount = mLinearLayoutManager.getChildCount();
                                                      totalItemCount = mLinearLayoutManager.getItemCount();
                                                      pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              currentSuggestedPage++;
                                                              getExplorePeople(ExplorePeopleType.SUGGESTED_FOLLOWS.getType());
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            currentSuggestedPage = 1;
            mAdapter.clearAllItems();
            mSwipeRefreshLayout.setRefreshing(false);
            setInitAdapterData();
            getExplore();
        });
    }

    private void setInitAdapterData() {
        mAdapter.addItem(new PeopleGroup(getString(R.string.explore_people_rising_star), new ArrayList<>()));
        mAdapter.addItem(new PeopleGroup(getString(R.string.explore_people_top_users), new ArrayList<>()));
        mAdapter.addItem(new PeopleGroup(getString(R.string.explore_people_new), new ArrayList<>()));
    }

    private void getExplorePeople(String type) {
        boolean isShowLoading = true;
        GetListRequest requestModel = new GetListRequest();
        if (type.equals(ExplorePeopleType.SUGGESTED_FOLLOWS.getType())) {
            requestModel.setPaginate(currentSuggestedPage);
            isShowLoading = false;
        } else {
            requestModel.setPaginate(1);
        }
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestModel.setType(type);
        requestApi(requestModel, isShowLoading, RequestTarget.GET_EXPLORE_PEOPLE, this);
    }

    private void getExplore() {
        getExplorePeople(ExplorePeopleType.RISING_STAR.getType());
        getExplorePeople(ExplorePeopleType.TOP_USERS.getType());
        getExplorePeople(ExplorePeopleType.NEW.getType());
        getExplorePeople(ExplorePeopleType.SUGGESTED_FOLLOWS.getType());
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_EXPLORE_PEOPLE:
                    String type = ((UsersResponse) response.getData()).getType();
                    if (TextUtils.isEmpty(type)) {
                        return;
                    }

                    List<User> users = ((UsersResponse) response.getData()).getUsers();

                    if (type.equals(ExplorePeopleType.SUGGESTED_FOLLOWS.getType())) {
                        if (isMoreLoading && mAdapter.getItemCount() > 0) {
                            mAdapter.removeItem(mAdapter.getItemCount() - 1);
                            isMoreLoading = false;
                        }
                        for (User user : users) {
                            mAdapter.addItem(new SuggestedFollow(user), 0, false);
                        }
                    } else {
                        if (users != null && !users.isEmpty()) {
                            mAdapter.addItem(new PeopleGroup(mAdapter.getGroupName(type), users), mAdapter.getPositionOfType(type), true);
                        } else {
//                            mAdapter.setEmptyGroupName(getGroupNameFromType(type));
                        }
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    private String getGroupNameFromType(String type) {
        if (type.equals(ExplorePeopleType.RISING_STAR.getType())) {
            return getString(R.string.explore_people_rising_star);
        } else if (type.equals(ExplorePeopleType.TOP_USERS.getType())) {
            return getString(R.string.explore_people_top_users);
        }
        if (type.equals(ExplorePeopleType.NEW.getType())) {
            return getString(R.string.explore_people_new);
        }

        return "";
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
