package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.NonSwipeableViewPager;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.fragments.GiftStoreFragment;
import com.airvtinginc.airvtinglive.gui.fragments.BottomMyGiftFragment;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BottomGiftDialog extends BaseFragmentDialog implements GiftStoreFragment.BuyGiftListener, BottomMyGiftFragment.SendGiftListener {
    private static final String TAG = BottomGiftDialog.class.getSimpleName();

    @BindView(R.id.vp_bottom_gift)
    NonSwipeableViewPager mViewPager;
    @BindView(R.id.gift_store_bottom_line)
    View mGiftStoreBottomLine;
    @BindView(R.id.my_gift_bottom_line)
    View mMyGiftBottomLine;
    @BindView(R.id.tv_bottom_gift_token)
    TextView mTvBottomGiftToken;

    private String mStreamerId;
    private String mPostId;
    private GiftStoreFragment mGiftStoreFragment;
    private BottomMyGiftFragment mBottomMyGiftFragment;

    @OnClick(R.id.ll_bottom_gift_empty)
    void dismissDialog() {
        dismiss();
    }

    @OnClick(R.id.ll_bottom_gift_gift_store_header)
    void displayGiftStore() {
        mGiftStoreBottomLine.setVisibility(View.GONE);
        mMyGiftBottomLine.setVisibility(View.VISIBLE);
        mViewPager.setCurrentItem(0);
    }

    @OnClick(R.id.ll_bottom_gift_my_gift_header)
    void displayMyGift() {
        mMyGiftBottomLine.setVisibility(View.GONE);
        mGiftStoreBottomLine.setVisibility(View.VISIBLE);
        mViewPager.setCurrentItem(1);
    }

    @OnClick(R.id.ll_bottom_gift_token)
    void goToBuyAirToken() {
        dismissDialog();
        Intent walletTokenIntent = new Intent(getActivity(), DetailActivity.class);
        DetailType.SHOW_WALLET_TOKEN.attachTo(walletTokenIntent);
        startActivity(walletTokenIntent);
    }

    public static BottomGiftDialog newInstance(String streamerId, String postId) {
        BottomGiftDialog dialog = new BottomGiftDialog();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, streamerId);
        args.putString(Constants.EXTRA_POST_ID, postId);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BOTTOM);
        if (getArguments() != null) {
            mStreamerId = getArguments().getString(Constants.EXTRA_USER_ID);
            mPostId = getArguments().getString(Constants.EXTRA_POST_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_bottom_gift, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);

        setUpViewPager();
        int currentToken = SharedPreferencesManager.getInstance(getContext()).getInt(Constants.PREF_CURRENT_USER_TOKEN, 0);
        mTvBottomGiftToken.setText(String.valueOf(currentToken));
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void setUpViewPager() {
        PageFragmentAdapter adapter = new PageFragmentAdapter(getChildFragmentManager());
        mGiftStoreFragment = GiftStoreFragment.newInstance(false, mStreamerId, mPostId);
        mGiftStoreFragment.setBuyGiftListener(this);

        mBottomMyGiftFragment = BottomMyGiftFragment.newInstance(mStreamerId, mPostId);
        mBottomMyGiftFragment.setSendGiftListener(this);

        adapter.addFragment(mGiftStoreFragment, "");
        adapter.addFragment(mBottomMyGiftFragment, "");
        mViewPager.setAdapter(adapter);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {

            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }

    @Override
    public void onBuyGift(int currentToken) {
        mTvBottomGiftToken.setText(String.valueOf(currentToken));
        if (mBottomMyGiftFragment != null) {
            mBottomMyGiftFragment.refreshMyGifts();
        }
    }

    @Override
    public void onSendGift() {
        if (mGiftStoreFragment != null) {
            mGiftStoreFragment.refreshGiftStore();
        }
    }
}
