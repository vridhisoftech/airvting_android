package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ChoseTextAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.CommentMediaAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.components.hashtag.EditTextHashtag;
import com.airvtinginc.airvtinglive.gui.dialog.ListDialog;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.dialog.OptionsOnPostDialog;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.request.BookmarkPostRequest;
import com.airvtinginc.airvtinglive.models.request.PostRequest;
import com.airvtinginc.airvtinglive.models.request.ReportRequest;
import com.airvtinginc.airvtinglive.models.response.CommentItem;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostDetailResponse;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ReportResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class DetailGalleryFragment extends BaseFragment {
    private static final String TAG = DetailGalleryFragment.class.getSimpleName();

    @BindView(R.id.ivMedia)
    ImageView ivMedia;
    @BindView(R.id.pbMedia)
    ProgressBar pbMedia;
    @BindView(R.id.ivBookmark)
    ImageView ivBookmark;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.edComments)
    EditTextHashtag edComments;
    @BindView(R.id.tvTotalComments)
    TextView tvTotalComments;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.tvShowEmpty)
    TextView tvShowEmpty;
    @BindView(R.id.ivShowEmpty)
    ImageView ivShowEmpty;

    @BindView(R.id.clDetailGallery)
    CoordinatorLayout clDetailGallery;
    @BindView(R.id.ablDetailGallery)
    AppBarLayout ablDetailGallery;

    @BindView(R.id.ivPlay)
    ImageView ivPlay;
    String userId;
    private String galleryId;
    private PostDetail postDetail;

    private LinearLayoutManager mLinearLayoutManager;
    private CommentMediaAdapter mAdapter;
    private int mCurrentPage = 1;
    private String mMaxId;
    private boolean isMoreLoading;
    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isCurrentUser;
    private CommentItem commentItemDelete;

    public Unregistrar mUnregistrar;

    @OnClick(R.id.lnBookmark)
    void bookmarkClick() {
        BookmarkPostRequest requestModel = new BookmarkPostRequest(postDetail.getId());
        requestApi(requestModel, true, RequestTarget.BOOKMARK_POST, this);
    }

    @OnClick(R.id.ibOptionPost)
    void options() {
        OptionsOnPostDialog optionsOnPostDialog = OptionsOnPostDialog.newInstance();
        optionsOnPostDialog.setOnActionListener(new OptionsOnPostDialog.OnActionListener() {
            @Override
            public void onReportClick() {
                reportPost();
            }

            @Override
            public void onDeleteClick() {
                deletePost();
            }
        });
        Bundle args = new Bundle();
        args.putString("userId", userId);
        optionsOnPostDialog.setArguments(args);
        optionsOnPostDialog.setCurrentUser(isCurrentUser);
        optionsOnPostDialog.show(getActivity().getSupportFragmentManager(), "Options Dialog");
    }

    @OnClick(R.id.ivSend)
    void sendComment() {
        if (!TextUtils.isEmpty(edComments.getText().toString())) {
            Utils.hideSoftKeyboard(getActivity());
            addComment(edComments.getText().toString());
        }
    }

    @OnClick({R.id.ivPlay, R.id.rlMedia})
    void playMedia() {
        Uri uriMedia;
        String type;
        if (postDetail.getType().equals(com.airvtinginc.airvtinglive.gui.enums.MediaType.VIDEO.getType())) {
            uriMedia = Uri.parse(postDetail.getMediaUrl());
            type = "video/mp4";
        } else {
            uriMedia = Uri.parse(postDetail.getFeaturedImage());
            type = "image/*";
        }
        Log.d(TAG, "uriMedia: " + uriMedia.toString());
        Intent intent = new Intent(Intent.ACTION_VIEW, uriMedia);
        intent.setDataAndType(uriMedia, type);
        startActivity(intent);
    }

    public DetailGalleryFragment() {
        // Required empty public constructor
    }

    public static DetailGalleryFragment newInstance(String galleryId) {
        DetailGalleryFragment fragment = new DetailGalleryFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_GALLERY_ID, galleryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.galleryId = getArguments().getString(Constants.EXTRA_GALLERY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        Log.d(TAG, "galleryId: " + galleryId);
        setUpToolbar();
        setUpRecyclerView();
        getDetailMedia();

        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(getActivity(), isOpen -> {
            if (isOpen) {
                if (mAdapter != null) {
                    mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                }
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ablDetailGallery.getLayoutParams();
                AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
                if (behavior != null) {
                    behavior.onNestedFling(clDetailGallery, ablDetailGallery, null, 0, 0, true);
                }
            } else {
            }
        });
        Utils.hideSoftKeyboard(getActivity());
    }

    private void setUpToolbar() {
        Intent intent = new Intent(DetailActivity.BroadcastActionChangeToolbarToTransparent);
        getActivity().sendBroadcast(intent);
    }

    private void setUpRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new CommentMediaAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                openUserDetail(((CommentItem) item).getUser().getUserId());
            }
        });
        mAdapter.setActionItemClick((commentItem, position) -> {
            OptionsOnPostDialog optionsOnPostDialog = OptionsOnPostDialog.newInstance();
            optionsOnPostDialog.setOnActionListener(new OptionsOnPostDialog.OnActionListener() {
                @Override
                public void onReportClick() {
                    reportComment(commentItem);
                }

                @Override
                public void onDeleteClick() {
                    deleteComment(commentItem);
                }
            });
            Bundle args = new Bundle();
            args.putString("userId", userId);
            optionsOnPostDialog.setArguments(args);
            optionsOnPostDialog.setCurrentUser(commentItem.getUser().getUserId().equals(Utils.getCurrentUserId(getContext())));
            optionsOnPostDialog.show(getActivity().getSupportFragmentManager(), "Options Dialog");
        });

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) emptyView.getLayoutParams();
        layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        emptyView.setLayoutParams(layoutParams);
        tvShowEmpty.setText(getString(R.string.recycle_view_empty_comment));
        ivShowEmpty.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.post_detail_no_data);

        mRecyclerView.setEmptyView(emptyView);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.white));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = mLinearLayoutManager.getChildCount();
                                                      totalItemCount = mLinearLayoutManager.getItemCount();
                                                      pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getComments(++mCurrentPage);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            mMaxId = null;
            getComments(mCurrentPage);
        });
        getComments(mCurrentPage);
    }

    private void setData(PostDetail postDetail) {
        this.postDetail = postDetail;
        isCurrentUser = (postDetail.getUser().getUserId().equals(Utils.getCurrentUserId(getContext())));
        User user = postDetail.getUser();
        userId = postDetail.getUser().getUserId();
        if (!TextUtils.isEmpty(postDetail.getFeaturedImage())) {
            pbMedia.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(postDetail.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbMedia.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbMedia.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(ivMedia);
        } else {
        }
        updateBookmark();

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                openUserDetail(postDetail.getUser().getUserId());
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        Utils.spanAndCheckContentUsername(getActivity(), user.getUsername(), postDetail.getTitle(), user.isOnline(), tvContent, clickableSpan, getActivity().getResources().getColor(R.color.colorHeader));

        if (postDetail.getType().equals(com.airvtinginc.airvtinglive.gui.enums.MediaType.VIDEO.getType())) {
            ivPlay.setVisibility(View.VISIBLE);
        } else {
            ivPlay.setVisibility(View.GONE);
        }

        try {
            tvDate.setText((DateTimeUtils.getElapsedInterval(getActivity(), DateTimeUtils.getDateFormatUTC(postDetail.getCreatedAt()))).toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openUserDetail(String userId) {
        String currentUserId = SharedPreferencesManager.getInstance(getActivity()).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (userId.equals(currentUserId)) {
            Intent intent = new Intent(getContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(intent);
            getActivity().sendBroadcast(new Intent(MainActivity.BroadcastActionGoToProfileTab));
        } else {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            Bundle args = new Bundle();
            args.putString(Constants.EXTRA_USER_ID, userId);
            intent.putExtras(args);
            // Sender usage
            DetailType.PROFILE.attachTo(intent);
            getActivity().startActivity(intent);
        }
    }

    private void updateBookmark() {
        if (postDetail.isBookmark()) {
            ivBookmark.setImageResource(R.drawable.ic_bookmark_fill);
        } else {
            ivBookmark.setImageResource(R.drawable.ic_bookmark);
        }
    }

    private void getComments(int page) {
        PostRequest modelRequest = new PostRequest(galleryId, page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING * 2, mMaxId);
        requestApi(modelRequest, true, RequestTarget.GET_COMMENTS, this);
    }

    private void getDetailMedia() {
        PostRequest modelRequest = new PostRequest(galleryId);
        requestApi(modelRequest, true, RequestTarget.GET_POST_DETAIL, this);
    }

    private void addComment(String cmt) {
        PostRequest modelRequest = new PostRequest(galleryId);
        Map<String, RequestBody> bodyMap = new HashMap<>();
        bodyMap.put("comment", RequestBody.create(MediaType.parse("application/json"), cmt));
        modelRequest.setParams(bodyMap);
        requestApi(modelRequest, true, RequestTarget.ADD_COMMENT, this);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    private void reportPost() {
        ListDialog report = ListDialog.newInstance();
        report.setShowDialogType(ShowDialogType.REPORT);
        report.setTitle(getActivity(), R.string.dialog_report_title);
        report.setBackgroundColorRes(getActivity(), R.color.white);
        report.setTitleColorRes(getActivity(), R.color.black);
        ChoseTextAdapter choseTextAdapter = new ChoseTextAdapter(getActivity(), new ArrayList<>(), null);
        choseTextAdapter.setTextColorRes(getActivity(), R.color.black);
        choseTextAdapter.setBackgroundColorRes(getActivity(), android.R.color.white);
        report.setAdapter(choseTextAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        report.setOnSendListener(content -> {
            if (!TextUtils.isEmpty(postDetail.getUser().getUserId())) {
                Map<String, RequestBody> bodyMap = new HashMap<>();
                bodyMap.put("postId", RequestBody.create(MediaType.parse("application/json"), galleryId));
                bodyMap.put("content", RequestBody.create(MediaType.parse("application/json"), content));
                ReportRequest reportRequest = new ReportRequest(bodyMap);
                requestApi(reportRequest, true, RequestTarget.REPORT, this);
            }
        });
        report.setOnDismissListener(() -> {
            report.dismiss();
        });
        report.show(getFragmentManager(), "Add Report Dialog");
    }

    private void deletePost() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogTitle(getString(R.string.dialog_delete_post_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_delete_post_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                requestApi(postDetail.getId(), true, RequestTarget.DELETE_POST, DetailGalleryFragment.this);
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {
                messageDialog.dismiss();
            }
        });
        messageDialog.show(getActivity().getSupportFragmentManager(), "Delete Dialog");
    }

    private void reportComment(CommentItem commentItem) {
        ListDialog report = ListDialog.newInstance();
        report.setShowDialogType(ShowDialogType.REPORT);
        report.setTitle(getActivity(), R.string.dialog_report_title);
        report.setBackgroundColorRes(getActivity(), R.color.white);
        report.setTitleColorRes(getActivity(), R.color.black);
        ChoseTextAdapter choseTextAdapter = new ChoseTextAdapter(getActivity(), new ArrayList<>(), null);
        choseTextAdapter.setTextColorRes(getActivity(), R.color.black);
        choseTextAdapter.setBackgroundColorRes(getActivity(), android.R.color.white);
        report.setAdapter(choseTextAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        report.setOnSendListener(content -> {
            if (!TextUtils.isEmpty(commentItem.getId())) {
                Map<String, RequestBody> bodyMap = new HashMap<>();
                bodyMap.put("commentId", RequestBody.create(MediaType.parse("application/json"), commentItem.getId()));
                bodyMap.put("content", RequestBody.create(MediaType.parse("application/json"), content));
                ReportRequest reportRequest = new ReportRequest(bodyMap);
                requestApi(reportRequest, true, RequestTarget.REPORT, this);
            }
        });
        report.setOnDismissListener(() -> {
            report.dismiss();
        });
        report.show(getFragmentManager(), "Add Report Dialog");
    }

    private void deleteComment(CommentItem commentItem) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogTitle(getString(R.string.dialog_delete_comment_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_delete_comment_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                commentItemDelete = commentItem;
                requestApi(commentItem.getId(), true, RequestTarget.DELETE_COMMENT, DetailGalleryFragment.this);
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {
                messageDialog.dismiss();
            }
        });
        messageDialog.show(getActivity().getSupportFragmentManager(), "Delete Dialog");
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_POST_DETAIL:
                    PostDetailResponse postDetailResponse = (PostDetailResponse) response.getData();
                    PostDetail postDetail = postDetailResponse.getPostDetail();
                    if (postDetail != null) {
                        setData(postDetail);
                    }
                    break;
                case BOOKMARK_POST:
                    boolean isBookmark = ((PostDetailResponse) response.getData()).getPostDetail().isBookmark();
                    this.postDetail.setBookmark(isBookmark);
                    updateBookmark();
                    break;
                case GET_COMMENTS:
                    hideLoadMore();
                    PostResponse postResponse = ((PostResponse) response.getData());
                    if (postResponse != null && mAdapter != null) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mAdapter.addAllItems(postResponse.getCommentDetail());
                        mMaxId = postResponse.getMaxId();
                        tvTotalComments.setText(" (" + String.valueOf(postResponse.getTotalComments()) + ")");
                    }
                    break;
                case ADD_COMMENT:
                    mSwipeRefreshLayout.setRefreshing(true);
                    edComments.setText("");
                    mCurrentPage = 1;
                    mMaxId = null;
                    getComments(mCurrentPage);
                    break;
                case REPORT:
                    ReportResponse reportResponse = ((UsersResponse) response.getData()).getReportResponse();
                    DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_report_title), getString(R.string.dialog_thanks_report));
                    break;
                case DELETE_POST: {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().startActivity(intent);
                    getActivity().sendBroadcast(new Intent(MainActivity.BroadcastActionGoToProfileTab));
                    break;
                }
                case DELETE_COMMENT: {
                    mAdapter.removeItem(commentItemDelete);
                    edComments.setText("");
                    mCurrentPage = 1;
                    mMaxId = null;
                    getComments(mCurrentPage);
                    break;
                }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        switch (requestTarget) {
            case REPORT:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_report_title), failMessage);
                break;
            case DELETE_COMMENT:
            case DELETE_POST:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_delete_title), failMessage);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mUnregistrar.unregister();
    }

    @Override
    public void onStop() {
        super.onStop();
        mUnregistrar.unregister();
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.hideSoftKeyboard(getActivity());
    }
}
