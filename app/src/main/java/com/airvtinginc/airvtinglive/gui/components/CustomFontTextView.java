package com.airvtinginc.airvtinglive.gui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.airvtinginc.airvtinglive.R;

import static android.graphics.Typeface.BOLD;

public class CustomFontTextView extends AppCompatTextView {

    public CustomFontTextView(Context context) {
        super(context);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.CustomFontTextView_fonts:
                    String customFontName = a.getInteger(attr, 0) == 0 ? "Lato-Regular" : "Montserrat-Regular";
                    tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + customFontName + ".ttf");
                    break;
                default:
                    break;
            }
        }
        a.recycle();
        if (getTypeface() == null) setTypeface(tf);
        else if (getTypeface().isBold()) setTypeface(tf, BOLD);
        else setTypeface(tf);
    }

}