package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAddLiveHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivProduct)
    ImageView ivProduct;
    public @BindView(R.id.pbProductLoading)
    ProgressBar pbProductLoading;
    public @BindView(R.id.tvPrice)
    TextView tvPrice;
    public @BindView(R.id.ivPriceSale)
    ImageView ivPriceSale;
    public @BindView(R.id.tvNameProduct)
    TextView tvNameProduct;
    public @BindView(R.id.llAddToCard)
    LinearLayout llAddToCard;
    public @BindView(R.id.ivAddToCart)
    ImageView ivAddToCart;

    public ProductAddLiveHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}