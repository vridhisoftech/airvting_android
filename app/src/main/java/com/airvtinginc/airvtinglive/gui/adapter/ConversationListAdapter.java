package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ConversationsViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.MessageType;
import com.airvtinginc.airvtinglive.models.MessageContentModel;
import com.airvtinginc.airvtinglive.models.response.Conversation;
import com.airvtinginc.airvtinglive.models.response.Message;
import com.airvtinginc.airvtinglive.models.response.Sender;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ConversationListAdapter extends BaseAdapter {
    public <T> ConversationListAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_conversations_list, parent, false);
        return new ConversationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ConversationsViewHolder viewHolder = (ConversationsViewHolder) holder;
        if (list.size() > 0) {
            Conversation conversation = (Conversation) list.get(position);
            if (conversation == null) {
                return;
            }

            String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
            Sender sender = conversation.getSender();
            Sender receiver = conversation.getReceiver();
            String userID, avatar, firstName, lastName, displayName;

            if (currentUserId.equals(sender.getUserId())) {
                userID = receiver.getUserId();
                avatar = receiver.getFeaturedImage();
                firstName = receiver.getFirstName();
                lastName = receiver.getLastName();
                displayName = receiver.getDisplayName();
            } else {
                userID = sender.getUserId();
                avatar = sender.getFeaturedImage();
                firstName = sender.getFirstName();
                lastName = sender.getLastName();
                displayName = sender.getDisplayName();
            }

            if (TextUtils.isEmpty(avatar)) {
                User user = new User();
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setDisplayName(displayName);

                viewHolder.pbAvatar.setVisibility(View.GONE);
                Utils.setDefaultAvatar(context, viewHolder.ivDefaultAvatar, viewHolder.tvDefaultAvatar, viewHolder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
            } else {
                viewHolder.pbAvatar.setVisibility(View.VISIBLE);
                viewHolder.ivAvatar.setVisibility(View.VISIBLE);
                Glide.with(context).load(avatar)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                User user = new User();
                                user.setFirstName(firstName);
                                user.setLastName(lastName);
                                user.setDisplayName(displayName);
                                viewHolder.pbAvatar.setVisibility(View.GONE);
                                Utils.setDefaultAvatar(context, viewHolder.ivDefaultAvatar, viewHolder.tvDefaultAvatar, viewHolder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.pbAvatar.setVisibility(View.GONE);
                                Utils.setHideDefaultAvatar(context, viewHolder.ivDefaultAvatar, viewHolder.tvDefaultAvatar, viewHolder.ivAvatar);
                                return false;
                            }
                        })
                        .into(viewHolder.ivAvatar);
            }

            viewHolder.ivAvatar.setOnClickListener(view -> {
                Utils.openUserDetail(context, userID);
            });

            viewHolder.tvDisplayName.setText(displayName);

            String title = conversation.getTitle();
            viewHolder.tvTitle.setText(title);
            viewHolder.tvTitle.setVisibility(View.VISIBLE);

            if (conversation.getMessageList().size() > 0) {
                String contentLastMessage;

                Message message = conversation.getMessageList().get(0);
                MessageContentModel messageContentModel = Utils.parseMessageContent(message.getContent());
                if (messageContentModel.getType().equals(MessageType.TEXT.getType())) {
                    String content = messageContentModel.getMessage(); // display newest message
                    String[] contentLastMessages = content.split("\n");
                    contentLastMessage = contentLastMessages[0];
                } else {
                    if (currentUserId.equals(message.getSenderId())) {
                        contentLastMessage = context.getString(R.string.conversation_text_sent_photo);
                    } else {
                        contentLastMessage = context.getString(R.string.conversation_text_received_photo);
                    }
                }

                viewHolder.tvContent.setText(contentLastMessage);

                String createAt;
                try {
                    createAt = DateTimeUtils.getElapsedInterval(context, DateTimeUtils.getDateFormatUTC(message.getCreatedAt()));
                    viewHolder.tvCreateAt.setText(createAt.toLowerCase());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            int unReadCount = conversation.getUnReadCount();
            if (unReadCount > 0) {
                if (unReadCount < 10) {
                    viewHolder.tvBadgeSingle.setText(String.valueOf(unReadCount));
                    viewHolder.tvBadgeSingle.setVisibility(View.VISIBLE);
                    viewHolder.tvBadgeMulti.setVisibility(View.GONE);
                } else {
                    viewHolder.tvBadgeMulti.setText(String.valueOf(unReadCount));
                    viewHolder.tvBadgeMulti.setVisibility(View.VISIBLE);
                    viewHolder.tvBadgeSingle.setVisibility(View.GONE);
                }
                viewHolder.rlBadge.setVisibility(View.VISIBLE);
            } else {
                viewHolder.rlBadge.setVisibility(View.INVISIBLE);
            }

            viewHolder.cvBound.setOnClickListener(v -> this.onItemClickListener.onItemClicked(conversation, position));
        }
    }

    public String getConversationIdByPosition(int position) {
        return ((Conversation) list.get(position)).getId();
    }
}
