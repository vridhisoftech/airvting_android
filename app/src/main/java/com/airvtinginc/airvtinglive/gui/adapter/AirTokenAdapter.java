package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.AirTokenViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.models.AirToken;
import com.airvtinginc.airvtinglive.tools.NumberUtils;

public class AirTokenAdapter extends BaseAdapter {
    private static final int VIEW_TYPE_LOAD_MORE = 0;
    private static final int VIEW_TYPE_TOKEN = 1;

    public AirTokenAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(v);
            }
            case VIEW_TYPE_TOKEN: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_air_token, parent, false);
                return new AirTokenViewHolder(v);
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                return new EmptyViewHolder(v);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder || list.get(position) == null) {
            return;
        }

        AirTokenViewHolder holder = (AirTokenViewHolder) mHolder;
        AirToken airToken = (AirToken) list.get(position);
        if (airToken == null) {
            return;
        }

        holder.tvAirTokenQuantity.setText(String.valueOf(airToken.getQuantity()));
        if (airToken.getCurrency() != null && !airToken.getCurrency().equalsIgnoreCase("")) {
            holder.tvAirTokenPrice.setText(NumberUtils.formatPriceByCurrency(airToken.getPrice(), airToken.getCurrency()));
        }
        holder.mView.setOnClickListener(view -> onItemClickListener.onItemClicked(airToken, position));
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_TOKEN : VIEW_TYPE_LOAD_MORE;
    }
}
