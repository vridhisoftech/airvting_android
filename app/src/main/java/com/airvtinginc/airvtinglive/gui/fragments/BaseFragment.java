package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.interfaces.BaseInterface;
import com.airvtinginc.airvtinglive.gui.activities.BaseActivity;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements BaseInterface, APIResponseListener {
    private static final String TAG = BaseFragment.class.getSimpleName();
    /**
     * The unbinder of Butterknife to unbind views when the fragment view is destroyed
     */
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, getView());
        getView().setOnClickListener(v -> {
        });
    }

    @Override
    public <T> void requestApi(T model, boolean isLoading, RequestTarget requestTarget, APIResponseListener listener) {
        ((BaseActivity) getActivity()).requestApi(model, isLoading, requestTarget, listener);
    }

    public void cancelRequestApi() {
        ((BaseActivity) getActivity()).cancelRequestApi();
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + failMessage);
        switch (statusCode) {
            case ERR_NO_INTERNET_CONNECTION:
//                DialogUtils.showNoNetworkDialog(getContext(), getFragmentManager(), getString(R.string.error_network_title), failMessage);
                break;
            case ERR_UNAUTHORIZED:
                DialogUtils.showUnauthorizedDialog(getContext(), getFragmentManager(), failMessage);
                break;
        }
    }

    @Override
    public void onDestroy() {
        try {
            System.out.println("onDestroyFragment:" + this.getClass().getSimpleName());
            if (unbinder != null) {
                unbinder.unbind();
            }
        } catch (Exception ignored) {
        }
        super.onDestroy();

    }
}