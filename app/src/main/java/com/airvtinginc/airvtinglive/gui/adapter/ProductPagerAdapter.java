package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductPagerAdapter extends PagerAdapter {
    private final String TAG = ProductPagerAdapter.class.getSimpleName();

    @BindView(R.id.ivProductImage)
    ImageView mIvProductImage;
    @BindView(R.id.pbProductImage)
    ProgressBar pbProductImage;

    private Context mContext;
    private List<FeaturedImage> mFeaturedImages = new ArrayList<>();

    public ProductPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (mFeaturedImages != null) {
            return mFeaturedImages.size();
        }
        return 0;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View imageSlideLayout = LayoutInflater.from(mContext).inflate(R.layout.row_product_image, container, false);
        ButterKnife.bind(this, imageSlideLayout);
        String productImage = mFeaturedImages.get(position).getFeaturedImage();

        if (!TextUtils.isEmpty(productImage)) {
            pbProductImage.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(productImage)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbProductImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbProductImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(mIvProductImage);

            mIvProductImage.setOnClickListener(view -> {
                showImage(productImage);
            });
        }

        container.addView(imageSlideLayout, 0);

        return imageSlideLayout;
    }

    public void setProductImages(List<FeaturedImage> featuredImages) {
        mFeaturedImages.clear();
        mFeaturedImages = featuredImages;
        notifyDataSetChanged();
    }

    private void showImage(String image) {
        Uri uriMedia;
        String type;
        uriMedia = Uri.parse(image);
        type = "image/*";
        Log.d(TAG, "uriMedia: " + uriMedia.toString());
        Intent intent = new Intent(Intent.ACTION_VIEW, uriMedia);
        intent.setDataAndType(uriMedia, type);
        mContext.startActivity(intent);
    }
}
