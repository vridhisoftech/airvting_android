package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.os.CountDownTimer;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivProduct)
    ImageView ivProductImage;
    public @BindView(R.id.tvLimited)
    TextView tvLimited;
    public @BindView(R.id.tvTitle)
    TextView tvTitle;
    public @BindView(R.id.tvPriceBefore)
    TextView tvPriceBefore;
    public @BindView(R.id.tvPriceAfter)
    TextView tvPriceAfter;
    public @BindView(R.id.tvDiscount)
    TextView tvDiscount;
    public @BindView(R.id.tvDescription)
    TextView tvDescription;
    public @BindView(R.id.btnShow)
    AppCompatButton btnShow;
    public @BindView(R.id.ivPriceTag)
    ImageView ivPriceTag;
    public @BindView(R.id.tvContent)
    TextView tvContent;
    public @BindView(R.id.rlDeal)
    RelativeLayout rlDeal;
    public @BindView(R.id.tvCountDownTimer)
    TextView tvCountDownTimer;
    public CountDownTimer timer;

    public ProductViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}