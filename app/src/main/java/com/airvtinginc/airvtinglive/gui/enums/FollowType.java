package com.airvtinginc.airvtinglive.gui.enums;

public enum FollowType {
    FOLLOWERS("followers"),
    FOLLOWING("following");

    private String type;

    FollowType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
