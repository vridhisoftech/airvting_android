package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.ProductInCart;
import com.airvtinginc.airvtinglive.models.request.CheckoutRequest;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmPaymentDialog extends BaseFragmentDialog {
    private static final String TAG = ConfirmPaymentDialog.class.getSimpleName();

    @BindView(R.id.tv_confirm_payment_title)
    TextView mTvTitle;
    @BindView(R.id.tv_confirm_payment_total_price)
    TextView mTvTotalPrice;
    @BindView(R.id.tv_confirm_payment_confirm_msg)
    TextView mTvMessage;
    @BindView(R.id.rl_confirm_payment_ok)
    RelativeLayout mRlConfirmPayment;
    @BindView(R.id.confirm_payment_tv_total_token)
    TextView tvTotalAirToken;
    @BindView(R.id.ll_confirm_payment_total_air_token)
    LinearLayout llAirToken;

    private float mTotalAmount;
    private int airToken;
    private OnConfirmPaymentListener mOnConfirmPaymentListener;
    private List<Product> productList = new ArrayList<>();
    private List<ProductInCart> productInCartList = new ArrayList<>();

    @OnClick(R.id.iv_confirm_payment_close)
    void closeDialog() {
        dismiss();
    }

    @OnClick(R.id.mrl_confirm_payment_ok)
    void confirmPayment() {
        makePayment();
    }

    public static ConfirmPaymentDialog newInstance() {
        return new ConfirmPaymentDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BORDER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_confirm_payment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        showConfirmPayment();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void showConfirmPayment() {
        mTvTotalPrice.setText(NumberUtils.formatPrice(mTotalAmount));
        tvTotalAirToken.setText(String.valueOf(airToken));
    }

    private void makePayment() {
        for (Product product : productList) {
            ProductInCart productInCart = new ProductInCart();
            productInCart.setCurrency(ServiceConstants.CURRENCY);
            productInCart.setPrice(product.getPaymentPrice());
            productInCart.setQuantity(product.getQuantityToBuy());
            productInCart.setSellerId(product.getSellerId());
            productInCart.setPostId(product.getPostId());
            productInCart.setProductId(product.getProductId());
            productInCart.setTitle(product.getTitle());
            productInCart.setFeaturedImage(product.getDisplayImage());
            productInCart.setDescription(product.getDescription());
            productInCartList.add(productInCart);
        }

        String currentUserId = Utils.getCurrentUserId(getContext());
        CheckoutRequest request = new CheckoutRequest();
        request.setUserId(currentUserId);
        request.setProducts(productInCartList);
        request.setTotalPrice(mTotalAmount);
        request.setUseAirToken(airToken);
        request.setTotalQuantity(productList.size());
        request.setPaymentMethodId(Utils.getPaymentMethodId(getContext()));

        requestApi(request, true, RequestTarget.CHECKOUT, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            mTvTitle.setText(getString(R.string.dialog_my_cart_confirm_payment_thank_you));
            mTvMessage.setText(getString(R.string.dialog_my_cart_confirm_payment_successful));
            mRlConfirmPayment.setVisibility(View.GONE);
            if (mOnConfirmPaymentListener != null) {
                mOnConfirmPaymentListener.onConfirmPaymentSuccessful();
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.dialog_my_cart_payment), response.getMessage());
            dismiss();
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        if (failMessage.equals(ServiceConstants.ERROR_MSG_REQUEST_TIME_OUT)) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.dialog_my_cart_payment), getString(R.string.dialog_payment_timeout));
        } else {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_my_cart_payment), failMessage);
        }
        dismiss();
    }

    public void setTotalAmount(float totalAmount) {
        mTotalAmount = totalAmount;
    }

    public void setTotalAirToken(int airToken) {
        this.airToken = airToken;
    }

    public void setProducts(List<Product> products) {
        productList = products;
    }

    public interface OnConfirmPaymentListener {
        void onConfirmPaymentSuccessful();
    }

    public void setOnConfirmPaymentListener(OnConfirmPaymentListener onConfirmPaymentListener) {
        mOnConfirmPaymentListener = onConfirmPaymentListener;
    }
}
