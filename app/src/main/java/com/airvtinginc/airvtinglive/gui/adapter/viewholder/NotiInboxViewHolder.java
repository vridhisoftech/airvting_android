package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotiInboxViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivProfile)
    ImageView ivProfile;
    public @BindView(R.id.tvUsername)
    TextView tvUsername;
    public @BindView(R.id.tvMessage)
    TextView tvMessage;
    public @BindView(R.id.tvDate)
    TextView tvDate;

    public NotiInboxViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}