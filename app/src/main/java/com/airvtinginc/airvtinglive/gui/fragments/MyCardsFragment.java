package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.UserCardAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.SearchUserModelRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.PaymentDetails;
import com.airvtinginc.airvtinglive.models.response.PaymentDetailResponse;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyCardsFragment extends BaseFragment implements BaseAdapter.OnItemClickListener {
    private static final String TAG = MyCardsFragment.class.getName();

    @BindView(R.id.fm_my_cards_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fm_my_cards_rv_cards)
    EmptyRecyclerView rvCards;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;

    private BroadcastReceiver mReceiver;
    private UserCardAdapter adapter;

    public static MyCardsFragment newInstance() {
        return new MyCardsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_cards, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        registerReceiver();
        rvCards.setEmptyView(emptyView);
        Utils.setUpRecycleView(getContext(), rvCards);

        adapter = new UserCardAdapter(getContext(), new ArrayList<>(), MyCardsFragment.this);
        rvCards.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(true);
            getUserListCards();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserListCards();
        configToolbar();
    }

    private void configToolbar() {
        Intent intent = new Intent();

        intent.setAction(DetailActivity.BroadcastActionHideAllToolbarIcons);
        getContext().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getActivity().getResources().getString(R.string.title_my_card);
        getContext().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionChangeToolbarToWhiteColor);
        getContext().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionShowIconAdd);
        getContext().sendBroadcast(intent);
    }

    private void registerReceiver() {
        // create new receiver
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (Objects.requireNonNull(intent.getAction())) {
                        case DetailActivity.BroadcastActionAdd:
                            requestApi(Utils.getCurrentUserId(getContext()), true, RequestTarget.GET_USER_PROFILE, MyCardsFragment.this);
                            break;
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(DetailActivity.BroadcastActionAdd);
            getActivity().registerReceiver(mReceiver, filter);
        }
    }

    private void checkEmailForAddCard() {
        if (!Utils.hasEmail(getContext())) {
            DialogUtils.showUpdateEmailDialog(getContext(), getFragmentManager());
        } else if (!Utils.isActiveEmail(getContext())) {
            DialogUtils.showVerifyEmailDialog(getContext(), getFragmentManager());
        } else {
            Intent addCardIntent = new Intent(getContext(), DetailActivity.class);
            DetailType.ADD_CARD.attachTo(addCardIntent);
            startActivity(addCardIntent);
        }
    }

    private void getUserListCards() {
        String currentUserId = Utils.getCurrentUserId(getContext());
        SearchUserModelRequest modelRequest = new SearchUserModelRequest();
        modelRequest.setUserId(currentUserId);
        modelRequest.setPaginate(1);
        modelRequest.setPerPage(20);
        requestApi(modelRequest, false, RequestTarget.GET_USER_LIST_CARDS, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_USER_LIST_CARDS:
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    List<PaymentDetails> paymentDetailsList = ((PaymentDetailResponse) response.getData()).getPaymentDetailList();
                    if (paymentDetailsList.size() == 0) {
                        Utils.saveHasPaymentMethod(getContext(), false);
                    }
                    paymentDetailsList.add(0, new PaymentDetails());
                    adapter.setList(paymentDetailsList);
                    break;
                case GET_USER_PROFILE: {
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null) {
                        Utils.saveEmailAndPaymentInfo(getContext(), userResponse.getUser());
                        checkEmailForAddCard();
                    }
                    break;
                }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constants.EXTRA_PAYMENT_DETAILS, (PaymentDetails) item);
        DetailType.EDIT_CARD.attachTo(intent);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
                mReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
