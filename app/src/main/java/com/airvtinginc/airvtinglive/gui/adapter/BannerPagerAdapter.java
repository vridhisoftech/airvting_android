package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.enums.HomeFeedType;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lightfire.gradienttextcolor.GradientTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BannerPagerAdapter extends PagerAdapter {
    @BindView(R.id.ivBanner)
    ImageView ivBanner;
    @BindView(R.id.pbBanner)
    ProgressBar pbBanner;
    @BindView(R.id.ivLiveRec)
    ImageView ivLiveRec;
    @BindView(R.id.ivVideo)
    ImageView ivVideo;
    @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;

    private Context context;
    private List<PostDetail> mBanners = new ArrayList<>();

    public BaseAdapter.OnItemClickListener onItemClickListener;

    public BannerPagerAdapter(Context context, BaseAdapter.OnItemClickListener onItemClickListener) {
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (mBanners != null) {
            return mBanners.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View imageSlideLayout = LayoutInflater.from(context).inflate(R.layout.row_home_banner, container, false);
        ButterKnife.bind(this, imageSlideLayout);
        PostDetail banner = mBanners.get(position);

        if (!TextUtils.isEmpty(banner.getFeaturedImage())) {
            pbBanner.setVisibility(View.GONE);
            Glide.with(context).load(banner.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbBanner.setVisibility(View.GONE);
                            ivBanner.setVisibility(View.INVISIBLE);
                            tvDefaultAvatar.setText(Utils.getDefaultAvatarText(banner.getUser()));
                            tvDefaultAvatar.setVisibility(View.VISIBLE);
                            ivBanner.setImageResource(R.drawable.ic_white);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbBanner.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(ivBanner);

            ivBanner.setVisibility(View.VISIBLE);
            tvDefaultAvatar.setText("");
            tvDefaultAvatar.setVisibility(View.GONE);
        } else {
            pbBanner.setVisibility(View.GONE);
            ivBanner.setVisibility(View.INVISIBLE);
            tvDefaultAvatar.setText(Utils.getDefaultAvatarText(banner.getUser()));
            tvDefaultAvatar.setVisibility(View.VISIBLE);
            ivBanner.setImageResource(R.drawable.ic_white);
        }

        if (banner.getType().equals(HomeFeedType.LIVE.getType())) {
            if (banner.isLive()) {
                ivLiveRec.setImageResource(R.drawable.ic_live);
            } else {
                ivLiveRec.setImageResource(R.drawable.ic_rec);
            }
            ivLiveRec.setVisibility(View.VISIBLE);
            ivVideo.setVisibility(View.GONE);
        } else if (banner.getType().equals(HomeFeedType.VIDEO.getType())) {
            ivLiveRec.setVisibility(View.GONE);
            ivVideo.setVisibility(View.VISIBLE);
        }

        ivBanner.setOnClickListener(view -> onItemClickListener.onItemClicked(banner, position));

        container.addView(imageSlideLayout, 0);

        return imageSlideLayout;
    }

    public void setBanners(List<PostDetail> banners) {
        mBanners.clear();
        mBanners = banners;
        notifyDataSetChanged();
    }
}
