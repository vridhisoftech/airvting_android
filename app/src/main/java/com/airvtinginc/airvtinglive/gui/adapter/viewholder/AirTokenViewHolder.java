package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AirTokenViewHolder extends BaseViewHolder {
    public View mView;
    public @BindView(R.id.tv_air_token_quantity)
    TextView tvAirTokenQuantity;
    public @BindView(R.id.tv_air_token_price)
    TextView tvAirTokenPrice;

    public AirTokenViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}
