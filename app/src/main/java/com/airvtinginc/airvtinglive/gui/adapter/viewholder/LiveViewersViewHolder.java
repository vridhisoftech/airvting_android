package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveViewersViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivAvatar)
    ImageView ivProfile;
    public @BindView(R.id.pbLoadAvatar)
    ProgressBar pbLoadAvatar;
    public @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    public @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;

    public LiveViewersViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}