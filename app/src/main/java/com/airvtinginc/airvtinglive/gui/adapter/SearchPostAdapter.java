package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.models.response.LiveStream;
import com.airvtinginc.airvtinglive.tools.fonts.CustomTypefaceSpan;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchPostAdapter extends BaseAdapter {

    private SearchPostListener searchPostListener;

    public interface SearchPostListener {
        void onBookmarkClick(int position);
    }

    public SearchPostAdapter(Context context, List<LiveStream> liveStreams, OnItemClickListener onItemClickListener, SearchPostListener searchPostListener) {
        super(context, liveStreams, onItemClickListener);
        this.searchPostListener = searchPostListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_post, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        ViewHolder holder = (ViewHolder) mHolder;
        final LiveStream liveStream = (LiveStream) list.get(position);
        int drawableResourceId = context.getResources().getIdentifier(liveStream.getImageUrl(), "drawable", context.getPackageName());
        holder.ivLive.setImageResource(drawableResourceId);

        String username = liveStream.getUsername();
        String content = liveStream.getCaption();
        String itemValue = username + " " + content;
        SpannableStringBuilder SS = new SpannableStringBuilder(itemValue);
        int startPos = itemValue.toLowerCase().indexOf(username.toLowerCase());
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        if (startPos != -1) {
            int endPos = startPos + username.length();
            SS.setSpan(clickableSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            SS.setSpan(new ForegroundColorSpan(this.context.getResources().getColor(R.color.colorHeader)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            SS.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_semibold)), startPos, endPos, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_regular)), endPos, itemValue.length() - endPos, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        holder.tvContent.setText(SS);

        holder.tvContent.setOnClickListener(view -> {
        });

        holder.cvBound.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(liveStream, position);
        });

        holder.btnBookmark.setOnClickListener(view -> {
            searchPostListener.onBookmarkClick(position);
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_search_post_cv_bound)
        CardView cvBound;
        @BindView(R.id.item_search_post_iv_live)
        ImageView ivLive;
        @BindView(R.id.item_search_post_iv_live_rec)
        ImageView ivLiveRec;
        @BindView(R.id.item_search_post_tv_viewer_count)
        TextView tvViewerCount;
        @BindView(R.id.item_search_post_tv_date)
        TextView tvDate;
        @BindView(R.id.item_search_post_tv_countdown_timer)
        TextView tvCountdownTimer;
        @BindView(R.id.item_search_post_iv_price_tag)
        ImageView ivPriceTag;
        @BindView(R.id.item_search_post_content)
        TextView tvContent;
        @BindView(R.id.item_search_post_btn_bookmark)
        ImageButton btnBookmark;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
