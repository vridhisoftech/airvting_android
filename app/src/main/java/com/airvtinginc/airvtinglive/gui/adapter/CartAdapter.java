package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.CartViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.models.PriceWhenStream;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CartAdapter extends BaseAdapter {

    public CartAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cart_item, parent, false);
        return new CartViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        CartViewHolder holder = (CartViewHolder) mHolder;
        Product product = (Product) list.get(position);

        if (!TextUtils.isEmpty(product.getDisplayImage())) {
            holder.pbProductImageLoading.setVisibility(View.VISIBLE);
            Glide.with(context).load(product.getDisplayImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbProductImageLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbProductImageLoading.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivProductImage);
        }

        holder.tvProductName.setText(product.getTitle());

        checkAndShowPrice(product, holder);

        holder.tvQuantity.setText(String.valueOf(product.getQuantityToBuy()));

        holder.ivDecrease.setOnClickListener(view -> {
            int quantity = Integer.valueOf(holder.tvQuantity.getText().toString());

            if (quantity > 1) {
                product.setQuantityToBuy(quantity - 1);
                onItemClickListener.onItemClicked("Delete" ,position);
                holder.tvQuantity.setText(String.valueOf(quantity - 1));
            }
            else if(quantity==1){
                holder.tvQuantity.setText("1");
                onItemClickListener.onItemClicked("DeleteLast" ,position);
            }
        });

        holder.ivIncrease.setOnClickListener(view -> {
            int quantity = Integer.valueOf(holder.tvQuantity.getText().toString());
            if (quantity < Constants.MAX_CART_QUANTITY) {
                onItemClickListener.onItemClicked("Add" ,position);
                product.setQuantityToBuy(quantity + 1);
                holder.tvQuantity.setText(String.valueOf(quantity + 1));
            }
        });

        holder.mView.setOnClickListener(view -> openProductDetail(product.getProductId()));
    }

    private void checkAndShowPrice(Product product, CartViewHolder holder) {
        PriceWhenStream priceWhenStream = product.getPriceWhenStream();
        if (priceWhenStream != null && priceWhenStream.isActive()) {
            showPrice(product, true, false, holder);
        } else {
            String discountStartTimeUTC = product.getStartedAt();
            String discountExpiredTimeUTC = product.getExpiredAt();
            if (!TextUtils.isEmpty(discountStartTimeUTC) && !TextUtils.isEmpty(discountExpiredTimeUTC)) {
                try {
                    String discountStartTime = DateTimeUtils.getDateFormatUTC(discountStartTimeUTC);
                    String discountExpiredTime = DateTimeUtils.getDateFormatUTC(discountExpiredTimeUTC);
                    Date discountStartDate = DateTimeUtils.getDateFollowFormat(discountStartTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date discountExpiredDate = DateTimeUtils.getDateFollowFormat(discountExpiredTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date currentDate = DateTimeUtils.getCurrentDate(true);

                    if (currentDate.compareTo(discountStartDate) >= 0 && currentDate.compareTo(discountExpiredDate) <= 0) {
                        if (!TextUtils.isEmpty(product.getDiscount()) && !product.getDiscount().equals("0%")) {
                            showPrice(product, false, true, holder);
                        } else {
                            showPrice(product, false, false, holder);
                        }
                    } else {
                        showPrice(product, false, false, holder);
                    }
                } catch (Exception e) {
                    AppLog.e(e.getMessage());
                }
            } else {
                showPrice(product, false, false, holder);
            }
        }
    }

    private void showPrice(Product product, boolean isShowPriceInStream, boolean isShowPriceSale, CartViewHolder holder) {
        String displayPrice = NumberUtils.formatPrice(product.getPrice());
        String displayPriceSale;
        String discount = "";

        holder.ivProductImageSaleOverlay.setVisibility(View.VISIBLE);
        holder.tvProductSaleOverlay.setVisibility(View.VISIBLE);

        if (isShowPriceInStream) {
            displayPriceSale = NumberUtils.formatPrice(product.getPriceWhenStream().getPriceDiscount());
            discount = product.getPriceWhenStream().getDiscount();

            holder.tvPrice.setText(getPriceSpan(displayPrice, displayPriceSale, false));

            holder.tvPrice.post(() -> {
                if (holder.tvPrice.getLineCount() > 1) {
                    // If show in 2 lines: Add line break for displaying price sale in new line
                    holder.tvPrice.setText(getPriceSpan(displayPrice, displayPriceSale, true));
                } else {
                    holder.tvPrice.setText(getPriceSpan(displayPrice, displayPriceSale, false));
                }
            });

            // Set payment price is discount price -> get & send to server discount price (ConfirmPaymentDialog)
            product.setPaymentPrice(product.getPriceWhenStream().getPriceDiscount());
        } else if (isShowPriceSale) {
            displayPriceSale = NumberUtils.formatPrice(product.getPriceSale());
            discount = product.getDiscount();

            holder.tvPrice.setText(getPriceSpan(displayPrice, displayPriceSale, false));

            holder.tvPrice.post(() -> {
                if (holder.tvPrice.getLineCount() > 1) {
                    // If show in 2 lines: Add line break for displaying price sale in new line
                    holder.tvPrice.setText(getPriceSpan(displayPrice, displayPriceSale, true));
                } else {
                    holder.tvPrice.setText(getPriceSpan(displayPrice, displayPriceSale, false));
                }
            });

            // Set payment price is discount price -> get & send to server discount price (ConfirmPaymentDialog)
            product.setPaymentPrice(product.getPriceSale());
        } else {
            // Set payment price is discount price -> get & send to server discount price (ConfirmPaymentDialog)
            product.setPaymentPrice(product.getPrice());
            holder.tvPrice.setText(displayPrice);
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.tvPrice.setTextColor(context.getResources().getColor(R.color.black));
            holder.tvPrice.setTypeface(holder.tvPrice.getTypeface(), Typeface.BOLD);

            holder.ivProductImageSaleOverlay.setVisibility(View.GONE);
            holder.tvProductSaleOverlay.setVisibility(View.GONE);
        }

        holder.tvPrice.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(discount)) {
            if (discount.contains("%")) {
                 String displayDiscount = "-" + discount;
                holder.tvProductSaleOverlay.setText(displayDiscount);
            } else {
                String displayDiscount = "-S$" + NumberUtils.formatPrice(Float.valueOf(discount)).replace("S$", "");
                holder.tvProductSaleOverlay.setText(displayDiscount);

                String finalDiscount = discount;
                holder.tvProductSaleOverlay.post(() -> {
                    if (holder.tvProductSaleOverlay.getLineCount() > 1) {
                        // If show in 2 lines: Add line break for displaying price sale in new line
                        String displayFinalDiscount = "-S$\n" + NumberUtils.formatPrice(Float.valueOf(finalDiscount)).replace("S$", "");
                        ;
                        holder.tvProductSaleOverlay.setText(displayFinalDiscount);
                    }
                });
            }
        }
    }

    private SpannableStringBuilder getPriceSpan(String displayPrice, String displayPriceSale, boolean isPriceSaleInNewLine) {
        int priceSize = (int) context.getResources().getDimension(R.dimen.text_size_my_cart_product_price);
        int priceSaleSize = (int) context.getResources().getDimension(R.dimen.text_size_my_cart_product_price_large);
        String separator = "   ";
        String spanPrice = displayPrice + separator + displayPriceSale;
        SpannableStringBuilder ssb = new SpannableStringBuilder(spanPrice);

        // Set span for base price in sale
        ssb.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorBody)), 0, displayPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new StrikethroughSpan(), 0, displayPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new AbsoluteSizeSpan(priceSize), 0, displayPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Set span for price sale
        ssb.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.color_product_price_sale)), displayPrice.length(), spanPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new AbsoluteSizeSpan(priceSaleSize), displayPrice.length(), spanPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), displayPrice.length(), spanPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (isPriceSaleInNewLine) {
            // If show in 2 lines: Add line break for displaying price sale in new line
            ssb.replace(displayPrice.length() + separator.length(), displayPrice.length() + separator.length(), " \n");
        }
        return ssb;
    }

    private void openProductDetail(String productId) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_PRODUCT_ID, productId);
        args.putBoolean(Constants.EXTRA_IS_HIDE_ADD_TO_CART, true);
        intent.putExtras(args);
        // Sender usage
        DetailType.PRODUCT.attachTo(intent);
        context.startActivity(intent);
    }

    public List<Product> getListProduct() {
        List<Product> products = new ArrayList<>();
        for (Object object : list) {
            Product product = (Product) object;
            products.add(product);
        }
        return products;
    }

    public Product getProduct(int position) {
        return (Product) list.get(position);
    }
}
