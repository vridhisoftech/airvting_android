package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.activities.PhotoFilterActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMenuDialog extends DialogFragment {

    private Context context;

    @OnClick(R.id.llBackground)
    void close() {
        dismiss();
    }

    @OnClick(R.id.mrlCamera)
    void openPhotoDialog() {
        AddPhotoDialog addPhotoDialog = AddPhotoDialog.newInstance();
        addPhotoDialog.setReturnImageListener((data) -> {
            Intent intent = new Intent(context, PhotoFilterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Bundle args = new Bundle();
            args.putString(Constants.EXTRA_IMAGE_URI, data.toString());
            intent.putExtras(args);
            context.startActivity(intent);
            addPhotoDialog.dismiss();
        });
        addPhotoDialog.show(getFragmentManager(), "Add Photo Dialog");
        dismiss();
    }

    @OnClick(R.id.mrlVideo)
    void openVideoDialog() {
        AddVideoDialog addVideoDialog = AddVideoDialog.newInstance();
        addVideoDialog.show(getFragmentManager(), "Add Video Dialog");
        dismiss();
    }

    @OnClick(R.id.mrlLive)
    void openLiveDialog() {
        Intent intent = new Intent(getActivity(), LiveStreamActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, "");
        args.putString(Constants.EXTRA_POST_ID, "");
        args.putString(Constants.EXTRA_MEDIA_URL, "");
        intent.putExtras(args);
        startActivity(intent);
        dismiss();
    }

    public static AddMenuDialog newInstance() {
        return new AddMenuDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        context = getActivity();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }
}
