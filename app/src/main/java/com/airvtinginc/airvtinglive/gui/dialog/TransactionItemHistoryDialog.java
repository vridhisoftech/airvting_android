package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.tools.NumberUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransactionItemHistoryDialog extends DialogFragment {

    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.btnClose)
    ImageButton btnClose;
    @BindView(R.id.tvPriceAirToken)
    TextView tvPriceAirToken;
    @BindView(R.id.tvTotalItem)
    TextView tvTotalItem;
    @BindView(R.id.tvPrice)
    TextView tvPrice;

    private RecyclerView.Adapter<?> adapter;
    private RecyclerView.LayoutManager layoutManager;
    private int priceAirToken;
    private float price;
    private int totalQuantity;
    private OnDismissListener onDismissListener;

    @OnClick(R.id.btnClose)
    void close() {
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        } else {
            dismiss();
        }
    }

    public static TransactionItemHistoryDialog newInstance() {
        return new TransactionItemHistoryDialog();
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    public OnDismissListener getOnDismissListener() {
        return onDismissListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_transaction_item_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        updateUI();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void updateUI() {
        setPriceAirTokenUI();
        setPriceUI();
        setTotalQuantityUI();
        setRecyclerViewUI();
    }

    private void setPriceAirTokenUI() {
        tvPriceAirToken.setText(String.valueOf(priceAirToken));
    }

    private void setPriceUI() {
        tvPrice.setText(NumberUtils.formatPrice(price));
    }

    private void setTotalQuantityUI() {
        tvTotalItem.setText("(" + totalQuantity + ")");
    }

    private void setRecyclerViewUI() {
        if (layoutManager != null) {
            mRecyclerView.setLayoutManager(layoutManager);
        }
        if (adapter != null) {
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setBackgroundColor(getResources().getColor(R.color.white));
        }
    }

    public void setAdapter(RecyclerView.Adapter<?> adapter, @Nullable RecyclerView.LayoutManager layoutManager) {
        if (layoutManager != null
                && !(layoutManager instanceof LinearLayoutManager)
                && !(layoutManager instanceof GridLayoutManager)) {
            throw new IllegalStateException(
                    "You can currently only use LinearLayoutManager"
                            + " and GridLayoutManager with this library.");
        }
        this.adapter = adapter;
        this.layoutManager = layoutManager;
    }

    public void setPriceAirToken(int priceAirToken) {
        this.priceAirToken = priceAirToken;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public interface OnDismissListener {
        void onDismiss();
    }
}