package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChoseTextHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.row_text_ll_bound)
    LinearLayout llBackground;
    public @BindView(R.id.row_text_tv_text)
    TextView tvText;
    public @BindView(R.id.row_text_iv_check)
    ImageView ivCheck;

    public ChoseTextHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}