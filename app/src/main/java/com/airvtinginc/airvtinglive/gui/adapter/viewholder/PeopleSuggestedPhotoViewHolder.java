package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleSuggestedPhotoViewHolder extends BaseViewHolder {
    public View mView;
    @BindView(R.id.ivPhoto)
    public RoundedImageView ivPhoto;
    @BindView(R.id.ivPostVideo)
    public ImageView ivPostVideo;
    @BindView(R.id.ivLive)
    public ImageView ivLive;
    @BindView(R.id.tvDefaultAvatar)
    public TextView tvDefaultAvatar;
    @BindView(R.id.pbMedia)
    public ProgressBar pbMedia;

    public PeopleSuggestedPhotoViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}
