package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.TransactionHolder;
import com.airvtinginc.airvtinglive.gui.enums.TransactionItemType;
import com.airvtinginc.airvtinglive.models.response.TransactionItem;
import com.airvtinginc.airvtinglive.models.response.TransactionItemHistory;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Date;
import java.util.List;

public class TransactionAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    public TransactionAdapter(Context context, List<TransactionItem> transactionItems, OnItemClickListener onItemClickListener) {
        super(context, transactionItems, onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_transaction, parent, false);
                TransactionHolder vh = new TransactionHolder(v);
                return vh;
            }
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder)
            return;
        if (list.get(position) == null) return;
        TransactionHolder holder = (TransactionHolder) mHolder;
        TransactionItem transactionItem = (TransactionItem) list.get(position);

        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(transactionItem, position);
        });
        if (transactionItem.getType().equals(TransactionItemType.PRODUCT.getType())) {
            holder.tvPrice.setVisibility(View.VISIBLE);
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvPriceAirToken.setVisibility(View.VISIBLE);
            if (transactionItem.getProducts().size() <= 0) {
                return;
            }
            holder.ivProduct.setBorderWidth((float) 0);
            holder.ivProduct.setBorderColor(context.getResources().getColor(R.color.color_border_gift));
            if (transactionItem.getProducts().size() > 1) {
                holder.ivProduct.setImageResource(R.drawable.ic_cart_grey);
                holder.tvUserName.setVisibility(View.GONE);
                holder.tvQuantity.setVisibility(View.GONE);
                String title = "";
                String separator = ", ";
                for (TransactionItemHistory transactionItemHistory : transactionItem.getProducts()) {
                    title += transactionItemHistory.getTitle() + separator;
                }
                holder.tvTitle.setText(title.substring(0, title.length() - separator.length()));
            } else {
                TransactionItemHistory transactionItemHistory = transactionItem.getProducts().get(0);
                holder.ivProduct.setImageResource(android.R.color.transparent);
                if (!TextUtils.isEmpty(transactionItemHistory.getFeaturedImage())) {
                    holder.pbProduct.setVisibility(View.VISIBLE);
                    Glide.with(context).load(transactionItemHistory.getFeaturedImage())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    holder.pbProduct.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    holder.pbProduct.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(holder.ivProduct);
                } else {
                }
                holder.tvUserName.setVisibility(View.VISIBLE);
                holder.tvQuantity.setVisibility(View.VISIBLE);
                holder.tvUserName.setText(transactionItemHistory.getDisplayName());
                holder.tvTitle.setText(transactionItemHistory.getTitle());
                holder.tvQuantity.setText(String.valueOf(transactionItem.getTotalQuantity()));
            }
            holder.tvStatus.setText(transactionItem.getTransaction());
            holder.tvPrice.setText(NumberUtils.formatPrice((transactionItem.getTotalPrice())));
            holder.tvPriceAirToken.setText(String.valueOf(transactionItem.getTotalToken()));
        } else if (transactionItem.getType().equals(TransactionItemType.GIFT.getType())) {
            holder.tvUserName.setVisibility(View.GONE);
            holder.tvQuantity.setVisibility(View.VISIBLE);
            holder.tvPrice.setVisibility(View.GONE);
            holder.tvStatus.setVisibility(View.GONE);
            holder.tvPriceAirToken.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(transactionItem.getGift().getTitle());
            holder.tvPriceAirToken.setText(String.valueOf(transactionItem.getTotalToken()));
            holder.tvQuantity.setText(String.valueOf(transactionItem.getTotalQuantity()));
            holder.ivProduct.setImageResource(android.R.color.transparent);
            holder.ivProduct.setBorderWidth((float) 2.0);
            holder.ivProduct.setBorderColor(context.getResources().getColor(R.color.color_border_gift));
            if (!TextUtils.isEmpty(transactionItem.getGift().getFeaturedImage())) {
                holder.pbProduct.setVisibility(View.VISIBLE);
                Glide.with(context).load(transactionItem.getGift().getFeaturedImage())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbProduct.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbProduct.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.ivProduct);
            } else {
            }
        } else if (transactionItem.getType().equals(TransactionItemType.AIR_TOKEN.getType())) {
            holder.tvUserName.setVisibility(View.GONE);
            holder.tvQuantity.setVisibility(View.VISIBLE);
            holder.tvPrice.setVisibility(View.VISIBLE);
            holder.tvPriceAirToken.setVisibility(View.GONE);
            holder.tvStatus.setVisibility(View.GONE);
            holder.tvTitle.setText(context.getString(R.string.wallet_token));
            holder.tvPrice.setText(NumberUtils.formatPriceByCurrency((transactionItem.getTotalPrice()), transactionItem.getCurrency()));
            holder.tvQuantity.setText(String.valueOf(transactionItem.getTotalQuantity()));
            holder.ivProduct.setImageResource(R.drawable.ic_token_small);
        }

        try {
            holder.tvDateFull.setText(DateTimeUtils.convertStringDateToOtherFormat(DateTimeUtils.getDateFormatUTC(transactionItem.getCreatedAt()), DateTimeUtils.DATE_TIME_FORMAT, DateTimeUtils.DATE_FORMAT));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String currentBookmarkDateString = transactionItem.getCreatedAt();
        try {
            holder.tvDate.setText(DateTimeUtils.getDisplayBookmarkTime(context, DateTimeUtils.getDateFormatUTC(currentBookmarkDateString)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (position > 0) {
            String previousBookmarkDateString = ((TransactionItem) list.get(position - 1)).getCreatedAt();
            Date currentBookMarkDate = DateTimeUtils.getDateFollowFormat(currentBookmarkDateString, DateTimeUtils.DATE_TIME_FORMAT);
            Date previousBookMarkDate = DateTimeUtils.getDateFollowFormat(previousBookmarkDateString, DateTimeUtils.DATE_TIME_FORMAT);

            if (DateTimeUtils.isSameDate(currentBookMarkDate, previousBookMarkDate)) {
                holder.tvDate.setVisibility(View.GONE);
            } else {
                holder.tvDate.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tvDate.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }
}
