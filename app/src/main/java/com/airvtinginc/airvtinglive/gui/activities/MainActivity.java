package com.airvtinginc.airvtinglive.gui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.services.FireStoreManager;
import com.airvtinginc.airvtinglive.components.services.MainService;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.gui.components.NonSwipeableViewPager;
import com.airvtinginc.airvtinglive.gui.dialog.AddMenuDialog;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.dialog.SearchDialog;
import com.airvtinginc.airvtinglive.gui.dialog.WebViewDialog;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.gui.fragments.ExploreFragment;
import com.airvtinginc.airvtinglive.gui.fragments.HomeFragment;
import com.airvtinginc.airvtinglive.gui.fragments.ProfileFragment;
import com.airvtinginc.airvtinglive.gui.fragments.StoreFragment;
import com.airvtinginc.airvtinglive.models.request.SendFCMTokenRequest;
import com.airvtinginc.airvtinglive.models.response.LeftMenuResponse;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lightfire.gradienttextcolor.GradientTextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.airvtinginc.airvtinglive.app.MainApplication.getContext;

public class MainActivity extends BaseActivity implements APIResponseListener {

    private static final String TAG = MainActivity.class.getName();

    public static final String BroadcastActionHideDialog = "com.airvtinginc.airvting.broadcast.action.hide.dialog";
    public static final String BroadcastActionGoToProfileTab = "com.airvtinginc.airvting.broadcast.action.go.to.profile.tab";
    public static final String BroadcastActionGoToStoreTab = "com.airvtinginc.airvting.broadcast.action.go.to.store.tab";
    public static final String BroadcastActionUpdateMainBarTitle = "com.airvtinginc.airvting.broadcast.action.ipdate.main.bar.title";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_main_bar_title)
    TextView tvMainBarTitle;
    @BindView(R.id.mViewPager)
    NonSwipeableViewPager mViewPager;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.ivAvatar)
    CircleImageView civAvatar;
    @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;
    @BindView(R.id.pbLoadAvatar)
    ProgressBar pbAvatar;
    @BindView(R.id.nav_header_tv_name)
    TextView tvName;
    @BindView(R.id.nav_header_tv_username)
    TextView tvUsername;
    @BindView(R.id.nav_header_ib_setting)
    ImageButton ibSetting;
    @BindView(R.id.nav_menu_ll_gift_store)
    LinearLayout llGiftStore;
    @BindView(R.id.nav_menu_ll_cart)
    LinearLayout llMyCart;
    @BindView(R.id.nav_menu_ll_card)
    LinearLayout llMenuCard;
    @BindView(R.id.nav_menu_ll_my_bookmark)
    LinearLayout llMyBookmark;
    @BindView(R.id.nav_menu_ll_message)
    LinearLayout llMessage;
    @BindView(R.id.nav_menu_rl_badge_message)
    RelativeLayout rlBadgeMessage;
    @BindView(R.id.nav_menu_tv_badge_message_single)
    TextView tvBadgeMessageSingle;
    @BindView(R.id.nav_menu_tv_badge_message_multi)
    TextView tvBadgeMessageMulti;
    @BindView(R.id.nav_menu_rl_badge_cart)
    RelativeLayout rlBadgeCart;
    @BindView(R.id.nav_menu_tv_badge_cart_single)
    TextView tvBadgeCartSingle;
    @BindView(R.id.nav_menu_tv_badge_cart_multi)
    TextView tvBadgeCartMulti;
    @BindView(R.id.nav_menu_ll_wallets)
    LinearLayout llWallets;
    @BindView(R.id.nav_menu_ll_help)
    LinearLayout llHelpAndFAQ;
    @BindView(R.id.nav_menu_ll_terms)
    LinearLayout llTermsAndPrivacy;
    @BindView(R.id.nav_footer_ll_logout)
    LinearLayout llLogout;
    @BindView(R.id.bottom_nav_ll_home)
    LinearLayout llMenuHome;
    @BindView(R.id.bottom_nav_ll_explore)
    LinearLayout llMenuExplore;
    @BindView(R.id.bottom_nav_ll_add)
    LinearLayout llMenuAdd;
    @BindView(R.id.bottom_nav_ll_cart)
    LinearLayout llMenuCart;
    @BindView(R.id.bottom_nav_ll_user)
    LinearLayout llMenuUser;
    @BindView(R.id.bottom_nav_iv_home)
    ImageView ivBottomHome;
    @BindView(R.id.bottom_nav_iv_explore)
    ImageView ivBottomExplore;
    @BindView(R.id.bottom_nav_iv_cart)
    ImageView ivBottomCart;
    @BindView(R.id.bottom_nav_iv_user)
    ImageView ivBottomUser;

    @OnClick(R.id.rl_nav_header_civ_avatar)
    void avatartLeftMenuClick() {
        drawer.closeDrawers();
        goToProfileTab();
    }

    private MenuItem miSearch, miNotification, miAdd;
    private boolean mIsDoubleTapToLogout;
    private BroadcastReceiver mReceiver;
    public static int mNumberOfCartItems = 0;
    private PageFragmentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        signInAnonymously();

        Intent mainService = new Intent(this, MainService.class);
        startService(mainService);

        String fcmToken = SharedPreferencesManager.getInstance(this).getString(Constants.PREF_CURRENT_FCM_TOKEN, "");
        if (!TextUtils.isEmpty(fcmToken)) {
            SendFCMTokenRequest modelRequest = new SendFCMTokenRequest();
            modelRequest.setToken(fcmToken);
            requestApi(modelRequest, false, RequestTarget.SEND_FCM_TOKEN, this);
        }

        DialogUtils.showLoadingProgress(this, false);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        prepareViewPager();
        setupLeftMenuNavigation();
        setUpBottomNavigation();
        try {
            registerReceiver();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void prepareViewPager() {
        mAdapter = new PageFragmentAdapter(getSupportFragmentManager());
        mAdapter.addFragment(HomeFragment.newInstance(), null);
        mAdapter.addFragment(ExploreFragment.newInstance(), null);
        mAdapter.addFragment(StoreFragment.newInstance(), null);
        mAdapter.addFragment(ProfileFragment.newInstance("", true), null);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(3);
    }

    public void goToProfileTab() {
        bottomMenuClick(EnumManager.MenuType.User.ordinal());
    }

    public void goToStoreTab() {
        bottomMenuClick(EnumManager.MenuType.Cart.ordinal());
        if (mAdapter != null && mAdapter.getItem(2) != null) {
            ((StoreFragment) mAdapter.getItem(2)).refresh();
        }
    }

    private void setupLeftMenuNavigation() {
        setNameAndAvatar();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                rlBadgeCart.setVisibility(View.GONE);
                rlBadgeMessage.setVisibility(View.GONE);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                updateLeftMenu();
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        ibSetting.setOnClickListener(view -> {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailType.SETTING.attachTo(intent);
            startActivity(intent);
        });
        llGiftStore.setOnClickListener(view -> {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailType.GIFT_STORE.attachTo(intent);
            startActivity(intent);
        });
        llMyCart.setOnClickListener(view -> {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailType.MY_CART.attachTo(intent);
            startActivity(intent);
        });
        llMenuCard.setOnClickListener(v -> new Handler().postDelayed(() -> {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailType.CARD_LIST.attachTo(intent);
            startActivity(intent);
        }, 300));
        llMyBookmark.setOnClickListener(view -> {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailType.BOOKMARK.attachTo(intent);
            startActivity(intent);
        });
        llMessage.setOnClickListener(view -> new Handler().postDelayed(() -> {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailType.CONVERSATIONS_LIST.attachTo(intent);
            startActivity(intent);
        }, 300));
        llWallets.setOnClickListener(view -> {
            Intent intent = new Intent(this, DetailActivity.class);
            DetailType.WALLET.attachTo(intent);
            startActivity(intent);
        });
        llHelpAndFAQ.setOnClickListener(view -> {
            WebViewDialog dialog = WebViewDialog.newInstance(ServiceConstants.URL_HELP_AND_FAQ,
                    getString(R.string.navigation_drawer_menu_help_faq));
            dialog.show(getSupportFragmentManager(), "Help and FAQ Dialog");
        });
        llTermsAndPrivacy.setOnClickListener(view -> {
//            WebViewDialog dialog = WebViewDialog.newInstance(ServiceConstants.URL_TERMS_AND_CONDITIONS,
//                    getString(R.string.navigation_drawer_menu_terms_condition));
//            dialog.show(getSupportFragmentManager(), "Terms and Privacy Dialog");
            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
            DetailType.TERMS_CONDITIONS.attachTo(intent);
            startActivity(intent);
        });
        llLogout.setOnClickListener(view -> showLogoutConfirmationDialog());
    }

    private void setNameAndAvatar() {
        String displayName = SharedPreferencesManager.getInstance(MainActivity.this).getString(Constants.PREF_CURRENT_USER_DISPLAY_NAME, "");
        String firstName = SharedPreferencesManager.getInstance(MainActivity.this).getString(Constants.PREF_CURRENT_USER_FIRST_NAME, "");
        String lastName = SharedPreferencesManager.getInstance(MainActivity.this).getString(Constants.PREF_CURRENT_USER_LAST_NAME, "");
        String username = SharedPreferencesManager.getInstance(MainActivity.this).getString(Constants.PREF_CURRENT_USER_USERNAME, "");
        String avatarUrl = SharedPreferencesManager.getInstance(MainActivity.this).getString(Constants.PREF_CURRENT_USER_PHOTO_URL, "");

        tvName.setText(getName(firstName, lastName, displayName));
        tvUsername.setText(new StringBuilder().append("@").append(username));

        if (!TextUtils.isEmpty(avatarUrl)) {
            pbAvatar.setVisibility(View.VISIBLE);
            civAvatar.setVisibility(View.VISIBLE);
            Glide.with(MainActivity.this).load(avatarUrl)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            User user = new User();
                            user.setDisplayName(displayName);
                            user.setFirstName(firstName);
                            user.setLastName(lastName);

                            pbAvatar.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(getApplicationContext(), ivDefaultAvatar, tvDefaultAvatar, civAvatar, user, R.dimen.profile_default_avatar_text_size);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbAvatar.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(getApplicationContext(), ivDefaultAvatar, tvDefaultAvatar, civAvatar);
                            return false;
                        }
                    })
                    .into(civAvatar);

        } else {
            User user = new User();
            user.setDisplayName(displayName);
            user.setFirstName(firstName);
            user.setLastName(lastName);

            pbAvatar.setVisibility(View.GONE);
            Utils.setDefaultAvatar(getApplicationContext(), ivDefaultAvatar, tvDefaultAvatar, civAvatar, user, R.dimen.profile_default_avatar_text_size);
        }
    }

    private String getName(String firstName, String lastName, String displayName) {
        StringBuilder name = new StringBuilder();
        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName)) {
            name.append(displayName);
        } else if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)) {
            name.append(firstName).append(" ").append(lastName);
        } else {
            if (!TextUtils.isEmpty(firstName)) {
                name.append(firstName);
            }

            if (!TextUtils.isEmpty(lastName)) {
                name.append(lastName);
            }
        }

        return name.toString();
    }

    private void setUpBottomNavigation() {
        bottomMenuClick(EnumManager.MenuType.Home.ordinal());

        llMenuHome.setOnClickListener(view -> bottomMenuClick(EnumManager.MenuType.Home.ordinal()));
        llMenuExplore.setOnClickListener(view -> bottomMenuClick(EnumManager.MenuType.Explore.ordinal()));
        llMenuAdd.setOnClickListener(view -> bottomMenuClick(EnumManager.MenuType.Add.ordinal()));
        llMenuCart.setOnClickListener(view -> bottomMenuClick(EnumManager.MenuType.Cart.ordinal()));
        llMenuUser.setOnClickListener(view -> bottomMenuClick(EnumManager.MenuType.User.ordinal()));
    }

    private void bottomMenuClick(int index) {
        //Don't reset bottom menu when show Add dialog
        if (index != EnumManager.MenuType.Add.ordinal()) {
            resetMenuItem();
        }

        EnumManager.MenuType menuType = EnumManager.MenuType.values()[index];
        switch (menuType) {
            case Home:
                ivBottomHome.setImageResource(R.drawable.ic_menu_home_selected);
                changeMenuIconTint(Color.WHITE);
                mViewPager.setCurrentItem(0);
                tvMainBarTitle.setVisibility(View.GONE);
                break;
            case Explore:
                ivBottomExplore.setImageResource(R.drawable.ic_explore_selected);
                changeMenuIconTint(Color.BLACK);
                mViewPager.setCurrentItem(1);
                tvMainBarTitle.setVisibility(View.GONE);
                break;
            case Add:
                AddMenuDialog addMenuDialog = AddMenuDialog.newInstance();
                addMenuDialog.show(getSupportFragmentManager(), "Add Menu Dialog");
                break;
            case Cart:
                ivBottomCart.setImageResource(R.drawable.ic_store_selected);
                changeMenuIconTint(Color.BLACK);
                mViewPager.setCurrentItem(2);
                tvMainBarTitle.setVisibility(View.GONE);
                break;
            case User:
                ivBottomUser.setImageResource(R.drawable.ic_menu_user_selected);
                changeMenuIconTint(Color.WHITE);
                mViewPager.setCurrentItem(3);
                tvMainBarTitle.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void resetMenuItem() {
        ivBottomHome.setImageResource(R.drawable.ic_menu_home);
        ivBottomExplore.setImageResource(R.drawable.ic_menu_explore);
        ivBottomCart.setImageResource(R.drawable.ic_menu_store);
        ivBottomUser.setImageResource(R.drawable.ic_menu_user);
//        Glide.with(getApplicationContext()).load(R.drawable.ic_menu_home).into(ivBottomHome);
//        Glide.with(getApplicationContext()).load(R.drawable.ic_menu_explore).into(ivBottomExplore);
//        Glide.with(getApplicationContext()).load(R.drawable.ic_menu_store).into(ivBottomCart);
//        Glide.with(getApplicationContext()).load(R.drawable.ic_menu_user).into(ivBottomUser);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (mIsDoubleTapToLogout) {
                showExitAppDialog();
            }

            mIsDoubleTapToLogout = true;
            Toast.makeText(this, "Tap Again To Exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> {
                mIsDoubleTapToLogout = false;
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        miNotification = menu.findItem(R.id.action_notification);
        miSearch = menu.findItem(R.id.action_search);
        changeMenuIconTint(Color.WHITE);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notification:
                startActivity(new Intent(this, NotificationsActivity.class));
                break;
            case R.id.action_search:
                SearchDialog dialog = new SearchDialog();
                dialog.show(getSupportFragmentManager(), "Search");
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeMenuIconTint(int color) {
        // change burger icon tint
        final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            final View v = toolbar.getChildAt(i);
            if (v instanceof ImageButton) {
                ((ImageButton) v).setColorFilter(colorFilter);
            }
        }
        if (miNotification != null) {
            Drawable drawableCart = miNotification.getIcon();
            drawableCart = DrawableCompat.wrap(drawableCart);
            DrawableCompat.setTint(drawableCart, color);
            miNotification.setIcon(drawableCart);
        }

        if (miSearch != null) {
            Drawable drawableSearch = miSearch.getIcon();
            drawableSearch = DrawableCompat.wrap(drawableSearch);
            DrawableCompat.setTint(drawableSearch, color);
            miSearch.setIcon(drawableSearch);
        }
    }

    private void showLogoutConfirmationDialog() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.LOG_OUT);
        messageDialog.setDialogTitle(getString(R.string.dialog_logout));
        messageDialog.setDialogMessage(getString(R.string.logout_confirm_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_logout));
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                logout();
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {

            }
        });
        messageDialog.show(getSupportFragmentManager(), "Show Confirm Logout Dialog");
    }


    private void showExitAppDialog() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.LOG_OUT);
        messageDialog.setDialogTitle("AirVTing");
        messageDialog.setDialogMessage("Are you sure you want to exit?");
        messageDialog.setDialogPositiveButtonTitle("Yes");
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                finish();
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {

            }
        });
        messageDialog.show(getSupportFragmentManager(), "Show Confirm Exit Dialog");
    }

    private void logout() {
        requestApi(null, true, RequestTarget.LOG_OUT, MainActivity.this);
    }

    private void handleLogout(String message) {
//        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Utils.removeCurrentUserInfoFromPreferences(this);
        Utils.removeEmailAndPaymentInfo(this);
        Utils.resetNotificationSetting(this);
        clearCart();
        setResult(RESULT_OK);

        Intent intent = new Intent(this, WelcomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void clearCart() {
        Realm realm = Utils.getCartRealm(this);
        realm.close();
        Realm.deleteRealm(Utils.getCartRealmConfiguration(this));
    }

    private void updateCartItemsNumber() {
        int cartItems = getCartItems();
        if (cartItems > 0) {
            if (cartItems < 10) {
                tvBadgeCartSingle.setText(String.valueOf(cartItems));
                tvBadgeCartSingle.setVisibility(View.VISIBLE);
                tvBadgeCartMulti.setVisibility(View.GONE);
            } else {
                tvBadgeCartMulti.setText(String.valueOf(cartItems));
                tvBadgeCartMulti.setVisibility(View.VISIBLE);
                tvBadgeCartSingle.setVisibility(View.GONE);
            }

            rlBadgeCart.setVisibility(View.VISIBLE);
        } else {
            rlBadgeCart.setVisibility(View.GONE);
        }
    }

    private void updateUnreadMessages(int unreadMessages) {
        if (unreadMessages > 0) {
            if (unreadMessages < 10) {
                tvBadgeMessageSingle.setText(String.valueOf(unreadMessages));
                tvBadgeMessageSingle.setVisibility(View.VISIBLE);
                tvBadgeMessageMulti.setVisibility(View.GONE);
            } else {
                tvBadgeMessageMulti.setText(String.valueOf(unreadMessages));
                tvBadgeMessageMulti.setVisibility(View.VISIBLE);
                tvBadgeMessageSingle.setVisibility(View.GONE);
            }
            rlBadgeMessage.setVisibility(View.VISIBLE);
        } else {
            rlBadgeMessage.setVisibility(View.GONE);
        }
    }

    private int getCartItems() {
        Realm realm = Utils.getCartRealm(this);
        realm.beginTransaction();
        RealmResults<Product> realmProducts = realm.where(Product.class).findAll();
        int quantity = 0;
        if (realmProducts != null && !realmProducts.isEmpty()) {
            for (Product countProduct : realmProducts) {
                quantity += countProduct.getQuantityToBuy();
            }
        }
        realm.commitTransaction();

        return quantity;
    }

    private void registerReceiver() {
        // create new receiver
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (Objects.requireNonNull(intent.getAction())) {
                        case BroadcastActionHideDialog:
                            //We will dismiss all dialog after add new post success
                            dismissAllDialogs(getSupportFragmentManager());
                            break;
                        case BroadcastActionGoToProfileTab:
                            goToProfileTab();
                            break;
                        case BroadcastActionGoToStoreTab:
                            goToStoreTab();

                            break;
                        case BroadcastActionUpdateMainBarTitle:
                            if (intent.getExtras() != null) {
                                // Profile screen
                                String displayName = intent.getStringExtra(Constants.EXTRA_TITLE);
                                tvMainBarTitle.setText(displayName);
                            }
                            break;
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(BroadcastActionHideDialog);
            filter.addAction(BroadcastActionGoToProfileTab);
            filter.addAction(BroadcastActionUpdateMainBarTitle);
            registerReceiver(mReceiver, filter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("mainActivity", "onResume: ");
        Utils.hideSoftKeyboard(this);
        checkBackgroundNotification();
        updateLeftMenu();
//        dismissAllDialogs(getSupportFragmentManager());
    }

    private void updateLeftMenu() {
        setNameAndAvatar();
        updateCartItemsNumber();
        requestApi(null, false, RequestTarget.GET_LEFT_MENU_INFO, MainActivity.this);
    }

    private void checkBackgroundNotification() {
        String notificationData = SharedPreferencesManager.getInstance(this).getString(Constants.PREF_NOTIFICATION, "");
        if (!TextUtils.isEmpty(notificationData)) {
            SharedPreferencesManager.getInstance(this).putString(Constants.PREF_NOTIFICATION, "");
            Intent intent = new Intent(this, NotificationsActivity.class);
            intent.putExtra(ServiceConstants.NOTIFICATION, notificationData);
            startActivity(intent);
        }
    }

    public void dismissAllDialogs(FragmentManager manager) {
        List<Fragment> fragments = manager.getFragments();

        if (fragments == null)
            return;

        for (Fragment fragment : fragments) {
            if (fragment instanceof DialogFragment) {
                DialogFragment dialogFragment = (DialogFragment) fragment;
                dialogFragment.dismissAllowingStateLoss();
            }

            FragmentManager childFragmentManager = fragment.getChildFragmentManager();
            if (childFragmentManager != null)
                dismissAllDialogs(childFragmentManager);
        }
    }


    private void signInAnonymously() {
        FirebaseAuth.getInstance().signInAnonymously()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInAnonymously:success");
                        FireStoreManager.getInstance().getConfig(getApplicationContext());
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInAnonymously:failure", task.getException());
                    }
                });
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        if (response.isSuccess()) {
            switch (requestTarget) {
                case LOG_OUT:
                    if (response.isSuccess()) {
                        handleLogout("");
                        LoginManager.getInstance().logOut();
                    } else {
                        Log.e(TAG, "LOG_OUT: fail");
                    }
                    break;
                case GET_LEFT_MENU_INFO:
                    int unreadMessages = ((LeftMenuResponse) response.getData()).getTotalUnReadCountMessages();
                    updateUnreadMessages(unreadMessages);
                    break;
                case SEND_FCM_TOKEN:
                    UserResponse userResponse = (UserResponse) response.getData();
//                    Log.d(TAG, "Send FCM token to server successfully: " + userResponse.getUser().getFcmToken());

                    break;
            }
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        switch (statusCode) {
            case ERR_NO_INTERNET_CONNECTION:
//                DialogUtils.showMessageDialog(this, getSupportFragmentManager(), getString(R.string.error_network_title), failMessage);
                break;
            case ERR_UNAUTHORIZED:
                DialogUtils.showUnauthorizedDialog(getContext(), getSupportFragmentManager(), failMessage);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setIsOnline(true, false);
    }

    public void setIsOnline(boolean isOnline, boolean isCreated) {
        String id = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (TextUtils.isEmpty(id)) {
            return;
        }
        CollectionReference userRef = FirebaseFirestore.getInstance().collection(Constants.COLLECTION_USERS);
        Map<String, Object> post = new HashMap<>();
        post.put(Constants.FIELD_IS_ONLINE, isOnline);

        if (isCreated) {
            userRef.document(id).set(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "DocumentSnapshot set");
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "Error adding document", e));
        } else {
            userRef.document(id).update(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "DocumentSnapshot set");
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "Error adding document", e);
                        setIsOnline(isOnline, true);
                    });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.hideSoftKeyboard(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}