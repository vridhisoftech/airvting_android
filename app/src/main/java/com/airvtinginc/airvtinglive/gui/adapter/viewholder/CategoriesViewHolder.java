package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.tvCategoryName)
    TextView tvCategoryName;
    public @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    public @BindView(R.id.emptyView)
    LinearLayout emptyView;
    public @BindView(R.id.tvShowEmpty)
    TextView tvShowEmpty;

    public CategoriesViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}