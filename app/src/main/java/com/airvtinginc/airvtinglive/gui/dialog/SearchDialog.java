package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.LiveStreamHorizontalAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ProductsAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.SearchPeopleAdapter;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.SearchType;
import com.airvtinginc.airvtinglive.models.request.SearchModelRequest;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.SearchModelResponse;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class SearchDialog extends BaseFragmentDialog {

    private static final String TAG = SearchDialog.class.getSimpleName();

    @BindView(R.id.dialog_search_btn_cancel)
    Button btnCancel;
    @BindView(R.id.dialog_search_et_search)
    EditText etSearch;
    @BindView(R.id.dialog_search_ll_header_people)
    LinearLayout llHeaderPeople;
    @BindView(R.id.dialog_search_tv_search_people)
    TextView tvSearchPeople;
    @BindView(R.id.dialog_search_rv_search_people)
    RecyclerView rvSearchPeople;
    @BindView(R.id.dialog_search_ll_header_product)
    LinearLayout llHeaderProduct;
    @BindView(R.id.dialog_search_tv_search_product)
    TextView tvSearchProduct;
    @BindView(R.id.dialog_search_rv_search_product)
    RecyclerView rvSearchProduct;
    @BindView(R.id.dialog_search_ll_header_post)
    LinearLayout llHeaderPost;
    @BindView(R.id.dialog_search_tv_search_post)
    TextView tvSearchPost;
    @BindView(R.id.dialog_search_rv_search_post)
    RecyclerView rvSearchPost;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;

    private SearchPeopleAdapter peopleAdapter;
    private ProductsAdapter productAdapter;
    private LiveStreamHorizontalAdapter postAdapter;

    private List<User> userList;
    private List<Product> productList;
    private List<PostDetail> postDetailList;
    private String searchString;
    private int perPage = 20;
    private int paginatePeople = 1, paginateProduct = 1, paginatePost = 1;
    private int maxIdPeople, maxIdProduct, maxIdPost;
    private String textSearch;

    public void setTextSearch(String textSearch) {
        this.textSearch = textSearch;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        emptyView.setVisibility(View.VISIBLE);
        Utils.setUpRecycleViewHorizontal(getContext(), rvSearchPeople);
        Utils.setUpRecycleViewHorizontal(getContext(), rvSearchProduct);
        Utils.setUpRecycleViewHorizontal(getContext(), rvSearchPost);

        userList = new ArrayList<>();
        productList = new ArrayList<>();
        postDetailList = new ArrayList<>();

        btnCancel.setOnClickListener(v ->
                dismissDialog()
        );

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        etSearch.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchString = etSearch.getText().toString();
                hideKeyboard();
                searchPeople();
                searchProduct();
                searchPost();
                return true;
            }
            return false;
        });

        new Handler().postDelayed(() -> {
            etSearch.requestFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }, 500);

        if (!TextUtils.isEmpty(textSearch)) {
            searchString = textSearch;
            textSearch = "";
            etSearch.setText(searchString);
            hideKeyboard();
            searchPeople();
            searchProduct();
            searchPost();
        }

        peopleAdapter = new SearchPeopleAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                String currentUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
                if (!userList.get(position).getId().equals(currentUserId)) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    Bundle args = new Bundle();
                    args.putString(Constants.EXTRA_USER_ID, userList.get(position).getId());
                    intent.putExtras(args);
                    DetailType.PROFILE.attachTo(intent);
                    startActivity(intent);
                }
            }
        });
        rvSearchPeople.setAdapter(peopleAdapter);

        productAdapter = new ProductsAdapter(getActivity(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        });
        rvSearchProduct.setAdapter(productAdapter);

        postAdapter = new LiveStreamHorizontalAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        });
        rvSearchPost.setAdapter(postAdapter);

        updateEmptyList();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
    }

    private void dismissDialog() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
        dismiss();
    }

    private void searchPeople() {
        userList.clear();
        SearchModelRequest modelRequest = new SearchModelRequest();
        modelRequest.setType(SearchType.Users.getType());
        modelRequest.setKeyword(searchString);
        modelRequest.setMaxId(maxIdPeople);
        modelRequest.setPaginate(paginatePeople);
        modelRequest.setPerPage(perPage);
        requestApi(modelRequest, true, RequestTarget.SEARCH, this);
    }

    private void searchProduct() {
        productList.clear();
        SearchModelRequest modelRequest = new SearchModelRequest();
        modelRequest.setType(SearchType.Products.getType());
        modelRequest.setKeyword(searchString);
        modelRequest.setMaxId(maxIdProduct);
        modelRequest.setPaginate(paginateProduct);
        modelRequest.setPerPage(perPage);
        requestApi(modelRequest, true, RequestTarget.SEARCH, this);
    }

    private void searchPost() {
        postDetailList.clear();
        SearchModelRequest modelRequest = new SearchModelRequest();
        modelRequest.setType(SearchType.Posts.getType());
        modelRequest.setKeyword(searchString);
        modelRequest.setMaxId(maxIdProduct);
        modelRequest.setPaginate(paginateProduct);
        modelRequest.setPerPage(perPage);
        requestApi(modelRequest, false, RequestTarget.SEARCH, this);
    }

    private void updateEmptyList() {
        int totalData = peopleAdapter.getItemCount() + productAdapter.getItemCount() + postAdapter.getItemCount();
        if (totalData == 0) {
            emptyView.setVisibility(View.VISIBLE);
            llHeaderPeople.setVisibility(View.GONE);
            rvSearchPeople.setVisibility(View.GONE);
            llHeaderProduct.setVisibility(View.GONE);
            rvSearchProduct.setVisibility(View.GONE);
            llHeaderPost.setVisibility(View.GONE);
            rvSearchPost.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
        Log.e(TAG, "totalData: " + totalData);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            try {
                if (((SearchModelResponse) response.getData()).getUserList() != null) {
                    userList = ((SearchModelResponse) response.getData()).getUserList();
                    if (userList.size() > 0) {
                        tvSearchPeople.setText(String.format(getActivity().getResources().getString(R.string.search_people_with), searchString));
                        llHeaderPeople.setVisibility(View.VISIBLE);
                        rvSearchPeople.setVisibility(View.VISIBLE);
                        peopleAdapter.setList(userList);
                    } else {
                        llHeaderPeople.setVisibility(View.GONE);
                        rvSearchPeople.setVisibility(View.GONE);
                        peopleAdapter.clearAllItems();
                    }
                } else if (((SearchModelResponse) response.getData()).getProductDetail() != null) {
                    productList = ((SearchModelResponse) response.getData()).getProductDetail();
                    if (productList.size() > 0) {
                        tvSearchProduct.setText(String.format(getActivity().getResources().getString(R.string.search_product_with), searchString));
                        llHeaderProduct.setVisibility(View.VISIBLE);
                        rvSearchProduct.setVisibility(View.VISIBLE);
                        productAdapter.setList(productList);
                    } else {
                        llHeaderProduct.setVisibility(View.GONE);
                        rvSearchProduct.setVisibility(View.GONE);
                        productAdapter.clearAllItems();
                    }
                } else if (((SearchModelResponse) response.getData()).getPostDetails() != null) {
                    postDetailList = ((SearchModelResponse) response.getData()).getPostDetails();
                    if (postDetailList.size() > 0) {
                        tvSearchPost.setText(String.format(getActivity().getResources().getString(R.string.search_post_with), searchString));
                        llHeaderPost.setVisibility(View.VISIBLE);
                        rvSearchPost.setVisibility(View.VISIBLE);
                        postAdapter.setList(postDetailList);
                    } else {
                        llHeaderPost.setVisibility(View.GONE);
                        rvSearchPost.setVisibility(View.GONE);
                        postAdapter.clearAllItems();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
        updateEmptyList();
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        updateEmptyList();
    }
}
