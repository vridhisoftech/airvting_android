package com.airvtinginc.airvtinglive.gui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.LiveStreamHorizontalViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.HomeFeedType;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.Viewer;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class LiveStreamHorizontalAdapter extends BaseAdapter {
    private static final String TAG = LiveStreamHorizontalAdapter.class.getSimpleName();
    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;
    private CollectionReference viewersRef;

    public LiveStreamHorizontalAdapter(Context context, List<PostDetail> postDetailList, OnItemClickListener onItemClickListener) {
        super(context, postDetailList, onItemClickListener);
        viewersRef = FirebaseFirestore.getInstance().collection(Constants.COLLECTION_VIEWERS);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_horizontal_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_ITEM: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_livestream_horizontal, parent, false);
                LiveStreamHorizontalViewHolder vh = new LiveStreamHorizontalViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder) return;
        if (list.get(position) == null) return;
        final LiveStreamHorizontalViewHolder holder = (LiveStreamHorizontalViewHolder) mHolder;
        final PostDetail postDetail = (PostDetail) list.get(position);
        User user = postDetail.getUser();
        List<ProductInPost> productInPosts = postDetail.getProducts();
        ProductInPost product = null;

        if (productInPosts != null && !productInPosts.isEmpty()) {
            for (ProductInPost productInPost : productInPosts) {
                if (productInPost.isShow()) {
                    product = productInPost;
                }
            }
        }

        if (!TextUtils.isEmpty(postDetail.getFeaturedImage())) {
            holder.pbLoadingThumbnail.setVisibility(View.VISIBLE);
            Glide.with(context).load(postDetail.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbLoadingThumbnail.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbLoadingThumbnail.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivLive);

            holder.ivLive.setVisibility(View.VISIBLE);
            holder.ivLive.setBorderColor(context.getResources().getColor(android.R.color.transparent));
            holder.ivLive.setBorderWidth(0f);
            holder.tvDefaultAvatar.setText("");
            holder.tvDefaultAvatar.setVisibility(View.GONE);
        } else {
            holder.ivLive.setImageResource(R.drawable.ic_white);
            holder.ivLive.setBorderColor(context.getResources().getColor(R.color.colorBody));
            holder.ivLive.setBorderWidth(1f);
            holder.tvDefaultAvatar.setText(Utils.getDefaultAvatarText(user));
            holder.tvDefaultAvatar.setVisibility(View.VISIBLE);
        }

        if (postDetail.getType().equals(HomeFeedType.VIDEO.getType())) {
            holder.ivLiveRec.setVisibility(View.GONE);
            holder.ivPostVideo.setVisibility(View.VISIBLE);
            holder.tvCurrentViewers.setVisibility(View.GONE);
        } else if (postDetail.getType().equals(HomeFeedType.LIVE.getType())) {
            if (postDetail.isLive()) {
                holder.ivLiveRec.setImageResource(R.drawable.ic_live);
            } else {
                holder.ivLiveRec.setImageResource(R.drawable.ic_rec);
                Glide.with(context).load(R.drawable.ic_price_tag_sold).into(holder.ivPriceTag);
            }

            holder.ivPostVideo.setVisibility(View.GONE);
            holder.tvCurrentViewers.setVisibility(View.VISIBLE);
            holder.tvCurrentViewers.setText(postDetail.getNumberViewer());

            if (holder.listenerViewers != null) {
                holder.listenerViewers.remove();
            }
            EventListener<QuerySnapshot> listener = (queryDocumentSnapshots, err) -> {
                if (err != null) {
                    Log.e(TAG, "listenNewMessages failed " + err.getMessage(), err);
                    return;
                }
                if (queryDocumentSnapshots == null) {
                    Log.e(TAG, "queryDocumentSnapshots null");
                    return;
                }
                List<Viewer> listViewer = new ArrayList<>();
                for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                    DocumentSnapshot documentSnapshot = dc.getDocument();
                    Viewer viewer = null;
                    try {
                        viewer = documentSnapshot.toObject(Viewer.class);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    if (viewer == null) {
                        Log.e(TAG, "viewer null");
                        postDetail.setNumberViewer("0");
                        continue;
                    }

                    String id = documentSnapshot.getId();
                    int oldIndex = dc.getOldIndex();
                    int newIndex = dc.getNewIndex();

                    switch (dc.getType()) {
                        case ADDED:
                            Log.d(TAG, "Added: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                            Log.d(TAG, "ID: " + id + "; User Id: " + viewer.getUserId());
                            if (postDetail.isLive()) {
                                if (viewer.isView()) {
                                    listViewer.add(viewer);
                                }
                            } else {
                                listViewer.add(viewer);
                            }
                            break;
                        case MODIFIED:
                            Log.d(TAG, "Modified: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                            break;
                        case REMOVED:
                            Log.d(TAG, "Removed: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                            break;
                    }
                }
                Log.e(TAG, "listenerViewers End");
                try {
                    if (!postDetail.getNumberViewer().equals(String.valueOf(listViewer.size()))) {
                        postDetail.setNumberViewer(String.valueOf(listViewer.size()));
                        notifyDataSetChanged();
                    }
                } catch (Exception ex) {
                    postDetail.setNumberViewer(String.valueOf(listViewer.size()));
                    notifyDataSetChanged();
                }
            };

            if (postDetail.isLive()) {
                holder.listenerViewers = viewersRef
                        .whereEqualTo(Constants.FIELD_POST_ID, postDetail.getId())
                        .whereEqualTo(Constants.FIELD_IS_VIEW, true)
                        .addSnapshotListener((Activity) context, listener);
            } else {
                holder.listenerViewers = viewersRef
                        .whereEqualTo(Constants.FIELD_POST_ID, postDetail.getId())
                        .addSnapshotListener((Activity) context, listener);
            }
        }

        if (product != null) {
            if (!TextUtils.isEmpty(product.getFeaturedImage())) {
                holder.pbProductImage.setVisibility(View.VISIBLE);
                Glide.with(context).load(product.getFeaturedImage())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbProductImage.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbProductImage.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.ivProductCountDown);

                Glide.with(context).load(product.getFeaturedImage())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbProductImage.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbProductImage.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.ivProductNonSale);
            }

            ProductInPost productInPost = product;
            holder.rlDeal.setOnClickListener(view -> {
                if (productInPost != null) {
                    openProductDetail(productInPost.getProductId());
                }
            });
            holder.tvSaleQuantity.setText(String.valueOf(product.getQuantity()));
            String dealTime = product.getExpiredAt();

            if (!TextUtils.isEmpty(dealTime) && postDetail.isLive()) {
                holder.ivProductCountDown.setVisibility(View.VISIBLE);
                holder.llCountDownAndQuantity.setVisibility(View.VISIBLE);
                holder.ivProductNonSale.setVisibility(View.GONE);
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat(ServiceConstants.DATE_TIME_FORMAT, Locale.getDefault());
                Calendar currentCalendar = Calendar.getInstance();
                Calendar expiredCalendar = Calendar.getInstance();

                try {
                    expiredCalendar.setTimeInMillis(dateTimeFormat.parse(DateTimeUtils.getDateFormatUTC(dealTime)).getTime());
                } catch (Exception e) {
                    AppLog.e(e.getMessage());
                }

                long difference = expiredCalendar.getTimeInMillis() - currentCalendar.getTimeInMillis();

                if (holder.timer != null) {
                    holder.timer.cancel();
                }

                holder.timer = new CountDownTimer(difference, 1000) {
                    public void onTick(long millisUntilFinished) {
                        try {
                            holder.tvCountDownTimer.setText(DateTimeUtils.getDisplayTimeSaleFromMillis(millisUntilFinished));
                            holder.ivPriceTag.setImageResource(R.drawable.ic_price_tag);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            holder.timer.cancel();
                        }
                    }

                    public void onFinish() {
                        holder.tvCountDownTimer.setAnimation(null);
                        holder.tvCountDownTimer.setText(context.getString(R.string.discount_timer_end));
                        try {
                            holder.ivPriceTag.setImageResource(R.drawable.ic_price_tag_sold);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            holder.timer.cancel();
                        }
                        holder.ivProductCountDown.setVisibility(View.GONE);
                        holder.llCountDownAndQuantity.setVisibility(View.GONE);
                        holder.ivProductNonSale.setVisibility(View.VISIBLE);
                    }
                }.start();
            } else {
                holder.ivProductCountDown.setVisibility(View.GONE);
                holder.llCountDownAndQuantity.setVisibility(View.GONE);
                holder.ivProductNonSale.setVisibility(View.VISIBLE);
            }
        } else {
            holder.rlDeal.setVisibility(View.GONE);
        }

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                openUserDetail(position);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        Utils.spanAndCheckContentUsername(context, user.getUsername(), postDetail.getTitle(), user.isOnline(), holder.tvContent, clickableSpan, context.getResources().getColor(R.color.white));

        holder.mView.setOnClickListener(v -> {
            if (postDetail.getType().equals(HomeFeedType.LIVE.getType())) {
                openStream(postDetail);
            } else {
                openMedia(postDetail);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }

    private void openStream(PostDetail postDetail) {
        Intent intent = new Intent(context, LiveStreamActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, postDetail.getUser().getUserId());
        args.putString(Constants.EXTRA_POST_ID, postDetail.getId());
        args.putString(Constants.EXTRA_MEDIA_URL, postDetail.getMediaUrl());
        intent.putExtras(args);
        context.startActivity(intent);
    }

    private void openMedia(PostDetail media) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_GALLERY_ID, media.getId());
        intent.putExtras(args);
        // Sender usage
        DetailType.GALLERY.attachTo(intent);
        context.startActivity(intent);
    }

    private void openUserDetail(int position) {
        PostDetail postDetail = (PostDetail) list.get(position);
        String userId = postDetail.getUser().getUserId();
        Utils.openUserDetail(context, userId);
    }

    private void openProductDetail(String productId) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_PRODUCT_ID, productId);
        intent.putExtras(args);
        // Sender usage
        DetailType.PRODUCT.attachTo(intent);
        context.startActivity(intent);
    }
}
