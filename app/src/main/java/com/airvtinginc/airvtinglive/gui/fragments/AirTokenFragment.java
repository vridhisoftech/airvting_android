package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.AirTokenAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.AirToken;
import com.airvtinginc.airvtinglive.models.request.BuyAirTokenRequestModel;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.AirTokenListResponse;
import com.airvtinginc.airvtinglive.models.response.BuyAirTokenResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AirTokenFragment extends BaseFragment implements PurchasesUpdatedListener, BillingClientStateListener {

    private static final String TAG = AirTokenFragment.class.getSimpleName();

    @BindView(R.id.srl_air_token)
    SwipeRefreshLayout mSwipeRefreshAirToken;
    @BindView(R.id.tv_air_token_current_token)
    TextView mTvCurrentToken;
    @BindView(R.id.rv_air_token)
    RecyclerView mRvAirToken;

    private LinearLayoutManager mLinearLayoutManager;
    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private int mCurrentPage = 1;
    private int airQuantity = 0;
    private String airTokenId, title, currency;
    private float price;
    private AirTokenAdapter mAirTokenAdapter;
    private BillingClient mBillingClient;
    private boolean isBillingReady = false;
    private String purchaseToken;
    private Purchase purchase;

    public static AirTokenFragment newInstance() {
        AirTokenFragment fragment = new AirTokenFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_air_token, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        configToolbar();

        mBillingClient = BillingClient.newBuilder(getContext()).setListener(this).build();
        mBillingClient.startConnection(this);

        setUpRecyclerView();
    }

    private void configToolbar() {
        //Update header title
        DetailActivity.headerTitle = getString(R.string.air_token_title);
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);
    }

    private void setUpRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mAirTokenAdapter = new AirTokenAdapter(getActivity(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                AirToken airToken = (AirToken) item;
                airQuantity = airToken.getQuantity();
                airTokenId = airToken.getId();
                title = airToken.getTitle();
                currency = airToken.getCurrency();
                price = airToken.getPrice();

                if (isBillingReady) {
                    BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                            .setSku(airToken.getGoogleProductId())
                            .setType(BillingClient.SkuType.INAPP)
                            .build();
                    mBillingClient.launchBillingFlow(getActivity(), flowParams);
                }
            }
        });
        mRvAirToken.setLayoutManager(mLinearLayoutManager);
        mRvAirToken.setAdapter(mAirTokenAdapter);

        mRvAirToken.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                            @Override
                                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                super.onScrolled(recyclerView, dx, dy);
                                                if (dy > 0) {
                                                    visibleItemCount = mLinearLayoutManager.getChildCount();
                                                    totalItemCount = mLinearLayoutManager.getItemCount();
                                                    pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                                                    if (!isMoreLoading) {
                                                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                            mAirTokenAdapter.addItem(null);
                                                            isMoreLoading = true;
                                                            getAirToken(++mCurrentPage);
                                                        }
                                                    }
                                                }
                                            }
                                        }
        );
        mSwipeRefreshAirToken.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            getAirToken(mCurrentPage);
        });
    }

    private void getAirToken(int page) {
        GetListRequest modelRequest = new GetListRequest();
        modelRequest.setPaginate(page);
        modelRequest.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);

        boolean isLoading = true;
        if (mSwipeRefreshAirToken.isRefreshing()) {
            isLoading = false;
        }
        if (getActivity() != null && isAdded()) {
            requestApi(modelRequest, isLoading, RequestTarget.GET_AIR_TOKENS, this);
        }
    }


    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAirTokenAdapter.getItemCount() > 0) {
            mAirTokenAdapter.removeItem(mAirTokenAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    private void getAirTokenPrice(List<AirToken> airTokens) {
        List skuList = new ArrayList();
        for (AirToken airToken : airTokens) {
            skuList.add(airToken.getGoogleProductId());
        }
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        mBillingClient.querySkuDetailsAsync(params.build(),
                (responseCode, skuDetailsList) -> {
                    if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
                        for (AirToken airToken : airTokens) {
                            findIndexInAirTokenList(skuDetailsList, airToken);
                        }
                        mAirTokenAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void findIndexInAirTokenList(List<SkuDetails> skuDetails, AirToken airToken) {
        for (SkuDetails skuDetail : skuDetails) {
            if (airToken.getGoogleProductId().equals(skuDetail.getSku())) {
                airToken.setCurrency(skuDetail.getPriceCurrencyCode());
                airToken.setPrice((float) skuDetail.getPriceAmountMicros() / 1000000);
                airToken.setPriceInGoogle(skuDetail.getPrice());
                return;
            }
        }
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (getActivity() == null || !isAdded()) {
            return;
        }
        if (responseCode == BillingClient.BillingResponse.OK && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
//            Toast.makeText(getContext(), getString(R.string.air_token_cancel_purchase), Toast.LENGTH_SHORT).show();
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.air_token_title), getString(R.string.dialog_buy_air_token_failed));
        } else {
            // In this case, have error when verify purchase with server, so consumeAsync not called.
            // We need to call api verify to server again
            Purchase failedPurchase = Utils.getPurchase(getContext(), airTokenId);
            if (failedPurchase != null) {
                handlePurchase(failedPurchase);
            } else {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.air_token_title), getString(R.string.dialog_buy_air_token_failed));
            }
        }
    }

    @Override
    public void onBillingSetupFinished(int responseCode) {
        if (responseCode == BillingClient.BillingResponse.OK) {
            // The billing client is ready. You can query purchases here.
            isBillingReady = true;
            getAirToken(mCurrentPage);
        }
    }

    @Override
    public void onBillingServiceDisconnected() {
        mBillingClient.startConnection(this);
    }

    private void handlePurchase(Purchase purchase) {
        this.purchase = purchase;
        purchaseToken = purchase.getPurchaseToken();
        //Call api to verify purchase with server
String receipt=purchase.getOrderId();
        com.airvtinginc.airvtinglive.models.request.Purchase purchaseOrder=new com.airvtinginc.airvtinglive.models.request.Purchase();
        purchaseOrder.setReceipt(receipt);
        BuyAirTokenRequestModel requestModel = new BuyAirTokenRequestModel();
        requestModel.setQuantityToBuy(airQuantity);
        requestModel.setAirTokenId(airTokenId);
        requestModel.setCurrency(currency);
        requestModel.setPrice(price);
        requestModel.setTitle(title);
        requestModel.setPurchase(purchaseOrder);
        requestApi(requestModel, true, RequestTarget.BUY_AIR_TOKEN, AirTokenFragment.this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_AIR_TOKENS:
                    if (mSwipeRefreshAirToken.isRefreshing()) {
                        mAirTokenAdapter.clearAllItems();
                        mSwipeRefreshAirToken.setRefreshing(false);
                    }
                    AirTokenListResponse airTokenListResponse = (AirTokenListResponse) response.getData();
                    if (airTokenListResponse != null) {
                        mTvCurrentToken.setText(String.valueOf(airTokenListResponse.getAirTokenOfUser()));
                        mAirTokenAdapter.addAllItems(airTokenListResponse.getAirTokenList());
                        getAirTokenPrice(airTokenListResponse.getAirTokenList());
                    }
                    break;
                case BUY_AIR_TOKEN:
                    //We need to call consumeAsync to can re-buy this item
                    ConsumeResponseListener listener = (responseCode, purchaseToken) -> {
                        if (responseCode == BillingClient.BillingResponse.OK) {
                            int currentAirToken = ((BuyAirTokenResponse) response.getData()).getAirToken();
                            mTvCurrentToken.setText(String.valueOf(currentAirToken));
                            SharedPreferencesManager.getInstance(getContext()).putInt(Constants.PREF_CURRENT_USER_TOKEN, currentAirToken);
                            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.air_token_title), getString(R.string.dialog_buy_air_token_success));
                        } else {
                            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.air_token_title), getString(R.string.dialog_buy_air_token_failed));
                        }
                    };
                    mBillingClient.consumeAsync(purchaseToken, listener);
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }

        hideLoadMore();
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        //In this case, if verify with server fail, we need to store purchase info to re-verify in next time
        Gson gson = new Gson();
        String jsonPurchase = gson.toJson(purchase);
        Utils.savePurchase(getContext(), jsonPurchase, airTokenId);
        if (requestTarget == RequestTarget.BUY_AIR_TOKEN) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.air_token_title), getString(R.string.dialog_buy_air_token_failed));
        }
    }
}