package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.GalleryViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;


public class GalleryAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_GALLERY = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;

    public GalleryAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LOAD_MORE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
            v.getLayoutParams().height = Utils.getScreenWidth(parent.getContext()) / 3;
            return new EmptyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallery, parent, false);
            int padding = (int) context.getResources().getDimension(R.dimen.gallery_padding);
            v.getLayoutParams().height = (Utils.getScreenWidth(parent.getContext()) / 3) - padding * 2;
            return new GalleryViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof EmptyViewHolder) {
            return;
        }
        final GalleryViewHolder holder = (GalleryViewHolder) mHolder;
        final PostDetail media = (PostDetail) this.list.get(position);

        if (!TextUtils.isEmpty(media.getFeaturedImage())) {
            holder.pbMediaImage.setVisibility(View.VISIBLE);
            Glide.with(context).load(media.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                            holder.pbMediaImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbMediaImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivMedia);

            holder.ivMedia.setBorderColor(context.getResources().getColor(android.R.color.transparent));
            holder.ivMedia.setBorderWidth(0f);
            holder.tvDefaultAvatar.setText("");
            holder.tvDefaultAvatar.setVisibility(View.GONE);
        } else {
            holder.ivMedia.setImageResource(R.drawable.ic_white);
            holder.ivMedia.setBorderColor(context.getResources().getColor(R.color.colorBody));
            holder.ivMedia.setBorderWidth(1f);
            holder.tvDefaultAvatar.setText(Utils.getDefaultAvatarText(media.getUser()));
            holder.tvDefaultAvatar.setVisibility(View.VISIBLE);
        }

        if (media.getType().equals(MediaType.VIDEO.getType())) {
            holder.ivGalleryVideo.setVisibility(View.VISIBLE);
        } else {
            holder.ivGalleryVideo.setVisibility(View.GONE);
        }

        holder.mView.setOnClickListener(v -> onItemClickListener.onItemClicked(media, position));
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) == null) return VIEW_TYPE_LOAD_MORE;
        else return VIEW_TYPE_GALLERY;
    }
}