package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.GalleryAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryFragment extends BaseFragment implements BaseAdapter.OnItemClickListener {

    private static final String TAG = GalleryFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.tvShowEmpty)
    TextView tvShowEmpty;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private GridLayoutManager mGridLayoutManager;

    private GalleryAdapter mAdapter;

    private int mCurrentPage = 1;
    private String mMaxId;
    private String mUserId = "";
    private boolean mIsCurrentUser;

    public GalleryFragment() {
        // Required empty public constructor
    }

    public static GalleryFragment newInstance(String userId, boolean isCurrentUser) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, userId);
        args.putBoolean(Constants.EXTRA_USER_IS_CURRENT_USER, isCurrentUser);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(Constants.EXTRA_USER_ID);
            mIsCurrentUser = getArguments().getBoolean(Constants.EXTRA_USER_IS_CURRENT_USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mCurrentPage = 1;
        getGalleryMedia(mCurrentPage);
    }

    private void setupRecyclerView() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) emptyView.getLayoutParams();
        layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        emptyView.setPadding(0, getResources().getDimensionPixelSize(R.dimen.profile_padding_top_no_data), 0, 0);
        emptyView.setLayoutParams(layoutParams);
        emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        if (mIsCurrentUser) {
            tvShowEmpty.setText(getString(R.string.recycle_view_empty_post));
        }
        else {
            tvShowEmpty.setText("No Data Available.");

        }
        mRecyclerView.setEmptyView(emptyView);
        mGridLayoutManager = new GridLayoutManager(getContext(), 3);
        mAdapter = new GalleryAdapter(getActivity(), this);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = mGridLayoutManager.getChildCount();
                                                      totalItemCount = mGridLayoutManager.getItemCount();
                                                      pastVisibleItems = mGridLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getGalleryMedia(++mCurrentPage);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            mMaxId = null;
            getGalleryMedia(mCurrentPage);
        });
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        PostDetail media = (PostDetail) item;
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_GALLERY_ID, media.getId());
        intent.putExtras(args);
        // Sender usage
        DetailType.GALLERY.attachTo(intent);
        getActivity().startActivity(intent);
    }

    private void getGalleryMedia(int page) {
        if (TextUtils.isEmpty(mUserId) && mIsCurrentUser) {
            mUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        }
        String[] typePost = {MediaType.IMAGE.getType(), MediaType.VIDEO.getType()};

        GetListRequest modelRequest = new GetListRequest(mUserId, null, page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, mMaxId, null, typePost, 0);
        requestApi(modelRequest, true, RequestTarget.GET_POST_BY_USER_ID, this);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_POST_BY_USER_ID:
                    hideLoadMore();
                    PostResponse result = ((PostResponse) response.getData());
                    if (result != null && mAdapter != null) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mAdapter.addAllItems(result.getListPostDetail());
                        mMaxId = result.getMaxId();
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
