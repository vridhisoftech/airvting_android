package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.BookmarkViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.HomeFeedType;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.request.BookmarkPostRequest;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.Schedule;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.airvtinginc.airvtinglive.tools.fonts.CustomTypefaceSpan;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BookmarkAdapter extends BaseAdapter {

    private static final String TAG = BookmarkAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;

    private int mBookmarkPosition;

    public BookmarkAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_ITEM: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bookmark, parent, false);
                BookmarkViewHolder vh = new BookmarkViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder) return;
        if (list.get(position) == null) return;
        final BookmarkViewHolder holder = (BookmarkViewHolder) mHolder;
        final PostDetail bookmark = (PostDetail) list.get(position);
        List<ProductInPost> productInPosts = bookmark.getProducts();
        ProductInPost product = null;

        if (productInPosts != null && !productInPosts.isEmpty()) {
            for (ProductInPost productInPost : productInPosts) {
                if (productInPost.isShow()) {
                    product = productInPost;
                }
            }
        }

        if (!TextUtils.isEmpty(bookmark.getFeaturedImage())) {
            holder.pbFeedImage.setVisibility(View.VISIBLE);
            Glide.with(context).load(bookmark.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbFeedImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbFeedImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivLive);
        }

        if (product != null) {
            holder.pbProductImage.setVisibility(View.VISIBLE);
            Glide.with(context).load(product.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbProductImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbProductImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivProductCountDown);
        }

        String currentBookmarkDateString = bookmark.getBookmarkedAt();
        try {
            holder.tvDate.setText(DateTimeUtils.getDisplayBookmarkTime(context, DateTimeUtils.getDateFormatUTC(currentBookmarkDateString)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (position > 0) {
            String previousBookmarkDateString = ((PostDetail) list.get(position - 1)).getBookmarkedAt();
            Date currentBookMarkDate = DateTimeUtils.getDateFollowFormat(currentBookmarkDateString, DateTimeUtils.DATE_TIME_FORMAT);
            Date previousBookMarkDate = DateTimeUtils.getDateFollowFormat(previousBookmarkDateString, DateTimeUtils.DATE_TIME_FORMAT);

            if (DateTimeUtils.isSameDate(currentBookMarkDate, previousBookMarkDate)) {
                holder.tvDate.setVisibility(View.GONE);
            } else {
                holder.tvDate.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tvDate.setVisibility(View.VISIBLE);
        }

        String username = bookmark.getUser().getUsername();
        String content = bookmark.getTitle();
        String itemValue = username + " " + content;
        SpannableStringBuilder SS = new SpannableStringBuilder(itemValue);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        SS.setSpan(clickableSpan, 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SS.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorHeader)), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SS.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_semibold)), 0, username.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_regular)), username.length(), itemValue.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        holder.tvContent.setText(SS);
        holder.tvContent.setOnClickListener(view -> {
            openUserDetail(bookmark.getUser().getUserId());
        });

        //Show hashtag
        Linkify.addLinks(holder.tvContent, Utils.HASH_TAG_MATCHER, null);
        holder.tvContent.setLinkTextColor(context.getResources().getColor(R.color.md_blue_500));
        Utils.stripUnderlines(context, holder.tvContent);

        holder.lnBookmark.setOnClickListener(view -> {
            BookmarkPostRequest requestModel = new BookmarkPostRequest(bookmark.getId());
            requestApi(requestModel, true, RequestTarget.BOOKMARK_POST, this);
            mBookmarkPosition = position;
        });

        holder.mView.setOnClickListener(v -> {
            openStream(bookmark, position);
        });

        ProductInPost productInPost = product;
        holder.rlDeal.setOnClickListener(view -> {
            if (productInPost != null) {
                openProductDetail(productInPost.getProductId());
            }
        });

        if (bookmark.getType().equals(HomeFeedType.VIDEO.getType())) {
            holder.ivLiveRec.setImageResource(R.drawable.ic_rec);
            holder.rlDeal.setVisibility(View.INVISIBLE);
            holder.tvViewerCount.setVisibility(View.GONE);
            holder.tvSchedule.setVisibility(View.GONE);
        } else if (bookmark.getType().equals(HomeFeedType.LIVE.getType())) {
            if (bookmark.isLive()) {
                holder.ivLiveRec.setImageResource(R.drawable.ic_live);
                holder.ivLiveRec.setVisibility(View.VISIBLE);
                holder.rlDeal.setVisibility(View.VISIBLE);
            } else {
                holder.ivLiveRec.setVisibility(View.GONE);
                holder.rlDeal.setVisibility(View.GONE);
            }
            holder.tvViewerCount.setVisibility(View.VISIBLE);
            holder.tvViewerCount.setText(String.valueOf(bookmark.getViewers()));

            Schedule schedule = bookmark.getUser().getSchedule();
            if (schedule != null && schedule.getScheduleDates() != null && !schedule.getScheduleDates().isEmpty()) {
                holder.tvSchedule.setText(DateTimeUtils.getDisplaySchedule(context, schedule));
                holder.tvSchedule.setVisibility(View.VISIBLE);
            } else {
                holder.tvSchedule.setVisibility(View.GONE);
            }
        }


        if (product != null) {
            String dealTime = product.getExpiredAt();
            if (!TextUtils.isEmpty(dealTime) && bookmark.isLive()) {
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat(ServiceConstants.DATE_TIME_FORMAT, Locale.getDefault());
                Calendar currentCalendar = Calendar.getInstance();
                Calendar expiredCalendar = Calendar.getInstance();

                try {
                    expiredCalendar.setTimeInMillis(dateTimeFormat.parse(DateTimeUtils.getDateFormatUTC(dealTime)).getTime());
                } catch (Exception e) {
                    AppLog.e(e.getMessage());
                }

                long difference = expiredCalendar.getTimeInMillis() - currentCalendar.getTimeInMillis();

                if (holder.timer != null) {
                    holder.timer.cancel();
                }

                holder.timer = new CountDownTimer(difference, 1000) {
                    public void onTick(long millisUntilFinished) {
                        holder.tvCountDownTimer.setText(DateTimeUtils.getDisplayTimeFromMillis(millisUntilFinished));
                    }

                    public void onFinish() {
                        holder.tvCountDownTimer.setAnimation(null);
                        holder.tvCountDownTimer.setText(context.getString(R.string.dialog_add_discount_timer_end));
//                        if (bookmark.isLive()) {
//                            holder.tvCountDownTimer.setText("");
//                        } else {
//                            holder.rlDeal.setVisibility(View.GONE);
//                        }
                        holder.ivPriceTag.setImageResource(R.drawable.ic_price_tag_sold);
                    }
                }.start();
            }
        } else {
            holder.rlDeal.setVisibility(View.GONE);
        }

        if (!holder.tvCountDownTimer.getText().toString().equalsIgnoreCase(context.getString(R.string.my_user_sold))) {
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500); //You can manage the blinking time with this parameter
            anim.setStartOffset(0);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            holder.tvCountDownTimer.startAnimation(anim);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        if (requestTarget == RequestTarget.BOOKMARK_POST) {
            if (response.isSuccess()) {
                removeItem(mBookmarkPosition);
            } else {
                Toast.makeText(context, response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openUserDetail(String userId) {
        String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (!userId.equals(currentUserId)) {
            Utils.openUserDetailOrder(context, userId);
        }
    }

    private void openProductDetail(String productId) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_PRODUCT_ID, productId);
        intent.putExtras(args);
        // Sender usage
        DetailType.PRODUCT.attachTo(intent);
        context.startActivity(intent);
    }

    private void openStream(PostDetail postDetail, int position) {
        if (postDetail.getType().equals(HomeFeedType.LIVE.getType())) {
            Intent intent = new Intent(context, LiveStreamActivity.class);
            Bundle args = new Bundle();
            args.putString(Constants.EXTRA_USER_ID, postDetail.getUser().getUserId());
            args.putString(Constants.EXTRA_POST_ID, postDetail.getId());
            args.putString(Constants.EXTRA_MEDIA_URL, postDetail.getMediaUrl());
            intent.putExtras(args);
            context.startActivity(intent);
        }
    }
}
