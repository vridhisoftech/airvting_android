package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivityViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.cv_noti_activity_background)
    CardView cvBackground;
    public @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    public @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    public @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;
    public @BindView(R.id.pbLoadAvatar)
    ProgressBar pbAvatarLoading;
    public @BindView(R.id.rl_noti_activity_follow)
    RelativeLayout rlFollow;
    public @BindView(R.id.iv_noti_activity_follow)
    ImageView ivFollow;
    public @BindView(R.id.tv_noti_activity_username)
    TextView tvUsername;
    public @BindView(R.id.tv_noti_activity_status)
    TextView tvStatus;
    public @BindView(R.id.tv_noti_activity_time)
    TextView tvTime;
    public @BindView(R.id.ll_noti_activity_message)
    LinearLayout llMessage;
    public @BindView(R.id.tv_noti_activity_message)
    TextView tvMessage;
    public @BindView(R.id.iv_noti_activity_offer_image)
    RoundedImageView ivOfferImage;
    public @BindView(R.id.pb_noti_activity_offer_image_loading)
    ProgressBar pbOfferImageLoading;
    public @BindView(R.id.rv_noti_activity_liked_product_image)
    RecyclerView rvLikedProductImage;

    public NotificationActivityViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}