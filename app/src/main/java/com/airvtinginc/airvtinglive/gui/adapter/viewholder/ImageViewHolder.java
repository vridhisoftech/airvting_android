package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivImage)
    RoundedImageView ivImage;
    public @BindView(R.id.ivAddProduct)
    ImageView ivAddProduct;
    public @BindView(R.id.llRemove)
    LinearLayout llRemove;

    public ImageViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}