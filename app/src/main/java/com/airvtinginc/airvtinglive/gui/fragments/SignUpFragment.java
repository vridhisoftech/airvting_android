package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.SignUpModelRequest;
import com.airvtinginc.airvtinglive.models.request.SocicalSignInModelRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UserFacebook;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.airvtinginc.airvtinglive.tools.Validator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    private static final String TAG = SignUpFragment.class.getName();

    @BindView(R.id.mrlSignUp)
    MaterialRippleLayout mrlSignUp;
    @BindView(R.id.mrlFacebook)
    MaterialRippleLayout mrlFacebook;
    @BindView(R.id.mrlGoogle)
    MaterialRippleLayout mrlGoogle;

    @BindView(R.id.mEdtUsername)
    EditText mEdtUsername;
    @BindView(R.id.mEdtEmail)
    EditText mEdtEmail;
    @BindView(R.id.mEdtBirthday)
    EditText mEdtBirthday;
    @BindView(R.id.mEdtPassword)
    EditText mEdtPassword;
    @BindView(R.id.mEdtPasswordConfirm)
    EditText mEdtPasswordConfirm;
    @BindView(R.id.tvTermsAndPrivacyPolicy)
    TextView tvTermsAndPrivacyPolicy;
    @BindView(R.id.cbTermsAndPrivacyPolicy)
    CheckBox cbTermsAndPrivacyPolicy;

    private Calendar mBirthdayCalendar;
    private OnSignUpClickListener onSignUpClickListener;
    private GoogleSignInClient mGoogleSignInClient;

    private CallbackManager callbackManager;

    @OnClick(R.id.mrlFacebook)
    void getInfoFacebook() {
        if (!cbTermsAndPrivacyPolicy.isChecked()) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_not_check_terms));
            return;
        }
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_gender"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(TAG, "login facebook onSuccess: " + loginResult.getAccessToken().getToken());
                        handleSignInFacebookResult();
//                        LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "login facebook onCancel");
                        LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(TAG, "login facebook onError");
                        exception.printStackTrace();
                        LoginManager.getInstance().logOut();
                    }
                });
    }

    @OnClick(R.id.mrlGoogle)
    void getInfoGooglePlus() {
        if (!cbTermsAndPrivacyPolicy.isChecked()) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_not_check_terms));
            return;
        }
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, ServiceConstants.REQUEST_CODE_SIGN_IN_GOOGLE_PLUS);
    }

    @OnClick(R.id.mrlSignUp)
    void signUpClick() {
        if (!cbTermsAndPrivacyPolicy.isChecked()) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_not_check_terms));
            return;
        }
        signUp();
//        onSignUpClick();
    }

    @OnClick(R.id.tvTermsAndPrivacyPolicy)
    void showTermsAndPrivacy() {
//        WebViewDialog dialog = WebViewDialog.newInstance(ServiceConstants.URL_USER_AGREEMENT,
//                getString(R.string.navigation_drawer_menu_terms_condition));
//        dialog.show(getActivity().getSupportFragmentManager(), "Terms and Privacy Dialog");
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        DetailType.TERMS_CONDITIONS.attachTo(intent);
        startActivity(intent);
    }

    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    public void setOnItemClickListener(OnSignUpClickListener onSignUpClickListener) {
        this.onSignUpClickListener = onSignUpClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        ButterKnife.bind(this, view);
        initView();
        initGoogleServices();
    }

    private void initView() {
        mBirthdayCalendar = Calendar.getInstance();
        mEdtBirthday.setFocusable(false);
        mEdtBirthday.setOnLongClickListener(null);
        mEdtBirthday.setOnClickListener(view -> {
            showDatePickerDialog();
        });
        SpannableString terms = new SpannableString(getString(R.string.navigation_terms_and_conditions));
        terms.setSpan(new UnderlineSpan(), 0, terms.length(), 0);
        tvTermsAndPrivacyPolicy.setText(terms);
    }

    private void showDatePickerDialog() {
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                mBirthdayCalendar.get(Calendar.YEAR),
                mBirthdayCalendar.get(Calendar.MONTH),
                mBirthdayCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getActivity().getFragmentManager(), TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String day = String.valueOf(dayOfMonth);
        String month = String.valueOf(monthOfYear + 1);

        if (dayOfMonth < 10) {
            day = "0" + String.valueOf(dayOfMonth);
        }

        if (monthOfYear + 1 < 10) {
            month = "0" + String.valueOf(monthOfYear + 1);
        }

        String birthday = day + "/" + month + "/" + year;
        mEdtBirthday.setText(birthday);
        mBirthdayCalendar.set(year, monthOfYear, dayOfMonth);
    }

    public interface OnSignUpClickListener {
        void onSignUpClicked(User user);
    }

    private void onSignUpClick() {
        User user = new User();
        user.setUsername(mEdtUsername.getText().toString());
        user.setEmail(mEdtEmail.getText().toString());
        user.setBirthday(mEdtBirthday.getText().toString());
        user.setPassword(mEdtPassword.getText().toString());

        onSignUpClickListener.onSignUpClicked(user);
    }

    private void signUp() {
        if (isValidFields()) {
            String username = mEdtUsername.getText().toString();
            String email = mEdtEmail.getText().toString();
            String birthday = mEdtBirthday.getText().toString();
            String password = mEdtPassword.getText().toString();

            SignUpModelRequest modelRequest = new SignUpModelRequest(username, email, birthday, username, "", ServiceConstants.GENDER_MALE, "", password);

            requestApi(modelRequest, true, RequestTarget.SIGN_UP, this);
        }
    }

    private void goToHome() {
        Intent in = new Intent(getContext(), MainActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(in);
        getActivity().finish();
    }

    private boolean isValidFields() {
        if (TextUtils.isEmpty(mEdtUsername.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_username_required));
            return false;
        }


        if (TextUtils.isEmpty(mEdtEmail.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_email_required));
            return false;
        }

        if (!Validator.getInstance().isEmailValid(mEdtEmail.getText().toString())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_invalid_email));
            return false;
        }

        if (TextUtils.isEmpty(mEdtPassword.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_password_required));
            return false;
        }

        if (!Validator.getInstance().isPasswordValid(mEdtPassword.getText().toString())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_invalid_password));
            return false;
        }

        String password = mEdtPassword.getText().toString();
        String passwordConfirm = mEdtPasswordConfirm.getText().toString();

        if (!password.equals(passwordConfirm)) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up),
                    getString(R.string.validator_error_not_match_password));
            return false;
        }

        return true;
    }

    private void clearErrors() {
        mEdtUsername.setError(null);
        mEdtEmail.setError(null);
        mEdtBirthday.setError(null);
        mEdtPassword.setError(null);
        mEdtPasswordConfirm.setError(null);
    }

    private void initGoogleServices() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "resultCode: " + resultCode);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
                case ServiceConstants.REQUEST_CODE_SIGN_IN_GOOGLE_PLUS:
                    // The Task returned from this call is always completed, no need to attach
                    // a listener.
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    handleSignInGoogleResult(task);
                    break;
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInGoogleResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            Log.w(TAG, "Signed in successfully");
            Log.d(TAG, account.toJson().toString());
            Log.d(TAG, account.getIdToken());
            signUpWithGoogle(account);
//            mEdtUsername.setText(account.getDisplayName());
//            mEdtEmail.setText(account.getEmail());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            e.printStackTrace();
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Log.w(TAG, "signInResult:" + GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
        }
    }

    private void handleSignInFacebookResult() {
        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), (object, response) -> {
            Log.d(TAG, response.getJSONObject().toString());
            Gson gson = new Gson();
            UserFacebook userFacebook = gson.fromJson(response.getJSONObject().toString(), UserFacebook.class);
            userFacebook.setToken(AccessToken.getCurrentAccessToken().getToken());
            signUpWithFaceBook(userFacebook);
//            mEdtUsername.setText(userFacebook.getName());
//            mEdtEmail.setText(userFacebook.getEmail());
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email,first_name,last_name,picture,gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void signUpWithFaceBook(UserFacebook userFacebook) {
        int gender;
        String email,profilePic = null;
        try {
            profilePic = new URL("https://graph.facebook.com/"+userFacebook.getId()+"/picture?type=large").toString();
            SharedPreferencesManager.getInstance(getContext()).putString(Constants.PREF_CURRENT_USER_PHOTO_URL, profilePic);
        }
        catch (MalformedURLException m)
        {

        }
        if (userFacebook.getGender() != null) {
            gender = userFacebook.getGender().endsWith(ServiceConstants.FACEBOOK_GENDER_MALE) ?
                    ServiceConstants.GENDER_MALE : ServiceConstants.GENDER_FEMALE;
        } else {
            gender = 0;
        }
        if (userFacebook.getEmail() != null) {
            email = userFacebook.getEmail();
        } else {
            email = "";
        }
        SocicalSignInModelRequest modelRequest = new SocicalSignInModelRequest(userFacebook.getId(),
                ServiceConstants.SOCIAL_TYPE_FACEBOOK, userFacebook.getToken(),
                userFacebook.getFirstName(), email, "",
                userFacebook.getFirstName(), userFacebook.getLastName(),
                gender, "", profilePic);

        requestApi(modelRequest, true, RequestTarget.SIGN_IN_SOCIAL, this);
    }

    private void signUpWithGoogle(GoogleSignInAccount account) {
        int gender = 0;
        String email = "";
        try {
            email = account.getEmail();
        } catch (Exception ex) {

        }
        SocicalSignInModelRequest modelRequest = new SocicalSignInModelRequest(account.getId(),
                ServiceConstants.SOCIAL_TYPE_GOOGLE, account.getIdToken(),
                account.getGivenName(), email, "",
                account.getGivenName(), account.getFamilyName(),
                gender, "", account.getPhotoUrl().toString());

        requestApi(modelRequest, true, RequestTarget.SIGN_IN_SOCIAL, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        switch (requestTarget) {
            case SIGN_UP:
            case SIGN_IN_SOCIAL:
                if (response.isSuccess()) {
                    UserResponse userResponse = (UserResponse) response.getData();
                    Utils.saveCurrentUserInfoToPreferences(getActivity(), userResponse.getTokenId(), userResponse.getCountPostsFollowing(), userResponse.getUser());
                    goToHome();
                } else {
                    Log.e(TAG, "SIGN_UP: fail");
                }
                break;
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        switch (requestTarget) {
            case SIGN_UP:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_up_sign_up), failMessage);
                break;
        }
    }
}
