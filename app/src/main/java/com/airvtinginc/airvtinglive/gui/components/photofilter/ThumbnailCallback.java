package com.airvtinginc.airvtinglive.gui.components.photofilter;

import com.zomato.photofilters.imageprocessors.Filter;

/**
 * @author Varun on 01/07/15.
 */
public interface ThumbnailCallback {

    void onThumbnailClick(Filter filter, int position);
}
