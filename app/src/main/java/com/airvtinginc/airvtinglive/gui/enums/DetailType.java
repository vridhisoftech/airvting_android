package com.airvtinginc.airvtinglive.gui.enums;

import android.content.Intent;

public enum DetailType {
    PROFILE,
    EDIT_PROFILE,
    PRODUCT,
    GALLERY,
    ADD_NEW_PHOTO,
    ADD_NEW_VIDEO,
    MY_CART,
    ADD_NEW_PRODUCT,
    WALLET,
    CONVERSATIONS_LIST,
    CONVERSATIONS_DETAIL,
    CONVERSATIONS_ADD_NEW,
    GIFT_STORE,
    ADD_CARD,
    EDIT_CARD,
    SHOW_WALLET_TOKEN,
    CARD_LIST,
    BOOKMARK,
    SETTING,
    TERMS_CONDITIONS,
    CONTACT_US,
    FOLLOWS;

    private static final String name = DetailType.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static DetailType detachFrom(Intent intent) {
        if (!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }
}
