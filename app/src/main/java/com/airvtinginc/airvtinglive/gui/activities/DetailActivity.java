package com.airvtinginc.airvtinglive.gui.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.fragments.AddCardsFragment;
import com.airvtinginc.airvtinglive.gui.fragments.AddConversationsFragment;
import com.airvtinginc.airvtinglive.gui.fragments.AirTokenFragment;
import com.airvtinginc.airvtinglive.gui.fragments.BookmarkFragment;
import com.airvtinginc.airvtinglive.gui.fragments.ContactUsFragment;
import com.airvtinginc.airvtinglive.gui.fragments.ConversationsListFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailTermsAndConditionsFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailSettingFragment;
import com.airvtinginc.airvtinglive.gui.fragments.EditCardsFragment;
import com.airvtinginc.airvtinglive.gui.fragments.FollowsFragment;
import com.airvtinginc.airvtinglive.gui.fragments.GiftStoreFragment;
import com.airvtinginc.airvtinglive.gui.fragments.CartFragment;
import com.airvtinginc.airvtinglive.gui.fragments.ConversationsDetailFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailAddPhotoFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailAddProductFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailAddVideoFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailGalleryFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailProductFragment;
import com.airvtinginc.airvtinglive.gui.fragments.DetailWalletFragment;
import com.airvtinginc.airvtinglive.gui.fragments.EditProfileFragment;
import com.airvtinginc.airvtinglive.gui.fragments.MyCardsFragment;
import com.airvtinginc.airvtinglive.gui.fragments.ProfileFragment;
import com.airvtinginc.airvtinglive.models.response.PaymentDetails;
import com.airvtinginc.airvtinglive.tools.AppPermission;
import com.airvtinginc.airvtinglive.tools.RealPathUtil;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity {
    private static final String TAG = DetailActivity.class.getSimpleName();

    public static final int CAMERA_VIDEO_REQUEST = 1;
    public static final int GALLERY_VIDEO_REQUEST = 2;
    private final int videoTimeLimit = 60; //60 second
    public static final String BroadcastActionSearch = "com.airvtinginc.airvting.broadcast.action.search";
    public static final String BroadcastActionNotification = "com.airvtinginc.airvting.broadcast.action.notification";
    public static final String BroadcastActionTrash = "com.airvtinginc.airvting.broadcast.action.delete";
    public static final String BroadcastActionEdit = "com.airvtinginc.airvting.broadcast.action.edit";
    public static final String BroadcastActionAdd = "com.airvtinginc.airvting.broadcast.action.add";
    public static final String BroadcastActionSave = "com.airvtinginc.airvting.broadcast.action.save";
    public static final String BroadcastActionShowWalletToken = "com.airvtinginc.airvting.broadcast.action.show.wallet.token";
    public static final String BroadcastActionUpdateHeaderTitle = "com.airvtinginc.airvting.broadcast.action.updateHeaderTitle";
    public static final String BroadcastActionShowIconSearch = "com.airvtinginc.airvting.broadcast.action.show.icon.search";
    public static final String BroadcastActionShowIconNotification = "com.airvtinginc.airvting.broadcast.action.show.icon.notification";
    public static final String BroadcastActionShowIconTrash = "com.airvtinginc.airvting.broadcast.action.show.icon.trash";
    public static final String BroadcastActionHideIconTrash = "com.airvtinginc.airvting.broadcast.action.hide.icon.trash";
    public static final String BroadcastActionShowIconEdit = "com.airvtinginc.airvting.broadcast.action.show.icon.edit";
    public static final String BroadcastActionShowIconAdd = "com.airvtinginc.airvting.broadcast.action.show.icon.add";
    public static final String BroadcastActionShowIconSave = "com.airvtinginc.airvting.broadcast.action.show.icon.save";
    public static final String BroadcastActionShowIconWalletToken = "com.airvtinginc.airvting.broadcast.action.show.icon.wallet.token";
    public static final String BroadcastActionChangeToolbarToTransparent = "com.airvtinginc.airvting.broadcast.action.changeToolbarToTransparent";
    public static final String BroadcastActionChangeToolbarToWhiteColor = "com.airvtinginc.airvting.broadcast.action.changeToolbarToWhiteColor";
    public static final String BroadcastActionChangeToolbarToBlackColor = "com.airvtinginc.airvting.broadcast.action.changeToolbarToBlackColor";
    public static final String BroadcastActionHideToolbar = "com.airvtinginc.airvting.broadcast.action.hide.toolbar";
    public static final String BroadcastActionShowToolbar = "com.airvtinginc.airvting.broadcast.action.show.toolbar";
    public static final String BroadcastActionHideAllToolbarIcons = "com.airvtinginc.airvting.broadcast.action.hide.all.toolbar.icons";

    public static String headerTitle = "";
    public String productId;
    private BroadcastReceiver mReceiver;

    private AppPermission mAppPermission;

    @BindView(R.id.rl_action_bar_home)
    RelativeLayout rlActionBarHome;
    @BindView(R.id.action_bar_home_ll_back)
    LinearLayout llBack;
    @BindView(R.id.action_bar_home_ib_back)
    ImageButton btnBack;
    @BindView(R.id.action_bar_home_tv_header_title)
    TextView tvHeaderTitle;
    @BindView(R.id.action_bar_home_ib_notification)
    ImageButton ibNotification;
    @BindView(R.id.action_bar_home_ib_search)
    ImageButton ibSearch;
    @BindView(R.id.action_bar_home_ib_trash)
    ImageButton ibTrash;
    @BindView(R.id.action_bar_home_ib_edit)
    ImageButton ibEdit;
    @BindView(R.id.action_bar_home_ib_add)
    ImageButton ibAdd;
    @BindView(R.id.action_bar_home_ib_save)
    ImageButton ibSave;
    @BindView(R.id.action_bar_home_ib_wallet_token)
    ImageButton ibWalletToken;
    @BindView(R.id.divider)
    View divider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setUpPermission();
        registerReceiver();
        configActionBar();
        replaceFragments();
    }

    private void setUpPermission() {
        mAppPermission = new AppPermission(this, (requestCode -> {
            switch (requestCode) {
                case AppPermission.REQUEST_CODE_CAMERA:
                    recordVideo();
                    break;
                case AppPermission.REQUEST_CODE_GALLERY:
                    selectFromGallery();
                    break;
            }
        }), null);
    }

    public void configActionBar() {
        llBack.setOnClickListener(v -> finish());
        btnBack.setOnClickListener(v -> {
            Utils.hideSoftKeyboard(this);
            finish();
        });

        ibSearch.setOnClickListener(view -> {
            Intent intent = new Intent(BroadcastActionSearch);
            sendBroadcast(intent);
        });

        ibNotification.setOnClickListener(view -> {
            Intent intent = new Intent(BroadcastActionNotification);
            sendBroadcast(intent);
        });

        ibTrash.setOnClickListener(view -> {
            Intent intent = new Intent(BroadcastActionTrash);
            sendBroadcast(intent);
        });

        ibEdit.setOnClickListener(view -> {
            Intent intent = new Intent(BroadcastActionEdit);
            sendBroadcast(intent);
        });

        ibAdd.setOnClickListener(view -> {
            Intent intent = new Intent(BroadcastActionAdd);
            sendBroadcast(intent);
        });

        ibSave.setOnClickListener(view -> {
            Intent intent = new Intent(BroadcastActionSave);
            sendBroadcast(intent);
        });

        ibWalletToken.setOnClickListener(view -> {
            Intent intent = new Intent(BroadcastActionShowWalletToken);
            sendBroadcast(intent);
        });
    }

    private void replaceFragments() {
        DetailType type = DetailType.detachFrom(getIntent());

        switch (type) {
            case PROFILE: {
//                changeMenuIconTint(Color.WHITE);
                divider.setVisibility(View.GONE);
                String userId = (getIntent().getExtras().getString(Constants.EXTRA_USER_ID));
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, ProfileFragment.newInstance(userId, false)).commit();
                break;
            }
            case EDIT_PROFILE:
//                changeMenuIconTint(Color.BLACK);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, EditProfileFragment.newInstance()).commit();
                break;
            case PRODUCT: {
//                changeMenuIconTint(Color.BLACK);
                changeButtonAndBackgroundColor(Color.WHITE, Color.TRANSPARENT);
                String productId = (getIntent().getExtras().getString(Constants.EXTRA_PRODUCT_ID));
                boolean isHideAddToCart = false;
                if (getIntent().hasExtra(Constants.EXTRA_IS_HIDE_ADD_TO_CART)) {
                    isHideAddToCart = getIntent().getExtras().getBoolean(Constants.EXTRA_IS_HIDE_ADD_TO_CART);
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, DetailProductFragment.newInstance(productId, isHideAddToCart)).commit();
                break;
            }
            case GALLERY: {
//                changeMenuIconTint(Color.BLACK);
                String galleryId = (getIntent().getExtras().getString(Constants.EXTRA_GALLERY_ID));
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, DetailGalleryFragment.newInstance(galleryId)).commit();
                break;
            }
            case ADD_NEW_PHOTO:
//                changeMenuIconTint(Color.BLACK);
                String param = (getIntent().getExtras().getString(Constants.EXTRA_IMAGE_URI));
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, DetailAddPhotoFragment.newInstance(param)).commit();
                break;
            case ADD_NEW_VIDEO:
                //Get action type from previous page to take video or get video from gallery
                if (getIntent().getExtras() != null) {
                    if (getIntent().getIntExtra("chooseVideoType", 0) == EnumManager.ChooseVideoType.Camera.ordinal()) {
                        mAppPermission.checkPermissionCamera();
                    } else {
                        mAppPermission.checkPermissionGallery();
                    }
                }
                break;
            case MY_CART:
                divider.setVisibility(View.VISIBLE);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, CartFragment.newInstance()).commit();
                break;
            case ADD_NEW_PRODUCT:
                String productId = (getIntent().getExtras().getString(Constants.EXTRA_PRODUCT_ID));
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, DetailAddProductFragment.newInstance(productId)).commit();
                break;
            case WALLET:
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, DetailWalletFragment.newInstance()).commit();
                break;
            case CONVERSATIONS_LIST:
                divider.setVisibility(View.VISIBLE);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, ConversationsListFragment.newInstance()).commit();
                break;
            case CONVERSATIONS_ADD_NEW: {
                String userId = (getIntent().getExtras().getString(Constants.EXTRA_USER_ID));
                String username = (getIntent().getExtras().getString(Constants.EXTRA_USERNAME));
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, AddConversationsFragment.newInstance(userId, username)).commit();
                break;
            }
            case CONVERSATIONS_DETAIL:
                String conversationId = (getIntent().getExtras().getString(Constants.EXTRA_CONVERSATION_ID));
                if (conversationId != null) {
                    Bundle args = new Bundle();
                    args.putString(Constants.EXTRA_CONVERSATION_ID, conversationId);
                    getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, ConversationsDetailFragment.newInstance(args)).commit();
                }
                break;
            case GIFT_STORE:
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, GiftStoreFragment.newInstance(true, "", "")).commit();
                break;
            case CARD_LIST:
                divider.setVisibility(View.VISIBLE);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, MyCardsFragment.newInstance()).commit();
                break;
            case ADD_CARD:
                divider.setVisibility(View.VISIBLE);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, AddCardsFragment.newInstance()).commit();
                break;
            case EDIT_CARD:
                divider.setVisibility(View.VISIBLE);
                PaymentDetails paymentDetails = (PaymentDetails) getIntent().getExtras().getSerializable(Constants.EXTRA_PAYMENT_DETAILS);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.EXTRA_PAYMENT_DETAILS, paymentDetails);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, EditCardsFragment.newInstance(bundle)).commit();
                break;
            case SHOW_WALLET_TOKEN: {
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, AirTokenFragment.newInstance()).commit();
                break;
            }
            case BOOKMARK:
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, BookmarkFragment.newInstance()).commit();
                break;
            case SETTING:
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, DetailSettingFragment.newInstance()).commit();
                break;
            case TERMS_CONDITIONS:
                divider.setVisibility(View.VISIBLE);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, DetailTermsAndConditionsFragment.newInstance()).commit();
                break;
            case CONTACT_US:
                divider.setVisibility(View.VISIBLE);
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, ContactUsFragment.newInstance()).commit();
                break;
            case FOLLOWS: {
                divider.setVisibility(View.VISIBLE);
                String followType = getIntent().getExtras().getString(Constants.EXTRA_FOLLOW_TYPE);
                String userId = (getIntent().getExtras().getString(Constants.EXTRA_USER_ID));
                getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, FollowsFragment.newInstance(followType, userId)).commit();
                break;
            }
        }
    }

    private void changeButtonAndBackgroundColor(int buttonColor, int backgroundColor) {
        rlActionBarHome.setBackgroundColor(backgroundColor);
        btnBack.setColorFilter(buttonColor);
        tvHeaderTitle.setTextColor(buttonColor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    private void changeMenuIconTint(int color) {
//        // change burger icon tint
//        final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);
//        for (int i = 0; i < toolbar.getChildCount(); i++) {
//            final View v = toolbar.getChildAt(i);
//            if (v instanceof ImageButton) {
//                ((ImageButton) v).setColorFilter(colorFilter);
//            }
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedVideoUri;
        String videoRealPath;
        final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Bundle bundle = new Bundle();
        DetailAddVideoFragment detailAddVideoFragment;

        //Todo Danh note : Need to check crash here. Data return null
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAMERA_VIDEO_REQUEST: {
                    selectedVideoUri = data.getData();

                    try {
                        videoRealPath = RealPathUtil.getRealPath(DetailActivity.this, selectedVideoUri);
                    } catch (Exception e) {
                        videoRealPath = data.getData().getPath();
                    }
                    Log.e("Activity", "onActivityResult: videoRealPath : " + videoRealPath);
                    refreshGallery(videoRealPath);
                    scanIntent.setData(selectedVideoUri);
                    sendBroadcast(scanIntent);
                    bundle.putString("selectedVideoUri", selectedVideoUri.toString());
                    bundle.putString("videoFilePath", videoRealPath);
                    detailAddVideoFragment = DetailAddVideoFragment.newInstance(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, detailAddVideoFragment).commit();

                    break;
                }
                case GALLERY_VIDEO_REQUEST: {
                    selectedVideoUri = data.getData();
                    try {
                        videoRealPath = RealPathUtil.getRealPath(DetailActivity.this, selectedVideoUri);
                    } catch (Exception e) {
                        videoRealPath = data.getData().getPath();
                    }
                    Log.e("Activity", "onActivityResult: videoRealPath : " + videoRealPath);
                    refreshGallery(videoRealPath);
                    scanIntent.setData(selectedVideoUri);
                    sendBroadcast(scanIntent);
                    try {
                        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), selectedVideoUri);
                        int duration = mp.getDuration();
                        mp.release();

                        if ((duration / 1000) > videoTimeLimit) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.video_limit_duration), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            bundle.putString("selectedVideoUri", selectedVideoUri.toString());
                            bundle.putString("videoFilePath", videoRealPath);
                            detailAddVideoFragment = DetailAddVideoFragment.newInstance(bundle);
                            getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, detailAddVideoFragment).commit();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mAppPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void recordVideo() {
        // create a file to save the video
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, videoTimeLimit);
        startActivityForResult(cameraIntent, CAMERA_VIDEO_REQUEST);
    }

    private void selectFromGallery() {
        Intent intent;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        } else {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        }
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.select_video)), GALLERY_VIDEO_REQUEST);
    }

    private void refreshGallery(String filePath) {
        MediaScannerConnection.scanFile(this, new String[]{filePath}, null, (path, uri) -> {
            Log.i("ExternalStorage", "Scanned " + path + ":");
            Log.i("ExternalStorage", "-> uri=" + uri);
        });
    }

    private void registerReceiver() {
        // create new receiver
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (Objects.requireNonNull(intent.getAction())) {
                        case BroadcastActionEdit:
                            Intent editIntent = new Intent(context, DetailActivity.class);
                            editIntent.putExtra(Constants.EXTRA_PRODUCT_ID, productId);
                            DetailType.ADD_NEW_PRODUCT.attachTo(editIntent);
                            startActivity(editIntent);
                            break;
                        case BroadcastActionUpdateHeaderTitle:
                            tvHeaderTitle.setText(headerTitle);
                            break;
                        case BroadcastActionShowIconSearch:
                            ibSearch.setVisibility(View.VISIBLE);
                            break;
                        case BroadcastActionShowIconNotification:
                            ibNotification.setVisibility(View.VISIBLE);
                            break;
                        case BroadcastActionShowIconTrash:
                            ibTrash.setVisibility(View.VISIBLE);
                            break;
                        case BroadcastActionHideIconTrash:
                            ibTrash.setVisibility(View.GONE);
                            break;
                        case BroadcastActionShowIconEdit:
                            if (intent.getExtras() != null) {
                                ibEdit.setVisibility(View.VISIBLE);
                                productId = intent.getStringExtra(Constants.EXTRA_PRODUCT_ID);
                            }
                            break;
                        case BroadcastActionShowIconAdd:
                            ibAdd.setVisibility(View.VISIBLE);
                            break;
                        case BroadcastActionShowIconSave:
                            ibSave.setVisibility(View.VISIBLE);
                            break;
                        case BroadcastActionShowIconWalletToken:
                            if (intent.getExtras() != null) {
                                boolean isShowWalletToken = intent.getExtras().getBoolean(Constants.EXTRA_SHOW_WALLET_TOKEN);
                                if (isShowWalletToken) {
                                    ibWalletToken.setVisibility(View.VISIBLE);
                                } else {
                                    ibWalletToken.setVisibility(View.GONE);
                                }
                            }
                            break;
                        case BroadcastActionShowWalletToken:
                            Intent walletTokenIntent = new Intent(context, DetailActivity.class);
                            DetailType.SHOW_WALLET_TOKEN.attachTo(walletTokenIntent);
                            startActivity(walletTokenIntent);
                            break;
                        case BroadcastActionChangeToolbarToTransparent:
                            changeButtonAndBackgroundColor(Color.WHITE, Color.TRANSPARENT);
                            break;
                        case BroadcastActionChangeToolbarToWhiteColor:
                            changeButtonAndBackgroundColor(Color.BLACK, Color.WHITE);
                            break;
                        case BroadcastActionChangeToolbarToBlackColor:
                            changeButtonAndBackgroundColor(Color.WHITE, Color.BLACK);
                            break;
                        case BroadcastActionHideToolbar:
                            rlActionBarHome.setVisibility(View.GONE);
                            break;
                        case BroadcastActionShowToolbar:
                            rlActionBarHome.setVisibility(View.VISIBLE);
                            break;
                        case BroadcastActionHideAllToolbarIcons:
                            ibNotification.setVisibility(View.GONE);
                            ibSearch.setVisibility(View.GONE);
                            ibTrash.setVisibility(View.GONE);
                            ibEdit.setVisibility(View.GONE);
                            ibAdd.setVisibility(View.GONE);
                            ibSave.setVisibility(View.GONE);
                            ibWalletToken.setVisibility(View.GONE);
                            break;
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(DetailActivity.BroadcastActionEdit);
            filter.addAction(DetailActivity.BroadcastActionUpdateHeaderTitle);
            filter.addAction(DetailActivity.BroadcastActionShowIconSearch);
            filter.addAction(DetailActivity.BroadcastActionShowIconNotification);
            filter.addAction(DetailActivity.BroadcastActionShowIconTrash);
            filter.addAction(DetailActivity.BroadcastActionShowIconEdit);
            filter.addAction(DetailActivity.BroadcastActionShowIconAdd);
            filter.addAction(DetailActivity.BroadcastActionShowIconSave);
            filter.addAction(DetailActivity.BroadcastActionShowIconWalletToken);
            filter.addAction(DetailActivity.BroadcastActionChangeToolbarToTransparent);
            filter.addAction(DetailActivity.BroadcastActionChangeToolbarToWhiteColor);
            filter.addAction(DetailActivity.BroadcastActionChangeToolbarToBlackColor);
            filter.addAction(DetailActivity.BroadcastActionHideToolbar);
            filter.addAction(DetailActivity.BroadcastActionShowToolbar);
            filter.addAction(DetailActivity.BroadcastActionHideAllToolbarIcons);
            filter.addAction(DetailActivity.BroadcastActionHideIconTrash);
            filter.addAction(DetailActivity.BroadcastActionShowWalletToken);
            registerReceiver(mReceiver, filter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.hideSoftKeyboard(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideSoftKeyboard(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
