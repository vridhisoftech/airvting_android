package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.NonSwipeableViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.android.segmented.SegmentedGroup;

public class DetailWalletFragment extends Fragment {

    @BindView(R.id.mWalletViewPager)
    NonSwipeableViewPager mWalletViewPager;
    @BindView(R.id.segmentedButtons)
    SegmentedGroup mSegmentedButtons;
    @BindView(R.id.tvBalance)
    TextView tvBalance;

    public DetailWalletFragment() {
        // Required empty public constructor
    }

    public static DetailWalletFragment newInstance() {
        DetailWalletFragment fragment = new DetailWalletFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_wallet, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        configToolbar();
        setUpTabs();
        setUpSegmentedButton();
        setUpToken();
    }

    private void configToolbar() {
        //Update header title
        DetailActivity.headerTitle = getString(R.string.store_wallet);
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);
    }

    private void setUpSegmentedButton() {
        showView(R.id.btnTransaction);
        mSegmentedButtons.setOnCheckedChangeListener((radioGroup, i) ->
                showView(i)
        );
        mSegmentedButtons.check(R.id.btnTransaction);
    }

    private void setUpTabs() {
        PageFragmentAdapter adapter = new PageFragmentAdapter(getChildFragmentManager());
        adapter.addFragment(DetailWalletTransactionFragment.newInstance(), getActivity().getString(R.string.wallet_transaction));
        adapter.addFragment(DetailWalletGiftFragment.newInstance(), getActivity().getString(R.string.wallet_gift));
        mWalletViewPager.setAdapter(adapter);
    }

    private void showView(int id) {
        Intent intent = new Intent(DetailActivity.BroadcastActionShowIconWalletToken);
        switch (id) {
            case R.id.btnTransaction:
                mWalletViewPager.setCurrentItem(0, true);

                intent.putExtra(Constants.EXTRA_SHOW_WALLET_TOKEN, false);
                getContext().sendBroadcast(intent);
                break;
            case R.id.btnGift:
                mWalletViewPager.setCurrentItem(1, true);

                intent.putExtra(Constants.EXTRA_SHOW_WALLET_TOKEN, true);
                getContext().sendBroadcast(intent);
                break;
        }
    }

    private void setUpToken() {
        tvBalance.setText(String.valueOf(SharedPreferencesManager.getInstance(getContext()).getInt(Constants.PREF_CURRENT_USER_TOKEN, 0)));
    }
}
