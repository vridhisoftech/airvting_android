package com.airvtinginc.airvtinglive.gui.enums;

public enum SearchType {
    Users("people"), Posts("post"), Products("products");

    private String type;

    SearchType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
