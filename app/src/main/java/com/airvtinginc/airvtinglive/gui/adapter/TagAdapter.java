package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.TextViewHolder;
import com.airvtinginc.airvtinglive.models.response.TagChip;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.pchmn.materialchips.model.ChipInterface;

import java.util.List;

/**
 * Simple adapter example for custom items in the dialog
 */
public class TagAdapter extends BaseAdapter {

    private List<? extends ChipInterface> selectedChipList;
    private int textColor;
    private int backgroundColor;

    public TagAdapter(Context context, List<TagChip> searchChips, List<? extends ChipInterface> selectedChipList, OnItemClickListener onItemClickListener) {
        super(context, searchChips, onItemClickListener);
        this.selectedChipList = selectedChipList;
    }

    public void setTextColor(@ColorInt int color) {
        this.textColor = color;
    }

    public void setTextColorRes(Context context, @ColorRes int colorRes) {
        setTextColor(Utils.getColor(context, colorRes));
    }


    public void setBackgroundColor(@ColorInt int color) {
        this.backgroundColor = color;
    }

    public void setBackgroundColorRes(Context context, @ColorRes int colorRes) {
        setBackgroundColor(Utils.getColor(context, colorRes));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_text, parent, false);
        return new TextViewHolder(view);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null) return;
        TextViewHolder holder = (TextViewHolder) mHolder;
        TagChip tagChip = (TagChip) list.get(position);

        holder.tvText.setText(tagChip.getLabel());

        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(tagChip, position);
        });

        if (textColor != 0) {
            holder.tvText.setTextColor(textColor);
            holder.ivCheck.setColorFilter(textColor);
        }

        if (backgroundColor != 0) {
            holder.llBackground.setBackgroundColor(backgroundColor);
        }

        //Show tick icon if User already tagged
        if (isExist(tagChip.getId())) {
            holder.ivCheck.setVisibility(View.VISIBLE);
        } else {
            holder.ivCheck.setVisibility(View.GONE);
        }
    }

    private boolean isExist(Object id) {
        if (selectedChipList != null) {
            for (ChipInterface currentChip : selectedChipList) {
                if (currentChip.getId().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void removeItem(int position) {
        list.remove(position);
        if (selectedChipList != null) {
            selectedChipList.remove(position);
        }
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }
}
