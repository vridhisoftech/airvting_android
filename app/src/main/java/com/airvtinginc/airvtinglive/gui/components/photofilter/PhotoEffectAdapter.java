package com.airvtinginc.airvtinglive.gui.components.photofilter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.airvtinginc.airvtinglive.R;

import java.util.List;

public class PhotoEffectAdapter extends RecyclerView.Adapter<PhotoEffectAdapter.ViewHolder> {
    private Context context;
    private int resource;
    private static int lastPosition = -1;
    private ThumbnailCallback thumbnailCallback;
    private List<ThumbnailItem> dataSet;
    private int selectedItem = -1;

    public PhotoEffectAdapter(Context context, int resource, List<ThumbnailItem> dataSet, ThumbnailCallback thumbnailCallback) {
        this.context = context;
        this.resource = resource;
        this.dataSet = dataSet;
        this.thumbnailCallback = thumbnailCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(resource, parent, false);
        return new ViewHolder(contactView);
    }

    public void setSelectedItem(int selectItemid){
        if (this.selectedItem != selectItemid) {
            this.selectedItem = selectItemid;
            notifyDataSetChanged();
        }
    }

    public int getSelectedItem(){
        return this.selectedItem;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ThumbnailItem thumbnailItem = dataSet.get(position);

        holder.tvEffectName.setText(thumbnailItem.effectName);
        holder.ivThumbnail.setImageBitmap(thumbnailItem.image);
        holder.ivThumbnail.setOnClickListener(v -> {
            if (lastPosition != position) {
                thumbnailCallback.onThumbnailClick(thumbnailItem.filter, position);
                lastPosition = position;
            }
        });

        if (selectedItem == position){
            //selected item will show different
            holder.tvEffectName.setTextColor(context.getResources().getColor(R.color.black));
        }else {
            holder.tvEffectName.setTextColor(context.getResources().getColor(R.color.color_name));
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivThumbnail;
        TextView tvEffectName;

        ViewHolder(View v) {
            super(v);
            ivThumbnail = (ImageView) v.findViewById(R.id.gallery_image_preview_item);
            tvEffectName = (TextView) v.findViewById(R.id.gallery_text);
        }
    }
}
