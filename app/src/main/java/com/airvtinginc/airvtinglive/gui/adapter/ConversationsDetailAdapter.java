package com.airvtinginc.airvtinglive.gui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ConversationsFromFriendViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ConversationsFromMeViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageType;
import com.airvtinginc.airvtinglive.models.MessageContentModel;
import com.airvtinginc.airvtinglive.models.response.Conversation;
import com.airvtinginc.airvtinglive.models.response.Message;
import com.airvtinginc.airvtinglive.models.response.Sender;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ConversationsDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private Conversation conversation;
    private List<Message> messageList;
    private List<String> avatarIndexList;
    private List<String> timeHeaderIndexList;
    private ItemClickImage itemClickImage;

    public ConversationsDetailAdapter(Context context, Conversation conversation, ItemClickImage itemClickImage) {
        this.context = context;
        this.conversation = conversation;
        this.itemClickImage = itemClickImage;
        messageList = new ArrayList<>();
        avatarIndexList = new ArrayList<>();
        timeHeaderIndexList = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messageList.get(position);
        String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (currentUserId.equals(message.getSenderId())) {
            return EnumManager.MessageType.FromMe.getValue();
        } else {
            return EnumManager.MessageType.FromFriend.getValue();
        }
    }

    @Override
    public int getItemCount() {
        if (messageList != null) {
            return messageList.size();
        }
        return 0;
    }

    public void addAllItem(List<Message> messageList, List<String> avatarIndexList, List<String> timeHeaderIndexList) {
        this.messageList.addAll(messageList);
        this.avatarIndexList.addAll(avatarIndexList);
        this.timeHeaderIndexList.addAll(timeHeaderIndexList);
        notifyDataSetChanged();
    }

    public void addMoreData(List<Message> messageList, List<String> avatarIndexList, List<String> timeHeaderIndexList) {
        this.messageList.addAll(0, messageList);
        this.avatarIndexList.addAll(0, avatarIndexList);
        this.timeHeaderIndexList.addAll(0, timeHeaderIndexList);
//        notifyItemRangeChanged(0, messageList.size());
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView;
        if (viewType == EnumManager.MessageType.FromFriend.getValue()) {
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conversations_detail_friend, parent, false);
            return new ConversationsFromFriendViewHolder(layoutView);
        } else {
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conversations_detail_me, parent, false);
            return new ConversationsFromMeViewHolder(layoutView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        Message message = messageList.get(position);
        MessageContentModel content = Utils.parseMessageContent(message.getContent());
        String createAt = null;
        try {
            createAt = DateTimeUtils.getElapsedIntervalConversation(context, DateTimeUtils.getDateFormatUTC(message.getCreatedAt()), new Locale("en", "EN"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mHolder instanceof ConversationsFromFriendViewHolder) {
            final ConversationsFromFriendViewHolder holder = (ConversationsFromFriendViewHolder) mHolder;

            showContent(holder.tvContent, holder.ivContent, holder.pbImageContent, content);
            String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
            Sender sender = conversation.getSender();
            Sender receiver = conversation.getReceiver();
            String avatar, firstName, lastName, displayName;

            if (currentUserId.equals(sender.getUserId())) {
                avatar = receiver.getFeaturedImage();
                firstName = receiver.getFirstName();
                lastName = receiver.getLastName();
                displayName = receiver.getDisplayName();
            } else {
                avatar = sender.getFeaturedImage();
                firstName = sender.getFirstName();
                lastName = sender.getLastName();
                displayName = sender.getDisplayName();
            }

            //Display avatar of friend
            if (avatarIndexList.get(position).equals("showAvatar")) {
                if (TextUtils.isEmpty(avatar)) {
                    User user = new User();
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setDisplayName(displayName);

                    holder.pbAvatar.setVisibility(View.GONE);
                    Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
                } else {
                    holder.pbAvatar.setVisibility(View.VISIBLE);
                    holder.ivAvatar.setVisibility(View.VISIBLE);
                    Glide.with(context).load(avatar)
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    User user = new User();
                                    user.setFirstName(firstName);
                                    user.setLastName(lastName);
                                    user.setDisplayName(displayName);

                                    holder.pbAvatar.setVisibility(View.GONE);
                                    Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    holder.pbAvatar.setVisibility(View.GONE);
                                    Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                                    return false;
                                }
                            })
                            .into(holder.ivAvatar);
                }
            } else {
                holder.ivAvatar.setVisibility(View.INVISIBLE);
                holder.pbAvatar.setVisibility(View.GONE);
            }

            //Display time message
            holder.tvDate.setText(createAt);
            if (timeHeaderIndexList.get(position).equals("showTimeHeader")) {
                holder.tvDate.setVisibility(View.VISIBLE);
            } else {
                holder.tvDate.setVisibility(View.GONE);
                if (position > 0 && getItemViewType(position - 1) != EnumManager.MessageType.FromFriend.getValue()) {
                    holder.marginTopMe.setVisibility(View.VISIBLE);
                    holder.marginTopFriend.setVisibility(View.GONE);
                } else {
                    holder.marginTopMe.setVisibility(View.GONE);
                    holder.marginTopFriend.setVisibility(View.VISIBLE);
                }
            }

        } else if (mHolder instanceof ConversationsFromMeViewHolder) {
            final ConversationsFromMeViewHolder holder = (ConversationsFromMeViewHolder) mHolder;
            showContent(holder.tvContent, holder.ivContent, holder.pbImageContent, content);

            //Display time message
            holder.tvDate.setText(createAt);
            if (timeHeaderIndexList.get(position).equals("showTimeHeader")) {
                holder.tvDate.setVisibility(View.VISIBLE);
            } else {
                holder.tvDate.setVisibility(View.GONE);
                if (position > 0 && getItemViewType(position - 1) != EnumManager.MessageType.FromMe.getValue()) {
                    holder.marginTopMe.setVisibility(View.GONE);
                    holder.marginTopFriend.setVisibility(View.VISIBLE);
                } else {
                    holder.marginTopMe.setVisibility(View.VISIBLE);
                    holder.marginTopFriend.setVisibility(View.GONE);
                }
            }
        }
    }

    private void showContent(TextView tvContent, ImageView ivContent, ProgressBar progressBarLoading, MessageContentModel content) {
        try {
            if (content.getType().equals(MessageType.IMAGE.getType())) {
                Glide.with(context).clear(ivContent);
                if (content.getWidth() > content.getHeight()) {
                    ivContent.getLayoutParams().width = Utils.getWidthScreen((Activity) context) * 2 / 3;
                    ivContent.getLayoutParams().height = Utils.getNewHeightScreenByRatio(content.getWidth(), content.getHeight(), ivContent.getLayoutParams().width);
                } else {
                    ivContent.getLayoutParams().width = context.getResources().getDimensionPixelSize(R.dimen.conversation_image_width_portrait);
                    ivContent.getLayoutParams().height = Utils.getNewHeightScreenByRatio(content.getWidth(), content.getHeight(), ivContent.getLayoutParams().width);
                }
                tvContent.setVisibility(View.GONE);
                ivContent.setVisibility(View.VISIBLE);
                progressBarLoading.setVisibility(View.VISIBLE);
                Glide.with(context).load(content.getMessage())
                        .apply(new RequestOptions().fitCenter())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                progressBarLoading.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                progressBarLoading.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivContent);
                ivContent.setOnClickListener(v -> itemClickImage.onClickImage(Uri.parse(content.getMessage())));
            } else {
                showContentText(tvContent, ivContent, progressBarLoading, content);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            showContentText(tvContent, ivContent, progressBarLoading, content);
        }
    }

    private void showContentText(TextView tvContent, ImageView ivContent, ProgressBar progressBarLoading, MessageContentModel content) {
        tvContent.setVisibility(View.VISIBLE);
        ivContent.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.GONE);
        Glide.with(context).clear(ivContent);
        tvContent.setText(content.getMessage());
    }

    public interface ItemClickImage {
        void onClickImage(Uri uri);
    }
}
