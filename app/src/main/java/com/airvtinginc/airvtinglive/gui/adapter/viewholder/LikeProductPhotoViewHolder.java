package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LikeProductPhotoViewHolder extends BaseViewHolder {
    public View mView;
    public @BindView(R.id.iv_product_photo)
    ImageView ivProductPhoto;
    public @BindView(R.id.pb_product_photo_loading)
    ProgressBar pbPhotoLoading;

    public LikeProductPhotoViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}
