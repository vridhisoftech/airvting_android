package com.airvtinginc.airvtinglive.gui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.RadioButton;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.gui.enums.NotiActivityType;
import com.airvtinginc.airvtinglive.gui.fragments.ActivityNotiFragment;
import com.airvtinginc.airvtinglive.gui.fragments.InboxNotiFragment;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.LeftMenuResponse;
import com.airvtinginc.airvtinglive.models.response.Notification;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.google.gson.Gson;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;

import static com.airvtinginc.airvtinglive.app.MainApplication.getContext;

public class NotificationsActivity extends BaseActivity implements APIResponseListener {

    public static final String BroadcastActionRefreshUnred = "com.airvtinginc.airvting.broadcast.action.notification.refresh";

    @BindView(R.id.mContainer)
    FrameLayout mContainer;
    @BindView(R.id.segmentedButtons)
    SegmentedGroup segmentedButtons;
    @BindView(R.id.btnInbox)
    RadioButton btnInbox;
    @BindView(R.id.btnActivity)
    RadioButton btnActivity;

    private BroadcastReceiver mReceiver;

    @OnClick(R.id.action_bar_home_ib_back)
    void onBackPress() {
        finish();
    }

    @OnClick(R.id.action_bar_home_iv_clear)
    void clearAll() {
        boolean isInboxScreen = (getSupportFragmentManager().findFragmentById(R.id.mContainer) instanceof InboxNotiFragment);

        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogTitle(isInboxScreen ? getString(R.string.dialog_delete_clear_all_inbox_title) : getString(R.string.dialog_delete_clear_all_activity_title));
        messageDialog.setDialogMessage(isInboxScreen ? getString(R.string.dialog_delete_clear_all_inbox_message) : getString(R.string.dialog_delete_clear_all_activity_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                GetListRequest requestModel = new GetListRequest();
                requestModel.setUserId(SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, ""));
                requestModel.setGetNotificationInbox(isInboxScreen);

                requestApi(requestModel, true, RequestTarget.DELETE_NOTIFICATION, NotificationsActivity.this);
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {
                messageDialog.dismiss();
            }
        });
        messageDialog.show(getSupportFragmentManager(), "Delete Dialog");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        registerReceiver();
        replaceFragments(R.id.btnInbox);

        segmentedButtons.setOnCheckedChangeListener((radioGroup, i) ->
                replaceFragments(i)
        );

        segmentedButtons.check(R.id.btnInbox);

        checkBackgroundNotification();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestApi(null, false, RequestTarget.GET_LEFT_MENU_INFO, NotificationsActivity.this);
    }

    private void replaceFragments(int id) {
        Fragment newFragment = null;
        switch (id) {
            case R.id.btnInbox:
                newFragment = InboxNotiFragment.newInstance();
                break;
            case R.id.btnActivity:
                newFragment = ActivityNotiFragment.newInstance();
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.mContainer, newFragment).commit();
    }

    private void checkBackgroundNotification() {
        if (getIntent() != null) {
            String notificationData = getIntent().getStringExtra(ServiceConstants.NOTIFICATION);
            if (!TextUtils.isEmpty(notificationData)) {
                Gson gson = new Gson();
                Notification notification = gson.fromJson(notificationData, Notification.class);
                if (notification != null) {
                    if (notification.getType().equals(NotiActivityType.MESSAGE.getType())) {
                        segmentedButtons.check(R.id.btnInbox);
                    } else {
                        segmentedButtons.check(R.id.btnActivity);
                    }
                }
            }
        }
    }

    private void updateUnreadInbox(int unread) {
        if (unread > 0) {
            btnInbox.setText(String.format(getString(R.string.notifications_number_unread), getString(R.string.notifications_tab_inbox), String.valueOf(unread)));
        } else {
            btnInbox.setText(getString(R.string.notifications_tab_inbox));
        }
    }

    private void updateUnreadActivity(int unread) {
        if (unread > 0) {
            btnActivity.setText(String.format(getString(R.string.notifications_number_unread), getString(R.string.notifications_tab_activity), String.valueOf(unread)));
        } else {
            btnActivity.setText(getString(R.string.notifications_tab_activity));
        }
    }

    private void registerReceiver() {
        // create new receiver
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (Objects.requireNonNull(intent.getAction())) {
                        case BroadcastActionRefreshUnred: {
                            requestApi(null, false, RequestTarget.GET_LEFT_MENU_INFO, NotificationsActivity.this);
                            break;
                        }
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(BroadcastActionRefreshUnred);
            registerReceiver(mReceiver, filter);
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        if (response.isSuccess()) {
            switch (requestTarget) {
                case DELETE_NOTIFICATION:
                    if ((getSupportFragmentManager().findFragmentById(R.id.mContainer) instanceof InboxNotiFragment)) {
                        InboxNotiFragment inboxNotiFragment = (InboxNotiFragment) getSupportFragmentManager().findFragmentById(R.id.mContainer);
                        inboxNotiFragment.refreshNotificationInbox();
                    } else {
                        ActivityNotiFragment activityNotiFragment = (ActivityNotiFragment) getSupportFragmentManager().findFragmentById(R.id.mContainer);
                        activityNotiFragment.refreshActivityNotification();
                    }
                    requestApi(null, false, RequestTarget.GET_LEFT_MENU_INFO, NotificationsActivity.this);
                    break;
                case GET_LEFT_MENU_INFO: {
                    int unreadInbox = ((LeftMenuResponse) response.getData()).getTotalUnReadCountNotifyMessages();
                    updateUnreadInbox(unreadInbox);
                    int unreadActivity = ((LeftMenuResponse) response.getData()).getTotalUnReadCountActivities();
                    updateUnreadActivity(unreadActivity);
                    break;
                }
            }
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        switch (statusCode) {
            case ERR_NO_INTERNET_CONNECTION:
                DialogUtils.showMessageDialog(this, getSupportFragmentManager(), getString(R.string.error_network_title), failMessage);
                break;
            case ERR_UNAUTHORIZED:
                DialogUtils.showUnauthorizedDialog(getContext(), getSupportFragmentManager(), failMessage);
                break;
        }
    }
}
