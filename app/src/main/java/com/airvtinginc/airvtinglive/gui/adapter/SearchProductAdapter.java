package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.ResizableImageView;
import com.airvtinginc.airvtinglive.models.response.Product;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchProductAdapter extends BaseAdapter {
    private SearchProductListener searchProductListener;

    public interface SearchProductListener {
        void onLikeClick(int position);
    }

    public SearchProductAdapter(Context context, List<Product> productList, OnItemClickListener onItemClickListener, SearchProductListener searchProductListener) {
        super(context, productList, onItemClickListener);
        this.searchProductListener = searchProductListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_products, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        ViewHolder holder = (ViewHolder) mHolder;
        if (list != null && list.size() > 0) {
//            Product product = productList.get(position);
            //Set product data
            holder.cvBound.setOnClickListener(view -> onItemClickListener.onItemClicked(list.get(position), position));
            holder.btnLike.setOnClickListener(view -> searchProductListener.onLikeClick(position));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_search_product_cv_bound)
        CardView cvBound;
        @BindView(R.id.item_search_product_iv_thumbnail)
        ResizableImageView ivThumbnail;
        @BindView(R.id.item_search_product_tv_productName)
        TextView tvProductName;
        @BindView(R.id.item_search_product_tv_price)
        TextView tvProductPrice;
        @BindView(R.id.item_search_product_tv_date)
        AppCompatTextView tvDate;
        @BindView(R.id.item_search_product_tv_heart_count)
        TextView tvHeartCount;
        @BindView(R.id.item_search_product_btn_like)
        ImageButton btnLike;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
