package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.LiveCommentsViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.CommentType;
import com.airvtinginc.airvtinglive.gui.enums.LiveStreamType;
import com.airvtinginc.airvtinglive.models.response.Comment;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.airvtinginc.airvtinglive.tools.fonts.CustomTypefaceSpan;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

public class LiveCommentsAdapter extends BaseAdapter {

    private LiveStreamType liveStreamType;

    public LiveCommentsAdapter(Context context, OnItemClickListener onItemClickListener, LiveStreamType liveStreamType) {
        super(context, new ArrayList<>(), onItemClickListener);
        this.liveStreamType = liveStreamType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_live_comment, parent, false);
        LiveCommentsViewHolder vh = new LiveCommentsViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null) {
            return;
        }
        final LiveCommentsViewHolder holder = (LiveCommentsViewHolder) mHolder;
        Comment comment = (Comment) list.get(position);

        String separator = " ";
        String username = "@" + comment.getUserName();
        String content;
        if (comment.getType().equals(CommentType.GIFT.getType())) {
            content = context.getString(R.string.give_a);
            holder.ivContent.setVisibility(View.VISIBLE);
            Glide.with(context).load(comment.getComment())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(holder.ivContent);
        } else {
            content = comment.getComment();
            holder.ivContent.setVisibility(View.GONE);
        }

        String itemValue = username + separator + content;
        SpannableStringBuilder SS = new SpannableStringBuilder(itemValue);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                String currentUserId = SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, "");
                if (!comment.getUserId().equals(currentUserId)) {
                    Utils.openUserDetailOrder(context, comment.getUserId());
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        if (liveStreamType != LiveStreamType.LIVE) {
            SS.setSpan(clickableSpan, 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        SS.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.color_comment_username)), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_semibold)), 0, username.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_regular)), username.length(), itemValue.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        holder.tvContent.setText(SS);
        if (liveStreamType != LiveStreamType.LIVE) {
            holder.tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        }

        holder.mView.setOnClickListener(view -> {
        });
    }
}
