package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.ProductPagerAdapter;
import com.airvtinginc.airvtinglive.gui.components.AutoScrollViewPager;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.models.PriceWhenStream;
import com.airvtinginc.airvtinglive.models.response.LikeProductResponse;
import com.airvtinginc.airvtinglive.models.response.ProductDetailResponse;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class DetailProductFragment extends BaseFragment {

    private static final String TAG = DetailProductFragment.class.getName();

    @BindView(R.id.mViewPager)
    AutoScrollViewPager mViewPager;
    @BindView(R.id.mIndicator)
    CirclePageIndicator mIndicator;
    @BindView(R.id.llLikeProduct)
    LinearLayout mLlLikeProduct;
    @BindView(R.id.ivLike)
    ImageView mIvLike;
    @BindView(R.id.tvLikeCount)
    TextView mTvLikeCount;
    @BindView(R.id.tvProductName)
    TextView mTvProductName;
    //    @BindView(R.id.tvLikeCount)
//    TextView mTvLikeCount;
    @BindView(R.id.tvCategory)
    TextView mTvCategory;
    @BindView(R.id.tvDate)
    TextView mTvDate;
    @BindView(R.id.tvDescription)
    TextView mTvDescription;
    @BindView(R.id.pbDateSale)
    ProgressBar pbDateSale;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.tvPriceSale)
    TextView tvPriceSale;
    @BindView(R.id.tvDiscountSale)
    TextView tvDiscountSale;
    @BindView(R.id.llAddToCart)
    LinearLayout llAddToCart;
    @BindView(R.id.tvNumberDateSale)
    TextView tvNumberDateSale;
    @BindView(R.id.tvDateFormat)
    TextView tvDateFormat;
    @BindView(R.id.rlDateSale)
    RelativeLayout rlDateSale;

    private ProductPagerAdapter mProductPagerAdapter;
    private String mProductId;
    private boolean mIsHideAddToCart;

    private boolean mIsUpdateLike;
    private int mCurrentLikes;
    private String mUserId;
    private boolean isLike;
    private Product product;

    public DetailProductFragment() {
        // Required empty public constructor
    }

    public static DetailProductFragment newInstance(String productId, boolean isHideAddToCart) {
        DetailProductFragment fragment = new DetailProductFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_PRODUCT_ID, productId);
        args.putBoolean(Constants.EXTRA_IS_HIDE_ADD_TO_CART, isHideAddToCart);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductId = getArguments().getString(Constants.EXTRA_PRODUCT_ID);
            mIsHideAddToCart = getArguments().getBoolean(Constants.EXTRA_IS_HIDE_ADD_TO_CART);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_product, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setUpViewPager();
        Intent intent = new Intent(DetailActivity.BroadcastActionChangeToolbarToTransparent);
        getActivity().sendBroadcast(intent);

        mLlLikeProduct.setOnClickListener(v -> {
            if (isLike) {
                mIvLike.setImageResource(R.drawable.ic_heart_full);
                isLike = false;
            } else {
                mIvLike.setImageResource(R.drawable.ic_heart_white_empty);
                isLike = true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getProductDetail();
        setHeaderTitle("");
    }

    private void setUpViewPager() {
        mProductPagerAdapter = new ProductPagerAdapter(getContext());
        mViewPager.setAdapter(mProductPagerAdapter);
        mIndicator.setViewPager(mViewPager);
        mViewPager.setInterval(3000);
        mViewPager.startAutoScroll();
    }

    private void getProductDetail() {
        if (!TextUtils.isEmpty(mProductId)) {
            requestApi(mProductId, true, RequestTarget.GET_PRODUCT_DETAIL, this);
        }
    }

    private void setHeaderTitle(String title) {
        DetailActivity.headerTitle = title;
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);
    }

    private void showProductDetail(Product product) {
        if (product == null || TextUtils.isEmpty(product.getProductId())) {
            productNotFind();
            return;
        }
        this.product = product;
        List<FeaturedImage> featuredImages = product.getFeaturedImages();
        if (featuredImages != null && !featuredImages.isEmpty()) {
//            String displayNumberPhotos;
//            if (featuredImages.size() > 1) {
//                displayNumberPhotos = featuredImages.size() + " " + getString(R.string.product_photos);
//            } else {
//                displayNumberPhotos = getString(R.string.product_one_photo);
//            }
//
//            mTvPhotoCount.setText(displayNumberPhotos);
//            mTvPhotoCount.setVisibility(View.VISIBLE);

            mProductPagerAdapter.setProductImages(featuredImages);
        }
        if (product.getProductCategories() != null && !product.getProductCategories().isEmpty()) {
            mTvCategory.setText(product.getProductCategories().get(0).getTitle());
        }
        mTvProductName.setText(product.getTitle());
        mTvDescription.setText(product.getDescription());

        try {
            mTvDate.setText((DateTimeUtils.getElapsedInterval(getActivity(), DateTimeUtils.getDateFormatUTC(product.getCreatedAt()))).toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkAndShowPrice();

        mUserId = product.getOwner().getUserId();
        String currentUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");

        if (!currentUserId.equals(mUserId)) {
            llAddToCart.setVisibility(View.VISIBLE);
        } else {
            llAddToCart.setVisibility(View.GONE);

            // Show Edit button
            Intent intent = new Intent(DetailActivity.BroadcastActionShowIconEdit);
            intent.putExtra(Constants.EXTRA_PRODUCT_ID, product.getProductId());
            getActivity().sendBroadcast(intent);
        }

        if (mIsHideAddToCart) {
            llAddToCart.setVisibility(View.GONE);
        }

        // Need to check current user or not to add product to cart
        llAddToCart.setOnClickListener(view -> {
            addProductToCart();
        });


        isLike = product.isLike();

        setLikeUI(false);

        mLlLikeProduct.setOnClickListener(view -> {
            isLike = !isLike;
            setLikeUI(true);
        });
    }

    private void addProductToCart() {
        Realm realm = Utils.getCartRealm(getContext());
        realm.beginTransaction();
        Product realmProduct = realm.where(Product.class).equalTo(Constants.REALM_PRODUCT_ID_PRIMARY_KEY, product.getProductId()).findFirst();
        Product cartProduct;
        if (realmProduct != null) {
            cartProduct = new Product();
            cartProduct.setQuantityToBuy(realmProduct.getQuantityToBuy() + 1);
        } else {
            cartProduct = realm.createObject(Product.class);
            cartProduct.setQuantityToBuy(1);
        }

        cartProduct.setProductId(product.getProductId());
        cartProduct.setDisplayImage(product.getFeaturedImages().get(0).getFeaturedImage());
        cartProduct.setTitle(product.getTitle());
        cartProduct.setSellerId(mUserId);

        if (realmProduct != null) {
            realm.copyToRealmOrUpdate(cartProduct);
        }

        realm.commitTransaction();
        Toast.makeText(getContext(), String.format(getString(R.string.my_cart_add_product), product.getTitle()), Toast.LENGTH_SHORT).show();
    }

    private void checkAndShowPrice() {
        PriceWhenStream priceWhenStream = product.getPriceWhenStream();
        if (priceWhenStream != null && priceWhenStream.isActive()) {
            showPrice(product.getPrice(), priceWhenStream.getPriceDiscount(), priceWhenStream.getDiscount(), true);
            showPriceTime(priceWhenStream.getStartedAt(), priceWhenStream.getExpiredAt());
        } else {
            String discountStartTimeUTC = product.getStartedAt();
            String discountExpiredTimeUTC = product.getExpiredAt();
            if (!TextUtils.isEmpty(discountStartTimeUTC) && !TextUtils.isEmpty(discountExpiredTimeUTC)) {
                try {
                    String discountStartTime = DateTimeUtils.getDateFormatUTC(discountStartTimeUTC);
                    String discountExpiredTime = DateTimeUtils.getDateFormatUTC(discountExpiredTimeUTC);
                    Date discountStartDate = DateTimeUtils.getDateFollowFormat(discountStartTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date discountExpiredDate = DateTimeUtils.getDateFollowFormat(discountExpiredTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date currentDate = DateTimeUtils.getCurrentDate(true);

                    if (currentDate.compareTo(discountStartDate) >= 0 && currentDate.compareTo(discountExpiredDate) <= 0) {
                        if (!TextUtils.isEmpty(product.getDiscount()) && !product.getDiscount().equals("0%")) {
                            showPrice(product.getPrice(), product.getPriceSale(), product.getDiscount(), true);
                            showPriceTime(product.getStartedAt(), product.getExpiredAt());
                        } else {
                            showPrice(product.getPrice(), 0, product.getDiscount(), false);
                        }
                    } else {
                        showPrice(product.getPrice(), 0, product.getDiscount(), false);
                    }
                } catch (Exception e) {
                    AppLog.e(e.getMessage());
                }
            } else {
                showPrice(product.getPrice(), 0, product.getDiscount(), false);
            }
        }
    }

    private void showPrice(float price, float priceSale, String discount, boolean isShowPriceSale) {
        String displayPrice = NumberUtils.formatPrice(price);

        if (isShowPriceSale) {
            SpannableStringBuilder ssbPrice = new SpannableStringBuilder(displayPrice);
            // Set span for base price in sale
            ssbPrice.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorBody)), 0, displayPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssbPrice.setSpan(new StrikethroughSpan(), 0, displayPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvPrice.setText(ssbPrice);
            rlDateSale.setVisibility(View.VISIBLE);
            tvPriceSale.setVisibility(View.VISIBLE);
            tvDiscountSale.setVisibility(View.VISIBLE);

            String discountFormat = String.format(getResources().getString(R.string.product_price_sale), (discount.substring(discount.length() - 1).equals("%") ? discount : NumberUtils.formatPrice(Float.parseFloat(discount))));

            tvPriceSale.setText(NumberUtils.formatPrice(priceSale));
            tvDiscountSale.setText(discountFormat);
        } else {
            tvPrice.setText(displayPrice);
            tvPriceSale.setVisibility(View.GONE);
            tvDiscountSale.setVisibility(View.GONE);
            rlDateSale.setVisibility(View.GONE);
        }
    }

    private void showPriceTime(String startedAt, String expiredAt) {
        long timeSaleMillisecond = DateTimeUtils.getTimeStamp(expiredAt) - DateTimeUtils.getTimeStamp(startedAt);
        long timeExpireMillisecond = 0l;
        try {
            timeExpireMillisecond = DateTimeUtils.getTimeStamp(DateTimeUtils.getDateFormatUTC(expiredAt)) - DateTimeUtils.getCurrentDate(true).getTime();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        long minExpire = timeExpireMillisecond / 1000 / 60;
        long hourExpire = minExpire / 60;
        long dayExpire = hourExpire / 24;

        pbDateSale.setProgress(100 - (int) (timeExpireMillisecond * 100 / timeSaleMillisecond));

        if (dayExpire > 0) {
            if (dayExpire == 1) {
                tvDateFormat.setText(getString(R.string.product_sale_text_day));
                tvNumberDateSale.setText("1");
            } else {
                tvDateFormat.setText(getString(R.string.product_sale_text_days));
                tvNumberDateSale.setText(String.valueOf((int) dayExpire));
            }
        } else {
            if (hourExpire > 0) {
                if (hourExpire == 1) {
                    tvDateFormat.setText(getString(R.string.product_sale_text_hour));
                    tvNumberDateSale.setText("1");
                } else {
                    tvDateFormat.setText(getString(R.string.product_sale_text_hours));
                    tvNumberDateSale.setText(String.valueOf((int) hourExpire));
                }
            } else {
                if (minExpire == 1) {
                    tvDateFormat.setText(getString(R.string.product_sale_text_minute));
                    tvNumberDateSale.setText("1");
                } else {
                    tvDateFormat.setText(getString(R.string.product_sale_text_minutes));
                    tvNumberDateSale.setText(String.valueOf((int) minExpire));
                }
            }
        }
    }

    private void setLikeUI(boolean isSendToServer) {
        int currentTotalLikes;
        if (isLike) {
            mIvLike.setImageResource(R.drawable.ic_heart_full);
        } else {
            mIvLike.setImageResource(R.drawable.ic_heart_white_empty);
        }

        if (isLike == product.isLike()) {
            currentTotalLikes = product.getTotalLike();
        } else {
            if (product.isLike()) {
                currentTotalLikes = product.getTotalLike() - 1;
            } else {
                currentTotalLikes = product.getTotalLike() + 1;
            }
        }
        mTvLikeCount.setText(String.valueOf(currentTotalLikes));
        if (isSendToServer) {
            likeProduct();
        }
    }

    private void likeProduct() {
        mIvLike.setEnabled(false);
        if (TextUtils.isEmpty(mProductId)) {
            return;
        }

        requestApi(mProductId, false, RequestTarget.LIKE_PRODUCT, this);
    }

    private void productNotFind() {
        getActivity().finish();
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        mIvLike.setEnabled(true);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_PRODUCT_DETAIL:
                    showProductDetail(((ProductDetailResponse) response.getData()).getProduct());

                    break;
                case LIKE_PRODUCT:
                    LikeProductResponse likeProductResponse = (LikeProductResponse) response.getData();
                    isLike = likeProductResponse.isLike();
                    setLikeUI(false);
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        mIvLike.setEnabled(true);
    }
}
