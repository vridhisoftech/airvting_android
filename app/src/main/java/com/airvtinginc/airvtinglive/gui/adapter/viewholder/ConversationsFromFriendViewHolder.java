package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConversationsFromFriendViewHolder extends BaseViewHolder {
    @BindView(R.id.ivDefaultAvatar)
    public CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    public GradientTextView tvDefaultAvatar;
    @BindView(R.id.ivAvatar)
    public CircleImageView ivAvatar;
    @BindView(R.id.pbLoadAvatar)
    public ProgressBar pbAvatar;
    @BindView(R.id.item_conversations_detail_friend_tv_date)
    public TextView tvDate;
    @BindView(R.id.item_conversations_detail_friend_tv_content)
    public TextView tvContent;
    @BindView(R.id.item_conversations_detail_friend_iv_content)
    public ImageView ivContent;
    @BindView(R.id.pb_image_content)
    public ProgressBar pbImageContent;
    @BindView(R.id.marginTopMe)
    public View marginTopMe;
    @BindView(R.id.marginTopFriend)
    public View marginTopFriend;

    public ConversationsFromFriendViewHolder(View paramView) {
        super(paramView);
        ButterKnife.bind(this, paramView);
    }
}
