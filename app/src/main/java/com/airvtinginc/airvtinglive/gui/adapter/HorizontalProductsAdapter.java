package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.HorizontalProductsViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.models.PriceWhenStream;
import com.airvtinginc.airvtinglive.models.response.LikeProductResponse;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Date;
import java.util.List;

public class HorizontalProductsAdapter extends BaseAdapter {

    private static final String TAG = HorizontalProductsAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    private Product productUpdate;
    private boolean isUpdate;

    public HorizontalProductsAdapter(Context context, List<Product> productList, OnItemClickListener onItemClickListener) {
        super(context, productList, onItemClickListener);
    }

    public HorizontalProductsAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        context = parent.getContext();
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_horizontal_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_horizontal_product, parent, false);
                HorizontalProductsViewHolder vh = new HorizontalProductsViewHolder(v);
                return vh;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder)
            return;
        isUpdate = false;
        final HorizontalProductsViewHolder holder = (HorizontalProductsViewHolder) mHolder;
        Product product = (Product) this.list.get(position);

        List<FeaturedImage> featuredImages = product.getFeaturedImages();

        if (featuredImages != null && !featuredImages.isEmpty()) {
            holder.pbLoadingThumbnail.setVisibility(View.VISIBLE);
            Glide.with(context).load(featuredImages.get(0).getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbLoadingThumbnail.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbLoadingThumbnail.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivThumbnail);
        } else {
        }

        holder.tvLikeCount.setText(String.valueOf(product.getTotalLike()));
        holder.tvProductName.setText(product.getTitle());

        checkAndShowPrice(product, holder);

        try {
            holder.tvDate.setText((DateTimeUtils.getElapsedInterval(context, DateTimeUtils.getDateFormatUTC(product.getCreatedAt()))).toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (product.isLike()) {
            holder.ivLike.setImageResource(R.drawable.ic_heart_full);
        } else {
            holder.ivLike.setImageResource(R.drawable.ic_heart_empty);
        }
        holder.ivLike.setOnClickListener(view -> {
            holder.ivLike.setEnabled(false);
            if (TextUtils.isEmpty(product.getProductId())) {
                return;
            }

            requestApi(product.getProductId(), false, RequestTarget.LIKE_PRODUCT, new APIResponseListener() {
                @Override
                public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
                    DialogUtils.hideLoadingProgress();
                    switch (requestTarget) {
                        case LIKE_PRODUCT:
                            holder.ivLike.setEnabled(true);
                            if (response.isSuccess()) {
                                LikeProductResponse likeProductResponse = (LikeProductResponse) response.getData();
                                product.setLike(likeProductResponse.isLike());
                                if (likeProductResponse.isLike()) {
                                    product.setTotalLike(product.getTotalLike() + 1);
                                } else {
                                    product.setTotalLike(product.getTotalLike() - 1);
                                }
                                notifyDataSetChanged();
                            } else {
                                Log.e(TAG, "LIKE_PRODUCT: fail");
                            }
                            break;
                    }
                }

                @Override
                public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
                    DialogUtils.hideLoadingProgress();
                }
            });
        });
        holder.mView.setOnClickListener(v -> {
            openProductDetail(position);
        });
    }

    private void checkAndShowPrice(Product product, HorizontalProductsViewHolder holder) {
        PriceWhenStream priceWhenStream = product.getPriceWhenStream();
        if (priceWhenStream != null && priceWhenStream.isActive()) {
            showPrice(priceWhenStream.getPriceDiscount(), true, holder);
        } else {
            String discountStartTimeUTC = product.getStartedAt();
            String discountExpiredTimeUTC = product.getExpiredAt();
            if (!TextUtils.isEmpty(discountStartTimeUTC) && !TextUtils.isEmpty(discountExpiredTimeUTC)) {
                try {
                    String discountStartTime = DateTimeUtils.getDateFormatUTC(discountStartTimeUTC);
                    String discountExpiredTime = DateTimeUtils.getDateFormatUTC(discountExpiredTimeUTC);
                    Date discountStartDate = DateTimeUtils.getDateFollowFormat(discountStartTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date discountExpiredDate = DateTimeUtils.getDateFollowFormat(discountExpiredTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date currentDate = DateTimeUtils.getCurrentDate(true);

                    if (currentDate.compareTo(discountStartDate) >= 0 && currentDate.compareTo(discountExpiredDate) <= 0) {
                        if (!TextUtils.isEmpty(product.getDiscount()) && !product.getDiscount().equals("0%")) {
                            showPrice(product.getPriceSale(), true, holder);
                        } else {
                            showPrice(product.getPrice(), false, holder);
                        }
                    } else {
                        showPrice(product.getPrice(), false, holder);
                    }
                } catch (Exception e) {
                    AppLog.e(e.getMessage());
                }
            } else {
                showPrice(product.getPrice(), false, holder);
            }
        }
    }

    private void showPrice(float price, boolean isShowPriceSale, HorizontalProductsViewHolder holder) {
        String displayPrice = NumberUtils.formatPrice(price);
        holder.tvPrice.setText(displayPrice);

        int priceColorResource = context.getResources().getColor(R.color.black);
        if (isShowPriceSale) {
            priceColorResource = context.getResources().getColor(R.color.color_product_price_sale);
        }
        holder.tvPrice.setTextColor(priceColorResource);

        if (isShowPriceSale) {
            holder.ivPriceSale.setVisibility(View.VISIBLE);
        } else {
            holder.ivPriceSale.setVisibility(View.GONE);
        }
    }

    private void openProductDetail(int position) {
        Product product = (Product) list.get(position);
        productUpdate = product;
        isUpdate = true;
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_PRODUCT_ID, product.getProductId());
        intent.putExtras(args);
        // Sender usage
        DetailType.PRODUCT.attachTo(intent);
        context.startActivity(intent);
    }

    public void updateProduct() {
        if (productUpdate == null || !isUpdate) {
            return;
        }

        requestApi(productUpdate.getProductId(), false, RequestTarget.GET_PRODUCT_DETAIL, new APIResponseListener() {
            @Override
            public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
                DialogUtils.hideLoadingProgress();
                switch (requestTarget) {
                    case GET_PRODUCT_DETAIL:
                        Product product = ((ProductDetailResponse) response.getData()).getProduct();
                        for (int index = 0; index < list.size(); index++) {
                            if (list.get(index) != null && product.getProductId().equals(((Product) list.get(index)).getProductId())) {
                                list.set(index, product);
                                notifyDataSetChanged();
                            }
                        }
                        break;
                }
            }

            @Override
            public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
                DialogUtils.hideLoadingProgress();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }

    public Product getProduct(int position) {
        return (Product) list.get((position));
    }
}
