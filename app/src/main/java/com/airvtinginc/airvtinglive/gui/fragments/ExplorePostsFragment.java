package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ExplorePostsAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.models.request.CategoryPostsRequest;
import com.airvtinginc.airvtinglive.models.request.TypeRequest;
import com.airvtinginc.airvtinglive.models.response.CategoryItem;
import com.airvtinginc.airvtinglive.models.response.CategoryListModelResponse;
import com.airvtinginc.airvtinglive.models.response.ExplorePosts;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExplorePostsFragment extends BaseFragment implements APIResponseListener, BaseAdapter.OnItemClickListener {

    private static final String TAG = ExplorePostsFragment.class.getSimpleName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.tvShowEmpty)
    TextView tvShowEmpty;

    private ExplorePostsAdapter mAdapter;
    private List<CategoryItem> mCategoryList;

    public ExplorePostsFragment() {
        // Required empty public constructor
    }

    public static ExplorePostsFragment newInstance() {
        ExplorePostsFragment fragment = new ExplorePostsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setUpRecyclerView();
        getAllCategories();
    }

    private void setUpRecyclerView() {
        // use a linear layout manager
        emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        mRecyclerView.setEmptyView(emptyView);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new ExplorePostsAdapter(getContext(), this);
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mAdapter.clearAllItems();
            mSwipeRefreshLayout.setRefreshing(false);
            getAllCategories();
        });
    }

    private void getAllCategories() {
        TypeRequest modelRequest = new TypeRequest();
        modelRequest.setType("post");
        requestApi(modelRequest, false, RequestTarget.GET_CATEGORY, this);
    }

    private void getCategory() {
        if (mCategoryList != null && !mCategoryList.isEmpty()) {
            for (CategoryItem category : mCategoryList) {
                getCategoryPosts(category);
            }
        }
    }

    private void getCategoryPosts(CategoryItem category) {
        String[] typePost = {MediaType.STREAM.getType(), MediaType.VIDEO.getType()};
        CategoryPostsRequest requestModel = new CategoryPostsRequest();
        requestModel.setCategoryId(category.getId());
        requestModel.setTypePost(typePost);
        requestModel.setPaginate(1);
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestModel.setMaxId(null);

        requestApi(requestModel, true, RequestTarget.GET_CATEGORY_POSTS, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_CATEGORY:
                    mCategoryList = ((CategoryListModelResponse) response.getData()).getCategoryItemList();
                    for (CategoryItem category : mCategoryList) {
                        mAdapter.addItem(new ExplorePosts(category.getId(), category.getTitle(), "", new ArrayList<>()), 0, false);
                    }
                    getCategory();
                    break;
                case GET_CATEGORY_POSTS:
                    List<PostDetail> posts = ((PostResponse) response.getData()).getListPostDetail();
                    String categoryId = ((PostResponse) response.getData()).getCategoryId();
                    String maxId = ((PostResponse) response.getData()).getMaxId();
                    for (int i = 0; i < mCategoryList.size(); i++) {
                        CategoryItem category = mCategoryList.get(i);
                        if (categoryId.equals(category.getId())) {
                            mAdapter.addItem(new ExplorePosts(categoryId, category.getTitle(), maxId, posts), i, true);
                            break;
                        }
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.clearAllItems();
        emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        tvShowEmpty.setText("No Network Available, Please Connect with the Internet.");
        mRecyclerView.setEmptyView(emptyView);
    }

    @Override
    public <T> void onItemClicked(T item, int position) {

    }
}
