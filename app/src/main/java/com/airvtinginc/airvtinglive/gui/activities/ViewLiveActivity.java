package com.airvtinginc.airvtinglive.gui.activities;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.red5.BasePublishTestFragment;
import com.airvtinginc.airvtinglive.components.red5.PublishTestListener;
import com.airvtinginc.airvtinglive.components.red5.Red5LiveDetailFragment;
import com.airvtinginc.airvtinglive.components.red5.SubscribeTest.SubscribeTestFragment;
import com.airvtinginc.airvtinglive.components.services.FireStoreManager;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.LiveCommentsAdapter;
import com.airvtinginc.airvtinglive.gui.dialog.SaveDiscardDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.LiveStreamType;
import com.airvtinginc.airvtinglive.models.request.PostRequest;
import com.airvtinginc.airvtinglive.models.request.SaveStreamRequest;
import com.airvtinginc.airvtinglive.models.response.GiftInFireStore;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ReportResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;
import com.airvtinginc.airvtinglive.models.response.Viewer;
import com.airvtinginc.airvtinglive.tools.AppPermission;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.ListenerRegistration;

import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.airvtinginc.airvtinglive.app.MainApplication.getContext;

public class ViewLiveActivity extends BaseActivity implements PublishTestListener, SaveDiscardDialog.EndStreamListener, APIResponseListener {
    private static final String TAG = ViewLiveActivity.class.getSimpleName();

    public Red5LiveDetailFragment fragment = null;
    private boolean requiresBufferDialog = false;
    private final int BUFFER_EVT = 1000;

    public OnActionLive onActionLive;

    public String streamerId;
    public String postId;
    public String mediaUrl;
    public String deviceName;
    public String viewerId;
    public boolean isLike;
    public int statusLive = -1;

    public ArrayList<Viewer> likes;
    public ArrayList<GiftInFireStore> gifts;
    public LiveCommentsAdapter commentsAdapter;
    public LiveStreamType liveStreamType;

    public int mCurrentCamera;

    public Unregistrar mUnregistrar;

    public AppPermission mAppPermission;

    public ListenerRegistration listenerComments;

    public ListenerRegistration listenerViewers;

    public ListenerRegistration listenerPost;

    public ListenerRegistration listenerGifts;

    @Override
    protected void onPause() {
        super.onPause();
        if (fragment != null && fragment.isPublisherTest()) {
            BasePublishTestFragment pFragment = (BasePublishTestFragment) fragment;
            pFragment.stopPublish(this);

            FireStoreManager.getInstance().setIsLive(postId, false, false);
            setDiscardLive();
            fragment = null;
            finish();
        }
    }

    private void setDiscardLive() {
        Log.d(TAG, "postId setSaveLive: " + postId);
        if (!TextUtils.isEmpty(postId)) {
            PostRequest postRequest = new PostRequest(postId);
            SaveStreamRequest saveStreamRequest = new SaveStreamRequest();
            saveStreamRequest.setSaveStream(false);
            postRequest.setSaveStreamRequest(saveStreamRequest);

            requestApi(postRequest, false, RequestTarget.END_STREAM, this);
        }
        SharedPreferencesManager.getInstance(getContext()).putString(Constants.PREF_CURRENT_POST_ID, "");
    }

    @Override
    public void onBackPressed() {
        if (TextUtils.isEmpty(streamerId) && !TextUtils.isEmpty(postId)) {
            SaveDiscardDialog saveDiscardDialog = SaveDiscardDialog.newInstance(postId, commentsAdapter.getItemCount(), likes.size(), gifts.size(), mCurrentCamera);
            saveDiscardDialog.setEndStreamListener(this);
            saveDiscardDialog.show(getSupportFragmentManager(), "Show Save/Discard Dialog");
        } else {
            onCloseStream();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (listenerComments != null) {
            listenerComments.remove();
        }
        if (listenerViewers != null) {
            listenerViewers.remove();
        }
        if (listenerPost != null) {
            listenerPost.remove();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mAppPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void onCloseStream() {
        if (fragment != null && fragment.isPublisherTest()) {
            BasePublishTestFragment pFragment = (BasePublishTestFragment) fragment;
            pFragment.stopPublish(this);
            FireStoreManager.getInstance().setIsLive(postId, false, false);
            SharedPreferencesManager.getInstance(getContext()).putString(Constants.PREF_CURRENT_POST_ID, "");
        }
        if (fragment != null && fragment instanceof SubscribeTestFragment) {
            SubscribeTestFragment subscribeTestFragment = (SubscribeTestFragment) fragment;
            if (subscribeTestFragment.getSubscribe() != null) {
                subscribeTestFragment.getSubscribe().stop();
            }
        }
        super.onBackPressed();
        mUnregistrar.unregister();
        fragment = null;
        setViewerView(false);
        finish();
    }

    public void setViewerView(boolean isView) {
        if (TextUtils.isEmpty(streamerId)) {
            return;
        }
        if (postId.equals("")) {
            return;
        }
        if (TextUtils.isEmpty(viewerId)) {
            getIdView(isView);
        } else {
            updateViewerSave(viewerId, isView);
            FireStoreManager.getInstance().getViewersRef().document(viewerId)
                    .update(Constants.FIELD_IS_VIEW, isView)
                    .addOnSuccessListener(aVoid -> Log.d(TAG, "setViewerView DocumentSnapshot successfully updated!"))
                    .addOnFailureListener(e -> Log.w(TAG, "setViewerView Error updating document", e));
        }
    }

    public void getIdView(boolean isView) {
        FireStoreManager.getInstance().getViewersRef().whereEqualTo(Constants.FIELD_POST_ID, postId)
                .whereEqualTo(Constants.FIELD_USER_ID, SharedPreferencesManager.getInstance(ViewLiveActivity.this).getString(Constants.PREF_CURRENT_USER_ID, ""))
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<String> listViewer = new ArrayList<>();
                        for (DocumentSnapshot document : task.getResult()) {
                            Log.d(TAG, document.getId() + " => " + document.getData());
                            listViewer.add(document.getId());
                        }
                        if (listViewer.size() == 0) {
                            String id = SharedPreferencesManager.getInstance(ViewLiveActivity.this).getString(Constants.PREF_CURRENT_USER_ID, "");
                            String featuredImage = SharedPreferencesManager.getInstance(ViewLiveActivity.this).getString(Constants.PREF_CURRENT_USER_PHOTO_URL, "");
                            String displayName = SharedPreferencesManager.getInstance(ViewLiveActivity.this).getString(Constants.PREF_CURRENT_USER_DISPLAY_NAME, "");

                            if (TextUtils.isEmpty(id) || (TextUtils.isEmpty(featuredImage) && TextUtils.isEmpty(displayName))) {
                                return;
                            }
                            Log.d(TAG, "getIdView streamerId: " + streamerId);
                            if (TextUtils.isEmpty(streamerId)) {
                                FireStoreManager.getInstance().getViewersRef().add(new Viewer(id, featuredImage, displayName, postId, false, isLike))
                                        .addOnSuccessListener(documentReference -> {
                                            Log.d(TAG, "getIdView DocumentSnapshot added with ID: " + documentReference.getId());
                                            viewerId = documentReference.getId();
                                            updateViewerSave(viewerId, isView);
                                        })
                                        .addOnFailureListener(e -> Log.w(TAG, "getIdView Error adding document", e));
                                return;
                            }
                            FireStoreManager.getInstance().getViewersRef().add(new Viewer(id, featuredImage, displayName, postId, isView, isLike))
                                    .addOnSuccessListener(documentReference -> {
                                        Log.d(TAG, "getIdView DocumentSnapshot added with ID: " + documentReference.getId());
                                        viewerId = documentReference.getId();
                                        updateViewerSave(viewerId, isView);
                                    })
                                    .addOnFailureListener(e -> Log.w(TAG, "getIdView Error adding document", e));
                        } else {
                            viewerId = listViewer.get(0);
                            setViewerView(isView);
                        }
                    } else {
                        Log.w(TAG, "getIdView Error getting documents.", task.getException());
                    }
                });
    }

    private void updateViewerSave(String viewerId, boolean isView) {
        if (isView) {
            SharedPreferencesManager.getInstance(getContext()).putString(Constants.PREF_CURRENT_VIEWER_ID, viewerId);
        } else {
            SharedPreferencesManager.getInstance(getContext()).putString(Constants.PREF_CURRENT_VIEWER_ID, "");
        }
    }

    public int getIndexByViewer(List list, Viewer view) {
        for (int i = 0; i < list.size(); i++) {
            Viewer viewer = (Viewer) list.get(i);
            if (viewer.getUserId().equals(view.getUserId())) {
                return i;
            }
        }
        return -1;
    }

    public void startListenerGift() {
        if (listenerGifts != null) {
            listenerGifts.remove();
        }
        if (gifts != null) {
            gifts.clear();
        } else {
            gifts = new ArrayList<>();
        }
        listenerGifts = FireStoreManager.getInstance().getGiftRef()
                .whereEqualTo(Constants.FIELD_POST_ID, postId)
                .addSnapshotListener(this, (queryDocumentSnapshots, err) -> {
                    if (err != null) {
                        Log.e(TAG, "listenNewMessages failed " + err.getMessage(), err);
                        return;
                    }
                    if (queryDocumentSnapshots == null) {
                        Log.e(TAG, "queryDocumentSnapshots null");
                        return;
                    }
                    List<GiftInFireStore> listGifts = new ArrayList<>();
                    for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                        DocumentSnapshot documentSnapshot = dc.getDocument();
                        GiftInFireStore giftInFireStore = null;
                        try {
                            giftInFireStore = documentSnapshot.toObject(GiftInFireStore.class);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (giftInFireStore == null) {
                            Log.e(TAG, "comment null");
                            continue;
                        }

                        String id = documentSnapshot.getId();
                        int oldIndex = dc.getOldIndex();
                        int newIndex = dc.getNewIndex();

                        switch (dc.getType()) {
                            case ADDED:
                                Log.d(TAG, "Added: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                Log.d(TAG, "ID: " + id + "; Comment: " + giftInFireStore.getSenderId());
                                listGifts.add(giftInFireStore);
                                break;
                            case MODIFIED:
                                Log.d(TAG, "Modified: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                break;
                            case REMOVED:
                                Log.d(TAG, "Removed: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                break;
                        }
                    }
                    Collections.reverse(listGifts);
                    gifts.addAll(listGifts);
                    Log.e(TAG, "ListenerGift End: " + gifts.size());
                    onActionLive.onUpdateTotalGift();
                });
    }

    private Handler bufferHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BUFFER_EVT:

                    if (!requiresBufferDialog) {
                        return;
                    }

//                    new MaterialDialog.Builder(getApplicationContext())
//                            .content(getString(R.string.buffer_evt))
//                            .negativeText(R.string.dialog_ok)
//                            .onNegative((dialog, which) -> {
//                                dialog.dismiss();
//                            })
//                            .typeface(ResourcesCompat.getFont(getApplicationContext(), R.font.quicksand_medium), ResourcesCompat.getFont(getApplicationContext(), R.font.quicksand_regular))
//                            .show();
            }
        }
    };

    @Override
    public void onPublishFlushBufferStart() {
        // show alert.
        requiresBufferDialog = true;
        Message msg = bufferHandler.obtainMessage(BUFFER_EVT);
        bufferHandler.sendMessageDelayed(msg, 500);
    }

    @Override
    public void onPublishFlushBufferComplete() {
        requiresBufferDialog = false;
    }

    @Override
    public void onEndStream() {
        onCloseStream();
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_POST_DETAIL: {
                    PostDetailResponse postDetailResponse = (PostDetailResponse) response.getData();
                    PostDetail postDetail = postDetailResponse.getPostDetail();
                    if (postDetail != null) {
                        onActionLive.onSetUpViewLive(postDetail);
                    }
                    break;
                }
                case LIKE_POST: {
                    PostDetailResponse postDetailResponse = (PostDetailResponse) response.getData();
                    PostDetail postDetail = postDetailResponse.getPostDetail();
                    if (postDetail != null) {
                        onActionLive.onSetLike(postDetail);
                    }
                    break;
                }
                case FOLLOW_USER: {
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null && userResponse.getUser() != null) {
                        onActionLive.onSetFollow(userResponse.getUser().isFollow());
                    }
                    break;
                }
                case EXPIRE_POST_PRODUCT: {
                    PostDetailResponse postResponse = (PostDetailResponse) response.getData();
                    break;
                }
                case REPORT: {
                    ReportResponse reportResponse = ((UsersResponse) response.getData()).getReportResponse();
                    DialogUtils.showMessageDialog(getContext(), getSupportFragmentManager(), getString(R.string.dialog_report_title), getString(R.string.dialog_thanks_report));
                    break;
                }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail");
        }
        DialogUtils.hideLoadingProgress();
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        Log.e(TAG, requestTarget.toString() + ": fail");

        switch (statusCode) {
            case ERR_NO_INTERNET_CONNECTION:
                DialogUtils.showMessageDialog(this, getSupportFragmentManager(), getString(R.string.error_network_title), failMessage);
                break;
            case ERR_UNAUTHORIZED:
                DialogUtils.showUnauthorizedDialog(getContext(), getSupportFragmentManager(), failMessage);
                break;
        }

        if (requestTarget == RequestTarget.REPORT) {
            DialogUtils.showMessageDialog(getContext(), getSupportFragmentManager(), getString(R.string.dialog_report_title), failMessage);
        }
    }

    public interface OnActionLive {
        void onSetUpViewLive(PostDetail postDetail);

        void onSetLike(PostDetail postDetail);

        void onSetFollow(boolean isFollow);

        void onUpdateTotalGift();
    }
}
