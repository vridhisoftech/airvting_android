package com.airvtinginc.airvtinglive.gui.enums;

import android.content.Intent;

public enum SignType {
    SIGN_IN,
    SIGN_UP;

    private static final String name = SignType.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static SignType detachFrom(Intent intent) {
        if (!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }
}
