package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.CartAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.components.SwipeHelper;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.dialog.PlaceOrderDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.models.ProductInCart;
import com.airvtinginc.airvtinglive.models.request.CartProductRequest;
import com.airvtinginc.airvtinglive.models.request.CartProductsRequest;
import com.airvtinginc.airvtinglive.models.request.CheckoutRequest;
import com.airvtinginc.airvtinglive.models.response.AirTokenExchangeResponse;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class CartFragment extends BaseFragment implements PlaceOrderDialog.OnPlaceOrderListener, SwipeHelper.UnderlayButtonClickListener {
    @BindView(R.id.rlMyCart)
    RelativeLayout mRlMyCart;
    @BindView(R.id.rvCart)
    EmptyRecyclerView mRvCart;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.tvShowEmpty)
    TextView tvShowEmpty;
    @BindView(R.id.placeOrderText)
    TextView placeOrderText;
    @BindView(R.id.mrlPlaceOrder)
    MaterialRippleLayout mMrlPlaceOrder;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private CartAdapter mCartAdapter;
    private List<Product> mProducts = new ArrayList<>();
    private int exchangeDollarToAirToken = -1;
    private List<CartProductRequest> mCartProductsRequest;
    private boolean isPlaceOrder;

    @OnClick(R.id.mrlPlaceOrder)
    void placeOrder() {
       // isPlaceOrder = true;
        preCheckoutCheck();
        checkItemInCart();
        getCartProduct();
    }

    public static CartFragment newInstance() {
        return new CartFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cart, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setUpRecyclerView();
        getCartProductIdFromRealm();
        getCartProduct();
        exchangeAirToken();
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            refresh();
        });
        checkItemInOnCart();
    }

    private void checkItemInOnCart() {
        if (mCartProductsRequest != null && !mCartProductsRequest.isEmpty()) {
            mMrlPlaceOrder.setBackground(getResources().getDrawable(R.drawable.bg_sign_up_btn));
            placeOrderText.setTextColor(getResources().getColor(R.color.white));
        } else {
            mMrlPlaceOrder.setBackground(getResources().getDrawable(R.drawable.bg_place_order_inactive));
            placeOrderText.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private void refresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        getCartProduct();
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = new Intent();
        intent.setAction(DetailActivity.BroadcastActionHideAllToolbarIcons);
        getActivity().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getString(R.string.my_cart_title_capitalize);
        getActivity().sendBroadcast(intent);

        intent.setAction(DetailActivity.BroadcastActionChangeToolbarToWhiteColor);
        getActivity().sendBroadcast(intent);
        refresh();
    }

    private void setUpRecyclerView() {

        mCartAdapter = new CartAdapter(getActivity(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                if (item.equals("Delete")) {
                    removeProductFromCart(position);
                }
                else if(item.equals("DeleteLast"))
                {
                    showConfirmRemove(position);
                }
                else{
                    addProductToCart(position);
                }
            }
        });
        emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        tvShowEmpty.setText(getString(R.string.recycle_view_empty_shopping_cart));
        mRvCart.setEmptyView(emptyView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRvCart.setLayoutManager(linearLayoutManager);
        mRvCart.setAdapter(mCartAdapter);

        new SwipeHelper(getContext(), mRvCart) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new UnderlayButton(getContext(), "", R.drawable.ic_trash, getResources().getColor(R.color.md_red_600), position -> {
                    showConfirmRemove(position);

                }));
            }
        };
    }
    private void addProductToCart(int position) {
        Product product = mCartAdapter.getProduct(position);
        Realm realm = Utils.getCartRealm(getContext());
        realm.beginTransaction();
        Product realmProduct = realm.where(Product.class).equalTo(Constants.REALM_PRODUCT_ID_PRIMARY_KEY, product.getProductId()).findFirst();
        Product cartProduct;
        if (realmProduct != null) {
            cartProduct = new Product();
            cartProduct.setQuantityToBuy(realmProduct.getQuantityToBuy() + 1);
        } else {
            cartProduct = realm.createObject(Product.class);
            cartProduct.setQuantityToBuy(1);
        }

        cartProduct.setProductId(product.getProductId());
        cartProduct.setDisplayImage(product.getFeaturedImages().get(0).getFeaturedImage());
        cartProduct.setTitle(product.getTitle());
        cartProduct.setSellerId(product.getSellerId());

        if (realmProduct != null) {
            realm.copyToRealmOrUpdate(cartProduct);
        }

        realm.commitTransaction();
        Toast.makeText(getContext(), String.format(getString(R.string.my_cart_add_product), product.getTitle()), Toast.LENGTH_SHORT).show();
    }

    private void removeProductFromCart(int position) {
        Product product = mCartAdapter.getProduct(position);
        Realm realm = Utils.getCartRealm(getContext());
        realm.beginTransaction();
        Product realmProduct = realm.where(Product.class).equalTo(Constants.REALM_PRODUCT_ID_PRIMARY_KEY, product.getProductId()).findFirst();
        Product cartProduct;
        if (realmProduct != null) {
            cartProduct = new Product();
            cartProduct.setQuantityToBuy(realmProduct.getQuantityToBuy() - 1);
        } else {
            cartProduct = realm.createObject(Product.class);
            cartProduct.setQuantityToBuy(1);
        }

        cartProduct.setProductId(product.getProductId());
        cartProduct.setDisplayImage(product.getFeaturedImages().get(0).getFeaturedImage());
        cartProduct.setTitle(product.getTitle());
        cartProduct.setSellerId(product.getSellerId());

        if (realmProduct != null) {
            realm.copyToRealmOrUpdate(cartProduct);
        }

        realm.commitTransaction();
        Toast.makeText(getContext(), String.format(getString(R.string.dialog_cart_remove_product_success), product.getTitle()), Toast.LENGTH_SHORT).show();

    }

    void showConfirmRemove(int productPosition) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.REMOVE_CART_ITEM);
        messageDialog.setDialogTitle(getString(R.string.dialog_remove_cart_item_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_cart_remove_product_confirm_msg));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_remove));
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                removeCartItem(productPosition);
                messageDialog.dismiss();
                checkItemInOnCart();
            }

            @Override
            public void onCloseActionButtonClick() {

            }
        });
        messageDialog.show(getFragmentManager(), "Show Remove Cart Item Dialog");
    }

    private void checkItemInCart() {
        if (mCartProductsRequest != null && !mCartProductsRequest.isEmpty()) {
            mMrlPlaceOrder.setBackground(getResources().getDrawable(R.drawable.bg_sign_up_btn));
            placeOrderText.setTextColor(getResources().getColor(R.color.white));
        } else {
            mMrlPlaceOrder.setBackground(getResources().getDrawable(R.drawable.bg_place_order_inactive));
            placeOrderText.setTextColor(getResources().getColor(R.color.white));
            Toast.makeText(getContext(), "Add Items To Cart", Toast.LENGTH_LONG).show();
        }

    }

    private void removeCartItem(int productPosition) {
        Product product = mCartAdapter.getProduct(productPosition);
        if (product != null) {
            Realm realm = Utils.getCartRealm(getContext());
            realm.beginTransaction();
            Product realmProduct = realm.where(Product.class).equalTo(Constants.REALM_PRODUCT_ID_PRIMARY_KEY, product.getProductId()).findFirst();
            if (realmProduct != null) {
                realmProduct.removeFromRealm();
                mCartAdapter.removeItem(productPosition);

                int cartProductIndex = getCartProductIndex(product.getProductId());

                if (cartProductIndex != -1) {
                    mCartProductsRequest.remove(cartProductIndex);
                    mProducts.remove(cartProductIndex);
                }

                Toast.makeText(getContext(), String.format(getString(R.string.dialog_cart_remove_product_success), product.getTitle()), Toast.LENGTH_SHORT).show();
            }
            realm.commitTransaction();
        }
    }

    private int getCartProductIndex(String productId) {
        for (int i = 0; i < mCartProductsRequest.size(); i++) {
            if (mCartProductsRequest.get(i).getProductId().equals(productId)) {
                return i;
            }
        }
        return -1;
    }

    private void getCartProductIdFromRealm() {
        Realm realm = Utils.getCartRealm(getContext());
        realm.beginTransaction();
        RealmResults<Product> realmResults = realm.where(Product.class).findAll();
        if (realmResults != null) {
            mCartProductsRequest = new ArrayList<>();
            for (Product realmProduct : realmResults) {
                Product product = new Product();
                product.setProductId(realmProduct.getProductId());
                product.setTitle(realmProduct.getTitle());
                product.setDisplayImage(realmProduct.getDisplayImage());
                product.setQuantityToBuy(realmProduct.getQuantityToBuy());
                product.setSellerId(realmProduct.getSellerId());
                product.setPostId(realmProduct.getPostId());
                mProducts.add(product);
            }

            Collections.reverse(mProducts); // Reverse to show
            for (Product product : mProducts) {
                mCartProductsRequest.add(new CartProductRequest(product.getProductId()));
            }
        }
        realm.commitTransaction();
    }

    private void getCartProduct() {
        mSwipeRefreshLayout.setRefreshing(false);
        if (mCartProductsRequest != null && !mCartProductsRequest.isEmpty()) {
            CartProductsRequest cartProductsRequest = new CartProductsRequest();
            cartProductsRequest.setProducts(mCartProductsRequest);

            requestApi(cartProductsRequest, true, RequestTarget.GET_CART_PRODUCTS, this);
        }
    }

    private void preCheckoutCheck() {
        List<Product> products = mCartAdapter.getListProduct();
        if (products != null && !products.isEmpty()) {
//            if (!Utils.hasEmail(getContext())) {
//                DialogUtils.showUpdateEmailDialog(getContext(), getFragmentManager());
//                Log.v("VerifedEmail","Verified");
//
//            } else if (!Utils.isActiveEmail(getContext())) {
//                Log.v("VerifyMail","Verify");
//                DialogUtils.showVerifyEmailDialog(getContext(), getFragmentManager());
//            } else if (!Utils.hasPaymentMethod(getContext())) {
//                Log.v("PaymentDialog","Payment"+  Utils.hasPaymentMethod(getContext()));
//
//                DialogUtils.showAddPaymentMethodDialog(getContext(), getFragmentManager());
//            } else {
//                Log.v("preCheckout","preCheckout");

                preCheckout();
//            }
        }
    }

    private void showPlaceOrderDialog() {
        PlaceOrderDialog placeOrderDialog = new PlaceOrderDialog();
        placeOrderDialog.setProducts(mCartAdapter.getListProduct());
        if (exchangeDollarToAirToken != -1) {
            placeOrderDialog.setExchange(exchangeDollarToAirToken);
        }
        placeOrderDialog.setOnPlaceOrderListener(this);
        placeOrderDialog.show(getFragmentManager(), "Show Place Order Dialog");
    }

    private void exchangeAirToken() {
        requestApi(null, false, RequestTarget.EXCHANGE_AIR_TOKEN, this);
    }

    @Override
    public void onPlaceOrderSuccessful() {
        clearCart();
        mCartAdapter.clearAllItems();
    }

    private void clearCart() {
        Realm realm = Utils.getCartRealm(getContext());
        realm.close();
        Realm.deleteRealm(Utils.getCartRealmConfiguration(getContext()));
    }

    private void preCheckout() {
        List<ProductInCart> productInCartList = new ArrayList<>();
        for (Product product : mProducts) {
            ProductInCart productInCart = new ProductInCart();
            productInCart.setCurrency(ServiceConstants.CURRENCY);
            productInCart.setPrice(product.getPaymentPrice());
            productInCart.setQuantity(product.getQuantityToBuy());
            productInCart.setSellerId(product.getSellerId());
            productInCart.setPostId(product.getPostId());
            productInCart.setProductId(product.getProductId());
            productInCart.setTitle(product.getTitle());
            productInCart.setFeaturedImage(product.getDisplayImage());
            productInCart.setDescription(product.getDescription());
            productInCartList.add(productInCart);
        }

        String currentUserId = Utils.getCurrentUserId(getContext());
        CheckoutRequest request = new CheckoutRequest();
        request.setUserId(currentUserId);
        request.setProducts(productInCartList);
        request.setTotalPrice(0);
        request.setUseAirToken(0);
        request.setTotalQuantity(mProducts.size());
        request.setPaymentMethodId(Utils.getPaymentMethodId(getContext()));
        requestApi(request, true, RequestTarget.PRE_CHECKOUT, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_CART_PRODUCTS:
                    ProductsResponse productsResponse = (ProductsResponse) response.getData();
                    if (productsResponse != null) {
                        List<Product> products = productsResponse.getProducts();
                        if (products != null && !products.isEmpty()) {
                            for (Product product : products) {
                                for (Product cartProduct : mProducts) {
                                    if (product.getProductId().equals(cartProduct.getProductId())) {
                                        cartProduct.setPrice(product.getPrice());
                                        cartProduct.setPriceSale(product.getPriceSale());
                                        cartProduct.setDiscount(product.getDiscount());
                                        cartProduct.setPriceWhenStream(product.getPriceWhenStream());
                                        cartProduct.setStartedAt(product.getStartedAt());
                                        cartProduct.setExpiredAt(product.getExpiredAt());
                                        cartProduct.setFeaturedImages(product.getFeaturedImages());
                                        cartProduct.setDescription(product.getDescription());
                                    }
                                }
                            }
                            List<Product> adapterList = new ArrayList<>(mProducts);
                            mCartAdapter.setList(adapterList);
                            if (isPlaceOrder) {
                                requestApi(Utils.getCurrentUserId(getContext()), false, RequestTarget.GET_USER_PROFILE, this);
                            }
                        }
                    }
                    break;
                case PRE_CHECKOUT:
                    showPlaceOrderDialog();
                    break;
                case GET_USER_PROFILE: {
                    UserResponse userResponse = (UserResponse) response.getData();
                    if (userResponse != null) {
                        Utils.saveEmailAndPaymentInfo(getContext(), userResponse.getUser());
//                        if (isPlaceOrder) {
//                            preCheckoutCheck();
//                        }
                    }
                    break;
                }
                case EXCHANGE_AIR_TOKEN:
                    AirTokenExchangeResponse exchangeResponse = (AirTokenExchangeResponse) response.getData();
                    exchangeDollarToAirToken = exchangeResponse.getExchangeDollaToAirToken();
                    break;
            }
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        switch (requestTarget) {
            case PRE_CHECKOUT:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                        getString(R.string.dialog_my_cart_place_order), failMessage);
                break;
//            default:
//                DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
//                        getString(R.string.my_cart_title_capitalize), failMessage);
//                break;
        }
    }

    @Override
    public void onClick(int pos) {

    }
}
