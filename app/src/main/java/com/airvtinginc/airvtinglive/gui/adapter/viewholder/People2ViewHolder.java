package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class People2ViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;

    public People2ViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}