package com.airvtinginc.airvtinglive.gui.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.NonSwipeableViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExploreFragment extends BaseFragment {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.mViewPager)
    NonSwipeableViewPager mViewPager;

    public ExploreFragment() {
        // Required empty public constructor
    }

    public static ExploreFragment newInstance() {
        ExploreFragment fragment = new ExploreFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_explore, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        prepareViewPagerAndTabLayout();

        mViewPager.setCurrentItem(1);
    }

    private void prepareViewPagerAndTabLayout() {
        PageFragmentAdapter mAdapter = new PageFragmentAdapter(getChildFragmentManager());
//        mAdapter.addFragment(ShowsFragment.newInstance(), getString(R.string.explore_shows));
        mAdapter.addFragment(ExplorePostsFragment.newInstance(), getString(R.string.explore_posts));
        mAdapter.addFragment(ExploreProductsFragment.newInstance(), getString(R.string.explore_products));
        mAdapter.addFragment(ExplorePeopleFragment.newInstance(), getString(R.string.explore_people));

        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextView tabText = (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0))
                    .getChildAt(i))
                    .getChildAt(1));
            if (tabText != null) {
                tabText.setTypeface(tabText.getTypeface(), Typeface.BOLD);
            }
        }

        mViewPager.setOffscreenPageLimit(1);
    }
}
