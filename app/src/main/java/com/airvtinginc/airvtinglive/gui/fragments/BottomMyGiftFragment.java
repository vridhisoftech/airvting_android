package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.GiftAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.dialog.SendGiftDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.GiftDetail;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.request.SendGiftRequest;
import com.airvtinginc.airvtinglive.models.response.GiftDetailListResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomMyGiftFragment extends BaseFragment implements SendGiftDialog.OnSendGiftListener {

    private static final String TAG = BottomMyGiftFragment.class.getSimpleName();

    @BindView(R.id.rv_bottom_my_gift)
    EmptyRecyclerView mRvBottomMyGift;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;

    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private int mCurrentPage = 1;
    private GridLayoutManager mLayoutManager;
    private GiftAdapter mGiftAdapter;
    private String mStreamerId;
    private String mPostId;
    private SendGiftListener mSendGiftListener;
    private SendGiftRequest sendGiftRequest;

    public static BottomMyGiftFragment newInstance(String streamerId, String postId) {
        BottomMyGiftFragment fragment = new BottomMyGiftFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, streamerId);
        args.putString(Constants.EXTRA_POST_ID, postId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStreamerId = getArguments().getString(Constants.EXTRA_USER_ID);
            mPostId = getArguments().getString(Constants.EXTRA_POST_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_my_gift, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setUpRecyclerView();
        getMyGifts(mCurrentPage);
    }

    private void setUpRecyclerView() {
        mRvBottomMyGift.setEmptyView(emptyView);
        mLayoutManager = new GridLayoutManager(getContext(), 4);
        mRvBottomMyGift.setLayoutManager(mLayoutManager);
        mRvBottomMyGift.setHasFixedSize(true);

        mGiftAdapter = new GiftAdapter(getContext(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                showSendGiftDialog((GiftDetail) item);
            }
        });
        mGiftAdapter.setTextColor(getResources().getColor(R.color.white));
        mGiftAdapter.setIsShowInMyGift(true);
        mRvBottomMyGift.setAdapter(mGiftAdapter);

        mRvBottomMyGift.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isMoreLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            mGiftAdapter.addItem(null);
                            isMoreLoading = true;
                            getMyGifts(++mCurrentPage);
                        }
                    }
                }
            }
        });

    }

    private void getMyGifts(int page) {
        String currentUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        GetListRequest getListRequest = new GetListRequest();
        getListRequest.setUserId(currentUserId);
        getListRequest.setPaginate(page);
        getListRequest.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestApi(getListRequest, false, RequestTarget.GET_USER_GIFTS, this);
    }

    private void hideLoadMore() {
        if (isMoreLoading && mGiftAdapter.getItemCount() > 0) {
            mGiftAdapter.removeItem(mGiftAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    private void showSendGiftDialog(GiftDetail giftDetail) {
        if (giftDetail == null) {
            return;
        }

        SendGiftDialog sendGiftDialog = SendGiftDialog.newInstance(mStreamerId, mPostId);
        sendGiftDialog.setGiftDetail(giftDetail);
        sendGiftDialog.setOnSendGiftListener(this);
        sendGiftDialog.show(getFragmentManager(), "Show Send Gift Dialog");
    }

    @Override
    public void refreshMyGifts() {
        mCurrentPage = 1;
        mGiftAdapter.clearAllItems();
        getMyGifts(mCurrentPage);

        if (mSendGiftListener != null) {
            mSendGiftListener.onSendGift();
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_USER_GIFTS:
                    hideLoadMore();
                    GiftDetailListResponse giftDetailListResponse = (GiftDetailListResponse) response.getData();
                    if (giftDetailListResponse != null && giftDetailListResponse.getGiftDetailList().size() > 0) {
                        for (GiftDetail giftDetail : giftDetailListResponse.getGiftDetailList()) {
                            if (giftDetail != null && giftDetail.getQuantity() != 0) {
                                mGiftAdapter.addItem(giftDetail);
                            }
                        }

                        if (mGiftAdapter != null && mGiftAdapter.getItemCount() < ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING) {
                            getMyGifts(++mCurrentPage);
                        }
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        switch (requestTarget) {
            case GET_USER_GIFTS:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_gifts_title), failMessage);
                break;
        }
    }

    public interface SendGiftListener {
        void onSendGift();
    }

    public void setSendGiftListener(SendGiftListener sendGiftListener) {
        mSendGiftListener = sendGiftListener;
    }
}
