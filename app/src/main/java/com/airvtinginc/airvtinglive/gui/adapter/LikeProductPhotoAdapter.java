package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.LikeProductPhotoViewHolder;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class LikeProductPhotoAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    public LikeProductPhotoAdapter(Context context, BaseAdapter.OnItemClickListener onItemClickListener) {
        super(context, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE:
                View loadMoreView = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_horizontal_layout, parent, false);
                return new EmptyViewHolder(loadMoreView);
            default:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_product_photo, parent, false);
                view.getLayoutParams().width = (Utils.getScreenWidth(parent.getContext())) / 4;
                return new LikeProductPhotoViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder || list.get(position) == null) {
            return;
        }

        LikeProductPhotoViewHolder holder = (LikeProductPhotoViewHolder) mHolder;
        FeaturedImage featureImage = (FeaturedImage) list.get(position);
        if (!TextUtils.isEmpty(featureImage.getFeaturedImage())) {
            holder.pbPhotoLoading.setVisibility(View.VISIBLE);
            Glide.with(context).load(featureImage.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbPhotoLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbPhotoLoading.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivProductPhoto);
        }
        holder.mView.setOnClickListener(view -> onItemClickListener.onItemClicked(featureImage, position));
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }
}
