package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionItemHistoryHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivProduct)
    ImageView ivProduct;
    public @BindView(R.id.tvPrice)
    TextView tvPrice;
    public @BindView(R.id.tvQuantity)
    TextView tvQuantity;
    public @BindView(R.id.tvUserName)
    TextView tvUserName;
    public @BindView(R.id.tvTitle)
    TextView tvTitle;
    public @BindView(R.id.pbProduct)
    ProgressBar pbProduct;

    public TransactionItemHistoryHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}