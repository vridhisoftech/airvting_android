package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveCommentsViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.tvContent)
    TextView tvContent;
    public @BindView(R.id.ivContent)
    ImageView ivContent;

    public LiveCommentsViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}