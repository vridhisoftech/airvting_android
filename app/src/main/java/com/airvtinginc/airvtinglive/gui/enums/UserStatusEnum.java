package com.airvtinginc.airvtinglive.gui.enums;

public enum UserStatusEnum {
    ACTIVE(1),
    NOT_ACTIVE(2);

    private int status;

    UserStatusEnum(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
