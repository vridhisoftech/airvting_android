package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.CategoriesViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.CategoryProductRequest;
import com.airvtinginc.airvtinglive.models.response.ExploreProducts;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;

import java.util.ArrayList;
import java.util.List;

public class ExploreProductsAdapter extends BaseAdapter {

    private static final String TAG = ExploreProductsAdapter.class.getSimpleName();

    public ExploreProductsAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_explore_category, parent, false);
        CategoriesViewHolder vh = new CategoriesViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        final CategoriesViewHolder holder = (CategoriesViewHolder) mHolder;
        ExploreProducts exploreProducts = (ExploreProducts) list.get(position);
        holder.tvCategoryName.setText(exploreProducts.getCategoryName());

        // use a linear layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.mRecyclerView.setLayoutManager(linearLayoutManager);
        holder.mRecyclerView.setAdapter(exploreProducts.getHorizontalProductsAdapter());
        holder.mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount, pastVisibleItems, totalItemCount;
                super.onScrolled(recyclerView, dx, dy);
                if (dx > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!exploreProducts.isMoreLoading()) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            exploreProducts.getHorizontalProductsAdapter().addItem(null);
                            exploreProducts.setMoreLoading(true);
                            getCategoryProducts(exploreProducts);
                        }
                    }
                }
            }
        });

        holder.emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        holder.tvShowEmpty.setText(context.getString(R.string.recycle_view_empty));

        if (exploreProducts.getHorizontalProductsAdapter().getItemCount() > 0) {
            holder.mRecyclerView.setVisibility(View.VISIBLE);
            holder.emptyView.setVisibility(View.GONE);
        } else {
            holder.mRecyclerView.setVisibility(View.GONE);
            holder.emptyView.setVisibility(View.VISIBLE);
        }
    }

    public <T> void addItem(T item, int position, boolean isReplace) {
        ExploreProducts exploreProducts = (ExploreProducts) item;
        exploreProducts.setHorizontalProductsAdapter(new HorizontalProductsAdapter(context, exploreProducts.getProductList(), new OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        }));

        if (isReplace && list.get(position) != null) {
            list.set(position, exploreProducts);
            notifyItemChanged(position);
        } else {
            list.add(exploreProducts);
            notifyItemInserted(list.size() - 1);
            notifyItemChanged(list.size() - 1);
        }
    }

    private void getCategoryProducts(ExploreProducts exploreProducts) {
        if (exploreProducts.getCurrentPage() > 1 && TextUtils.isEmpty(exploreProducts.getMaxId())) {
            hideLoadMore(exploreProducts);
            return;
        }
        CategoryProductRequest requestModel = new CategoryProductRequest();

        requestModel.setCategoryId(exploreProducts.getCategoryId());
        requestModel.setPaginate(exploreProducts.getCurrentPage() + 1);
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestModel.setMaxId(exploreProducts.getMaxId());

        requestApi(requestModel, false, RequestTarget.GET_CATEGORY_PRODUCT, this);
    }

    private void hideLoadMore(ExploreProducts exploreProducts) {
        if (exploreProducts.isMoreLoading() && exploreProducts.getHorizontalProductsAdapter().getItemCount() > 0) {
            exploreProducts.getHorizontalProductsAdapter().removeItem(exploreProducts.getHorizontalProductsAdapter().getItemCount() - 1);
            exploreProducts.setMoreLoading(false);
        }
    }

    public void updateProduct() {
        for (int index = 0; index < list.size(); index++) {
            ExploreProducts exploreProducts = (ExploreProducts) list.get(index);
            if (exploreProducts != null && exploreProducts.getHorizontalProductsAdapter() != null) {
                exploreProducts.getHorizontalProductsAdapter().updateProduct();
            }
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_CATEGORY_PRODUCT:
                    List<Product> products = ((ProductsResponse) response.getData()).getProducts();
                    String categoryId = ((ProductsResponse) response.getData()).getCategoryId();
                    String maxId = ((ProductsResponse) response.getData()).getMaxId();

                    for (int i = 0; i < list.size(); i++) {
                        ExploreProducts exploreProducts = (ExploreProducts) list.get(i);
                        if (categoryId.equals(exploreProducts.getCategoryId())) {
                            hideLoadMore(exploreProducts);
                            if (products != null && !products.isEmpty()) {
                                exploreProducts.getHorizontalProductsAdapter().addAllItems(products);
                                exploreProducts.setCurrentPage(exploreProducts.getCurrentPage() + 1);
                                exploreProducts.setMaxId(maxId);
                            }
                        }
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
