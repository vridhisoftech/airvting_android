package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.BuildConfig;
import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.WelcomeActivity;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.models.request.ConfigRequest;
import com.airvtinginc.airvtinglive.models.response.ConfigsResponse;
import com.airvtinginc.airvtinglive.models.response.ConfigSettings;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.facebook.login.LoginManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import io.realm.Realm;


public class DetailSettingFragment extends BaseFragment {
    private static final String TAG = DetailSettingFragment.class.getSimpleName();

    @BindView(R.id.swLiveStream)
    Switch mSwLiveStream;
    @BindView(R.id.swMessage)
    Switch mSwMessage;
    @BindView(R.id.swSystem)
    Switch mSwSystem;
    @BindView(R.id.tvVersion)
    TextView tvVersion;

    private boolean mIsCheckChangeByUser;

    @OnClick({R.id.rlTermsAndCondition, R.id.icTermsAndConditions, R.id.tvTermsAndConditions})
    void clickTermsAndConditions() {
//        WebViewDialog dialog = WebViewDialog.newInstance(ServiceConstants.URL_TERMS_AND_CONDITIONS,
//                getString(R.string.navigation_drawer_menu_terms_condition));
//        dialog.show(getFragmentManager(), getString(R.string.setting_term_and_conditions));

        Intent intent = new Intent(getActivity(), DetailActivity.class);
        DetailType.TERMS_CONDITIONS.attachTo(intent);
        startActivity(intent);
    }

    @OnClick({R.id.rlContactUs, R.id.icContactUs, R.id.tvContactUs})
    void clickContactUs() {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        DetailType.CONTACT_US.attachTo(intent);
        startActivity(intent);
    }

    @OnClick({R.id.rlAccount, R.id.tvAccountDeactivate, R.id.tvAccount})
    void clickAccount() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.CONFIRM_DEACTIVATE);
        messageDialog.setDialogTitle(getString(R.string.dialog_confirm_deactivate_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_confirm_deactivate_msg));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_deactivate));
        messageDialog.show(getFragmentManager(), "Show Confirm Deactivate Dialog");
    }

    @OnClick({R.id.rlLogOut, R.id.tvLogOut})
    void clickLogOut() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.LOG_OUT);
        messageDialog.setDialogTitle(getString(R.string.dialog_logout));
        messageDialog.setDialogMessage(getString(R.string.logout_confirm_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_logout));
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                logout();
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {

            }
        });
        messageDialog.show(getFragmentManager(), "Show Confirm Logout Dialog");
    }

    @OnCheckedChanged(R.id.swLiveStream)
    void swLiveStreamCheckedChanged(boolean isChecked) {
        Log.d(TAG, "swLiveStream: " + isChecked);
        if (mIsCheckChangeByUser) {
            ConfigRequest configRequest = new ConfigRequest(ServiceConstants.NOTIFICATION_LIVE_STREAM);
            requestApi(configRequest, true, RequestTarget.CONFIG_SETTINGS, this);
        }
    }

    @OnCheckedChanged(R.id.swMessage)
    void swMessageCheckedChanged(boolean isChecked) {
        Log.d(TAG, "swMessage: " + isChecked);

        if (mIsCheckChangeByUser) {
            ConfigRequest configRequest = new ConfigRequest(ServiceConstants.NOTIFICATION_MESSAGE);
            requestApi(configRequest, true, RequestTarget.CONFIG_SETTINGS, this);
        }
    }

    @OnCheckedChanged(R.id.swSystem)
    void swSystemCheckedChanged(boolean isChecked) {
        Log.d(TAG, "swSystem: " + isChecked);
        if (mIsCheckChangeByUser) {
            ConfigRequest configRequest = new ConfigRequest(ServiceConstants.NOTIFICATION_SYSTEM);
            requestApi(configRequest, true, RequestTarget.CONFIG_SETTINGS, this);
        }
    }

    private void logout() {
        requestApi(null, true, RequestTarget.LOG_OUT, this);
    }

    private void handleLogout() {
        Utils.removeCurrentUserInfoFromPreferences(getContext());
        Utils.removeEmailAndPaymentInfo(getContext());
        Utils.resetNotificationSetting(getContext());
        clearCart();

        Intent intent = new Intent(getActivity(), WelcomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    private void clearCart() {
        Realm realm = Utils.getCartRealm(getActivity());
        realm.close();
        Realm.deleteRealm(Utils.getCartRealmConfiguration(getActivity()));
    }

    public DetailSettingFragment() {
        // Required empty public constructor
    }

    public static DetailSettingFragment newInstance() {
        DetailSettingFragment fragment = new DetailSettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_setting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        configToolbar();
        configNotifications();
        configVersion();
    }

    private void configToolbar() {
        //Update header title
        DetailActivity.headerTitle = getString(R.string.setting_title);
        Intent intentUpdateHeaderTitle = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        getActivity().sendBroadcast(intentUpdateHeaderTitle);
    }

    private void configNotifications() {
        boolean isNotifyLiveStream = SharedPreferencesManager.getInstance(getContext()).getBoolean(Constants.PREF_NOTIFICATION_LIVE_STREAM, false);
        boolean isNotifyMessage = SharedPreferencesManager.getInstance(getContext()).getBoolean(Constants.PREF_NOTIFICATION_MESSAGE, false);
        boolean isNotifySystem = SharedPreferencesManager.getInstance(getContext()).getBoolean(Constants.PREF_NOTIFICATION_SYSTEM, false);

        mSwLiveStream.setChecked(isNotifyLiveStream);
        mSwMessage.setChecked(isNotifyMessage);
        mSwSystem.setChecked(isNotifySystem);
        mIsCheckChangeByUser = true;
    }

    private void configVersion() {
        tvVersion.setText(BuildConfig.VERSION_NAME_DISPLAY);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case CONFIG_SETTINGS:
                    ConfigsResponse configsResponse = (ConfigsResponse) response.getData();
                    if (configsResponse != null && configsResponse.getConfigSettings() != null) {
                        ConfigSettings configSettings = configsResponse.getConfigSettings();
                        if (configSettings.getNotificationsConfig() != null) {
                            Utils.saveNotificationSetting(getContext(), configSettings.getNotificationsConfig());
                            mIsCheckChangeByUser = false;
                            configNotifications();
                        }
                    }
                    break;
                case LOG_OUT:
                    if (response.isSuccess()) {
                        handleLogout();
                        LoginManager.getInstance().logOut();
                    } else {
                        Log.e(TAG, "LOG_OUT: fail");
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        String failDialogTitle = "";
        switch (requestTarget) {
            case LOG_OUT:
                failDialogTitle = getString(R.string.dialog_logout);
                break;
        }
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), failDialogTitle, failMessage);
    }
}
