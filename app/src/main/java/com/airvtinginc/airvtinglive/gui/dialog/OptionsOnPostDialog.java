package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.tools.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OptionsOnPostDialog extends DialogFragment {

    @BindView(R.id.rlReport)
    RelativeLayout rlReport;
    @BindView(R.id.rlDelete)
    RelativeLayout rlDelete;

    private OnActionListener onActionListener;
    private boolean isCurrentUser = false;

    @OnClick({R.id.ll_options_background, R.id.btn_options_close})
    void close() {
        dismiss();
    }

    @OnClick(R.id.rlReport)
    void report() {
        close();
        onActionListener.onReportClick();
    }

    @OnClick(R.id.rlDelete)
    void delete() {
        close();
        onActionListener.onDeleteClick();
    }

    public static OptionsOnPostDialog newInstance() {
        return new OptionsOnPostDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_options_on_post, container, false);
        ButterKnife.bind(this, view);
        Utils.systemKeyboard(getDialog());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        String user = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");

        Bundle mArgs = getArguments();
        String userId = mArgs.getString("userId");
        if (user.equals(userId)) {
            rlReport.setVisibility(View.GONE);
        } else {
            rlReport.setVisibility(View.VISIBLE);

        }
        updateUICurrentUser();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    public void setCurrentUser(boolean currentUser) {
        isCurrentUser = currentUser;
    }

    private void updateUICurrentUser() {
        if (isCurrentUser) {
            rlDelete.setVisibility(View.VISIBLE);
        } else {
            rlDelete.setVisibility(View.GONE);
        }
    }

    public interface OnActionListener {
        void onReportClick();

        void onDeleteClick();
    }

    public void setOnActionListener(OnActionListener onActionListener) {
        this.onActionListener = onActionListener;
    }
}
