package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddStockAvailabilityDialog extends DialogFragment {
    @BindView(R.id.edt_add_stock_availability_quantity)
    EditText mEdtStockQuantity;

    private OnOkListener mOnOkListener;

    @OnClick({R.id.ll_add_stock_availability_background, R.id.btn_add_stock_availability_close})
    void close() {
        dismiss();
    }

    @OnClick(R.id.btn_add_stock_availability_ok)
    void okClick() {
        String quantity = mEdtStockQuantity.getText().toString();
        if (TextUtils.isEmpty(quantity)) {

            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_discount_stock_availability_title),
                    getString(R.string.dialog_add_stock_availability_empty_quantity));
        } else if (mOnOkListener != null) {
            mOnOkListener.onOkClick(quantity);
        }
    }


    public static AddStockAvailabilityDialog newInstance() {
        return new AddStockAvailabilityDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_stock_availability, container, false);
        ButterKnife.bind(this, view);
        Utils.systemKeyboard(getDialog());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        setUpEditTextInput();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void setUpEditTextInput() {

    }

    public interface OnOkListener {
        void onOkClick(String quantity);
    }

    public void setOnOkListener(OnOkListener onOkListener) {
        mOnOkListener = onOkListener;
    }
}
