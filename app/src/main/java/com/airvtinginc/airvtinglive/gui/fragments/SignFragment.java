package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.activities.SignActivity;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.models.response.User;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SignFragment extends BaseFragment implements SignUpFragment.OnSignUpClickListener {

    @BindView(R.id.mTabLayout)
    TabLayout mTabLayout;
    @BindView(R.id.mViewPager)
    ViewPager mViewPager;

    private OnSignUpClickListener mOnSignUpClickListener;

    public SignFragment() {
        // Required empty public constructor
    }

    public static SignFragment newInstance() {
        return new SignFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupTabs();
    }

    private void setupTabs() {
        int signTabPosition = 0;
        if (getArguments() != null) {
            signTabPosition = getArguments().getInt(SignActivity.TAB_POSITION, 0);
        }

        PageFragmentAdapter adapter = new PageFragmentAdapter(getChildFragmentManager());
        SignUpFragment signUpFragment = SignUpFragment.newInstance();
        signUpFragment.setOnItemClickListener(this);

        adapter.addFragment(signUpFragment, getString(R.string.sign_up_sign_up));
        adapter.addFragment(SignInFragment.newInstance(), getString(R.string.sign_in_sign_in));
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(signTabPosition);
    }

    @Override
    public void onSignUpClicked(User user) {
        if (mOnSignUpClickListener != null) {
            mOnSignUpClickListener.onSignUpClick(user);
        }
    }

    public interface OnSignUpClickListener {
        void onSignUpClick(User user);
    }

    public void setOnSignUpClick(OnSignUpClickListener onSignUpClickListener) {
        mOnSignUpClickListener = onSignUpClickListener;
    }
}
