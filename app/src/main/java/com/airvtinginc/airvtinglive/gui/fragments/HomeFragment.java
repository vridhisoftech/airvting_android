package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BannerPagerAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.AutoScrollViewPager;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.HomeFeedType;
import com.airvtinginc.airvtinglive.gui.enums.HomeFilterType;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.models.request.GetPostsFilterRequest;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.LogUtils;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements BaseAdapter.OnItemClickListener {

    private static final String TAG = HomeFragment.class.getName();

    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.mViewPager)
    AutoScrollViewPager mViewPager;
    @BindView(R.id.mHomeViewPager)
    ViewPager mHomeViewPager;
    @BindView(R.id.mIndicator)
    CirclePageIndicator mIndicator;
    @BindView(R.id.refreshButton)
    TextView refreshButton;

    private BannerPagerAdapter mAdapter;
    private String mMaxId;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupBanner();
        setupTabs();

        if (SharedPreferencesManager.getInstance(getContext()).getInt(Constants.PREF_CURRENT_USER_NUMBER_FOLLOW_POST, 0) < 10) {
            mHomeViewPager.setCurrentItem(1, true);
        } else {
            mHomeViewPager.setCurrentItem(0, true);
        }

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
            }
        });
    }

    private void refresh() {
        LogUtils.debug("HomeFragment", "Refresh");
        refreshButton.setVisibility(View.GONE);
        getBanners();
        setupBanner();
    }

    @Override
    public void onResume() {
        super.onResume();
        getBanners();
    }

    private void setupBanner() {
        mAdapter = new BannerPagerAdapter(getContext(), this);
        mViewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mViewPager);
        mViewPager.setInterval(3000);
        mViewPager.startAutoScroll();
    }

    private void setupTabs() {
        PageFragmentAdapter adapter = new PageFragmentAdapter(getChildFragmentManager());
        adapter.addFragment(HomeFollowFragment.newInstance(), getString(R.string.home_follow));
        adapter.addFragment(HomeNewFragment.newInstance(), getString(R.string.home_new));
        adapter.addFragment(HomeHotFragment.newInstance(), getString(R.string.home_hot));
        adapter.addFragment(HomeFeaturedFragment.newInstance(), getString(R.string.home_featured));
        mHomeViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mHomeViewPager);
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TextView tabText = (TextView) (((LinearLayout) ((LinearLayout) mTabLayout.getChildAt(0))
                    .getChildAt(i))
                    .getChildAt(1));
            if (tabText != null) {
                tabText.setTypeface(tabText.getTypeface(), Typeface.BOLD);
            }
        }

        mHomeViewPager.setOffscreenPageLimit(3);
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        PostDetail postDetail = (PostDetail) item;
        if (postDetail.getType().equals(HomeFeedType.LIVE.getType())) {
            openStream(postDetail);
        } else {
            openMedia(postDetail.getId());
        }
    }

    private void getBanners() {
        String filter = HomeFilterType.FEATURED.getType();
        String[] typePost = {MediaType.STREAM.getType(), MediaType.VIDEO.getType()};

        GetPostsFilterRequest modelRequest = new GetPostsFilterRequest(filter, typePost, 1, ServiceConstants.NUMBER_OF_BANNERS_PER_LOADING, mMaxId, true);
        requestApi(modelRequest, false, RequestTarget.GET_POSTS_BY_FILTER, this);
    }

    private void openStream(PostDetail postDetail) {
        Intent intent = new Intent(getActivity(), LiveStreamActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, postDetail.getUser().getUserId());
        args.putString(Constants.EXTRA_POST_ID, postDetail.getId());
        args.putString(Constants.EXTRA_MEDIA_URL, postDetail.getMediaUrl());
        intent.putExtras(args);
        startActivity(intent);
    }

    private void openMedia(String mediaId) {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_GALLERY_ID, mediaId);
        intent.putExtras(args);
        DetailType.GALLERY.attachTo(intent);
        startActivity(intent);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_POSTS_BY_FILTER:
                    PostResponse result = ((PostResponse) response.getData());
                    if (result != null && mAdapter != null) {
                        refreshButton.setVisibility(View.GONE);
                        mAdapter.setBanners(result.getListPostDetail());
                        mMaxId = result.getMaxId();
                    }

                    break;
            }
        } else {
            refreshButton.setVisibility(View.VISIBLE);

            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        refreshButton.setVisibility(View.VISIBLE);
    }
}
