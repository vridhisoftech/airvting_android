package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.TextViewHolder;
import com.airvtinginc.airvtinglive.models.response.CategoryItem;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.List;

/**
 * Simple adapter example for custom items in the dialog
 */
public class TextRecycleViewAdapter extends BaseAdapter {

    private int textColor;
    private int backgroundColor;

    public TextRecycleViewAdapter(Context context, List<CategoryItem> categoryList, OnItemClickListener onItemClickListener) {
        super(context, categoryList, onItemClickListener);
    }

    public void setTextColor(@ColorInt int color) {
        this.textColor = color;
    }

    public void setTextColorRes(Context context, @ColorRes int colorRes) {
        setTextColor(Utils.getColor(context, colorRes));
    }

    public void setBackgroundColor(@ColorInt int color) {
        this.backgroundColor = color;
    }

    public void setBackgroundColorRes(Context context, @ColorRes int colorRes) {
        setBackgroundColor(Utils.getColor(context, colorRes));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_text, parent, false);
        return new TextViewHolder(view);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        TextViewHolder holder = (TextViewHolder) mHolder;
        CategoryItem category = (CategoryItem) list.get(position);

        holder.tvText.setText(category.getTitle());
        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(category, position);
        });

        if (textColor != 0) {
            holder.tvText.setTextColor(textColor);
        }

        if (backgroundColor != 0) {
            holder.llBackground.setBackgroundColor(backgroundColor);
        }
    }
}
