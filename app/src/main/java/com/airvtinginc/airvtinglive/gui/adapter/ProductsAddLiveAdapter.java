package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ProductAddLiveHolder;
import com.airvtinginc.airvtinglive.models.FeaturedImage;
import com.airvtinginc.airvtinglive.models.PriceWhenStream;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.NumberUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class ProductsAddLiveAdapter extends BaseAdapter {
    private static final int VIEW_TYPE_ITEM_STORE = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;

    private boolean isShowSalePrice;
    private String mUserId;
    private String mPostId;
    private OnUpdateCartItemsListener mOnUpdateCartItemsListener;
    private boolean isShowBottomStore;

    public ProductsAddLiveAdapter(Context context, List<Product> products, OnItemClickListener onItemClickListener,
                                  boolean isShowBottomStore, boolean isShowSalePrice) {
        super(context, products, onItemClickListener);
        this.isShowBottomStore = isShowBottomStore;
        this.isShowSalePrice = isShowSalePrice;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM_STORE: {
                View view;
                if (isShowBottomStore) {
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_bottom_store, parent, false);
                } else {
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_add_live, parent, false);
                }
                return new ProductAddLiveHolder(view);
            }
            case VIEW_TYPE_LOAD_MORE: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(view);
            }
            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(view);
            }
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null || mHolder instanceof EmptyViewHolder) {
            return;
        }

        if (mHolder instanceof ProductAddLiveHolder) {
            ProductAddLiveHolder holder = (ProductAddLiveHolder) mHolder;
            Product product = (Product) list.get(position);
            holder.tvNameProduct.setText(product.getTitle());

            checkAndShowPrice(product, holder);

            List<FeaturedImage> featuredImages = product.getFeaturedImages();

            if (featuredImages != null && !featuredImages.isEmpty()) {
                holder.pbProductLoading.setVisibility(View.VISIBLE);
                Glide.with(context).load(featuredImages.get(0).getFeaturedImage())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbProductLoading.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbProductLoading.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.ivProduct);
            }
            if (isShowBottomStore) {
                holder.ivAddToCart.setVisibility(View.VISIBLE);
                holder.llAddToCard.setOnClickListener(view -> addToCart(product, featuredImages));
            } else {
                holder.ivAddToCart.setVisibility(View.GONE);
                holder.llAddToCard.setOnClickListener(null);
            }

            holder.mView.setOnClickListener(view -> {
                onItemClickListener.onItemClicked(product, position);
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM_STORE : VIEW_TYPE_LOAD_MORE;
    }

    private void addToCart(Product product, List<FeaturedImage> featuredImages) {
        Realm realm = Utils.getCartRealm(context);
        realm.beginTransaction();
        Product realmProduct = realm.where(Product.class).equalTo(Constants.REALM_PRODUCT_ID_PRIMARY_KEY, product.getProductId()).findFirst();
        Product cartProduct;
        if (realmProduct != null) {
            cartProduct = new Product();
            cartProduct.setQuantityToBuy(realmProduct.getQuantityToBuy() + 1);
        } else {
            cartProduct = realm.createObject(Product.class);
            cartProduct.setQuantityToBuy(1);
        }

        cartProduct.setProductId(product.getProductId());
        cartProduct.setDisplayImage(featuredImages.get(0).getFeaturedImage());
        cartProduct.setTitle(product.getTitle());
        cartProduct.setSellerId(mUserId);
        cartProduct.setPostId(mPostId);

        if (realmProduct != null) {
            realm.copyToRealmOrUpdate(cartProduct);
        }

        if (mOnUpdateCartItemsListener != null) {
            RealmResults<Product> realmProducts = realm.where(Product.class).findAll();
            int quantity = 0;
            for (Product countProduct : realmProducts) {
                quantity += countProduct.getQuantityToBuy();
            }
            mOnUpdateCartItemsListener.onUpdateCartItemsQuantity(quantity);
        }

        realm.commitTransaction();
//        Toast.makeText(context, String.format(context.getString(R.string.my_cart_add_product), product.getTitle()), Toast.LENGTH_SHORT).show();
    }

    private void checkAndShowPrice(Product product, ProductAddLiveHolder holder) {
        PriceWhenStream priceWhenStream = product.getPriceWhenStream();
        if (isShowSalePrice && priceWhenStream != null && priceWhenStream.isActive()) {
            showPrice(priceWhenStream.getPriceDiscount(), true, holder);
        } else {
            String discountStartTimeUTC = product.getStartedAt();
            String discountExpiredTimeUTC = product.getExpiredAt();
            if (!TextUtils.isEmpty(discountStartTimeUTC) && !TextUtils.isEmpty(discountExpiredTimeUTC)) {
                try {
                    String discountStartTime = DateTimeUtils.getDateFormatUTC(discountStartTimeUTC);
                    String discountExpiredTime = DateTimeUtils.getDateFormatUTC(discountExpiredTimeUTC);
                    Date discountStartDate = DateTimeUtils.getDateFollowFormat(discountStartTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date discountExpiredDate = DateTimeUtils.getDateFollowFormat(discountExpiredTime, DateTimeUtils.DATE_TIME_FORMAT);
                    Date currentDate = DateTimeUtils.getCurrentDate(true);

                    if (isShowSalePrice && currentDate.compareTo(discountStartDate) >= 0 && currentDate.compareTo(discountExpiredDate) <= 0) {
                        if (!TextUtils.isEmpty(product.getDiscount()) && !product.getDiscount().equals("0%")) {
                            showPrice(product.getPriceSale(), true, holder);
                        } else {
                            showPrice(product.getPrice(), false, holder);
                        }
                    } else {
                        showPrice(product.getPrice(), false, holder);
                    }
                } catch (Exception e) {
                    AppLog.e(e.getMessage());
                }
            } else {
                showPrice(product.getPrice(), false, holder);
            }
        }
    }

    private void showPrice(float price, boolean isShowPriceSale, ProductAddLiveHolder holder) {
        String displayPrice = NumberUtils.formatPrice(price);
        holder.tvPrice.setText(displayPrice);

        int priceColorResource = context.getResources().getColor(R.color.white);
        if (isShowPriceSale) {
            priceColorResource = context.getResources().getColor(R.color.color_product_price_sale);
        }
        holder.tvPrice.setTextColor(priceColorResource);

        if (isShowPriceSale) {
            holder.ivPriceSale.setVisibility(View.VISIBLE);
        } else {
            holder.ivPriceSale.setVisibility(View.GONE);
        }
    }

    public void setPostId(String postId) {
        mPostId = postId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public interface OnUpdateCartItemsListener {
        void onUpdateCartItemsQuantity(int quantity);
    }

    public void setOnUpdateCartItemsListener(OnUpdateCartItemsListener onUpdateCartItemsListener) {
        mOnUpdateCartItemsListener = onUpdateCartItemsListener;
    }
}
