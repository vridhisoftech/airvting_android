package com.airvtinginc.airvtinglive.gui.enums;

public enum GiftType {
    SENT("sent"),
    RECEIVED("received");

    private String type;

    GiftType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
