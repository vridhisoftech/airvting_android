package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.CommentMediaHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.models.response.CommentItem;
import com.airvtinginc.airvtinglive.models.response.GiftHistory;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class CommentMediaAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    private ActionItemClick actionItemClick;

    public CommentMediaAdapter(Context context, List<GiftHistory> giftHistories, OnItemClickListener onItemClickListener) {
        super(context, giftHistories, onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment_media, parent, false);
                CommentMediaHolder vh = new CommentMediaHolder(v);
                return vh;
            }
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null) return;
        CommentMediaHolder holder = (CommentMediaHolder) mHolder;
        CommentItem comment = (CommentItem) list.get(position);

        holder.tvComment.setText(comment.getComment());
        holder.tvNameUser.setText(comment.getUser().getUsername());
        try {
            holder.tvDate.setText((DateTimeUtils.getElapsedInterval(context, DateTimeUtils.getDateFormatUTC(comment.getCreatedAt()))).toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(comment.getUser().getFeaturedImage())) {
            holder.pbAvatar.setVisibility(View.VISIBLE);
            holder.ivAvatar.setVisibility(View.VISIBLE);
            Glide.with(context).load(comment.getUser().getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            User user = new User();
                            user.setFirstName(comment.getUser().getFirstName());
                            user.setLastName(comment.getUser().getLastName());
                            user.setDisplayName(comment.getUser().getDisplayName());

                            holder.pbAvatar.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbAvatar.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                            return false;
                        }
                    })
                    .into(holder.ivAvatar);
        } else {
            User user = new User();
            user.setFirstName(comment.getUser().getFirstName());
            user.setLastName(comment.getUser().getLastName());
            user.setDisplayName(comment.getUser().getDisplayName());

            holder.pbAvatar.setVisibility(View.GONE);
            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, user, R.dimen.text_size_comment_default_avatar_text);
        }

        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(comment, position);
        });
        holder.ivOptionComment.setOnClickListener(view -> actionItemClick.onMenuClick(comment, position));
    }

    public void setActionItemClick(ActionItemClick actionItemClick) {
        this.actionItemClick = actionItemClick;
    }

    public interface ActionItemClick {
        void onMenuClick(CommentItem commentItem, int position);
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }
}
