package com.airvtinginc.airvtinglive.gui.components;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Rect;
import android.view.View;
import android.widget.FrameLayout;

public class ShowKeyboardOverlap {

    // For more information, see https://code.google.com/p/android/issues/detail?id=5497
    // To use this class, simply invoke assistActivity() on an Activity that already has its content view set.

    public static void assistActivity(Activity activity) {
        new ShowKeyboardOverlap(activity);
    }

    public static void assistDialog(Dialog dialog) {
        new ShowKeyboardOverlap(dialog);
    }

    private View mChildOfContent;
    private int usableHeightPrevious;
    private FrameLayout.LayoutParams frameLayoutParams;

    private ShowKeyboardOverlap(Activity activity) {
        try {
            mChildOfContent = ((FrameLayout) activity.findViewById(android.R.id.content)).getChildAt(0);
            mChildOfContent.getViewTreeObserver().addOnGlobalLayoutListener(() -> possiblyResizeChildOfContent());
            frameLayoutParams = (FrameLayout.LayoutParams) mChildOfContent.getLayoutParams();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private ShowKeyboardOverlap(Dialog dialog) {
        try {
            mChildOfContent = ((FrameLayout) dialog.findViewById(android.R.id.content)).getChildAt(0);
            mChildOfContent.getViewTreeObserver().addOnGlobalLayoutListener(() -> possiblyResizeChildOfContent());
            frameLayoutParams = (FrameLayout.LayoutParams) mChildOfContent.getLayoutParams();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void possiblyResizeChildOfContent() {
        int usableHeightNow = computeUsableHeight();
        if (usableHeightNow != usableHeightPrevious) {
            int usableHeightSansKeyboard = mChildOfContent.getRootView().getHeight();
            int heightDifference = usableHeightSansKeyboard - usableHeightNow;
            if (heightDifference > (usableHeightSansKeyboard / 4)) {
                // keyboard probably just became visible
                frameLayoutParams.height = usableHeightSansKeyboard - heightDifference;
            } else {
                // keyboard probably just became hidden
                frameLayoutParams.height = usableHeightSansKeyboard;
            }
            mChildOfContent.requestLayout();
            usableHeightPrevious = usableHeightNow;
        }
    }

    private int computeUsableHeight() {
        Rect r = new Rect();
        mChildOfContent.getWindowVisibleDisplayFrame(r);
        return r.bottom;
    }

}