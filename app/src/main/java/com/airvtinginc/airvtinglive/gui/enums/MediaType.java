package com.airvtinginc.airvtinglive.gui.enums;

public enum MediaType {
    IMAGE("image"), VIDEO("video"), STREAM("stream");

    private String type;

    MediaType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
