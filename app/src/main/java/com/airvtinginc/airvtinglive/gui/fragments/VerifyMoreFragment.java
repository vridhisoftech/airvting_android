package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.FormatType;
import com.airvtinginc.airvtinglive.components.enums.RequestMethod;
import com.airvtinginc.airvtinglive.components.interfaces.ResultListener;
import com.airvtinginc.airvtinglive.components.services.HttpRequest;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.activities.MainActivity;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;

public class VerifyMoreFragment extends Fragment {
    private static final String TAG = VerifyMoreFragment.class.getName();

    @BindView(R.id.mrlVerify)
    MaterialRippleLayout mrlVerify;

    @BindView(R.id.mEdtFirstName)
    EditText mEdtFirstName;
    @BindView(R.id.mEdtLastName)
    EditText mEdtLastName;
    @BindView(R.id.mEdtPhoneNumber)
    EditText mEdtPhoneNumber;
    @BindView(R.id.btnMale)
    RadioButton mBtnMale;
    @BindView(R.id.btnFemale)
    RadioButton mBtnFemale;
    @BindView(R.id.segmentedButtons)
    SegmentedGroup mSegmentedGroup;
    @BindView(R.id.ccpPhonePicker)
    CountryCodePicker mCcpPhonePicker;

    private String mToken;
    private User mUser;

    private OnVerifyMoreClickListener onVerifyMoreClickListener;

    public VerifyMoreFragment() {
        // Required empty public constructor
    }

    public static VerifyMoreFragment newInstance() {
        VerifyMoreFragment fragment = new VerifyMoreFragment();
        return fragment;
    }

    public void setOnItemClickListener(OnVerifyMoreClickListener onVerifyMoreClickListener) {
        this.onVerifyMoreClickListener = onVerifyMoreClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verify_more, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mBtnMale.setChecked(true);
    }

    @OnClick(R.id.mrlVerify)
    void onClickVerifyMore() {
        if (isValidFields()) {
            signUp();
        }
    }

    public interface OnVerifyMoreClickListener {
        void onVerifyMoreClicked(String token, int verifyCode);
    }

    public void setToken(String token) {
        mToken = token;
    }

    public void setUser(User user) {
        mUser = user;
    }

    private void signUp() {
        JSONObject jsonObject = new JSONObject();
        try {
            String firstName = mEdtFirstName.getText().toString();
            String lastName = mEdtLastName.getText().toString();
            String phoneNumber = mCcpPhonePicker.getSelectedCountryCode() + mEdtPhoneNumber.getText().toString();

            int gender = ServiceConstants.GENDER_MALE;
            if (mBtnFemale.isChecked()) {
                gender = ServiceConstants.GENDER_FEMALE;
            }

            jsonObject.put(ServiceConstants.USER_NAME, mUser.getUsername());
            jsonObject.put(ServiceConstants.EMAIL, mUser.getEmail());
            jsonObject.put(ServiceConstants.BIRTHDAY, mUser.getBirthday());
            jsonObject.put(ServiceConstants.PASSWORD, mUser.getPassword());
            jsonObject.put(ServiceConstants.FIRST_NAME, firstName);
            jsonObject.put(ServiceConstants.LAST_NAME, lastName);
            jsonObject.put(ServiceConstants.GENDER, gender);
            jsonObject.put(ServiceConstants.PHONE_NUMBER, Long.valueOf(phoneNumber));
        } catch (JSONException e) {
            AppLog.e(e.getMessage());
        }

        ResultListener<UserResponse> resultListener = new ResultListener<UserResponse>() {
            @Override
            public void onSucceed(UserResponse result, String msg) {
                Utils.saveCurrentUserInfoToPreferences(getContext(), result.getTokenId(), result.getCountPostsFollowing(), result.getUser());
                goToHome();
            }

            @Override
            public void onError(int code, String msg) {
                Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
            }
        };

        HttpRequest.request(ServiceConstants.URL_SIGN_UP, FormatType.FormBody, RequestMethod.POST,
                jsonObject, resultListener, UserResponse.class, true);
    }

    private void goToHome() {
        Intent in = new Intent(getContext(), MainActivity.class);
        startActivity(in);
        getActivity().finish();
    }

    private void sendSMSVerifyCode() {
        String firstName = mEdtFirstName.getText().toString();
        String lastName = mEdtLastName.getText().toString();
        String phoneNumber = mCcpPhonePicker.getSelectedCountryCode() + mEdtPhoneNumber.getText().toString();

        int gender = ServiceConstants.GENDER_MALE;
        if (mBtnFemale.isChecked()) {
            gender = ServiceConstants.GENDER_FEMALE;
        }

        JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put(ServiceConstants.FIRST_NAME, firstName);
//            jsonObject.put(ServiceConstants.LAST_NAME, lastName);
//            jsonObject.put(ServiceConstants.GENDER, gender);
            jsonObject.put(ServiceConstants.PHONE_NUMBER, Long.valueOf(phoneNumber));
        } catch (JSONException | NumberFormatException e) {
            AppLog.e(e.getMessage());
            return;
        }

        HashMap<String, String> header = new HashMap<>();
        header.put(ServiceConstants.HEADER_AUTHORIZATION, ServiceConstants.HEADER_AUTHORIZATION_PREFIX + mToken);

        ResultListener<UserResponse> resultListener = new ResultListener<UserResponse>() {
            @Override
            public void onSucceed(UserResponse result, String msg) {
                onVerifyMoreClickListener.onVerifyMoreClicked(result.getTokenId(), result.getVerifyCode());
            }

            @Override
            public void onError(int code, String msg) {
                Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
            }
        };

        HttpRequest.request(ServiceConstants.URL_GET_VERIFY_CODE, FormatType.FormBody,
                RequestMethod.PUT, header, jsonObject, resultListener, UserResponse.class, true);
    }

    private boolean isValidFields() {
        if (TextUtils.isEmpty(mEdtFirstName.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.verify_verify),
                    getString(R.string.validator_error_first_name_required));
            return false;
        }

        if (TextUtils.isEmpty(mEdtLastName.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.verify_verify),
                    getString(R.string.validator_error_last_name_required));
            return false;
        }

        if (TextUtils.isEmpty(mEdtPhoneNumber.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.verify_verify),
                    getString(R.string.validator_error_phone_number_required));
            return false;
        }

        return true;
    }

    private void clearErrors() {
        mEdtFirstName.setError(null);
        mEdtLastName.setError(null);
        mEdtPhoneNumber.setError(null);
    }

//    private void showVerifyConfirmationDialog() {
//        String phoneNumber = mCcpPhonePicker.getSelectedCountryCodeWithPlus() + mEdtPhoneNumber.getText().toString();
//        new MaterialDialog.Builder(getActivity())
//                .title(R.string.verify_verify_code)
//                .content(getString(R.string.verify_send_verify_code_confirm, phoneNumber))
//                .negativeText(R.string.dialog_cancel)
//                .positiveText(R.string.dialog_ok)
//                .onNegative((dialog, which) -> dialog.dismiss())
//                .onPositive((dialog, which) -> signUp())
//                .typeface(ResourcesCompat.getFont(getActivity(), R.font.quicksand_medium), ResourcesCompat.getFont(getActivity(), R.font.quicksand_regular))
//                .show();
//    }
}
