package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryViewHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivMedia)
    RoundedImageView ivMedia;
    public @BindView(R.id.pbMediaImage)
    ProgressBar pbMediaImage;
    public @BindView(R.id.ivGalleryVideo)
    ImageView ivGalleryVideo;
    public @BindView(R.id.tvDefaultAvatar)
    TextView tvDefaultAvatar;

    public GalleryViewHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}