package com.airvtinginc.airvtinglive.gui.components.hashtag;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.airvtinginc.airvtinglive.R;

public class EditTextHashtag extends android.support.v7.widget.AppCompatEditText {

    HashTagHelper mEditTextHashTagHelper;

    public EditTextHashtag(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextHashtag(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextHashtag(Context context) {
        super(context);
        init();
    }


    @Override
    public void setTypeface(Typeface tf) {
        String fontName = "opensans_regular.ttf";
        Typeface typeface =(Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName));
        super.setTypeface(typeface);
    }

    private void init(){
        mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.md_blue_500), null);
        mEditTextHashTagHelper.handle(this);
    }

    public HashTagHelper getHashTagHelper(){
        return mEditTextHashTagHelper;
    }


}
