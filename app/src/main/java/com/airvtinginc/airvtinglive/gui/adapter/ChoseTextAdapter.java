package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ChoseTextHolder;
import com.airvtinginc.airvtinglive.models.ReportItem;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Simple adapter example for custom items in the dialog
 */
public class ChoseTextAdapter extends BaseAdapter {

    private int textColor;
    private int backgroundColor;
    private int backgroundCheck;

    private int positionSelected = -1;

    public ChoseTextAdapter(Context context, List<ReportItem> categoryList, OnItemClickListener onItemClickListener) {
        super(context, categoryList, onItemClickListener);
        setBackgroundCheck(R.drawable.box_check);
    }

    public void setTextColor(@ColorInt int color) {
        this.textColor = color;
    }

    public void setTextColorRes(Context context, @ColorRes int colorRes) {
        setTextColor(Utils.getColor(context, colorRes));
    }

    public void setBackgroundColor(@ColorInt int color) {
        this.backgroundColor = color;
    }

    public void setBackgroundColorRes(Context context, @ColorRes int colorRes) {
        setBackgroundColor(Utils.getColor(context, colorRes));
    }

    public void setBackgroundCheck(int backgroundCheck) {
        this.backgroundCheck = backgroundCheck;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chose_text, parent, false);
        return new ChoseTextHolder(view);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        ChoseTextHolder holder = (ChoseTextHolder) mHolder;
        ReportItem reportItem = (ReportItem) list.get(position);

        holder.tvText.setText(reportItem.getName());
        holder.mView.setOnClickListener(view -> onCheck(position));

        if (position == positionSelected) {
            Glide.with(context).load(backgroundCheck).into(holder.ivCheck);
        } else {
            Glide.with(context).load(R.drawable.box_un_check).into(holder.ivCheck);
        }

        holder.ivCheck.setOnClickListener(view -> onCheck(position));

        if (textColor != 0) {
            holder.tvText.setTextColor(textColor);
        }

        if (backgroundColor != 0) {
            holder.llBackground.setBackgroundColor(backgroundColor);
        }
    }

    public String getContentReport() {
        if (positionSelected != -1) {
            return ((ReportItem) list.get(positionSelected)).getName();
        }

        return "";
    }

    private void onCheck(int position) {
        positionSelected = position;
        notifyDataSetChanged();
    }
}
