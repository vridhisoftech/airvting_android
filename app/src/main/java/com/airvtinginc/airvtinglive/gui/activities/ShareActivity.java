package com.airvtinginc.airvtinglive.gui.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareDialog;

public class ShareActivity extends AppCompatActivity {
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    Uri videoFileUri;
    String shareTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        FacebookSdk.sdkInitialize(getApplicationContext());

        // Create a callbackManager to handle the login responses.
        callbackManager = CallbackManager.Factory.create();

        shareDialog = new ShareDialog(this);
        Intent intent = getIntent();
        shareTo = intent.getStringExtra("share");
//        File folder = new File(Environment.getExternalStorageDirectory() + File.separator +Ai);
        Intent pickVideo = new Intent();
        pickVideo.setAction(Intent.ACTION_GET_CONTENT);
        pickVideo.setType("video/*");
        startActivityForResult(Intent.createChooser(pickVideo, "Select Live Video Recorded:"), 1);


        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

                Log.d("Success", "Uploaded");
                Log.d("bothin", shareTo);

                Intent intent = getIntent();
                shareTo = intent.getStringExtra("share");
//                Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                if (shareTo.equals("both")) {

                    Log.d("bothin", shareTo);
                    Intent share = new Intent(Intent.ACTION_SEND);
                    // Set the MIME type
                    share.setType("video/*");
//                 Add the URI to the Intent.
                    share.setPackage("com.instagram.android");
                    share.putExtra(Intent.EXTRA_STREAM, videoFileUri);
//
//                 Broadcast the Intent.
                    startActivity(Intent.createChooser(share, "Share to"));
                }
            }

            @Override
            public void onCancel() {
Toast.makeText(getApplicationContext(),"Cancelled",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("ResultCode", "" + resultCode + "URI" + data.getData());
        if (resultCode != RESULT_OK)
            return;
        Log.v("shareTo", "" + shareTo);

        if (requestCode == 1) {
            videoFileUri = data.getData();
            if (shareTo.equals("facebook")) {
                ShareVideo video = new ShareVideo.Builder()
                        .setLocalUrl(videoFileUri)
                        .build();
                ShareVideoContent content = new ShareVideoContent.Builder()
                        .setVideo(video)
                        .build();
                shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                finish();
            } else if (shareTo.equals("instagram")) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("video/*");
                share.setPackage("com.instagram.android");
                share.putExtra(Intent.EXTRA_STREAM, videoFileUri);
                startActivity(Intent.createChooser(share, "Share to"));
            } else if (shareTo.equals("both")) {
                ShareVideo video = new ShareVideo.Builder()
                        .setLocalUrl(videoFileUri)
                        .build();
                ShareVideoContent content = new ShareVideoContent.Builder()
                        .setVideo(video)
                        .build();
                Log.v("ResultCode", "" + resultCode + "URI" + videoFileUri);

                shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
            }

        }
    }


}