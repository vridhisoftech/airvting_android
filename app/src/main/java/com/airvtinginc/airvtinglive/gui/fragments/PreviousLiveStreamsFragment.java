package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.GetProfileCallBack;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.PostAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviousLiveStreamsFragment extends BaseFragment implements PostAdapter.OnItemClickListener {

    private static final String TAG = PreviousLiveStreamsFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.tvShowEmpty)
    TextView tvShowEmpty;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private int visibleItemCount, pastVisibleItems, totalItemCount;
    private boolean isMoreLoading;
    private GridLayoutManager mLayoutManager;

    private PostAdapter mAdapter;
    private int mCurrentPage = 1;
    private String mMaxId;
    private String mUserId = "";
    private boolean mIsCurrentUser;
    private PostDetail postDetailDeleted;
    private GetProfileCallBack getProfileCallBack;

    public PreviousLiveStreamsFragment() {
        // Required empty public constructor
    }

    public static PreviousLiveStreamsFragment newInstance(String userId, boolean isCurrentUser) {
        PreviousLiveStreamsFragment fragment = new PreviousLiveStreamsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, userId);
        args.putBoolean(Constants.EXTRA_USER_IS_CURRENT_USER, isCurrentUser);
        fragment.setArguments(args);
        return fragment;
    }

    public void setGetProfileCallBack(GetProfileCallBack getProfileCallBack) {
        this.getProfileCallBack = getProfileCallBack;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(Constants.EXTRA_USER_ID);
            mIsCurrentUser = getArguments().getBoolean(Constants.EXTRA_USER_IS_CURRENT_USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_previous_livestreams, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupRecyclerView();
        getPreviousLiveStream(mCurrentPage);
    }

    private void setupRecyclerView() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) emptyView.getLayoutParams();
        layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        emptyView.setPadding(0, getResources().getDimensionPixelSize(R.dimen.profile_padding_top_no_data), 0, 0);
        emptyView.setLayoutParams(layoutParams);
        emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
        if (mIsCurrentUser) {
            tvShowEmpty.setText(getString(R.string.recycle_view_empty_post));
        }
        else {
            tvShowEmpty.setText("No Data Available.");
        }
        mRecyclerView.setEmptyView(emptyView);
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mAdapter = new PostAdapter(getActivity(), this, false, mIsCurrentUser, true);
        mAdapter.setActionOnPostItem(this::showDeletePost);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = mLayoutManager.getChildCount();
                                                      totalItemCount = mLayoutManager.getItemCount();
                                                      pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getPreviousLiveStream(++mCurrentPage);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            mMaxId = null;
            getPreviousLiveStream(mCurrentPage);
        });
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        //modify
    }

    private void showDeletePost(PostDetail postDetail) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogTitle(getString(R.string.dialog_delete_livestream_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_delete_livestream_message));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                postDetailDeleted = postDetail;
                requestApi(postDetail.getId(), true, RequestTarget.DELETE_POST, PreviousLiveStreamsFragment.this);
                messageDialog.dismiss();
            }

            @Override
            public void onCloseActionButtonClick() {
                messageDialog.dismiss();
            }
        });
        messageDialog.show(getActivity().getSupportFragmentManager(), "Delete Dialog");
    }

    private void getPreviousLiveStream(int page) {
        if (TextUtils.isEmpty(mUserId) && mIsCurrentUser) {
            mUserId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        }
        String[] typePost = {MediaType.STREAM.getType()};

        GetListRequest modelRequest = new GetListRequest(mUserId, null, page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, mMaxId, null, typePost, 0);
        requestApi(modelRequest, true, RequestTarget.GET_POST_BY_USER_ID, this);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_POST_BY_USER_ID: {
                    hideLoadMore();
                    PostResponse result = ((PostResponse) response.getData());
                    if (result != null && mAdapter != null) {
                        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                            mAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mAdapter.addAllItems(result.getListPostDetail());
                        mMaxId = result.getMaxId();
                    }
                    break;
                }
                case DELETE_POST: {
                    if (postDetailDeleted != null) {
                        mAdapter.removeItem(postDetailDeleted);
                        getProfileCallBack.onGetProfile();
                    } else {
                        mCurrentPage = 1;
                        mMaxId = null;
                        getPreviousLiveStream(mCurrentPage);
                    }
                    break;
                }
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
        switch (requestTarget) {
            case DELETE_POST:
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_delete_title), failMessage);
                break;
        }
    }
}
