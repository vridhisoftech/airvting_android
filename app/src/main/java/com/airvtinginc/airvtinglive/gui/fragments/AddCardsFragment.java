package com.airvtinginc.airvtinglive.gui.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.AddCardModelRequest;
import com.airvtinginc.airvtinglive.models.response.PaymentDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;

public class AddCardsFragment extends BaseFragment {

    private static final String TAG = AddCardsFragment.class.getSimpleName();
    @BindView(R.id.fm_add_card_multiline_widget)
    CardMultilineWidget cardMultilineWidget;
    @BindView(R.id.fm_add_card_ll_checkbox)
    LinearLayout llCheckbox;
    @BindView(R.id.fm_add_card_checkbox)
    CheckBox checkBox;
    @BindView(R.id.fm_add_card_rl_card_type)
    RelativeLayout rlCardType;
    @BindView(R.id.fm_add_card_tv_card_type)
    TextView tvCardType;
    @BindView(R.id.fm_add_card_btn_save)
    Button btnSave;
    private int selectedIndex = 0;
    private String[] cardType;

    @OnTouch(R.id.viewHideKeyBoard)
    boolean hideKeyBoard() {
        Utils.hideSoftKeyboard(getActivity());
        return true;
    }

    public static AddCardsFragment newInstance() {
        return new AddCardsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_card, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initCardType();

        Intent intent = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getActivity().getResources().getString(R.string.add_card_title);
        getContext().sendBroadcast(intent);

        btnSave.setOnClickListener(v -> getCardToken());
        Utils.showKeyboardOnStart(getActivity());
    }

    private void initCardType() {
        cardType = new String[]{getContext().getResources().getString(R.string.card_personal_capitalize), getContext().getResources().getString(R.string.card_corporate_capitalize)};
        if (tvCardType.getText().toString().equals(cardType[0])) {
            selectedIndex = 0;
        } else {
            selectedIndex = 1;
        }
        rlCardType.setOnClickListener(v -> {
            new AlertDialog.Builder(getContext())
                    .setSingleChoiceItems(cardType, selectedIndex, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        selectedIndex = i;
                        tvCardType.setText(cardType[i]);
                    })
                    .show();
        });
    }

    private void getCardToken() {
        Card card = cardMultilineWidget.getCard();
        if (card != null) {
            DialogUtils.showLoadingProgress(getContext(), false);
            Stripe stripe = new Stripe(getContext(), getActivity().getResources().getString(R.string.stripe_public_key));
            stripe.createToken(card, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                            getString(R.string.add_card_title).toUpperCase(), error.getLocalizedMessage());
                    DialogUtils.hideLoadingProgress();
                }

                @Override
                public void onSuccess(Token token) {
                    String cardType = tvCardType.getText().toString();
                    boolean defaultPaymentMethod = checkBox.isChecked();

                    AddCardModelRequest modelRequest = new AddCardModelRequest();
                    modelRequest.setCardType(cardType);
                    modelRequest.setDefaultPaymentMethod(defaultPaymentMethod);
                    modelRequest.setStripeObject(token);
                    requestApi(modelRequest, false, RequestTarget.ADD_CARDS, AddCardsFragment.this);
                }
            });
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            DialogUtils.hideLoadingProgress();
            PaymentDetailResponse detailResponse = (PaymentDetailResponse) response.getData();
            Utils.saveHasPaymentMethod(getContext(), true);
            Utils.savePaymentMethodId(getContext(), detailResponse.getPaymentDetails().get_id());
            getActivity().finish();
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.add_card_title), failMessage);
    }

}
