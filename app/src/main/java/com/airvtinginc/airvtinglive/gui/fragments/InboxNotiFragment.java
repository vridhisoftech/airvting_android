package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.NotificationsActivity;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.NotificationInboxAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.Conversation;
import com.airvtinginc.airvtinglive.models.response.NotificationsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InboxNotiFragment extends BaseFragment implements BaseAdapter.OnItemClickListener {
    private static final String TAG = InboxNotiFragment.class.getSimpleName();

    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;

    private NotificationInboxAdapter mAdapter;
    private int mCurrentPage = 1;
    private int mTotalPage = 1;
    private boolean isMoreLoading;
    private int visibleItemCount, pastVisibleItems, totalItemCount;

    public InboxNotiFragment() {
        // Required empty public constructor
    }

    public static InboxNotiFragment newInstance() {
        return new InboxNotiFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycleview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mRecyclerView.setEmptyView(emptyView);
        Utils.setUpRecycleView(getContext(), mRecyclerView);

        mAdapter = new NotificationInboxAdapter(getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                                                      visibleItemCount = linearLayoutManager.getChildCount();
                                                      totalItemCount = linearLayoutManager.getItemCount();
                                                      pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getNotificationInbox(++mCurrentPage, true);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );


        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            Intent intent = new Intent(NotificationsActivity.BroadcastActionRefreshUnred);
            getActivity().sendBroadcast(intent);
            getNotificationInbox(mCurrentPage, false);
        });
    }

    private void getNotificationInbox(int page, boolean isShowLoading) {
        String userId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        GetListRequest requestModel = new GetListRequest();
        requestModel.setUserId(userId);
        requestModel.setPerPage(ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING);
        requestModel.setPaginate(page);
        requestModel.setGetNotificationInbox(true);

        requestApi(requestModel, isShowLoading, RequestTarget.GET_NOTIFICATIONS, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshNotificationInbox();
    }

    public void refreshNotificationInbox() {
        mSwipeRefreshLayout.setRefreshing(true);
        mCurrentPage = 1;
        Intent intent = new Intent(NotificationsActivity.BroadcastActionRefreshUnred);
        getActivity().sendBroadcast(intent);
        getNotificationInbox(mCurrentPage, false);
    }

    @Override
    public <T> void onItemClicked(T item, int position) {
        Conversation conversation = (Conversation) item;
        String conversationId = conversation.getId();
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra(Constants.EXTRA_CONVERSATION_ID, conversationId);
        DetailType.CONVERSATIONS_DETAIL.attachTo(intent);
        startActivity(intent);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            hideLoadMore();
            switch (requestTarget) {
                case GET_NOTIFICATIONS:
                    NotificationsResponse notificationActivities = (NotificationsResponse) response.getData();
                    if (notificationActivities != null && notificationActivities.getNotifications() != null) {
                        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                            mAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mAdapter.addAllItems(notificationActivities.getNotifications());
                    }
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }
}
