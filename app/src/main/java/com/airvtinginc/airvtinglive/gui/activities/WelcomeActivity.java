package com.airvtinginc.airvtinglive.gui.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.AutoScrollViewPager;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.fragments.WelcomeFragment;
import com.airvtinginc.airvtinglive.models.request.SocicalSignInModelRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.UserFacebook;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.net.MalformedURLException;
import java.net.URL;
public class WelcomeActivity extends BaseActivity implements APIResponseListener {
    private static final String TAG = WelcomeActivity.class.getName();

    @BindView(R.id.mrlFacebook)
    MaterialRippleLayout mrlFacebook;
    @BindView(R.id.mrlSignIn)
    MaterialRippleLayout mrlSignIn;
    @BindView(R.id.mrlSignUp)
    MaterialRippleLayout mrlSignUp;
    @BindView(R.id.mIndicator)
    CirclePageIndicator mIndicator;
    @BindView(R.id.mViewPager)
    AutoScrollViewPager mViewPager;
    @BindView(R.id.tvTermsAndPrivacyPolicy)
    TextView tvTermsAndPrivacyPolicy;

    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;


    @OnClick(R.id.tvTermsAndPrivacyPolicy)
    void showTermsAndPrivacy() {
//        WebViewDialog dialog = WebViewDialog.newInstance(ServiceConstants.URL_USER_AGREEMENT,
//                getString(R.string.navigation_drawer_menu_terms_condition));
//        dialog.show(this.getSupportFragmentManager(), "Terms and Privacy Dialog");

        Intent intent = new Intent(WelcomeActivity.this, DetailActivity.class);
        DetailType.TERMS_CONDITIONS.attachTo(intent);
        startActivity(intent);
    }

    @OnClick(R.id.mrlFacebook)
    void signInFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_gender"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(TAG, "login facebook onSuccess");
                        handleSignInFacebookResult();
//                        LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "login facebook onCancel");
                        LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(TAG, "login facebook onError");
                        exception.printStackTrace();
                        LoginManager.getInstance().logOut();
                    }
                });
    }

    @OnClick(R.id.mrlGoogle)
    void signInGooglePlus() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, ServiceConstants.REQUEST_CODE_SIGN_IN_GOOGLE_PLUS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        initGoogleServices();

        prepareViewPager();

        mrlSignIn.setOnClickListener(v ->
                startSignUpSignInActivity(1)
        );
        mrlSignUp.setOnClickListener(v ->
                startSignUpSignInActivity(0)
        );
        // get KeyHash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.airvtinginc.airvtinglive", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        SpannableString terms = new SpannableString(getString(R.string.navigation_terms_and_conditions));
        terms.setSpan(new UnderlineSpan(), 0, terms.length(), 0);
        tvTermsAndPrivacyPolicy.setText(terms);
    }

    private void initGoogleServices() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void startSignUpSignInActivity(int index) {
        Intent intent = new Intent(this, SignActivity.class);
        intent.putExtra(SignActivity.TAB_POSITION, index);
        startActivity(intent);
    }

    private void prepareViewPager() {
        PageFragmentAdapter mAdapter = new PageFragmentAdapter(getSupportFragmentManager());
        mAdapter.addFragment(WelcomeFragment.newInstance(R.drawable.welcome_bg_01), null);
        mAdapter.addFragment(WelcomeFragment.newInstance(R.drawable.welcome_bg_02), null);
        mAdapter.addFragment(WelcomeFragment.newInstance(R.drawable.welcome_bg_03), null);
        mViewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mViewPager);
        mViewPager.setInterval(3000);
        mViewPager.startAutoScroll();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
                case ServiceConstants.REQUEST_CODE_SIGN_IN_GOOGLE_PLUS:
                    // The Task returned from this call is always completed, no need to attach
                    // a listener.
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    handleSignInGoogleResult(task);
                    break;
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInFacebookResult() {
        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), (object, response) -> {
            Log.d(TAG, response.getJSONObject().toString());
            Gson gson = new Gson();
            UserFacebook userFacebook = gson.fromJson(response.getJSONObject().toString(), UserFacebook.class);
            userFacebook.setToken(AccessToken.getCurrentAccessToken().getToken());
            signUpWithFaceBook(userFacebook);
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email,first_name,last_name,picture,gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void handleSignInGoogleResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            Log.w(TAG, "Signed in successfully");
            Log.d(TAG, account.toJson().toString());
            Log.d(TAG, account.getIdToken());
            signUpWithGoogle(account);
//            mEdtUsername.setText(account.getDisplayName());
//            mEdtEmail.setText(account.getEmail());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            e.printStackTrace();
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Log.w(TAG, "signInResult:" + GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
        }
    }

    private void signUpWithFaceBook(UserFacebook userFacebook) {
        int gender;
        String email,profilePic = null;
        try {
            profilePic = new URL("https://graph.facebook.com/"+userFacebook.getId()+"/picture?type=large").toString();
            SharedPreferencesManager.getInstance(this).putString(Constants.PREF_CURRENT_USER_PHOTO_URL, profilePic);
        }
        catch (MalformedURLException m)
        {

        }
        if (userFacebook.getGender() != null) {
            gender = userFacebook.getGender().endsWith(ServiceConstants.FACEBOOK_GENDER_MALE) ?
                    ServiceConstants.GENDER_MALE : ServiceConstants.GENDER_FEMALE;
        } else {
            gender = 0;
        }
        if (userFacebook.getEmail() != null) {
            email = userFacebook.getEmail();
        } else {
            email = "";
        }
        SocicalSignInModelRequest modelRequest = new SocicalSignInModelRequest(userFacebook.getId(),
                ServiceConstants.SOCIAL_TYPE_FACEBOOK, userFacebook.getToken(),
                userFacebook.getFirstName(), email, "",
                userFacebook.getFirstName(), userFacebook.getLastName(),
                gender, "", profilePic);

        requestApi(modelRequest, true, RequestTarget.SIGN_IN_SOCIAL, this);
    }

    private void goToHome() {
        Intent in = new Intent(this, MainActivity.class);
        startActivity(in);
        finish();
    }

    private void signUpWithGoogle(GoogleSignInAccount account) {
        int gender = 0;
        String email = "";
        try {
            email = account.getEmail();
        } catch (Exception ex) {

        }
        SocicalSignInModelRequest modelRequest = new SocicalSignInModelRequest(account.getId(),
                ServiceConstants.SOCIAL_TYPE_GOOGLE, account.getIdToken(),
                account.getGivenName(), email, "",
                account.getGivenName(), account.getFamilyName(),
                gender, "", account.getPhotoUrl().toString());

        requestApi(modelRequest, true, RequestTarget.SIGN_IN_SOCIAL, this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        switch (requestTarget) {
            case SIGN_IN_SOCIAL:
                if (response.isSuccess()) {
                    UserResponse userResponse = (UserResponse) response.getData();
                    Utils.saveCurrentUserInfoToPreferences(getApplicationContext(), userResponse.getTokenId(), userResponse.getCountPostsFollowing(), userResponse.getUser());
                    Utils.savePaymentMethodId(getApplicationContext(), userResponse.getPaymentMethodIdDefault());
                    goToHome();
                } else {
                    Log.e(TAG, "SIGN_IN_SOCIAL: fail");
                }
                break;
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        DialogUtils.showMessageDialog(this, getSupportFragmentManager(), getString(R.string.sign_in_sign_in), failMessage);
    }
}
