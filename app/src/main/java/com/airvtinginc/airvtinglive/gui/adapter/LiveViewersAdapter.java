package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.LiveViewersViewHolder;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.models.response.UserInFireStore;
import com.airvtinginc.airvtinglive.models.response.Viewer;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.firestore.CollectionReference;

import java.util.ArrayList;
import java.util.List;


public class LiveViewersAdapter extends BaseAdapter {

    private CollectionReference usersRef;

    public void setUsersRef(CollectionReference usersRef) {
        this.usersRef = usersRef;
    }

    public LiveViewersAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_live_viewer, parent, false);
        LiveViewersViewHolder vh = new LiveViewersViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (list.get(position) == null) return;

        final LiveViewersViewHolder holder = (LiveViewersViewHolder) mHolder;
        Viewer viewer = (Viewer) list.get(position);

        if (!TextUtils.isEmpty(viewer.getFeaturedImage())) {
            holder.pbLoadAvatar.setVisibility(View.VISIBLE);
            holder.ivProfile.setVisibility(View.VISIBLE);
            Glide.with(context).load(viewer.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            User user = new User();
                            user.setDisplayName(viewer.getDisplayName());
                            holder.pbLoadAvatar.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, (CircleImageView) holder.ivProfile, user, R.dimen.people_default_avatar_text_size);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbLoadAvatar.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, (CircleImageView) holder.ivProfile);
                            return false;
                        }
                    })
                    .into(holder.ivProfile);
        } else {
            User user = new User();
            user.setDisplayName(viewer.getDisplayName());
            holder.pbLoadAvatar.setVisibility(View.GONE);
            Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, (CircleImageView) holder.ivProfile, user, R.dimen.profile_default_avatar_text_size);
        }
        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(view, position);
        });
    }

    private void getUserInfo(String id, int pos) {
        usersRef.document(id).get().addOnSuccessListener(documentSnapshot -> {
            try {
                UserInFireStore userInFireStore = documentSnapshot.toObject(UserInFireStore.class);
                userInFireStore.setId(documentSnapshot.getId());
                Viewer viewer = (Viewer) list.get(pos);
                viewer.setFeaturedImage(userInFireStore.getFeaturedImage());
                list.set(pos, viewer);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            notifyDataSetChanged();
        });
    }

    public List getList() {
        if (list != null) {
            return list;
        }
        return null;
    }
}