package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.WalletGiftHolder;
import com.airvtinginc.airvtinglive.models.response.GiftList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class WalletGiftAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    public WalletGiftAdapter(Context context, List<GiftList> giftLists, OnItemClickListener onItemClickListener) {
        super(context, giftLists, onItemClickListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_wallet_gift, parent, false);
                WalletGiftHolder vh = new WalletGiftHolder(v);
                return vh;
            }
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder)
            return;
        if (list.get(position) == null) return;
        WalletGiftHolder holder = (WalletGiftHolder) mHolder;
        GiftList giftList = (GiftList) list.get(position);
        holder.tvNameGift.setText(giftList.getTitle());
        holder.tvQuantityGift.setText(String.valueOf(giftList.getQuantity()));

        if (!TextUtils.isEmpty(giftList.getFeaturedImage())) {
            holder.pbGift.setVisibility(View.VISIBLE);
            Glide.with(context).load(giftList.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbGift.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbGift.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivGift);
        } else {
        }
        holder.mView.setOnClickListener(view -> {
            onItemClickListener.onItemClicked(giftList, position);
        });
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }
}
