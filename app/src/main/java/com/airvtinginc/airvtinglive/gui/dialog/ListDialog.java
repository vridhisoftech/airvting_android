package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.ChoseTextAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ProductsAddLiveAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TagAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TextRecycleViewAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.ReportItem;
import com.airvtinginc.airvtinglive.models.request.ProductListModelRequest;
import com.airvtinginc.airvtinglive.models.request.SearchUserModelRequest;
import com.airvtinginc.airvtinglive.models.request.TypeRequest;
import com.airvtinginc.airvtinglive.models.response.CategoryItem;
import com.airvtinginc.airvtinglive.models.response.CategoryListModelResponse;
import com.airvtinginc.airvtinglive.models.response.Product;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.SearchUserModelResponse;
import com.airvtinginc.airvtinglive.models.response.TagChip;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListDialog extends BaseFragmentDialog {

    private static final String TAG = ListDialog.class.getSimpleName();

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.rlBackground)
    RelativeLayout rlBackground;
    @BindView(R.id.btnClose)
    ImageButton btnClose;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.rlSend)
    RelativeLayout rlSend;
    @BindView(R.id.tvSend)
    TextView tvSend;

    private RecyclerView.Adapter<?> adapter;
    private RecyclerView.LayoutManager layoutManager;
    private CharSequence title;
    private int titleColor;
    private int backgroundColor;
    private int backgroundSendButton;
    private int sendButtonColor;
    private OnDismissListener onDismissListener;
    private OnSendListener onSendListener;
    private ShowDialogType showDialogType;

    //ExplorePosts
    private TextRecycleViewAdapter textRecycleViewAdapter;
    private boolean isPost;

    //Tag
    private TagAdapter tagAdapter;
    private BroadcastReceiver mReceiver;
    private String maxId;
    private int paginate = 1;
    private int perPage = 20;
    private List<TagChip> chipList;

    //Store
    private ProductsAddLiveAdapter productsAddLiveAdapter;

    //Report
    private ChoseTextAdapter choseTextAdapter;

    private TextView.OnEditorActionListener onEditorActionListener;

    Handler handler = new Handler(Looper.getMainLooper() /*UI thread*/);
    Runnable workRunnable;

    @OnClick(R.id.btnClose)
    void close() {
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        } else {
            dismiss();
        }
    }

    @OnClick(R.id.mrlSend)
    void send() {
        if (choseTextAdapter == null) {
            return;
        }
        String content = choseTextAdapter.getContentReport();
        if (content.equals("")) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_report_title), getString(R.string.dialog_non_selected_report));
            return;
        }
        close();
        if (onSendListener != null) {
            onSendListener.onSend(content);
        }
    }

    public static ListDialog newInstance() {
        return new ListDialog();
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    public OnDismissListener getOnDismissListener() {
        return onDismissListener;
    }

    public void setOnSendListener(OnSendListener onSendListener) {
        this.onSendListener = onSendListener;
    }

    public OnSendListener getOnSendListener() {
        return onSendListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.systemKeyboard(getDialog());
        setAllParentsClip(view, false);
        updateUI();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void updateUI() {
        setTitleUI();
        setShowSearch();
        switch (showDialogType) {
            case TAG:
                setUpTag();
                break;
            case CATEGORY:
                setUpCategory();
                break;
            case STORE:
                setUpMyStore();
                break;
            case REPORT:
                setUpReport();
                break;
        }
        setRecyclerViewUI();
        setBackgroundColorUI();
        setTitleColorUI();
    }

    private void setTitleUI() {
        if (title != null && !title.equals("")) {
            tvTitle.setText(title);
        }
    }

    private void setRecyclerViewUI() {
        if (showDialogType != ShowDialogType.REPORT) {
            mRecyclerView.setEmptyView(emptyView);
        }
        if (layoutManager != null) {
            mRecyclerView.setLayoutManager(layoutManager);
        }
        if (adapter != null) {
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setHasFixedSize(true);
        }
    }

    private void setBackgroundColorUI() {
        if (backgroundColor != 0) {
            rlBackground.setBackgroundColor(backgroundColor);
        }
    }

    private void setTitleColorUI() {
        if (titleColor != 0) {
            tvTitle.setTextColor(titleColor);
            btnClose.setColorFilter(titleColor);
        }
    }

    private void setShowSearch() {
        if (onEditorActionListener != null || showDialogType == ShowDialogType.TAG) {
            etSearch.setVisibility(View.VISIBLE);
        } else {
            etSearch.setVisibility(View.GONE);
        }
    }

    public void setAdapter(RecyclerView.Adapter<?> adapter, @Nullable RecyclerView.LayoutManager layoutManager) {
        if (layoutManager != null
                && !(layoutManager instanceof LinearLayoutManager)
                && !(layoutManager instanceof GridLayoutManager)) {
            throw new IllegalStateException(
                    "You can currently only use LinearLayoutManager"
                            + " and GridLayoutManager with this library.");
        }
        this.adapter = adapter;
        this.layoutManager = layoutManager;
    }

    public void setTitle(Context context, @StringRes int titleRes) {
        setTitle(context.getText(titleRes));
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public void setTitleColor(@ColorInt int color) {
        this.titleColor = color;
    }

    public void setTitleColorRes(Context context, @ColorRes int colorRes) {
        setTitleColor(Utils.getColor(context, colorRes));
    }

    public void setSendButtonColor(@ColorInt int color) {
        this.sendButtonColor = color;
    }

    public void setSendButtonColorRes(Context context, @ColorRes int colorRes) {
        setSendButtonColor(Utils.getColor(context, colorRes));
    }

    public void setBackgroundColor(@ColorInt int color) {
        this.backgroundColor = color;
    }

    public void setBackgroundColorRes(Context context, @ColorRes int colorRes) {
        setBackgroundColor(Utils.getColor(context, colorRes));
    }

    public void setBackgroundSendButton(int backgroundSendButton) {
        this.backgroundSendButton = backgroundSendButton;
    }

    public void setOnEditorActionListener(TextView.OnEditorActionListener onEditorActionListener) {
        this.onEditorActionListener = onEditorActionListener;
    }

    public ShowDialogType getShowDialogType() {
        return showDialogType;
    }

    public void setShowDialogType(ShowDialogType showDialogType) {
        this.showDialogType = showDialogType;
    }

    public boolean isPost() {
        return isPost;
    }

    public void setPost(boolean post) {
        isPost = post;
    }

    private void setUpCategory() {
        textRecycleViewAdapter = (TextRecycleViewAdapter) adapter;
        getCategory();
    }

    private void getCategory() {
        TypeRequest modelRequest = new TypeRequest();
        if (isPost) {
            modelRequest.setType("post");
        } else {
            modelRequest.setType("product");
        }
        requestApi(modelRequest, true, RequestTarget.GET_CATEGORY, this);
    }

    private void setUpTag() {
        tagAdapter = (TagAdapter) adapter;
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                handler.removeCallbacks(workRunnable);
                workRunnable = () -> searchUser(etSearch.getText().toString());
                handler.postDelayed(workRunnable, 1000 /*delay*/);
            }
        });
        etSearch.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchUser(textView.getText().toString());
                return true;
            }
            return false;
        });
        new Handler().postDelayed(() -> {
            etSearch.requestFocus();
            Utils.showSoftKeyboard(getActivity(), etSearch);
        }, 500);
    }

    private void searchUser(String userName) {
        if (userName.trim().equals("")) {
            cleanRecycleViewTag();
            return;
        }
        Utils.hideSoftKeyboard(getActivity());
        SearchUserModelRequest modelRequest = new SearchUserModelRequest();
        modelRequest.setUsername(userName);
        modelRequest.setMaxId(maxId);
        modelRequest.setPaginate(paginate);
        modelRequest.setPerPage(perPage);
        requestApi(modelRequest, true, RequestTarget.SEARCH_USER, this);
    }

    private void cleanRecycleViewTag() {
        tagAdapter.clearAllItems();
    }

    private void setUpMyStore() {
        productsAddLiveAdapter = (ProductsAddLiveAdapter) adapter;
        getMyStore();
    }

    private void getMyStore() {
        String userId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (userId.equals("")) {
            return;
        }
        ProductListModelRequest modelRequest = new ProductListModelRequest(userId, paginate, perPage, maxId);
        requestApi(modelRequest, true, RequestTarget.GET_LIST_PRODUCT, this);
    }

    private void setUpReport() {
        choseTextAdapter = (ChoseTextAdapter) adapter;
        rlSend.setVisibility(View.VISIBLE);
        if (backgroundSendButton != 0) {
            rlSend.setBackgroundResource(backgroundSendButton);
        }
        if (sendButtonColor != 0) {
            tvSend.setTextColor(sendButtonColor);
        }
        getReport();
    }

    private void getReport() {
        List<ReportItem> reportItems = new ArrayList<>();
        reportItems.add(new ReportItem("", "Sexual content"));
        reportItems.add(new ReportItem("", "Violent or repulsive content"));
        reportItems.add(new ReportItem("", "Hateful or abusive content"));
        reportItems.add(new ReportItem("", "Harmful dangerous acts"));
        reportItems.add(new ReportItem("", "Chid abuse"));
        reportItems.add(new ReportItem("", "Infringes my rights"));
        reportItems.add(new ReportItem("", "Promotes terrorism"));
        reportItems.add(new ReportItem("", "Spam or misleading"));
        choseTextAdapter.setList(reportItems);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_CATEGORY:
                    List<CategoryItem> categoryList = ((CategoryListModelResponse) response.getData()).getCategoryItemList();
                    Log.e(TAG, "onResponseSuccess: category size : " + categoryList.size());
                    textRecycleViewAdapter.setList(categoryList);

                    break;
                case SEARCH_USER:
                    List<User> userList = ((SearchUserModelResponse) response.getData()).getUserList();
                    if (userList.size() > 0) {
                        chipList = new ArrayList<>();
                        for (User user : userList) {
                            chipList.add(new TagChip(user.getId(), user.getUsername()));
                        }
                        tagAdapter.setList(chipList);
                    } else {
                        cleanRecycleViewTag();
                    }
                    break;
                case GET_LIST_PRODUCT:
                    List<Product> products = ((ProductsResponse) response.getData()).getProducts();
                    Log.e(TAG, "onResponseSuccess: product size : " + products.size());
                    productsAddLiveAdapter.setList(products);
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        onDismissListener.onDismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
                mReceiver = null;
            }
            if (workRunnable != null) {
                handler.removeCallbacks(workRunnable);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        } else {
            dismiss();
        }
    }

    public interface OnDismissListener {
        void onDismiss();
    }

    public interface OnSendListener {
        void onSend(String content);
    }
}