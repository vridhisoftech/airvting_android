package com.airvtinginc.airvtinglive.gui.activities;

import android.os.Bundle;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.PageFragmentAdapter;
import com.airvtinginc.airvtinglive.gui.components.NonSwipeableViewPager;
import com.airvtinginc.airvtinglive.gui.fragments.SignFragment;
import com.airvtinginc.airvtinglive.gui.fragments.VerifyFragment;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.template.viewpager.transformers.DefaultTransformer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignActivity extends BaseActivity implements SignFragment.OnSignUpClickListener {

    public static final String TAB_POSITION = SignActivity.class.getSimpleName();

    @BindView(R.id.mViewPager)
    NonSwipeableViewPager mViewPager;

    private VerifyFragment mVerifyFragment;

    @OnClick(R.id.action_bar_home_ib_back)
    void back() {
        handleBackPress();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        ButterKnife.bind(this);
        prepareViewPager();
    }

    private void prepareViewPager() {
        SignFragment signFragment = SignFragment.newInstance();
        signFragment.setOnSignUpClick(this);
        if (getIntent().getExtras() != null) {
            int signTabPosition = getIntent().getExtras().getInt(TAB_POSITION, 0);
            Bundle bundle = new Bundle();
            bundle.putInt(TAB_POSITION, signTabPosition);
            signFragment.setArguments(bundle);
        }
        mVerifyFragment = VerifyFragment.newInstance();

        PageFragmentAdapter adapter = new PageFragmentAdapter(getSupportFragmentManager());

        adapter.addFragment(signFragment, null);
        adapter.addFragment(mVerifyFragment, null);
        mViewPager.setAdapter(adapter);
        mViewPager.setPageTransformer(true, new DefaultTransformer());
    }

    @Override
    public void onBackPressed() {
        handleBackPress();
    }

    private void handleBackPress() {
        switch (mViewPager.getCurrentItem()) {
            case 0:
                finish();
                break;
            case 1:
                handleVerifyBackPress();
                break;
            default:
                finish();
                break;
        }
    }

    private void handleVerifyBackPress() {
        switch (mVerifyFragment.getVerifyViewPager().getCurrentItem()) {
            case 0:
                mViewPager.setCurrentItem(0, true);
                break;
            case 1:
                if (!mVerifyFragment.isLogout()) {
                    mVerifyFragment.getVerifyViewPager().setCurrentItem(0, true);
                } else {
                    finish();
                }
                break;
        }
    }

    @Override
    public void onSignUpClick(User user) {
        mVerifyFragment.setUser(user);
        mViewPager.setCurrentItem(1, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.hideSoftKeyboard(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideSoftKeyboard(this);
    }

}
