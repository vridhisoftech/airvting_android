package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.MyCardStripeViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.MyCardViewHolder;
import com.airvtinginc.airvtinglive.models.response.PaymentDetails;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;

import java.util.List;

public class UserCardAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM_CARDS = 0;
    private static final int VIEW_TYPE_ITEM_STRIPE = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    public UserCardAdapter(Context context, List<PaymentDetails> paymentDetailsList, OnItemClickListener onItemClickListener) {
        super(context, paymentDetailsList, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_ITEM_CARDS: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cards, parent, false);
                MyCardViewHolder vh = new MyCardViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_ITEM_STRIPE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_connect_stripe, parent, false);
                MyCardStripeViewHolder vh = new MyCardStripeViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder) return;
        if (list.get(position) == null) return;

        if (mHolder instanceof MyCardViewHolder) {
            MyCardViewHolder viewHolder = (MyCardViewHolder) mHolder;
            PaymentDetails paymentDetails = (PaymentDetails) list.get(position);
            String title = paymentDetails.getTitle();
            String cardType = paymentDetails.getCardType();
            String lastFour = paymentDetails.getLastFour();
            String expiryMonth = paymentDetails.getExpiryMonth();
            String expiryYear = paymentDetails.getExpiryYear();

            StringBuilder cardName = new StringBuilder(), cardNumber = new StringBuilder();
            viewHolder.tvCardName.setText(cardName.append(title).append(" - ").append(cardType));
            viewHolder.tvCardNumber.setText(cardNumber.append("xxxx-").append(lastFour).append(" - ").append(expiryMonth).append("/").append(expiryYear));
            Glide.with(context).load(paymentDetails.getFeaturedImage()).into(viewHolder.ivCardType);
            if (paymentDetails.isDefaultPaymentMethod()) {
                viewHolder.ivCheck.setVisibility(View.VISIBLE);
                Utils.saveHasPaymentMethod(context, true);
                Utils.savePaymentMethodId(context, paymentDetails.get_id());
            } else {
                viewHolder.ivCheck.setVisibility(View.GONE);
            }

            viewHolder.llBound.setOnClickListener(v -> onItemClickListener.onItemClicked(paymentDetails, position));

        } else if (mHolder instanceof MyCardStripeViewHolder) {
            MyCardStripeViewHolder holder = (MyCardStripeViewHolder) mHolder;
            String linkStripe = Utils.getCreateStripeAccountUrl(context);

            SpannableString link;
            if (TextUtils.isEmpty(linkStripe)) {
                holder.tvInfo.setText(context.getString(R.string.my_card_stripe_connected_info));
                link = new SpannableString(context.getString(R.string.my_card_stripe_connected_link));
            } else {
                holder.tvInfo.setText(context.getString(R.string.my_card_stripe_setup_info));
                link = new SpannableString(context.getString(R.string.my_card_stripe_setup_link));
            }
            link.setSpan(new UnderlineSpan(), 0, link.length(), 0);
            holder.tvLink.setText(link);

            holder.tvLink.setOnClickListener(v -> {
                if (TextUtils.isEmpty(linkStripe)) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServiceConstants.URL_STRIPE));
                    context.startActivity(browserIntent);
                } else {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkStripe));
                    context.startActivity(browserIntent);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? (position == 0 ? VIEW_TYPE_ITEM_STRIPE : VIEW_TYPE_ITEM_CARDS) : VIEW_TYPE_LOAD_MORE;
    }

}
