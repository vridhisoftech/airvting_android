package com.airvtinginc.airvtinglive.gui.enums;

public enum NotiActivityType {
    FOLLOW("follow"),
    OFFER("buyProduct"),
    LIKE("like"),
    LIVE_STREAM("liveStream"),
    TAGGED("tag"),
    COMMENT("comment"),
    MESSAGE("message");

    private String type;

    NotiActivityType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
