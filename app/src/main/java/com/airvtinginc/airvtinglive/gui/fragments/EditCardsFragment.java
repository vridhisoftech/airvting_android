package com.airvtinginc.airvtinglive.gui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.components.EditTextExpiryDate;
import com.airvtinginc.airvtinglive.gui.dialog.MessageDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.models.request.AddCardModelRequest;
import com.airvtinginc.airvtinglive.models.response.PaymentDetails;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.bumptech.glide.Glide;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditCardsFragment extends BaseFragment {

    private static final String TAG = EditCardsFragment.class.getName();

    @BindView(R.id.tv_edit_card_expiration_date)
    TextView tvExpirationDate;
    @BindView(R.id.fm_edit_card_et_expire_date)
    EditTextExpiryDate etExpireDate;
    @BindView(R.id.fm_edit_card_iv_card_type)
    ImageView ivCardType;
    @BindView(R.id.fm_edit_card_tv_card_last4)
    TextView tvCardLast4;
    @BindView(R.id.fm_edit_card_checkbox)
    CheckBox checkBox;
    @BindView(R.id.fm_edit_card_tv_card_type)
    TextView tvCardType;
    @BindView(R.id.fm_edit_card_btn_save)
    Button btnSave;

    private PaymentDetails paymentDetails;
    private BroadcastReceiver mReceiver;

    public static EditCardsFragment newInstance(Bundle bundle) {
        EditCardsFragment editCardsFragment = new EditCardsFragment();
        editCardsFragment.setArguments(bundle);
        return editCardsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_card, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        configToolbar();
        registerReceiver();
        setExpirationDateFocusListener();

        paymentDetails = (PaymentDetails) getArguments().getSerializable(Constants.EXTRA_PAYMENT_DETAILS);
        if (paymentDetails != null) {
            String last4 = paymentDetails.getLastFour();
            String cardType = paymentDetails.getCardType();
            String displayCardType = cardType.toUpperCase().substring(0, 1) + cardType.toLowerCase().substring(1);
            tvCardLast4.setText(last4);
            tvCardType.setText(displayCardType);

            String expiredMonth = paymentDetails.getExpiryMonth();
            String expiredYear = paymentDetails.getExpiryYear();
            StringBuilder expiredDate = new StringBuilder();
            if (Integer.parseInt(expiredMonth) < 10 && expiredMonth.length() == 1) {
                expiredDate.append("0").append(expiredMonth).append("/").append(expiredYear);
            } else {
                expiredDate.append(expiredMonth).append("/").append(expiredYear);
            }
            etExpireDate.setText(expiredDate);

            checkBox.setChecked(paymentDetails.isDefaultPaymentMethod());
            Glide.with(getContext()).load(paymentDetails.getFeaturedImage()).into(ivCardType);
        }
        btnSave.setOnClickListener(v -> updateCard());

    }

    private void configToolbar() {
        Intent intent = new Intent(DetailActivity.BroadcastActionUpdateHeaderTitle);
        DetailActivity.headerTitle = getActivity().getResources().getString(R.string.edit_card);
        getContext().sendBroadcast(intent);

        Intent intentShowIconTrash = new Intent(DetailActivity.BroadcastActionShowIconTrash);
        getContext().sendBroadcast(intentShowIconTrash);
    }

    private void registerReceiver() {
        // create new receiver
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (Objects.requireNonNull(intent.getAction())) {
                        case DetailActivity.BroadcastActionTrash:
                            showConfirmDeleteCard();
                            break;
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(DetailActivity.BroadcastActionTrash);
            getActivity().registerReceiver(mReceiver, filter);
        }
    }

    private void showConfirmDeleteCard() {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogType(MessageDialogType.DEFAULT);
        messageDialog.setDialogTitle(getString(R.string.dialog_delete_card_title));
        messageDialog.setDialogMessage(getString(R.string.dialog_delete_card_msg));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_delete));
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_delete));
        messageDialog.setOnActionButtonClickListener(new MessageDialog.OnActionClickListener() {
            @Override
            public void onPositiveActionButtonClick() {
                deleteCard();
            }

            @Override
            public void onCloseActionButtonClick() {

            }
        });
        messageDialog.show(getFragmentManager(), "Show Confirm Delete Card Dialog");
    }

    private void setExpirationDateFocusListener() {
        etExpireDate.setOnFocusChangeListener((view, isFocus) -> {
            int textColor = getResources().getColor(R.color.colorBody);
            if (isFocus) {
                textColor = getResources().getColor(R.color.colorAccent);
            }
            tvExpirationDate.setTextColor(textColor);
        });
    }

    private void updateCard() {
        AddCardModelRequest modelRequest = new AddCardModelRequest();
        modelRequest.setDefaultPaymentMethod(checkBox.isChecked());
        modelRequest.setPaymentMethodId(paymentDetails.get_id());
        String expireDate = etExpireDate.getText().toString();
        if (expireDate.equals("")) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                    getString(R.string.edit_card), getString(R.string.expiry_date_empty));
        } else {
            String[] expiry = expireDate.split("/");
            if (expiry.length < 2) {
                // Expiration date need both month and year
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                        getString(R.string.edit_card), getString(R.string.expiry_date_invalid));
                return;
            }

            String month = expiry[0];
            String year = "20" + expiry[1]; // Expiry year only have 2 last number (ex: 2018 -> 18, 2099 -> 9. So need to hardcode with beginning is "20"

            Calendar calendar = Calendar.getInstance();
            int currentMonth = calendar.get(Calendar.MONTH) + 1; // Because month is counting from 0, so need to +1
            int currentYear = calendar.get(Calendar.YEAR);

            if (Integer.valueOf(year) < currentYear) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                        getString(R.string.edit_card), getString(R.string.expiry_year_invalid));
            } else if ((Integer.valueOf(year) == currentYear && Integer.valueOf(month) < currentMonth) || Integer.valueOf(month) > 12) {
                DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                        getString(R.string.edit_card), getString(R.string.expiry_month_invalid));
            } else {
                modelRequest.setExpiryMonth(expiry[0]);
                modelRequest.setExpiryYear(expiry[1]);
                requestApi(modelRequest, true, RequestTarget.UPDATE_CARDS, EditCardsFragment.this);
            }
        }
    }

    private void deleteCard() {
        requestApi(paymentDetails.get_id(), true, RequestTarget.DELETE_CARDS, EditCardsFragment.this);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            getActivity().finish();
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.edit_card), failMessage);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
                mReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
