package com.airvtinginc.airvtinglive.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TransactionAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.TransactionItemHistoryAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.dialog.TransactionItemHistoryDialog;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.TransactionItem;
import com.airvtinginc.airvtinglive.models.response.WalletRespone;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailWalletTransactionFragment extends BaseFragment {

    private static final String TAG = DetailWalletTransactionFragment.class.getName();

    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private LinearLayoutManager mLinearLayoutManager;
    private TransactionAdapter mAdapter;
    private int mCurrentPage = 1;
    private String mMaxId;
    private boolean isMoreLoading;
    private int visibleItemCount, pastVisibleItems, totalItemCount;

    public DetailWalletTransactionFragment() {
        // Required empty public constructor
    }

    public static DetailWalletTransactionFragment newInstance() {
        return new DetailWalletTransactionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycleview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurrentPage = 1;
        mMaxId = null;
        getTransactionPosts(mCurrentPage);
    }

    private void setupRecyclerView() {
        mRecyclerView.setEmptyView(emptyView);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new TransactionAdapter(getActivity(), new ArrayList<>(), new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {
                TransactionItem transactionItem = (TransactionItem) item;
                if (transactionItem.getProducts().size() > 1) {
                    TransactionItemHistoryDialog transactionItemHistoryDialog = TransactionItemHistoryDialog.newInstance();
                    transactionItemHistoryDialog.setPriceAirToken(transactionItem.getTotalToken());
                    transactionItemHistoryDialog.setPrice(transactionItem.getTotalPrice());
                    transactionItemHistoryDialog.setTotalQuantity(transactionItem.getTotalQuantity());
                    TransactionItemHistoryAdapter transactionItemHistoryAdapter = new TransactionItemHistoryAdapter(getActivity(), transactionItem.getProducts(), new BaseAdapter.OnItemClickListener() {
                        @Override
                        public <T> void onItemClicked(T item, int position) {

                        }
                    });
                    transactionItemHistoryDialog.setAdapter(transactionItemHistoryAdapter, new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                    transactionItemHistoryDialog.setOnDismissListener(() -> {
                        transactionItemHistoryDialog.dismiss();
                    });
                    transactionItemHistoryDialog.show(getFragmentManager(), "Show Transaction item History Dialog");
                }
            }
        });
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.white));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  if (dy > 0) {
                                                      visibleItemCount = mLinearLayoutManager.getChildCount();
                                                      totalItemCount = mLinearLayoutManager.getItemCount();
                                                      pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                                                      if (!isMoreLoading) {
                                                          if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                              mAdapter.addItem(null);
                                                              isMoreLoading = true;
                                                              getTransactionPosts(++mCurrentPage);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mCurrentPage = 1;
            mMaxId = null;
            getTransactionPosts(mCurrentPage);
        });
    }


    private void getTransactionPosts(int page) {
        GetListRequest modelRequest = new GetListRequest(SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, ""), null,
                page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, mMaxId, null, null, -1);
        requestApi(modelRequest, false, RequestTarget.GET_TRANSACTIONS, this);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && mAdapter.getItemCount() > 0) {
            mAdapter.removeItem(mAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_TRANSACTIONS:
                    hideLoadMore();
                    WalletRespone result = ((WalletRespone) response.getData());
                    if (result != null && mAdapter != null) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        mAdapter.addAllItems(result.getTransactionDetail());
                        mMaxId = result.getMaxId();
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
