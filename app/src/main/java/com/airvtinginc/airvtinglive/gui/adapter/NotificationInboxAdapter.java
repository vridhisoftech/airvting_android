package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.NotificationInboxViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageType;
import com.airvtinginc.airvtinglive.models.MessageContentModel;
import com.airvtinginc.airvtinglive.models.response.Message;
import com.airvtinginc.airvtinglive.models.response.Notification;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class NotificationInboxAdapter extends BaseAdapter {
    private static final String TAG = NotificationInboxAdapter.class.getSimpleName();
    private static final int VIEW_TYPE_INBOX = 1;
    private static final int VIEW_TYPE_LOAD_MORE = 2;

    public <T> NotificationInboxAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE:
                View loadMoreView = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                return new EmptyViewHolder(loadMoreView);
            default:
                View offerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification_inbox, parent, false);
                return new NotificationInboxViewHolder(offerView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder || list.get(position) == null) {
            return;
        }
        NotificationInboxViewHolder holder = (NotificationInboxViewHolder) mHolder;
        Notification notification = (Notification) list.get(position);
        User notifier = notification.getNotifier();
        Message message = notification.getMessage();

        if (notifier != null) {
            if (!TextUtils.isEmpty(notifier.getAvatar())) {
                holder.pbAvatarLoading.setVisibility(View.VISIBLE);
                holder.ivAvatar.setVisibility(View.VISIBLE);
                Glide.with(context).load(notifier.getAvatar())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, notifier, R.dimen.text_size_comment_default_avatar_text);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar);
                                return false;
                            }
                        })
                        .into(holder.ivAvatar);
            } else {
                holder.pbAvatarLoading.setVisibility(View.GONE);
                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDefaultAvatar, holder.ivAvatar, notifier, R.dimen.text_size_comment_default_avatar_text);
            }

            if (!TextUtils.isEmpty(notifier.getDisplayName())) {
                holder.tvDisplayName.setText(notifier.getDisplayName());
            }

            holder.ivAvatar.setOnClickListener(view -> openUserDetail(notifier));
        }

        if (message != null) {
            MessageContentModel messageContentModel = Utils.parseMessageContent(message.getContent());
            if (messageContentModel != null) {
                if (messageContentModel.getType().equals(MessageType.TEXT.getType())) {
//                    if (!TextUtils.isEmpty(messageContentModel.getMessage())) {
//                        holder.tvContent.setText(messageContentModel.getMessage());
//                    }
                    holder.tvContent.setText(context.getString(R.string.conversation_text_received_message));
                } else {
                    holder.tvContent.setText(context.getString(R.string.conversation_text_received_photo));
                }
            }
        }

        if (notification.isRead()) {
            holder.tvContent.setTypeface(holder.tvContent.getTypeface(), Typeface.NORMAL);
            holder.cvBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.tvContent.setTypeface(holder.tvContent.getTypeface(), Typeface.BOLD);
            holder.cvBackground.setBackgroundColor(context.getResources().getColor(R.color.color_cell_day));
        }

        if (!TextUtils.isEmpty(notification.getCreatedAt())) {
            try {
                String displayDate = DateTimeUtils.getElapsedInterval(context, DateTimeUtils.getDateFormatUTC(notification.getCreatedAt()));
                holder.tvCreateAt.setText(displayDate.toLowerCase());
            } catch (Exception e) {
                AppLog.e(e.getMessage());
            }
        }

        holder.mView.setOnClickListener(view -> {
            readNotification(notification.getId());
            openConversation(message);
        });
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_INBOX : VIEW_TYPE_LOAD_MORE;
    }

    private void readNotification(String notificationId) {
        Notification notification = new Notification();
        notification.setId(notificationId);
        requestApi(notification, false, RequestTarget.READ_NOTIFICATION, this);
    }

    private void openConversation(Message message) {
        if (message != null && !TextUtils.isEmpty(message.getMessageId())) {
            Intent intent = new Intent(context, DetailActivity.class);
            intent.putExtra(Constants.EXTRA_CONVERSATION_ID, message.getMessageId());
            DetailType.CONVERSATIONS_DETAIL.attachTo(intent);
            context.startActivity(intent);
        }
    }

    private void openUserDetail(User notifier) {
        if (notifier != null && !TextUtils.isEmpty(notifier.getUserId())) {
            Utils.openUserDetail(context, notifier.getUserId());
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case READ_NOTIFICATION:
                    Log.d(TAG, "onResponseSuccess: Read notification inbox");
                    break;
            }
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        switch (requestTarget) {
            case READ_NOTIFICATION:
                Log.d(TAG, "onResponseFail: Read notification inbox - failMessage: " + failMessage);
                break;
        }
    }
}
