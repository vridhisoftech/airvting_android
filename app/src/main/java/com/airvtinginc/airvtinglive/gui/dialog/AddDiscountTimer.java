package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CustomRangeInputFilter;
import com.airvtinginc.airvtinglive.gui.enums.AddDiscountTimerDialogType;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddDiscountTimer extends DialogFragment {
    private static final String TAG = AddDiscountTimer.class.getName();

    private final int MAX_VALUE_HOUR = 1;
    private final int MAX_VALUE_MINUTE = 59;

    @BindView(R.id.llTimer)
    LinearLayout llTimer;
    @BindView(R.id.edHour)
    EditText edHour;
    @BindView(R.id.edMinutes)
    EditText edMinutes;
    @BindView(R.id.llTimerCalendar)
    LinearLayout llTimerCalendar;
    @BindView(R.id.rlFrom)
    RelativeLayout rlFrom;
    @BindView(R.id.edFrom)
    EditText edFrom;
    @BindView(R.id.rlTo)
    RelativeLayout rlTo;
    @BindView(R.id.edTo)
    EditText edTo;

    private AddDiscountTimerDialogType addDiscountTimerDialogType;

    private Calendar calendarFrom;
    private Calendar calendarTo;

    @OnClick({R.id.llBackground, R.id.btnClose})
    void close() {
        dismiss();
    }

    @OnClick(R.id.btnOk)
    void okClick() {
        switch (addDiscountTimerDialogType) {
            case LIVE:
                int hour = 0, minutes = 0;
                if (edHour.getText() != null && !edHour.getText().toString().equals("")) {
                    hour = Integer.parseInt(edHour.getText().toString());
                }
                if (edMinutes.getText() != null && !edMinutes.getText().toString().equals("")) {
                    minutes = Integer.parseInt(edMinutes.getText().toString());
                }
                if (onOkListener != null) {
                    onOkListener.onOkClick(hour, minutes);
                }
                break;
            case PRODUCT:
                if (calendarTo.getTimeInMillis() < calendarFrom.getTimeInMillis()) {
                    DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.dialog_add_discount_timer_tile),
                            getString(R.string.err_start_day_after_end_day));
                } else {
                    if (onOkListener != null && !edFrom.getText().toString().equals("") && !edTo.getText().toString().equals("")) {
                        onOkListener.onOkClick(edFrom.getText().toString(), edTo.getText().toString());
                    }
                }
                break;
        }
    }

    @OnClick({R.id.rlFrom, R.id.edFrom})
    void fromClick() {
        showDatePickerDialog(edFrom, calendarFrom);
    }

    @OnClick({R.id.rlTo, R.id.edTo})
    void toClick() {
        showDatePickerDialog(edTo, calendarTo);
    }

    public AddDiscountTimerDialogType getAddDiscountTimerDialogType() {
        return addDiscountTimerDialogType;
    }

    public void setAddDiscountTimerDialogType(AddDiscountTimerDialogType addDiscountTimerDialogType) {
        this.addDiscountTimerDialogType = addDiscountTimerDialogType;
    }

    private OnOkListener onOkListener;

    public OnOkListener getOnOkListener() {
        return onOkListener;
    }

    public void setOnOkListener(OnOkListener onOkListener) {
        this.onOkListener = onOkListener;
    }

    public static AddDiscountTimer newInstance() {
        return new AddDiscountTimer();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_discount_timer, container, false);
        ButterKnife.bind(this, view);
        Utils.systemKeyboard(getDialog());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        setUpView();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void setUpView() {
        switch (addDiscountTimerDialogType) {
            case PRODUCT:
                setUpCalendar();
                break;
            case LIVE:
                setUpEditTextInput();
                break;
        }
    }

    private void setUpCalendar() {
        llTimer.setVisibility(View.INVISIBLE);
        llTimerCalendar.setVisibility(View.VISIBLE);
        edFrom.setFocusable(false);
        edTo.setFocusable(false);
        calendarFrom = Calendar.getInstance();
        calendarTo = Calendar.getInstance();
    }

    private void showDatePickerDialog(EditText editText, Calendar calendar) {
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                (view, year, monthOfYear, dayOfMonth) -> {
                    String day = String.valueOf(dayOfMonth);
                    String month = String.valueOf(monthOfYear + 1);

                    if (dayOfMonth < 10) {
                        day = "0" + String.valueOf(dayOfMonth);
                    }

                    if (monthOfYear + 1 < 10) {
                        month = "0" + String.valueOf(monthOfYear + 1);
                    }

                    String date = day + "/" + month + "/" + year;
                    editText.setText(date);
                    calendar.set(year, monthOfYear, dayOfMonth);
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(Calendar.getInstance());
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getActivity().getFragmentManager(), TAG);
    }

    private void setUpEditTextInput() {
        llTimer.setVisibility(View.VISIBLE);
        llTimerCalendar.setVisibility(View.GONE);
        edHour.setFilters(new InputFilter[]{new CustomRangeInputFilter(0, MAX_VALUE_HOUR)});
        edMinutes.setFilters(new InputFilter[]{new CustomRangeInputFilter(0, MAX_VALUE_MINUTE)});
    }

    public interface OnOkListener {
        void onOkClick(int hour, int minutes);

        void onOkClick(String from, String to);
    }
}
