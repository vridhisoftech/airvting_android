package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.fragments.ShareBottomSheetFragment;
import com.airvtinginc.airvtinglive.models.request.PostRequest;
import com.airvtinginc.airvtinglive.models.request.SaveStreamRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.AppPermission;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class SaveDiscardDialog extends BaseFragmentDialog {

    private static final String TAG = SaveDiscardDialog.class.getSimpleName();

    private String mPostId;
    private int mCommentCount;
    private int mLikeCount;
    private int mGiftCount;
    private int mCurrentCamera;
    private EndStreamListener mEndStreamListener;
    private AppPermission mAppPermission;
    private boolean shareFlag;
    @OnClick(R.id.llBackground)
    void close() {
        dismiss();
    }

    @OnClick(R.id.btnShare)
    void Share() {
        mAppPermission.checkPermissionGallery();
        shareFlag=true;
        saveStream(true);
    }


    @OnClick(R.id.btnDiscard)
    void discard() {
        saveStream(false);
    }

    @OnClick(R.id.btnSave)
    void save() {
        saveStream(true);
    }

    public static SaveDiscardDialog newInstance(String postId, int commentCount, int likeCount, int giftCount, int currentCamera) {
        SaveDiscardDialog saveDiscardDialog = new SaveDiscardDialog();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_POST_ID, postId);
        args.putInt(Constants.EXTRA_COMMENT_COUNT, commentCount);
        args.putInt(Constants.EXTRA_LIKE_COUNT, likeCount);
        args.putInt(Constants.EXTRA_GIFT_COUNT, giftCount);
        args.putInt(Constants.EXTRA_CURRENT_CAMERA, currentCamera);
        saveDiscardDialog.setArguments(args);
        return saveDiscardDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
        shareFlag=false;
        if (getArguments() != null) {
            mPostId = getArguments().getString(Constants.EXTRA_POST_ID);
            mCommentCount = getArguments().getInt(Constants.EXTRA_COMMENT_COUNT);
            mLikeCount = getArguments().getInt(Constants.EXTRA_LIKE_COUNT);
            mGiftCount = getArguments().getInt(Constants.EXTRA_GIFT_COUNT);
            mCurrentCamera = getArguments().getInt(Constants.EXTRA_CURRENT_CAMERA);
        }
        Log.v("ShareValue",""+shareFlag);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_save_discard_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        setUpPermission();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void setUpPermission() {
        mAppPermission = new AppPermission(this, (requestCode -> {
            switch (requestCode) {

                case AppPermission.REQUEST_CODE_GALLERY:
//                    shareStream();
                    break;
            }
        }), null);
    }


    private void shareStream() {
//        Intent in = new Intent(getContext(), ShareActivity.class);
//        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(in); 
//        getActivity().finish();
        ShareBottomSheetFragment shareBottomSheetFragment = new ShareBottomSheetFragment();
        shareBottomSheetFragment.show(getFragmentManager(),"Share To");
    }

    private void saveStream(boolean isSaveStream) {
        if (!TextUtils.isEmpty(mPostId)) {
            PostRequest postRequest = new PostRequest(mPostId);
            SaveStreamRequest saveStreamRequest = new SaveStreamRequest();
            saveStreamRequest.setSaveStream(isSaveStream);
            saveStreamRequest.setCommentCount(mCommentCount);
            saveStreamRequest.setLikeCount(mLikeCount);
            saveStreamRequest.setGiftCount(mGiftCount);
            saveStreamRequest.setCameraPosition(mCurrentCamera);
            postRequest.setSaveStreamRequest(saveStreamRequest);
            requestApi(postRequest, true, RequestTarget.END_STREAM, this);
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case END_STREAM:
                    Log.v("ShareValue",""+shareFlag);

                    if(shareFlag==true)
                    {
                        shareStream();
//                        dismiss();
                    }
                    else {
                        dismiss();
                        if (mEndStreamListener != null) {
                            mEndStreamListener.onEndStream();
                        }
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
    }

    public interface EndStreamListener {
        void onEndStream();
    }

    public void setEndStreamListener(EndStreamListener endStreamListener) {
        mEndStreamListener = endStreamListener;
    }
}
