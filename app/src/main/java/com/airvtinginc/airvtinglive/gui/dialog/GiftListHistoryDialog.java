package com.airvtinginc.airvtinglive.gui.dialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.GiftHistoryAdapter;
import com.airvtinginc.airvtinglive.gui.components.EmptyRecyclerView;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.GetListRequest;
import com.airvtinginc.airvtinglive.models.response.GiftList;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.WalletRespone;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GiftListHistoryDialog extends BaseFragmentDialog {
    private static final String TAG = GiftListHistoryDialog.class.getSimpleName();

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.mRecyclerView)
    EmptyRecyclerView mRecyclerView;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.btnClose)
    ImageButton btnClose;
    @BindView(R.id.ivIconTitle)
    ImageView ivIconTitle;
    @BindView(R.id.tvPriceAirToken)
    TextView tvPriceAirToken;
    @BindView(R.id.tvTotalGift)
    TextView tvTotalGift;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.pbIconTitle)
    ProgressBar pbIconTitle;

    private RecyclerView.Adapter<?> adapter;
    private GiftHistoryAdapter giftHistoryAdapter;
    private LinearLayoutManager layoutManager;
    private CharSequence title;
    private GiftList giftList;
    private OnDismissListener onDismissListener;
    private int mCurrentPage = 1;
    private String mMaxId;
    private boolean isMoreLoading;
    private int visibleItemCount, pastVisibleItems, totalItemCount;

    @OnClick(R.id.btnClose)
    void close() {
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        } else {
            dismiss();
        }
    }

    public static GiftListHistoryDialog newInstance() {
        return new GiftListHistoryDialog();
    }

    public void setOnDismisskListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    public OnDismissListener getOnDismissListener() {
        return onDismissListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_gift_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        updateUI();
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void updateUI() {
        setTitleUI();
        updateData();
        setRecyclerViewUI();
        getGiftsHistory(mCurrentPage);
    }

    private void setTitleUI() {
        if (title != null && !title.equals("")) {
            tvTitle.setText(title.toString().toUpperCase());
        }
    }

    private void setRecyclerViewUI() {
        if (layoutManager != null) {
            mRecyclerView.setLayoutManager(layoutManager);
        }
        giftHistoryAdapter = (GiftHistoryAdapter) adapter;
        if (adapter != null) {
            mRecyclerView.setEmptyView(emptyView);
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setBackgroundColor(getResources().getColor(R.color.white));

            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                                  @Override
                                                  public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                      super.onScrolled(recyclerView, dx, dy);
                                                      if (dy > 0) {
                                                          visibleItemCount = layoutManager.getChildCount();
                                                          totalItemCount = layoutManager.getItemCount();
                                                          pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                                                          if (!isMoreLoading) {
                                                              if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                                                  giftHistoryAdapter.addItem(null);
                                                                  isMoreLoading = true;
                                                                  getGiftsHistory(++mCurrentPage);
                                                              }
                                                          }
                                                      }
                                                  }
                                              }
            );
            mSwipeRefreshLayout.setOnRefreshListener(() -> {
                mCurrentPage = 1;
                mMaxId = null;
                getGiftsHistory(mCurrentPage);
            });
        }
    }

    private void updateData() {
        if (giftList == null) {
            return;
        }
        if (!TextUtils.isEmpty(giftList.getFeaturedImage())) {
            pbIconTitle.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(giftList.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbIconTitle.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbIconTitle.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(ivIconTitle);
        } else {
        }
        tvTitle.setText(giftList.getTitle().toUpperCase());
        tvPriceAirToken.setText(String.valueOf(giftList.getAirToken()));
        tvTotalGift.setText(String.valueOf(giftList.getQuantity()));
    }

    public void setAdapter(RecyclerView.Adapter<?> adapter, @Nullable RecyclerView.LayoutManager layoutManager) {
        if (layoutManager != null
                && !(layoutManager instanceof LinearLayoutManager)
                && !(layoutManager instanceof GridLayoutManager)) {
            throw new IllegalStateException(
                    "You can currently only use LinearLayoutManager"
                            + " and GridLayoutManager with this library.");
        }
        this.adapter = adapter;
        this.layoutManager = (LinearLayoutManager) layoutManager;
    }

    public void setTitle(Context context, @StringRes int titleRes) {
        setTitle(context.getText(titleRes));
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public GiftList getGiftList() {
        return giftList;
    }

    public void setGiftList(GiftList giftList) {
        this.giftList = giftList;
    }

    public interface OnDismissListener {
        void onDismiss();
    }

    private void getGiftsHistory(int page) {
        GetListRequest modelRequest = new GetListRequest(SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, ""), giftList.getGiftId(),
                page, ServiceConstants.NUMBER_OF_ITEMS_PER_LOADING, mMaxId, null, null, -1);
        requestApi(modelRequest, false, RequestTarget.GET_GIFT_HISTORY, this);
    }

    private void hideLoadMore() {
        DialogUtils.hideLoadingProgress();
        if (isMoreLoading && giftHistoryAdapter.getItemCount() > 0) {
            giftHistoryAdapter.removeItem(giftHistoryAdapter.getItemCount() - 1);
            isMoreLoading = false;
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case GET_GIFT_HISTORY:
                    hideLoadMore();
                    WalletRespone result = ((WalletRespone) response.getData());
                    if (result != null && giftHistoryAdapter != null) {
                        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                            giftHistoryAdapter.clearAllItems();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        giftHistoryAdapter.addAllItems(result.getGiftsHistoryDetail());
                        mMaxId = result.getMaxId();
                    }

                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            hideLoadMore();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        hideLoadMore();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}