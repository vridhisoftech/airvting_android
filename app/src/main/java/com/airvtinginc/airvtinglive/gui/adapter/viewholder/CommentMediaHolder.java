package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.lightfire.gradienttextcolor.GradientTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentMediaHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    public @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    public @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;
    public @BindView(R.id.tvNameUser)
    TextView tvNameUser;
    public @BindView(R.id.tvDate)
    TextView tvDate;
    public @BindView(R.id.tvComment)
    TextView tvComment;
    public @BindView(R.id.ivOptionComment)
    ImageView ivOptionComment;
    public @BindView(R.id.pbAvatarLoading)
    ProgressBar pbAvatar;

    public CommentMediaHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}