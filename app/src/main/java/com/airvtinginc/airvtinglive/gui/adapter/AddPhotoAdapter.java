package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ImageViewHolder;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


public class AddPhotoAdapter extends BaseAdapter {

    private OnActionClickListener onActionClickListener;

    public AddPhotoAdapter(Context context, OnItemClickListener onItemClickListener, OnActionClickListener onActionClickListener) {
        super(context, new ArrayList<Uri>(), onItemClickListener);
        list.add(Uri.parse(""));
        this.onActionClickListener = onActionClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof EmptyViewHolder) {
            return;
        }
        final ImageViewHolder holder = (ImageViewHolder) mHolder;
        if (position == list.size() - 1) {
            holder.ivImage.setVisibility(View.GONE);
            holder.llRemove.setVisibility(View.GONE);
            holder.ivAddProduct.setVisibility(View.VISIBLE);
            holder.ivAddProduct.setOnClickListener(v -> onActionClickListener.onAddClicked());
        } else {
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.llRemove.setVisibility(View.VISIBLE);
            holder.ivAddProduct.setVisibility(View.GONE);
            holder.llRemove.setOnClickListener(view -> removeItem(position));
            if (list.get(position) != null) {
                if (list.get(position).toString().contains("http")) {
                    Glide.with(context).load(list.get(position).toString()).into(holder.ivImage);
                } else {
                    final Uri image = (Uri) this.list.get(position);
                    Glide.with(context).load(image).into(holder.ivImage);
                    holder.mView.setOnClickListener(v -> onItemClickListener.onItemClicked(image, position));
                }
            }
        }
    }

    @Override
    public <T> void addItem(T item) {
        int index = this.list.size() - 1;
        this.list.add(index, item);
        notifyItemInserted(index);
        notifyItemChanged(index);
    }

    @Override
    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
        if (onItemClickListener != null) {
            onActionClickListener.onRemovePhoto(position);
        }
    }

    public List<String> getURLItems() {
        List<String> urlList = new ArrayList<>();
        for (int i = 0; i < this.list.size() - 1; i++) {
            if (list.get(i).toString().contains("http")) {
                urlList.add(list.get(i).toString());
            }
        }

        return urlList;
    }

    public List<Uri> getUriItems() {
        List<Uri> uriList = new ArrayList<>();
        for (int i = 0; i < this.list.size() - 1; i++) {
            if (!list.get(i).toString().contains("http")) {
                uriList.add((Uri) list.get(i));
            }
        }

        return uriList;
    }

    public interface OnActionClickListener {

        void onAddClicked();

        void onRemovePhoto(int position);
    }
}