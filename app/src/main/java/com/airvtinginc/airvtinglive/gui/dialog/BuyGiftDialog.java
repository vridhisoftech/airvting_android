package com.airvtinginc.airvtinglive.gui.dialog;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.services.FireStoreManager;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.components.MaterialRippleLayout;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MessageDialogType;
import com.airvtinginc.airvtinglive.models.GiftDetail;
import com.airvtinginc.airvtinglive.models.request.BuyGiftRequest;
import com.airvtinginc.airvtinglive.models.response.GiftInFireStore;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BuyGiftDialog extends BaseFragmentDialog {
    private static final String TAG = BuyGiftDialog.class.getSimpleName();

    @BindView(R.id.pb_buy_gift_image_loading)
    ProgressBar mPbGiftImageLoading;
    @BindView(R.id.iv_buy_gift_image)
    ImageView mIvGiftImage;
    @BindView(R.id.ll_buy_gift_quantity)
    LinearLayout mLlBuyGiftQuantity;
    @BindView(R.id.tv_buy_gift_decrease)
    ImageView mIvDecrease;
    @BindView(R.id.tv_buy_gift_increase)
    ImageView mIvIncrease;
    @BindView(R.id.tv_buy_gift_title)
    TextView mTvGiftTitle;
    @BindView(R.id.tv_buy_gift_currently_gifts)
    TextView mTvCurrentlyGifts;
    @BindView(R.id.tv_buy_gift_confirm_msg)
    TextView mTvBuyGiftConfirmMsg;
    @BindView(R.id.tv_buy_gift_currently_token)
    TextView mTvCurrentlyToken;
    @BindView(R.id.tv_buy_gift_quantity)
    TextView mTvQuantity;
    @BindView(R.id.tv_buy_gift_total)
    TextView mTvTotal;
    @BindView(R.id.tv_buy_gift_total_token)
    TextView mTvTotalToken;
    @BindView(R.id.mrl_buy_gift_buy_gift)
    MaterialRippleLayout mMrlSend;
    @BindView(R.id.tv_buy_gift_buy_button_title)
    TextView mTvBuyButtonTitle;

    private GiftDetail mGiftDetail;
    private int mSpentToken = -1;
    private int mBoughtGiftQuantity = 1;
    private OnBuyGiftListener mOnBuyGiftListener;
    private boolean mIsShowMenuGiftStore;
    private String mStreamerId;
    private String mPostId;
    private BuyGiftRequest mBuyGiftRequest;

    @OnClick(R.id.iv_buy_gift_close)
    void closeDialog() {
        dismiss();
    }

    @OnClick(R.id.mrl_buy_gift_buy_gift)
    void buyGift() {
        if (mIsShowMenuGiftStore) {
            mBoughtGiftQuantity = Integer.valueOf(mTvQuantity.getText().toString());
        }
        buyGift(mGiftDetail, mBoughtGiftQuantity);
    }

    public static BuyGiftDialog newInstance(boolean isShowMenuGiftStore, String streamerId, String postId) {
        BuyGiftDialog buyGiftDialog = new BuyGiftDialog();
        Bundle args = new Bundle();
        args.putBoolean(Constants.EXTRA_SHOW_MENU_GIFT_STORE, isShowMenuGiftStore);
        args.putString(Constants.EXTRA_USER_ID, streamerId);
        args.putString(Constants.EXTRA_POST_ID, postId);
        buyGiftDialog.setArguments(args);
        return buyGiftDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BORDER);
        if (getArguments() != null) {
            mIsShowMenuGiftStore = getArguments().getBoolean(Constants.EXTRA_SHOW_MENU_GIFT_STORE, false);
            mStreamerId = getArguments().getString(Constants.EXTRA_USER_ID);
            mPostId = getArguments().getString(Constants.EXTRA_POST_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_buy_gift, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        setUpView();
        showBuyInfo();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    public void setGiftDetail(GiftDetail giftDetail) {
        mGiftDetail = giftDetail;
    }

    private void setUpView() {
        if (mIsShowMenuGiftStore) {
            mTvBuyGiftConfirmMsg.setText(getString(R.string.dialog_buy_gift_confirm_msg));
            mTvBuyButtonTitle.setText(getString(R.string.dialog_buy_gift_action_buy));
        } else {
            mTvBuyGiftConfirmMsg.setText(getString(R.string.dialog_buy_and_send_gift_confirm_msg));
            mTvBuyButtonTitle.setText(getString(R.string.dialog_send_gift_action_send));
        }
    }

    private void showBuyInfo() {
        if (mGiftDetail == null) {
            return;
        }

        int currentGifts = mGiftDetail.getCountGiftsOfUser();
        String displayCurrentlyGift;
        if (currentGifts > 1) {
            displayCurrentlyGift = String.format(getContext().getString(R.string.dialog_currently_gifts), currentGifts);
        } else {
            displayCurrentlyGift = String.format(getContext().getString(R.string.dialog_currently_gift), currentGifts);
        }

        mTvCurrentlyGifts.setText(displayCurrentlyGift);
        mTvGiftTitle.setText(mGiftDetail.getTitle());
        mTvTotalToken.setText(String.valueOf(mGiftDetail.getAirToken()));

        if (!TextUtils.isEmpty(mGiftDetail.getFeaturedImage())) {
            mPbGiftImageLoading.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(mGiftDetail.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            mPbGiftImageLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            mPbGiftImageLoading.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(mIvGiftImage);
        }

        if (mIsShowMenuGiftStore) {
            mLlBuyGiftQuantity.setVisibility(View.VISIBLE);
            mTvTotal.setVisibility(View.VISIBLE);

            mIvDecrease.setOnClickListener(view -> {
                int quantity = Integer.valueOf(mTvQuantity.getText().toString());
                if (quantity > 1) {
                    quantity--;
                    int totalToken = mGiftDetail.getAirToken() * quantity;
                    mTvTotalToken.setText(String.valueOf(totalToken));
                    mTvQuantity.setText(String.valueOf(quantity));
                }
            });

            mIvIncrease.setOnClickListener(view -> {
                int quantity = Integer.valueOf(mTvQuantity.getText().toString());
                if (quantity < Constants.MAX_CART_QUANTITY) {
                    quantity++;
                    int totalToken = mGiftDetail.getAirToken() * quantity;
                    mTvTotalToken.setText(String.valueOf(totalToken));
                    mTvQuantity.setText(String.valueOf(quantity));
                }
            });
        } else {
            mLlBuyGiftQuantity.setVisibility(View.GONE);
            mTvTotal.setVisibility(View.GONE);
        }
    }

    private void buyGift(GiftDetail giftDetail, int quantityToBuy) {
        mBuyGiftRequest = new BuyGiftRequest();
        mBuyGiftRequest.setGiftId(giftDetail.getGiftId());
        mBuyGiftRequest.setTitle(giftDetail.getTitle());
        mBuyGiftRequest.setFeaturedImage(giftDetail.getFeaturedImage());
        mBuyGiftRequest.setAirToken(giftDetail.getAirToken());
        mBuyGiftRequest.setQuantityToBuy(quantityToBuy);
        mBuyGiftRequest.setStream(!mIsShowMenuGiftStore);
        if (!mIsShowMenuGiftStore && !TextUtils.isEmpty(mStreamerId) && !TextUtils.isEmpty(mPostId)) {
            mBuyGiftRequest.setReceiverId(mStreamerId);
            mBuyGiftRequest.setPostId(mPostId);
            mBuyGiftRequest.setQuantityToSend(1);
        }
        mSpentToken = giftDetail.getAirToken() * quantityToBuy;

        requestApi(mBuyGiftRequest, true, RequestTarget.BUY_GIFT, this);
    }

    private void showNotEnoughTokenDialog(String failMessage) {
        MessageDialog messageDialog = MessageDialog.newInstance();
        messageDialog.setDialogTitle(getString(R.string.dialog_buy_gift_title));
        messageDialog.setDialogMessage(failMessage);
        messageDialog.setDialogPositiveButtonTitle(getString(R.string.dialog_ok));
        messageDialog.setDialogType(MessageDialogType.NOT_ENOUGH_TOKEN_BUY_GIFT);
        messageDialog.show(getFragmentManager(), "Show Not enough token for buy gift dialog");
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case BUY_GIFT:
                    if (mOnBuyGiftListener != null) {
                        mOnBuyGiftListener.onUpdateToken(mSpentToken);
                        if (mIsShowMenuGiftStore) {
                            mOnBuyGiftListener.onUpdateQuantity(mBoughtGiftQuantity);
                        } else if (mBuyGiftRequest != null) {
                            GiftInFireStore giftInFireStore = new GiftInFireStore(mBuyGiftRequest.getReceiverId(),
                                    mBuyGiftRequest.getPostId(), mBuyGiftRequest.getFeaturedImage(), 1);
                            FireStoreManager.getInstance().addGift(getActivity(), giftInFireStore);
                        }
                    }
                    dismiss();
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        mSpentToken = 0;
        showNotEnoughTokenDialog(failMessage);
    }

    public interface OnBuyGiftListener {
        void onUpdateToken(int spentToken);

        void onUpdateQuantity(int boughtQuantity);
    }

    public void setOnBuyGiftListener(OnBuyGiftListener onBuyGiftListener) {
        mOnBuyGiftListener = onBuyGiftListener;
    }
}
