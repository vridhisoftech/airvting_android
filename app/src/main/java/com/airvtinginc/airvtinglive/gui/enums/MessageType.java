package com.airvtinginc.airvtinglive.gui.enums;

public enum MessageType {
    TEXT("text"),
    IMAGE("image");

    private String type;

    MessageType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
