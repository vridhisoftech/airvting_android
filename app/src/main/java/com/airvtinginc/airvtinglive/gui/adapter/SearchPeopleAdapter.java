package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lightfire.gradienttextcolor.GradientTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchPeopleAdapter extends BaseAdapter {

    public SearchPeopleAdapter(Context context, List<User> userList, OnItemClickListener onItemClickListener) {
        super(context, userList, onItemClickListener);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_people, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        ViewHolder holder = (ViewHolder) mHolder;
        if (list.size() > 0) {
            User user = (User) list.get(position);
            if (!TextUtils.isEmpty(user.getUsername())) {
                holder.tvName.setText(user.getUsername());
            }

            if (user.isLive()) {
                holder.ivIsLive.setVisibility(View.VISIBLE);
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500);
                anim.setStartOffset(0);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                holder.ivIsLive.startAnimation(anim);

                holder.ivAvatar.setBorderStartColor(context.getResources().getColor(R.color.avatar_border_live_violet));
                holder.ivAvatar.setBorderEndColor(context.getResources().getColor(R.color.avatar_border_live_blue));
            } else {
                holder.ivIsLive.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(user.getAvatar())) {
                    holder.ivAvatar.setBorderStartColor(context.getResources().getColor(android.R.color.transparent));
                    holder.ivAvatar.setBorderEndColor(context.getResources().getColor(android.R.color.transparent));
                } else {
                    holder.ivAvatar.setBorderStartColor(context.getResources().getColor(R.color.md_grey_400));
                    holder.ivAvatar.setBorderEndColor(context.getResources().getColor(R.color.md_grey_400));
                }
            }

            if (!TextUtils.isEmpty(user.getAvatar())) {
                holder.pbAvatarLoading.setVisibility(View.VISIBLE);
                holder.ivAvatar.setVisibility(View.VISIBLE);
                Glide.with(context).load(user.getAvatar())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDisplayName, holder.ivAvatar, user, R.dimen.profile_default_avatar_text_size);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.pbAvatarLoading.setVisibility(View.GONE);
                                Utils.setHideDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDisplayName, holder.ivAvatar);
                                return false;
                            }
                        })
                        .into(holder.ivAvatar);
            } else {
                holder.pbAvatarLoading.setVisibility(View.GONE);
                Utils.setDefaultAvatar(context, holder.ivDefaultAvatar, holder.tvDisplayName, holder.ivAvatar, user, R.dimen.profile_default_avatar_text_size);
            }

            holder.llBound.setOnClickListener(view -> onItemClickListener.onItemClicked(user, position));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_search_people_ll_bound)
        LinearLayout llBound;
        @BindView(R.id.ivAvatar)
        CircleImageView ivAvatar;
        @BindView(R.id.item_search_people_iv_is_live)
        ImageView ivIsLive;
        @BindView(R.id.pbAvatarLoading)
        ProgressBar pbAvatarLoading;
        @BindView(R.id.item_search_people_tv_name)
        TextView tvName;
        @BindView(R.id.ivDefaultAvatar)
        CircleImageView ivDefaultAvatar;
        @BindView(R.id.tvDefaultAvatar)
        GradientTextView tvDisplayName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
