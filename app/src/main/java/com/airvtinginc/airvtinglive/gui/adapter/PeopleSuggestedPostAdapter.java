package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.PeopleSuggestedPhotoViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class PeopleSuggestedPostAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;

    public PeopleSuggestedPostAdapter(Context context, List<PostDetail> postDetailList, BaseAdapter.OnItemClickListener onItemClickListener) {
        super(context, postDetailList, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder vh;
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_horizontal_layout, parent, false);
                vh = new EmptyViewHolder(view);
                break;
            }
            case VIEW_TYPE_ITEM: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horizontal_people_suggested, parent, false);
                vh = new PeopleSuggestedPhotoViewHolder(view);
                break;
            }
            default: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                vh = new EmptyViewHolder(view);
                break;
            }
        }
        int marginStartEnd = (int) parent.getContext().getResources().getDimension(R.dimen.people_margin);
        int photoMargin = (int) parent.getContext().getResources().getDimension(R.dimen.people_suggested_photo_margin);
        view.getLayoutParams().width = (Utils.getScreenWidth(parent.getContext()) - 2 * marginStartEnd - 8 * photoMargin) / Constants.NUMBER_OF_USERS_DISPLAY_IN_PEOPLE_GROUP;
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        if (mHolder instanceof EmptyViewHolder) return;
        if (list == null || list.isEmpty()) return;
        PeopleSuggestedPhotoViewHolder holder = (PeopleSuggestedPhotoViewHolder) mHolder;
        PostDetail postDetail = (PostDetail) list.get(position);

        if (!TextUtils.isEmpty(postDetail.getFeaturedImage())) {
            holder.pbMedia.setVisibility(View.VISIBLE);
            Glide.with(context).load(postDetail.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.pbMedia.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.pbMedia.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivPhoto);

            holder.ivPhoto.setBorderColor(context.getResources().getColor(android.R.color.transparent));
            holder.ivPhoto.setBorderWidth(0f);
            holder.tvDefaultAvatar.setText("");
            holder.tvDefaultAvatar.setVisibility(View.GONE);
        } else {
            holder.ivPhoto.setImageResource(R.drawable.ic_white);
            holder.ivPhoto.setBorderColor(context.getResources().getColor(R.color.md_grey_200));
            holder.ivPhoto.setBorderWidth(1.5f);
            holder.tvDefaultAvatar.setText(Utils.getDefaultAvatarText(postDetail.getUser()));
            holder.tvDefaultAvatar.setVisibility(View.VISIBLE);
        }

        holder.mView.setOnClickListener(v -> openMedia(postDetail));

        if (postDetail.getType().equals(MediaType.IMAGE.getType())) {
            holder.ivPostVideo.setVisibility(View.GONE);
            holder.ivLive.setVisibility(View.GONE);
        } else if (postDetail.getType().equals(MediaType.VIDEO.getType())) {
            holder.ivPostVideo.setVisibility(View.VISIBLE);
            holder.ivLive.setVisibility(View.GONE);
        } else if (postDetail.getType().equals(MediaType.STREAM.getType())) {
            if (postDetail.isLive()) {
                holder.ivLive.setImageResource(R.drawable.ic_live);
            } else {
                holder.ivLive.setImageResource(R.drawable.ic_rec);
            }
            holder.ivPostVideo.setVisibility(View.GONE);
            holder.ivLive.setVisibility(View.VISIBLE);

            holder.mView.setOnClickListener(v -> openStream(postDetail));
        }

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOAD_MORE;
    }

    private void openMedia(PostDetail media) {
        Intent intent = new Intent(context, DetailActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_GALLERY_ID, media.getId());
        intent.putExtras(args);
        // Sender usage
        DetailType.GALLERY.attachTo(intent);
        context.startActivity(intent);
    }

    private void openStream(PostDetail postDetail) {
        Intent intent = new Intent(context, LiveStreamActivity.class);
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, postDetail.getUser().getUserId());
        args.putString(Constants.EXTRA_POST_ID, postDetail.getId());
        args.putString(Constants.EXTRA_MEDIA_URL, postDetail.getMediaUrl());
        intent.putExtras(args);
        context.startActivity(intent);
    }
}
