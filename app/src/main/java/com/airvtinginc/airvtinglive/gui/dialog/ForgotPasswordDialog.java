package com.airvtinginc.airvtinglive.gui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.LoginModelRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Validator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordDialog extends BaseFragmentDialog {
    private static final String TAG = ForgotPasswordDialog.class.getSimpleName();

    @BindView(R.id.edt_dialog_forgot_pw_email_input)
    EditText mEdtEmail;

    @OnClick(R.id.iv_dialog_forgot_password_close)
    void close() {
        dismiss();
    }

    @OnClick(R.id.mrl_dialog_forgot_password_submit_button)
    void submitForgotPassword() {
        if (isValidEmail()) {
            String email = mEdtEmail.getText().toString();
            LoginModelRequest modelRequest = new LoginModelRequest();
            modelRequest.setEmail(email);
            requestApi(modelRequest, true, RequestTarget.FORGOT_PASSWORD, this);
        }
    }

    public static ForgotPasswordDialog newInstance() {
        return new ForgotPasswordDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_BORDER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_forgot_password, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private boolean isValidEmail() {
        if (TextUtils.isEmpty(mEdtEmail.getText())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_forgot_password),
                    getString(R.string.validator_error_email_required));
            return false;
        }

        if (!Validator.getInstance().isEmailValid(mEdtEmail.getText().toString())) {
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_forgot_password),
                    getString(R.string.validator_error_invalid_email));
            return false;
        }

        return true;
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        super.onResponseSuccess(response, requestTarget);
        if (response.isSuccess()) {
            switch (requestTarget) {
                case FORGOT_PASSWORD:
                    close();
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
            DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_forgot_password), response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        super.onResponseFail(failMessage, statusCode, requestTarget);
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(), getString(R.string.sign_in_forgot_password), failMessage);
    }
}
