package com.airvtinginc.airvtinglive.gui.enums;

public class EnumManager {

    public enum StatusCode {
        ERR_NO_INTERNET_CONNECTION,
        ERR_UNAUTHORIZED,
        ERR_API_CALL_FAIL,
        TIMEOUT_CONNECTION
    }

    public enum MenuType {
        Home,
        Explore,
        Add,
        Cart,
        User
    }

    public enum FilterChangeType {
        Crop, Contrast
    }

    public enum FilterType {
        Edit, Filter
    }

    public enum ChooseVideoType {
        Camera, Video
    }

    public enum MessageType {
        FromFriend(0),
        FromMe(1);

        private int value;

        MessageType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
