package com.airvtinginc.airvtinglive.gui.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.activities.PhotoFilterActivity;
import com.airvtinginc.airvtinglive.gui.components.photofilter.FileProvider;
import com.airvtinginc.airvtinglive.tools.AppPermission;
import com.airvtinginc.airvtinglive.tools.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class AddPhotoDialog extends DialogFragment {

    @BindView(R.id.dialog_add_photo_pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.dialog_add_photo_iv_take_photo)
    ImageView ivTakePhoto;
    @BindView(R.id.dialog_add_photo_iv_gallery)
    ImageView ivGallery;

    private static final int REQUEST_TAKE_PHOTO = 111;
    private static final int REQUEST_CHOOSE_FROM_GALLERY = 222;
    private static final int REQUEST_EDIT_FILTER = 12345;

    private File file;
    private ReturnImageListener mReturnImageListener;
    private Uri contentSavedUri;
    private boolean haveFilter;
    private AppPermission mAppPermission;

    @OnClick(R.id.dialog_add_photo_ll_background)
    void close() {
        if (pbLoading.getVisibility() == View.GONE) {
            dismiss();
        }
    }

    @OnClick(R.id.dialog_add_photo_iv_take_photo)
    void takePhotoClick() {
        mAppPermission.checkPermissionCamera();
    }

    @OnClick(R.id.dialog_add_photo_iv_gallery)
    void getFromGallery() {
        mAppPermission.checkPermissionGallery();
    }

    public static AddPhotoDialog newInstance() {
        return new AddPhotoDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_photo, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAllParentsClip(view, false);
        setUpPermission();
    }

    private void setUpPermission() {
        mAppPermission = new AppPermission(this, (requestCode -> {
            switch (requestCode) {
                case AppPermission.REQUEST_CODE_CAMERA:
                    takeAPhoto();
                    break;
                case AppPermission.REQUEST_CODE_GALLERY:
                    selectFromGallery();
                    break;
            }
        }), null);
    }

    public static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO:
                    String fileName = System.currentTimeMillis() + ".jpg";
                    File out = new File(Utils.getApplicationFolder(), "AirVTingCapture.jpg");
                    if (!out.exists()) {
                        Toast.makeText(getActivity(), "Error while capturing image", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    loadingBar(true);
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());
                            ExifInterface ei;
                            Bitmap rotatedBitmap;
                            try {
                                ei = new ExifInterface(out.getAbsolutePath());
                                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                                switch (orientation) {

                                    case ExifInterface.ORIENTATION_ROTATE_90:
                                        rotatedBitmap = rotateImage(mBitmap, 90);
                                        break;

                                    case ExifInterface.ORIENTATION_ROTATE_180:
                                        rotatedBitmap = rotateImage(mBitmap, 180);
                                        break;

                                    case ExifInterface.ORIENTATION_ROTATE_270:
                                        rotatedBitmap = rotateImage(mBitmap, 270);
                                        break;

                                    case ExifInterface.ORIENTATION_NORMAL:
                                    default:
                                        rotatedBitmap = mBitmap;
                                }

                                Bitmap finalRotatedBitmap = rotatedBitmap;

                                saveBitmap(finalRotatedBitmap, fileName);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            Uri uri = Uri.fromFile(new File(Utils.getApplicationFolder(), fileName));
                            loadingBar(false);
                            if (haveFilter) {
                                Intent intent = new Intent(getActivity(), PhotoFilterActivity.class);
                                intent.setData(uri);
                                startActivityForResult(intent, REQUEST_EDIT_FILTER);
                            } else {
                                mReturnImageListener.onReturnImage(uri);
                            }
                        }
                    }.execute();
                    break;
                case REQUEST_CHOOSE_FROM_GALLERY:
                    if (haveFilter) {
                        Intent intent = new Intent(getActivity(), PhotoFilterActivity.class);
                        intent.setData(data.getData());
                        startActivityForResult(intent, REQUEST_EDIT_FILTER);
                    } else {
                        if (mReturnImageListener != null) {
                            mReturnImageListener.onReturnImage(data.getData());
                        }
                    }

                    break;

                case REQUEST_EDIT_FILTER:
                    if (mReturnImageListener != null) {
                        mReturnImageListener.onReturnImage(data.getData());
                    }
                    break;
            }
        }
    }

    private void saveBitmap(Bitmap bmp, String fileName) {
        try {
            String folderPath = Utils.getApplicationFolder().toString();
            File folder = new File(folderPath);
            if (!folder.exists()) {
                File wallpaperDirectory = new File(folderPath);
                wallpaperDirectory.mkdirs();
            }

            file = new File(folderPath, fileName);
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, PhotoFilterActivity.QUALITY_SAVE_IMAGE, fos);
            scanSdCard();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //We need to scan sd card to update and display new photo on device Gallery
    private void scanSdCard() {
        String folderPath = Utils.getApplicationFolder().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            contentSavedUri = Uri.fromFile(file);
            mediaScanIntent.setData(contentSavedUri);
            getActivity().sendBroadcast(mediaScanIntent);
        } else {
            getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + folderPath)));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mAppPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @SuppressLint("StaticFieldLeak")
    private void takeAPhoto() {
        new AsyncTask<Void, Void, Void>() {
            File mFile;
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    mFile = new File(Utils.getApplicationFolder(), "AirVTingCapture.jpg");
                    if (!mFile.exists()) {
                        mFile.createNewFile();
                    } else {
                        mFile.delete();
                        mFile = new File(Utils.getApplicationFolder(), "AirVTingCapture.jpg");
                        mFile.createNewFile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Uri photoUri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    photoUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), "com.airvtinginc.airvtinglive.provider", mFile);
                } else {
                    photoUri = Uri.fromFile(mFile);
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            }
        }.execute();
    }

    private void selectFromGallery() {
        Intent pickPhoto = new Intent();
        pickPhoto.setType("image/*");
        pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(pickPhoto, getResources().getString(R.string.select_photo)), REQUEST_CHOOSE_FROM_GALLERY);
    }

    public interface ReturnImageListener {
        void onReturnImage(Uri uri);
    }

    public void setReturnImageListener(ReturnImageListener returnImageListener) {
        this.mReturnImageListener = returnImageListener;
    }

    public void setHaveFilter(boolean haveFilter) {
        this.haveFilter = haveFilter;
    }

    private void loadingBar(boolean isLoading) {
        if (isLoading) {
            pbLoading.setVisibility(View.VISIBLE);
            ivGallery.setVisibility(View.GONE);
            ivTakePhoto.setVisibility(View.GONE);
        } else {
            pbLoading.setVisibility(View.GONE);
            ivGallery.setVisibility(View.VISIBLE);
            ivTakePhoto.setVisibility(View.VISIBLE);
        }
    }
}
