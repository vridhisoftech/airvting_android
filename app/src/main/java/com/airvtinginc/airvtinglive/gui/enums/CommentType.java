package com.airvtinginc.airvtinglive.gui.enums;

public enum CommentType {
    TEXT("text"),
    GIFT("gift");

    private String type;

    CommentType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
