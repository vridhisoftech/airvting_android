package com.airvtinginc.airvtinglive.gui.enums;

public enum DisplayMode {
    ORIGINAL,       // original aspect ratio
    FULL_SCREEN,    // fit to screen
    ZOOM            // zoom in
}
