package com.airvtinginc.airvtinglive.gui.activities;

import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.red5.BasePublishTestFragment;
import com.airvtinginc.airvtinglive.components.red5.DoneLiveFragment;
import com.airvtinginc.airvtinglive.components.red5.PublishRecordedTest.PublishRecordedTestFragment;
import com.airvtinginc.airvtinglive.components.red5.Red5PropertiesContent;
import com.airvtinginc.airvtinglive.components.red5.SubscribeTest.SubscribeTestFragment;
import com.airvtinginc.airvtinglive.components.red5.VodStream.VodTestFragment;
import com.airvtinginc.airvtinglive.components.services.FireStoreManager;
import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.adapter.BaseAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.ChoseTextAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.LiveCommentsAdapter;
import com.airvtinginc.airvtinglive.gui.adapter.LiveViewersAdapter;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;
import com.airvtinginc.airvtinglive.gui.dialog.AddLiveDialog;
import com.airvtinginc.airvtinglive.gui.dialog.AddToCartDialog;
import com.airvtinginc.airvtinglive.gui.dialog.BottomGiftDialog;
import com.airvtinginc.airvtinglive.gui.dialog.BottomStoreDialog;
import com.airvtinginc.airvtinglive.gui.dialog.ListDialog;
import com.airvtinginc.airvtinglive.gui.enums.CommentType;
import com.airvtinginc.airvtinglive.gui.enums.LiveStreamType;
import com.airvtinginc.airvtinglive.gui.enums.ShowDialogType;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.request.FollowUserRequest;
import com.airvtinginc.airvtinglive.models.request.PostRequest;
import com.airvtinginc.airvtinglive.models.request.ReportRequest;
import com.airvtinginc.airvtinglive.models.response.Comment;
import com.airvtinginc.airvtinglive.models.response.PostDetail;
import com.airvtinginc.airvtinglive.models.response.Viewer;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.airvtinginc.airvtinglive.tools.AppPermission;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.lightfire.gradienttextcolor.GradientTextView;
import com.makeramen.roundedimageview.RoundedImageView;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LiveStreamActivity extends ViewLiveActivity {
    private static final String TAG = LiveStreamActivity.class.getSimpleName();

    @BindView(R.id.rlLive)
    RelativeLayout rlLive;
    @BindView(R.id.rvViewers)
    RecyclerView rvViewers;
    @BindView(R.id.tvCountDownTimer)
    TextView tvCountDownTimer;
    @BindView(R.id.tvSaleQuantity)
    TextView tvSaleQuantity;
    @BindView(R.id.rvComments)
    RecyclerView rvComments;
    @BindView(R.id.etComment)
    EditText etComment;
    @BindView(R.id.pbProductImage)
    ProgressBar pbProductImage;
    @BindView(R.id.ivProductCountDown)
    RoundedImageView ivProductCountDown;
    @BindView(R.id.rlDeal)
    RelativeLayout rlDeal;
    @BindView(R.id.ivPriceTag)
    ImageView ivPriceTag;
    @BindView(R.id.llLiveProfile)
    LinearLayout llLiveProfile;
    @BindView(R.id.rl_bottom_store)
    RelativeLayout rlBottomStore;
    @BindView(R.id.tv_product_items)
    TextView tvProductItems;
    @BindView(R.id.tvViewerCount)
    TextView tvViewerCount;
    @BindView(R.id.ivAvatar)
    ImageView ivProfile;
    @BindView(R.id.pbLoadAvatar)
    ProgressBar pbLoadAvatar;
    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.iBtnFollow)
    ImageButton iBtnFollow;
    @BindView(R.id.tvNumberFollower)
    TextView tvNumberFollower;
    @BindView(R.id.ivDefaultAvatar)
    CircleImageView ivDefaultAvatar;
    @BindView(R.id.tvDefaultAvatar)
    GradientTextView tvDefaultAvatar;
    @BindView(R.id.act_live_stream_ib_like)
    AppCompatImageButton ibLike;
    @BindView(R.id.iBtn_bottom_gift)
    AppCompatImageButton iBtnBottomGift;
    @BindView(R.id.tv_gift_quantity)
    TextView tvGiftQuantity;
    @BindView(R.id.ivSend)
    ImageView ivSend;
    @BindView(R.id.tvCountLike)
    TextView tvCountLike;
    @BindView(R.id.rlBottomGift)
    RelativeLayout rlBottomGift;
    @BindView(R.id.tvSwitchCamera)
    TextView tvSwitchCamera;
    @BindView(R.id.rlDataLiveSave)
    RelativeLayout rlDataLiveSave;
    @BindView(R.id.tvDateRec)
    TextView tvDateRec;


    private LinearLayoutManager llm;

    private ProductInPost product;
    private CountDownTimer timer;
    private LiveViewersAdapter viewersAdapter;

    private boolean isRefreshProduct;
    private String mCurrentUserId;

    @OnClick(R.id.btnClose)
    void close() {

        SharedPreferencesManager.getInstance(this).putString("title","");
        onBackPressed();
    }

    @OnClick(R.id.iBtnFollow)
    void followStreamer() {
        if (!TextUtils.isEmpty(streamerId)) {
            FollowUserRequest followUserRequest = new FollowUserRequest(streamerId);
            requestApi(followUserRequest, true, RequestTarget.FOLLOW_USER, this);
        }
    }

    @OnClick(R.id.rl_bottom_store)
    void showUserBottomStoreDialog() {
        BottomStoreDialog bottomStoreDialog = BottomStoreDialog.newInstance(streamerId, postId, product);
        bottomStoreDialog.show(getSupportFragmentManager(), "Show Bottom Store Dialog");
    }

    @OnClick(R.id.iBtn_bottom_gift)
    void showBottomGiftDialog() {
        BottomGiftDialog bottomGiftDialog = BottomGiftDialog.newInstance(streamerId, postId);
        bottomGiftDialog.show(getSupportFragmentManager(), "Show Bottom Gift Dialog");
    }

    @OnClick(R.id.rlDeal)
    void onDealProductClick() {
        if (liveStreamType == LiveStreamType.LIVE) {
            changeProduct();
        } else if (!streamerId.equals(mCurrentUserId)) {
            showAddToCart();
//            if (!tvSaleQuantity.getText().equals("0")) {
//            } else {
//                DialogUtils.showMessageDialog(this, getSupportFragmentManager(),
//                        getString(R.string.dialog_add_to_cart_title),
//                        getString(R.string.dialog_add_to_cart_out_of_stock));
//            }
        }
    }

    @OnClick(R.id.act_live_stream_ib_like)
    void onClickLike() {
        commentsAdapter.notifyDataSetChanged();
        if (!TextUtils.isEmpty(postId)) {
            ibLike.setEnabled(false);
            requestApi(postId, false, RequestTarget.LIKE_POST, this);
        }
    }

    @OnClick(R.id.ivSend)
    void sendComment() {
        // Perform action on key press
        if (!etComment.getText().toString().isEmpty()) {
            Utils.hideSoftKeyboard(this);
            FireStoreManager.getInstance().addComment(getApplicationContext(), postId, etComment.getText().toString(), CommentType.TEXT.getType());
            etComment.setText("");
        }
    }

    @OnClick({R.id.tvReport, R.id.rlReport})
    void report() {
        rlLive.setVisibility(View.GONE);
        ListDialog report = ListDialog.newInstance();
        report.setShowDialogType(ShowDialogType.REPORT);
        report.setTitle(getApplicationContext(), R.string.dialog_report_title);
        report.setBackgroundColorRes(getApplicationContext(), R.color.transparent_70_black);
        report.setBackgroundSendButton(R.drawable.bg_edittext_white);
        report.setSendButtonColorRes(getApplicationContext(), R.color.black);
        report.setTitleColorRes(getApplicationContext(), R.color.white);
        ChoseTextAdapter choseTextAdapter = new ChoseTextAdapter(getApplicationContext(), new ArrayList<>(), null);
        choseTextAdapter.setTextColorRes(getApplicationContext(), R.color.white);
        choseTextAdapter.setBackgroundColorRes(getApplicationContext(), android.R.color.transparent);
        choseTextAdapter.setBackgroundCheck(R.drawable.box_check_while);
        report.setAdapter(choseTextAdapter, new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        report.setOnSendListener(content -> {
            Map<String, RequestBody> bodyMap = new HashMap<>();
            bodyMap.put("postId", RequestBody.create(MediaType.parse("application/json"), postId));
            bodyMap.put("content", RequestBody.create(MediaType.parse("application/json"), content));
            ReportRequest reportRequest = new ReportRequest(bodyMap);
            requestApi(reportRequest, true, RequestTarget.REPORT, this);
        });
        report.setOnDismissListener(() -> {
            report.dismiss();
            rlLive.setVisibility(View.VISIBLE);
        });
        report.show(getSupportFragmentManager(), "Add Report Dialog");
    }

    @OnClick(R.id.tvSwitchCamera)
    void changeCamera() {

        if (mCurrentCamera == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mCurrentCamera = Camera.CameraInfo.CAMERA_FACING_BACK;
        } else if (mCurrentCamera == Camera.CameraInfo.CAMERA_FACING_BACK) {
            mCurrentCamera = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }

        if (fragment != null && fragment instanceof PublishRecordedTestFragment) {
            ((PublishRecordedTestFragment) fragment).setCameraFacing(mCurrentCamera);
        }
    }

    @OnClick(R.id.tvHideComment)
    void hideComment() {
        if (rvComments.getVisibility() == View.GONE) {
            rvComments.setVisibility(View.VISIBLE);
        } else {
            rvComments.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (liveStreamType != null && liveStreamType == LiveStreamType.VIEWER) {
            setUpUserViewLive();
            setUpPermission();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_stream);
        ButterKnife.bind(this);
        mCurrentCamera = Camera.CameraInfo.CAMERA_FACING_FRONT;

        Utils.systemKeyboard(this);

        mCurrentUserId = SharedPreferencesManager.getInstance(this).getString(Constants.PREF_CURRENT_USER_ID, "");

        if (getIntent().getExtras() != null) {
            streamerId = getIntent().getExtras().getString(Constants.EXTRA_USER_ID);
            postId = getIntent().getExtras().getString(Constants.EXTRA_POST_ID);
            mediaUrl = getIntent().getExtras().getString(Constants.EXTRA_MEDIA_URL);
        }

        //Load XML TESTS
        Red5PropertiesContent.LoadTests(getResources().openRawResource(R.raw.tests));

        if (TextUtils.isEmpty(streamerId) || TextUtils.isEmpty(postId)) {
            liveStreamType = LiveStreamType.LIVE;
            setUpLive();
        } else {
            setUpUserViewLive();
        }
        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(this, isOpen -> {
            if (isOpen) {
                rlBottomStore.setVisibility(View.GONE);
                rlBottomGift.setVisibility(View.GONE);
                ibLike.setVisibility(View.GONE);
                ivSend.setVisibility(View.VISIBLE);
            } else {
                rlBottomStore.setVisibility((liveStreamType == LiveStreamType.LIVE) ? View.GONE : View.VISIBLE);
                rlBottomGift.setVisibility(View.VISIBLE);
                ibLike.setVisibility(View.VISIBLE);
                ivSend.setVisibility(View.GONE);
            }
        });

        onActionLive = new OnActionLive() {
            @Override
            public void onSetUpViewLive(PostDetail postDetail) {
                setUpViewLive(postDetail);
            }

            @Override
            public void onSetLike(PostDetail postDetail) {
                if (postDetail.getLike()) {
                    FireStoreManager.getInstance().addComment(getApplicationContext(), postId, getString(R.string.live_liked_post), CommentType.TEXT.getType());
                }
                setLike(postDetail);
                if (!TextUtils.isEmpty(viewerId)) {
                    FireStoreManager.getInstance().setIsLike(viewerId, postDetail.getLike());
                } else {
                    Log.d(TAG, "getIdView in LiveStream");
                    getIdView(false);
                }
            }

            @Override
            public void onSetFollow(boolean isFollow) {
                setFollow(isFollow);
                updateFollowers(isFollow);
            }

            @Override
            public void onUpdateTotalGift() {
                Log.d(TAG, "total gift: " + gifts.size());
                if (gifts.size() > 0) {
                    tvGiftQuantity.setText(String.valueOf(gifts.size()));
                    tvGiftQuantity.setVisibility(View.VISIBLE);
                }
            }
        };

        setUpPermission();

    }

    private void setUpLive() {
        setUpAddLiveInfo();
        llLiveProfile.setVisibility(View.GONE);
        rlBottomStore.setVisibility(View.GONE);
        tvSwitchCamera.setVisibility(View.VISIBLE);
        iBtnBottomGift.setEnabled(false);
        iBtnBottomGift.setColorFilter(getResources().getColor(R.color.md_grey_600));
    }

    private void setUpUserViewLive() {
        getPostDetail();
        tvGiftQuantity.setVisibility(View.GONE);
        tvSwitchCamera.setVisibility(View.GONE);
        if (!streamerId.equals(mCurrentUserId)) {
            llLiveProfile.setVisibility(View.VISIBLE);
            rlBottomStore.setVisibility(View.VISIBLE);
            iBtnBottomGift.setEnabled(true);
            iBtnBottomGift.setColorFilter(getResources().getColor(R.color.white));
        } else {
            llLiveProfile.setVisibility(View.GONE);
            rlBottomStore.setVisibility(View.GONE);
            iBtnBottomGift.setEnabled(false);
            iBtnBottomGift.setColorFilter(getResources().getColor(R.color.md_grey_600));
        }
    }

    public void getPostDetail() {
        if (!TextUtils.isEmpty(postId)) {
            PostRequest requestModel = new PostRequest(postId);

            requestApi(requestModel, true, RequestTarget.GET_POST_DETAIL, this);
        }
    }

    private void setUpPermission() {
        mAppPermission = new AppPermission(this, (requestCode -> {
            switch (requestCode) {
                case AppPermission.REQUEST_CODE_CAMERA:
                    preview();
                    break;
            }
        }), null);
        mAppPermission.checkPermissionCamera();
    }

    private void preview() {
        if (fragment != null && fragment.isPublisherTest()) {
            BasePublishTestFragment pFragment = (BasePublishTestFragment) fragment;
            pFragment.stopPublish(this);
        }
        fragment = null;

        if (liveStreamType != null) {
            switch (liveStreamType) {
                case LIVE:
                    //TODO: update button change camera.
                    fragment = PublishRecordedTestFragment.newInstance(postId, mCurrentCamera);
                    break;
                case VIEWER:
                    Log.d(TAG, "statusLive: " + statusLive);

                    switch (statusLive) {
                        case -1:
                            break;
                        case 0:
                            fragment = DoneLiveFragment.newInstance(statusLive);
                            break;
                        case 1:
                            fragment = SubscribeTestFragment.newInstance(postId, deviceName);
                            break;
                    }
                    break;
                case VIEW_LIVE_SAVE:
                    fragment = VodTestFragment.newInstance(mediaUrl);
                    break;
                case VIEW_LIVE_SAVE_NO_URL:
                    fragment = DoneLiveFragment.newInstance(1);
                    break;
            }

            if (fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameView, fragment)
                        .commit();
            }
        }
    }

    private void setUpAddLiveInfo() {
        rlLive.setVisibility(View.GONE);
        AddLiveDialog addLiveDialog = AddLiveDialog.newInstance();
        addLiveDialog.setOnAddLiveListener(new AddLiveDialog.OnAddLiveListener() {
            @Override
            public void onDismiss() {
                addLiveDialog.dismiss();
                onBackPressed();
            }

            @Override
            public void onGoLive(PostDetail postDetail) {
                SharedPreferencesManager.getInstance(LiveStreamActivity.this).putString(Constants.PREF_CURRENT_POST_ID, postDetail.getId());
                SharedPreferencesManager.getInstance(LiveStreamActivity.this).putString("title", postDetail.getTitle());
                setUpViewLive(postDetail);
            }

            @Override
            public void onChange(PostDetail postDetail) {
                if (postDetail.getProducts() != null && !postDetail.getProducts().isEmpty()) {
                    for (ProductInPost product : postDetail.getProducts()) {
                        if (product.isShow()) {
                            setUpProduct(product, postDetail.isLive());
                        }
                    }
                }
            }

            @Override
            public void onCameraChange(int cameraPosition) {
                if (mCurrentCamera != cameraPosition && fragment != null && fragment instanceof PublishRecordedTestFragment) {
                    mCurrentCamera = cameraPosition;
                    ((PublishRecordedTestFragment) fragment).setCameraFacing(mCurrentCamera);
                }
            }

            @Override
            public int getCameraPosition() {
                return mCurrentCamera;
            }
        });
        addLiveDialog.show(getSupportFragmentManager(), "Add Live Dialog");
    }

    private void setUpViewLive(PostDetail postDetail) {
        postId = postDetail.getId();
        statusLive = postDetail.getStatus();
        deviceName = postDetail.getDeviceName();
        if (!postDetail.isLive()) {
            mediaUrl = postDetail.getMediaUrl();
            if (statusLive == 0) {
                liveStreamType = LiveStreamType.VIEWER;
            } else {
                if (TextUtils.isEmpty(mediaUrl)) {
                    liveStreamType = LiveStreamType.VIEW_LIVE_SAVE_NO_URL;
                } else {
                    liveStreamType = LiveStreamType.VIEW_LIVE_SAVE;
                    rlDataLiveSave.setVisibility(View.VISIBLE);
                    rvViewers.setVisibility(View.GONE);

                    if (streamerId.equals(mCurrentUserId)) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlDataLiveSave.getLayoutParams();
                        params.removeRule(RelativeLayout.ALIGN_PARENT_END);
                        rlDataLiveSave.setLayoutParams(params);
                    }
                    try {
                        tvDateRec.setText((DateTimeUtils.getElapsedInterval(getApplicationContext(), DateTimeUtils.getDateFormatUTC(postDetail.getCreatedAt()))).toLowerCase());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            mediaUrl = "";
            if (liveStreamType != LiveStreamType.LIVE) {
                liveStreamType = LiveStreamType.VIEWER;
            }
        }

        if (!isRefreshProduct) {
            mAppPermission.checkPermissionCamera();
        }

        rlLive.setVisibility(View.VISIBLE);
        // use a linear layout manager
        llm = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvViewers.setLayoutManager(llm);
        rvViewers.setHasFixedSize(true);
        viewersAdapter = new LiveViewersAdapter(this, new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        });
        viewersAdapter.setUsersRef(FireStoreManager.getInstance().getDb().collection(Constants.COLLECTION_USERS));

        rvViewers.setAdapter(viewersAdapter);

        LinearLayoutManager llm2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvComments.setLayoutManager(llm2);
        rvComments.setHasFixedSize(true);

        commentsAdapter = new LiveCommentsAdapter(this, new BaseAdapter.OnItemClickListener() {
            @Override
            public <T> void onItemClicked(T item, int position) {

            }
        }, liveStreamType);
        rvComments.setAdapter(commentsAdapter);
        etComment.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND | actionId == EditorInfo.IME_ACTION_DONE) {
                sendComment();
                return true;
            }
            return false;
        });

        if (postDetail.getProducts() != null && !postDetail.getProducts().isEmpty()) {
            for (ProductInPost product : postDetail.getProducts()) {
                if (product.isShow()) {
                    setUpProduct(product, postDetail.isLive());
                }
            }
        }
        if (!TextUtils.isEmpty(postDetail.getUser().getAvatar())) {
            pbLoadAvatar.setVisibility(View.VISIBLE);
            ivProfile.setVisibility(View.VISIBLE);
            Glide.with(LiveStreamActivity.this).load(postDetail.getUser().getAvatar())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbLoadAvatar.setVisibility(View.GONE);
                            Utils.setDefaultAvatar(getApplicationContext(), ivDefaultAvatar, tvDefaultAvatar, (CircleImageView) ivProfile, postDetail.getUser(), R.dimen.people_default_avatar_text_size);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbLoadAvatar.setVisibility(View.GONE);
                            Utils.setHideDefaultAvatar(getApplicationContext(), ivDefaultAvatar, tvDefaultAvatar, (CircleImageView) ivProfile);
                            return false;
                        }
                    })
                    .into(ivProfile);
        } else {
            pbLoadAvatar.setVisibility(View.GONE);
            Utils.setDefaultAvatar(getApplicationContext(), ivDefaultAvatar, tvDefaultAvatar, (CircleImageView) ivProfile, postDetail.getUser(), R.dimen.people_default_avatar_text_size);
        }
        View.OnClickListener openProfileListener = v -> Utils.openUserDetail(LiveStreamActivity.this, postDetail.getUser().getUserId());
        ivProfile.setOnClickListener(openProfileListener);

        tvUsername.setOnClickListener(openProfileListener);

        tvUsername.setText(postDetail.getUser().getUsername());
        tvNumberFollower.setText(String.valueOf(postDetail.getUser().getNumberOfFollowers()));

        setFollow(postDetail.getUser().isFollow());

        setLike(postDetail);

        if (!isRefreshProduct) {
            setUpFireStore();
        } else {
            isRefreshProduct = false;
        }
    }

    private void setLike(PostDetail postDetail) {
        ibLike.setEnabled(true);
        isLike = postDetail.getLike();
        if (postDetail.getLike()) {
            ibLike.setImageResource(R.drawable.ic_star_liked);
        } else {
            ibLike.setImageResource(R.drawable.ic_star_like_empty);
        }
    }

    private void setFollow(boolean isFollow) {
        if (isFollow) {
            iBtnFollow.setImageResource(R.drawable.ic_followed);
        } else {
            iBtnFollow.setImageResource(R.drawable.ic_add_friend);
        }
    }

    private void updateFollowers(boolean isFollow) {
        int followers = Integer.valueOf(tvNumberFollower.getText().toString());
        if (isFollow) {
            followers++;
        } else {
            followers--;
        }
        tvNumberFollower.setText(String.valueOf(followers));

    }

    private void setUpProduct(ProductInPost product, boolean isLive) {
        this.product = product;
        if (liveStreamType == LiveStreamType.LIVE) {
            FireStoreManager.getInstance().setProduct(postId, this.product, false);
        }
        if (!TextUtils.isEmpty(product.getFeaturedImage())) {
            pbProductImage.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(product.getFeaturedImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbProductImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbProductImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(ivProductCountDown);
        }

        String dealTime = product.getExpiredAt();
        if (!TextUtils.isEmpty(dealTime) && isLive) {
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat(ServiceConstants.DATE_TIME_FORMAT, Locale.getDefault());
            Calendar currentCalendar = Calendar.getInstance();
            Calendar expiredCalendar = Calendar.getInstance();

            try {
                expiredCalendar.setTimeInMillis(dateTimeFormat.parse(DateTimeUtils.getDateFormatUTC(dealTime)).getTime());
            } catch (Exception e) {
                AppLog.e(e.getMessage());
            }

            long difference = expiredCalendar.getTimeInMillis() - currentCalendar.getTimeInMillis();

            if (timer != null) {
                timer.cancel();
            }

            timer = new CountDownTimer(difference, 1000) {
                public void onTick(long millisUntilFinished) {
                    try {
                        tvCountDownTimer.setText(DateTimeUtils.getDisplayTimeFromMillis(millisUntilFinished));
                        ivPriceTag.setImageResource(R.drawable.ic_price_tag);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        timer.cancel();
                    }
                }

                public void onFinish() {
                    displaySoldProduct();
                    try {
                        if (!TextUtils.isEmpty(postId)) {
                            PostRequest postRequest = new PostRequest(postId);
                            postRequest.setProductInPost(product);
                            requestApi(postRequest, true, RequestTarget.EXPIRE_POST_PRODUCT, LiveStreamActivity.this);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        timer.cancel();
                    }
                }
            }.start();
        } else {
            displaySoldProduct();
        }

        if (!tvCountDownTimer.getText().toString().equalsIgnoreCase(getResources().getString(R.string.dialog_add_discount_timer_end))) {
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500); //You can manage the blinking time with this parameter
            anim.setStartOffset(0);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            tvCountDownTimer.startAnimation(anim);
        }

        int quantity = product.getQuantity();
        tvSaleQuantity.setText(String.valueOf(quantity));
        rlDeal.setVisibility(View.VISIBLE);
        if (quantity == 0) {
            displaySoldProduct();
        }
    }

    private void displaySoldProduct() {
        tvCountDownTimer.setAnimation(null);
        tvCountDownTimer.setText(getString(R.string.dialog_add_discount_timer_end));
        ivPriceTag.setImageResource(R.drawable.ic_price_tag_sold);
    }

    private void changeProduct() {
        rlLive.setVisibility(View.GONE);
        AddLiveDialog changeLiveDialog = AddLiveDialog.newInstance();
        changeLiveDialog.setChangeInfo(true, product, postId);
        changeLiveDialog.setOnAddLiveListener(new AddLiveDialog.OnAddLiveListener() {
            @Override
            public void onDismiss() {
                rlLive.setVisibility(View.VISIBLE);
                changeLiveDialog.dismiss();
            }

            @Override
            public void onGoLive(PostDetail postDetail) {

            }

            @Override
            public void onChange(PostDetail postDetail) {
                if (postDetail.getProducts() != null && !postDetail.getProducts().isEmpty()) {
                    for (ProductInPost product : postDetail.getProducts()) {
                        if (product.isShow()) {
                            setUpProduct(product, postDetail.isLive());
                        }
                    }
                }
            }

            @Override
            public void onCameraChange(int cameraPosition) {
            }

            @Override
            public int getCameraPosition() {
                return -1;
            }
        });
        changeLiveDialog.show(getSupportFragmentManager(), "Add Live Dialog");
    }

    private void showAddToCart() {
        AddToCartDialog addToCartDialog = AddToCartDialog.newInstance();
        addToCartDialog.setProductInPost(product);
        addToCartDialog.setSellerId(streamerId);
        addToCartDialog.setPostId(postId);
        if (!tvCountDownTimer.getText().equals(getString(R.string.dialog_add_discount_timer_end))) {
            addToCartDialog.setIsShowSalePrice(true);
        }
        addToCartDialog.show(getSupportFragmentManager(), "Show Add To Cart Dialog");
    }

    private void startListenerComments() {
        if (listenerComments != null) {
            listenerComments.remove();
        }
        Log.d(TAG, "startListenerComments POST_ID: " + postId);
        if (commentsAdapter != null) {
            commentsAdapter.clearAllItems();
        }
        listenerComments = FireStoreManager.getInstance().getCommentsRef()
                .whereEqualTo(Constants.FIELD_POST_ID, postId)
                .orderBy(Constants.FIELD_CREATE_AT, Query.Direction.DESCENDING)
                .addSnapshotListener(this, (queryDocumentSnapshots, err) -> {
                    if (err != null) {
                        Log.e(TAG, "listenNewMessages failed " + err.getMessage(), err);
                        return;
                    }
                    if (queryDocumentSnapshots == null) {
                        Log.e(TAG, "queryDocumentSnapshots null");
                        return;
                    }
                    List<Comment> listComment = new ArrayList<>();
                    for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                        DocumentSnapshot documentSnapshot = dc.getDocument();
                        Comment comment = null;
                        try {
                            comment = documentSnapshot.toObject(Comment.class);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (comment == null) {
                            Log.e(TAG, "comment null");
                            continue;
                        }

                        String id = documentSnapshot.getId();
                        int oldIndex = dc.getOldIndex();
                        int newIndex = dc.getNewIndex();

                        switch (dc.getType()) {
                            case ADDED:
                                Log.d(TAG, "Added: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                Log.d(TAG, "ID: " + id + "; Comment: " + comment.getComment() + "; CreatedAt: " + comment.getCreatedAt());
                                listComment.add(comment);
                                break;
                            case MODIFIED:
                                Log.d(TAG, "Modified: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                break;
                            case REMOVED:
                                Log.d(TAG, "Removed: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                break;
                        }
                    }
                    Log.e(TAG, "listenerComments End");
                    Collections.reverse(listComment);
                    commentsAdapter.addAllItems(listComment);
                    rvComments.scrollToPosition(commentsAdapter.getItemCount() - 1);
                    if (liveStreamType == LiveStreamType.LIVE && commentsAdapter.getItemCount() > 0) {
                        FireStoreManager.getInstance().setCountComment(postId, commentsAdapter.getItemCount(), false);
                    }
                });
    }

    private void startListenerViewers() {
        if (listenerViewers != null) {
            listenerViewers.remove();
        }
        Log.d(TAG, "startListenerViewers POST_ID: " + postId);
        if (viewersAdapter != null) {
            viewersAdapter.clearAllItems();
        }
        if (likes != null) {
            likes.clear();
        } else {
            likes = new ArrayList<>();
        }

        listenerViewers = FireStoreManager.getInstance().getViewersRef()
                .whereEqualTo(Constants.FIELD_POST_ID, postId)
                .addSnapshotListener(this, (queryDocumentSnapshots, err) -> {
                    if (err != null) {
                        Log.e(TAG, "listenNewMessages failed " + err.getMessage(), err);
                        return;
                    }
                    if (queryDocumentSnapshots == null) {
                        Log.e(TAG, "queryDocumentSnapshots null");
                        return;
                    }
                    List<Viewer> listViewer = new ArrayList<>();
                    for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                        DocumentSnapshot documentSnapshot = dc.getDocument();
                        Viewer viewer = null;
                        try {
                            viewer = documentSnapshot.toObject(Viewer.class);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (viewer == null) {
                            Log.e(TAG, "viewer null");
                            continue;
                        }

                        String id = documentSnapshot.getId();
                        int oldIndex = dc.getOldIndex();
                        int newIndex = dc.getNewIndex();
                        int index;
                        switch (dc.getType()) {
                            case ADDED:
                                Log.d(TAG, "Added: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                Log.d(TAG, "ID: " + id + "; User Id: " + viewer.getUserId());
                                index = getIndexByViewer(viewersAdapter.getList(), viewer);
                                if (viewer.isView()) {
                                    if (index == -1) {
                                        listViewer.add(viewer);
                                    }
                                } else {
                                    if (index != -1) {
                                        viewersAdapter.removeItem(index);
                                    }
                                }
                                if (viewer.isLike()) {
                                    likes.add(viewer);
                                }
                                break;
                            case MODIFIED:
                                Log.d(TAG, "Modified: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                index = getIndexByViewer(viewersAdapter.getList(), viewer);
                                if (viewer.isView()) {
                                    if (index == -1) {
                                        listViewer.add(viewer);
                                    }
                                } else {
                                    if (index != -1) {
                                        viewersAdapter.removeItem(index);
                                    }
                                }
                                index = getIndexByViewer(likes, viewer);
                                if (viewer.isLike()) {
                                    if (index == -1) {
                                        likes.add(viewer);
                                    }
                                } else {
                                    if (index != -1) {
                                        likes.remove(index);
                                    }
                                }
                                break;
                            case REMOVED:
                                Log.d(TAG, "Removed: " + id + "; Old Index: " + oldIndex + "; New Index: " + newIndex);
                                break;
                        }
                    }
                    Log.e(TAG, "listenerViewers End");
                    tvCountLike.setText(String.valueOf(likes.size()));

                    viewersAdapter.addAllItems(listViewer);
                    tvViewerCount.setText(String.valueOf(viewersAdapter.getItemCount()));
                    if (liveStreamType == LiveStreamType.LIVE && viewersAdapter.getItemCount() > 0) {
                        FireStoreManager.getInstance().setCountViewer(postId, viewersAdapter.getItemCount(), false);
                    }
                });
    }

    private void startListenerPost() {
        if (listenerPost != null) {
            listenerPost.remove();
        }
        Log.d(TAG, "startListenerPost POST_ID: " + postId);

        listenerPost = FireStoreManager.getInstance().getPostRef().document(postId)
                .addSnapshotListener(this, (snapshot, err) -> {
                    if (err != null) {
                        Log.e(TAG, "listenerViewers failed " + err.getMessage(), err);
                        return;
                    }
                    if (snapshot == null || snapshot.getData() == null || snapshot.getBoolean(Constants.FIELD_IS_LIVE) == null) {
                        Log.e(TAG, "snapshot or FIELD_IS_LIVE null");
                    } else {
                        boolean isLive = snapshot.getBoolean(Constants.FIELD_IS_LIVE);
                        if (!isLive && liveStreamType == LiveStreamType.VIEWER) {

                            if (timer != null) {
                                timer.cancel();
                                tvCountDownTimer.setAnimation(null);
                                tvCountDownTimer.setText(getString(R.string.dialog_add_discount_timer_end));
                                ivPriceTag.setImageResource(R.drawable.ic_price_tag_sold);
                            }
                            fragment = DoneLiveFragment.newInstance(statusLive);

                            if (fragment != null) {
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frameView, fragment)
                                        .commit();
                            }
                        }
                    }
                    if (snapshot.get(Constants.FIELD_PRODUCT) == null) {
                        Log.e(TAG, "snapshot or FIELD_PRODUCT_ID null");
                        return;
                    }

                    if (product == null || product.getProductId() == null) {
                        return;
                    }

                    Map<String, Object> mapProduct = (Map<String, Object>) snapshot.getData().get(Constants.FIELD_PRODUCT);
                    if (mapProduct.get(Constants.FIELD_PRODUCT_ID) != null
                            && !mapProduct.get(Constants.FIELD_PRODUCT_ID).toString().equals(product.getProductId())) {
                        getPostDetail();
                        isRefreshProduct = true;
                        return;
                    }
                    boolean isChange = false;

                    if (mapProduct.get(Constants.FIELD_PRODUCT_EXPIRE_AT) != null
                            && !mapProduct.get(Constants.FIELD_PRODUCT_EXPIRE_AT).toString().equals(product.getExpiredAt())) {
                        product.setExpiredAt(mapProduct.get(Constants.FIELD_PRODUCT_EXPIRE_AT).toString());
                        isChange = true;
                    }

                    if (mapProduct.get(Constants.FIELD_PRODUCT_DISCOUNT) != null
                            && !mapProduct.get(Constants.FIELD_PRODUCT_DISCOUNT).toString().equals(product.getDiscount())) {
                        product.setDiscount(mapProduct.get(Constants.FIELD_PRODUCT_DISCOUNT).toString());
                        isChange = true;
                    }

                    if (mapProduct.get(Constants.FIELD_PRODUCT_PRICE_DISCOUNT) != null) {
                        float mapProductPriceDiscount = Float.valueOf(mapProduct.get(Constants.FIELD_PRODUCT_PRICE_DISCOUNT).toString());
                        if (mapProductPriceDiscount != product.getPriceDiscount()) {
                            product.setPriceDiscount(mapProductPriceDiscount);
                            isChange = true;
                        }
                    }

                    if (mapProduct.get(Constants.FIELD_PRODUCT_QUANTITY) != null) {
                        int mapProductQuantity = Integer.valueOf(mapProduct.get(Constants.FIELD_PRODUCT_QUANTITY).toString());
                        if (mapProductQuantity != product.getQuantity()) {
                            product.setQuantity(mapProductQuantity);
                            isChange = true;
                        }
                    }

                    if (isChange) {
                        setUpProduct(product, true);
                    }
                });
    }

    private void setUpFireStore() {
        setViewerView(true);
        if (liveStreamType == LiveStreamType.LIVE) {
            startListenerGift();
        }
        startListenerComments();
        startListenerViewers();
        startListenerPost();
    }
}
