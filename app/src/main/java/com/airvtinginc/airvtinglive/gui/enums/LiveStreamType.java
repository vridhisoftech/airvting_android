package com.airvtinginc.airvtinglive.gui.enums;

import android.content.Intent;

public enum LiveStreamType {
    LIVE,
    VIEWER,
    VIEW_LIVE_SAVE,
    VIEW_LIVE_SAVE_NO_URL;

    private static final String name = DetailType.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static LiveStreamType detachFrom(Intent intent) {
        if (!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }
}
