package com.airvtinginc.airvtinglive.gui.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.components.photofilter.ImageProcessingModel;
import com.airvtinginc.airvtinglive.gui.components.photofilter.MyEffect;
import com.airvtinginc.airvtinglive.gui.components.photofilter.PhotoEffectAdapter;
import com.airvtinginc.airvtinglive.gui.components.photofilter.ThumbnailCallback;
import com.airvtinginc.airvtinglive.gui.components.photofilter.ThumbnailItem;
import com.airvtinginc.airvtinglive.gui.dialog.AddPhotoDialog;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.tools.AppPermission;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubfilter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class PhotoFilterActivity extends Activity implements ThumbnailCallback {
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public static final int QUALITY_SAVE_IMAGE = 100;

    private int currentFilterType;
    private Bitmap bmThumb, bmOriginal, rotatedBitmap, filterBitmap;
    private ArrayList<ImageProcessingModel> effectList = new ArrayList<>();
    private Uri myUri, contentSavedUri;
    private File file;
    private PhotoEffectAdapter adapter;
    private Context context;
    private boolean isFromEditProfileScreen = false;
    private AppPermission mAppPermission;
    private AddPhotoDialog.ReturnImageListener mReturnImageListener;

    @BindView(R.id.photo_filter_ll_main)
    LinearLayout llMain;
    @BindView(R.id.photo_filter_rv_effect)
    RecyclerView rvEffect;
    @BindView(R.id.photo_filter_iv_display)
    ImageView ivDisplay;
    @BindView(R.id.photo_filter_sb_brightness)
    SeekBar sbBrightness;
    @BindView(R.id.photo_filter_ll_back)
    LinearLayout llBack;
    @BindView(R.id.photo_filter_ll_next)
    LinearLayout llNext;
    @BindView(R.id.photo_filter_iv_contract)
    ImageView ivContract;
    @BindView(R.id.photo_filter_iv_crop)
    ImageView ivCrop;
    @BindView(R.id.photo_filter_btn_edit)
    Button btnChange;
    @BindView(R.id.photo_filter_btn_filter)
    Button btnFilter;
    @BindView(R.id.photo_filter_ll_change)
    LinearLayout llChange;
    @BindView(R.id.photo_filter_iv_rotate)
    ImageView ivRotate;
    @BindView(R.id.photo_filter_ll_seekbar)
    LinearLayout llSeekBar;
    @BindView(R.id.photo_filter_crop_imageview)
    CropImageView cropImageFrame;
    @BindView(R.id.photo_filter_rl_crop)
    RelativeLayout rlCrop;
    @BindView(R.id.photo_filter_btn_crop)
    Button btnCrop;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_filter_activity);
        context = this;
        ButterKnife.bind(this);
        effectList.clear();

        //This case is from Edit profile
        if (getIntent().getData() != null) {
            isFromEditProfileScreen = true;
            contentSavedUri = getIntent().getData();
            loadBitMap(contentSavedUri);
        } else {
            //Get action type from previous page to take photo or get photo from gallery
            if (getIntent().getExtras() != null) {
                Uri uri = Uri.parse((getIntent().getExtras().getString(Constants.EXTRA_IMAGE_URI)));
                loadBitMap(uri);
            }
        }

        currentFilterType = EnumManager.FilterChangeType.Crop.ordinal();

        llBack.setOnClickListener(view -> finish());
        llNext.setOnClickListener(view -> next());
        btnChange.setOnClickListener(view -> {
            btnChange.setTextColor(getResources().getColor(R.color.white));
            btnFilter.setTextColor(getResources().getColor(R.color.black));
            btnChange.setBackgroundResource(R.drawable.bg_black_cornor);
            btnFilter.setBackgroundResource(R.drawable.bg_white_cornor);
            rvEffect.setVisibility(View.GONE);
            llChange.setVisibility(View.VISIBLE);
            if (currentFilterType == EnumManager.FilterChangeType.Crop.ordinal()) {
                rlCrop.setVisibility(View.VISIBLE);
                llSeekBar.setVisibility(View.GONE);
                cropImageFrame.setVisibility(View.VISIBLE);
                ivDisplay.setVisibility(View.GONE);
            } else {
                rlCrop.setVisibility(View.GONE);
                llSeekBar.setVisibility(View.VISIBLE);
                cropImageFrame.setVisibility(View.GONE);
                ivDisplay.setVisibility(View.VISIBLE);
            }
        });

        btnFilter.setOnClickListener(view -> {
            btnChange.setTextColor(getResources().getColor(R.color.black));
            btnFilter.setTextColor(getResources().getColor(R.color.white));
            btnFilter.setBackgroundResource(R.drawable.bg_black_cornor);
            btnChange.setBackgroundResource(R.drawable.bg_white_cornor);
            rvEffect.setVisibility(View.VISIBLE);
            llChange.setVisibility(View.GONE);
            rlCrop.setVisibility(View.GONE);
            llSeekBar.setVisibility(View.GONE);
            cropImageFrame.setVisibility(View.GONE);
            ivDisplay.setVisibility(View.VISIBLE);
        });

        ivCrop.setOnClickListener(view -> {
            currentFilterType = EnumManager.FilterChangeType.Crop.ordinal();
            rlCrop.setVisibility(View.VISIBLE);
            llSeekBar.setVisibility(View.GONE);
            Glide.with(getApplicationContext()).load(R.drawable.ic_crop_selected).into(ivCrop);
            Glide.with(getApplicationContext()).load(R.drawable.ic_filter_brightness).into(ivContract);
            cropImageFrame.setVisibility(View.VISIBLE);
            ivDisplay.setVisibility(View.GONE);
        });

        ivContract.setOnClickListener(view -> {
            currentFilterType = EnumManager.FilterChangeType.Contrast.ordinal();
            rlCrop.setVisibility(View.GONE);
            llSeekBar.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(R.drawable.ic_crop_normal).into(ivCrop);
            Glide.with(getApplicationContext()).load(R.drawable.ic_contrast_selected).into(ivContract);
            cropImageFrame.setVisibility(View.GONE);
            ivDisplay.setVisibility(View.VISIBLE);
            ivDisplay.setImageBitmap(rotatedBitmap);
        });

        ivRotate.setOnClickListener(view -> {
            rotatedBitmap = rotateImage(rotatedBitmap, -90);
            filterBitmap = rotatedBitmap;
            cropImageFrame.setImageBitmap(rotatedBitmap);
        });

        btnCrop.setOnClickListener(view -> {
            rotatedBitmap = cropImageFrame.getCroppedImage();
            cropImageFrame.setImageBitmap(rotatedBitmap);
            ivDisplay.setImageBitmap(rotatedBitmap);
            bmOriginal = rotatedBitmap;
            sbBrightness.setProgress(50);
            Handler handler = new Handler();
            Runnable r = () -> {
                MyEffect myEffect = new MyEffect();
                List<ThumbnailItem> thumbs = myEffect.getThumbList(getApplicationContext(), rotatedBitmap);

                adapter = new PhotoEffectAdapter(PhotoFilterActivity.this, R.layout.gallery_image_layout, thumbs, this);
                rvEffect.setAdapter(adapter);
                adapter.setSelectedItem(0);
            };
            handler.post(r);
        });

        sbBrightness.setProgress(50);
        sbBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @SuppressLint("StaticFieldLeak")
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                new AsyncTask<Void, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Void... voids) {
                        try {
                            Bitmap newBmp;
                            if (filterBitmap == null) {
                                newBmp = Bitmap.createScaledBitmap(bmOriginal, bmOriginal.getWidth(), bmOriginal.getHeight(), true);
                            } else {
                                newBmp = Bitmap.createScaledBitmap(filterBitmap, filterBitmap.getWidth(), filterBitmap.getHeight(), true);
                            }
                            Filter myFilter = new Filter();
                            myFilter.addSubFilter(new BrightnessSubfilter(seekBar.getProgress() - 50)); //Brightness range use here is from -50 -> 50
                            return myFilter.processFilter(newBmp);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;

                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        super.onPostExecute(bitmap);
                        ivDisplay.setImageBitmap(bitmap);
                        cropImageFrame.setImageBitmap(bitmap);
                        rotatedBitmap = bitmap;
                    }
                }.execute();
            }
        });
    }

    private void setUpPermission() {
        mAppPermission = new AppPermission(this, (requestCode -> {
            switch (requestCode) {
                case AppPermission.REQUEST_CODE_CAMERA:
                    break;
                case AppPermission.REQUEST_CODE_GALLERY:
                    break;
            }
        }), null);
    }

    @Override
    public void onPause() {

        super.onPause();
    }


    @SuppressLint("StaticFieldLeak")
    private void loadBitMap(Uri uri) {
        myUri = uri;
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                float layoutWith = Utils.getScreenWidth(getApplicationContext());
                float ratio = Utils.getScreenRatio(getApplicationContext());
                //Scale original image belong to screen size
                RequestOptions myOptionsOriginal = new RequestOptions().fitCenter().override((int) layoutWith, (int) (layoutWith / ratio));
                RequestOptions myOptionsThumb = new RequestOptions().centerCrop().override(100, 100);
                try {
                    bmThumb = Glide.with(getApplicationContext()).asBitmap().load(myUri).apply(myOptionsThumb).submit().get();
                    bmOriginal = Glide.with(getApplicationContext()).asBitmap().load(myUri).apply(myOptionsOriginal).submit().get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                ivDisplay.setImageBitmap(bmOriginal);
                rotatedBitmap = bmOriginal;
                setUpGalleryEffect();
                DialogUtils.hideLoadingProgress();
            }
        }.execute();

        cropImageFrame.setImageUriAsync(uri);
    }

    public void setUpGalleryEffect() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        rvEffect.setLayoutManager(layoutManager);
        rvEffect.setHasFixedSize(true);
        bindDataToAdapter();
    }

    private void bindDataToAdapter() {
        Handler handler = new Handler();
        Runnable r = () -> {
            MyEffect myEffect = new MyEffect();
            List<ThumbnailItem> thumbs = myEffect.getThumbList(getApplicationContext(), bmThumb);

            adapter = new PhotoEffectAdapter(PhotoFilterActivity.this, R.layout.gallery_image_layout, thumbs, this);
            rvEffect.setAdapter(adapter);
            adapter.setSelectedItem(0);
        };
        handler.post(r);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onThumbnailClick(Filter filter, int position) {
        //Make a copy of original bitmap, we will filter on it
        sbBrightness.setProgress(50); //Reset brightness level when choose another effect
        Bitmap newBmp = Bitmap.createScaledBitmap(bmOriginal, bmOriginal.getWidth(), bmOriginal.getHeight(), true);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                if (position == 0) {
                    filterBitmap = newBmp;
                } else {
                    filterBitmap = filter.processFilter(newBmp);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                ivDisplay.setImageBitmap(filterBitmap);
                cropImageFrame.setImageBitmap(filterBitmap);
                rotatedBitmap = filterBitmap;
                adapter.setSelectedItem(position);
            }
        }.execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mAppPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @SuppressLint("StaticFieldLeak")
    private void next() {
        String fileName = System.currentTimeMillis() + ".jpg";
        DialogUtils.showLoadingProgress(context, false);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                saveBitmap(rotatedBitmap, fileName);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                DialogUtils.hideLoadingProgress();
                if (isFromEditProfileScreen) {
                    Intent returnIntent = new Intent();
                    returnIntent.setData(contentSavedUri);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Intent intent = new Intent(context, DetailActivity.class);
                    Bundle args = new Bundle();
                    args.putString(Constants.EXTRA_IMAGE_URI, contentSavedUri.toString());
                    intent.putExtras(args);
                    DetailType.ADD_NEW_PHOTO.attachTo(intent);
                    startActivity(intent);
                }
            }
        }.execute();
    }

    private void saveBitmap(Bitmap bmp, String fileName) {
        try {
            String folderPath = Utils.getApplicationFolder().toString();
            File folder = new File(folderPath);
            if (!folder.exists()) {
                File wallpaperDirectory = new File(folderPath);
                wallpaperDirectory.mkdirs();
            }

            file = new File(folderPath, fileName);
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, QUALITY_SAVE_IMAGE, fos);
            scanSdCard();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //We need to scan sd card to update and display new photo on device Gallery
    private void scanSdCard() {
        String folderPath = Utils.getApplicationFolder().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            contentSavedUri = Uri.fromFile(file);
            mediaScanIntent.setData(contentSavedUri);
            this.sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + folderPath)));
        }
    }

    public void setReturnImageListener(AddPhotoDialog.ReturnImageListener returnImageListener) {
        this.mReturnImageListener = returnImageListener;
    }
}
