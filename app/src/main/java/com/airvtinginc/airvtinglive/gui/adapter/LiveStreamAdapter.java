package com.airvtinginc.airvtinglive.gui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.EmptyViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.LiveViewHolder;
import com.airvtinginc.airvtinglive.gui.adapter.viewholder.ProductViewHolder;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.HomeFeedType;
import com.airvtinginc.airvtinglive.models.response.LiveStream;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.fonts.CustomTypefaceSpan;

import java.util.ArrayList;
import java.util.Random;

public class LiveStreamAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_EMPTY = -1;
    private static final int VIEW_TYPE_LIVE = 0;
    private static final int VIEW_TYPE_LOAD_MORE = 1;
    private static final int VIEW_TYPE_PRODUCT = 2;
    private static final int VIEW_TYPE_VIDEO = 3;
    private static final int VIEW_TYPE_LIVE_SAVE = 4;
    private static final int VIEW_TYPE_NON_LIVE = 5;

    public LiveStreamAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(context, new ArrayList<>(), onItemClickListener);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOAD_MORE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loadmore_layout, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_VIDEO:
            case VIEW_TYPE_LIVE_SAVE:
            case VIEW_TYPE_NON_LIVE:
            case VIEW_TYPE_LIVE: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_livestream, parent, false);
                LiveViewHolder vh = new LiveViewHolder(v);
                return vh;
            }
            case VIEW_TYPE_PRODUCT: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_home, parent, false);
                ProductViewHolder vh = new ProductViewHolder(v);
                return vh;
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
                EmptyViewHolder vh = new EmptyViewHolder(v);
                return vh;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof EmptyViewHolder) return;

        final LiveStream liveStream = (LiveStream) list.get(position);

        if (mHolder instanceof LiveViewHolder) {

            final LiveViewHolder holder = (LiveViewHolder) mHolder;

            int drawableResourceId = context.getResources().getIdentifier(liveStream.getImageUrl(), "drawable", context.getPackageName());
            holder.ivLive.setImageResource(drawableResourceId);

            String username = liveStream.getUsername();
            String content = liveStream.getCaption();
            String itemValue = username + " " + content;
            SpannableStringBuilder SS = new SpannableStringBuilder(itemValue);
            int startPos = itemValue.toLowerCase().indexOf(username.toLowerCase());
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {

                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            if (startPos != -1) {
                int endPos = startPos + username.length();
                SS.setSpan(clickableSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                SS.setSpan(new ForegroundColorSpan(this.context.getResources().getColor(R.color.colorHeader)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                SS.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_semibold)), startPos, endPos, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                SS.setSpan(new CustomTypefaceSpan("", ResourcesCompat.getFont(context, R.font.opensans_regular)), endPos, itemValue.length() - endPos, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            }
            holder.tvUserName.setText(SS);

            holder.mView.setOnClickListener(v -> onItemClickListener.onItemClicked(liveStream, position));
            holder.rlDeal.setOnClickListener(view -> {
                //changeItemType(position);
            });
            holder.tvUserName.setOnClickListener(view -> openUserDetail(position));

            holder.tvCurrentViewers.setText(String.valueOf(new Random().nextInt(5000 - 100 + 1) + 100));

            if (liveStream.getType().equals(HomeFeedType.VIDEO.getType())) {
                holder.ivLiveRec.setVisibility(View.VISIBLE);
                holder.ivLiveRec.setImageResource(R.drawable.ic_rec);
                holder.tvCurrentViewers.setVisibility(View.INVISIBLE);
                holder.rlDeal.setVisibility(View.INVISIBLE);
                holder.tvSchedule.setVisibility(View.INVISIBLE);
                return;
            } else if (liveStream.getType().equals(HomeFeedType.LIVE.getType())) {
                holder.ivLiveRec.setVisibility(View.VISIBLE);
                holder.ivLiveRec.setImageResource(R.drawable.ic_live);
                holder.tvCurrentViewers.setVisibility(View.VISIBLE);
                holder.rlDeal.setVisibility(View.VISIBLE);
                holder.tvSchedule.setVisibility(View.VISIBLE);
            }

//        if (holder.timer != null) {
//            holder.timer.cancel();
//        }

            if (holder.timer == null) {
                final int randomTime = (new Random().nextInt(59 - 10 + 1) + 10) * 60 * 1000;
                holder.timer = new CountDownTimer(randomTime, 1000) {
                    public void onTick(long millisUntilFinished) {
                        long min = (millisUntilFinished / 1000) / 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        holder.tvCountDownTimer.setText((min < 10 ? "0" + min : min) + ":" + (sec < 10 ? "0" + sec : sec));
                    }

                    public void onFinish() {
                        holder.tvCountDownTimer.setAnimation(null);
                        holder.tvCountDownTimer.setText(context.getString(R.string.my_user_sold));
                        holder.ivPriceTag.setImageResource(R.drawable.ic_price_tag_sold);
                    }
                }.start();
            }

            if (!holder.tvCountDownTimer.getText().toString().equalsIgnoreCase(context.getString(R.string.my_user_sold))) {
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500); //You can manage the blinking time with this parameter
                anim.setStartOffset(0);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                holder.tvCountDownTimer.startAnimation(anim);
            }
        } else if (mHolder instanceof ProductViewHolder) {

            final ProductViewHolder holder = (ProductViewHolder) mHolder;

            holder.mView.setOnClickListener(view -> {
//                    onItemClickListener.onItemClicked(liveStream, position);
            });

            holder.rlDeal.setOnClickListener(view -> {
            });
            holder.tvContent.setOnClickListener(view -> openUserDetail(position));

//            int discount = liveStream.getProduct().getDiscount();
//            float priceBefore = liveStream.getProduct().getPrice();
//            float priceAfter = priceBefore * (100 - discount)/100;
//            String discountString = String.valueOf(discount) + "%";
//            String priceBeforeString = "$" + String.valueOf(priceBefore);
//            String priceAfterString = "$" + String.valueOf(priceAfter) + ServiceConstants.CURRENCY;
//
//            holder.tvTitle.setText(liveStream.getProduct().getTitle());
//            holder.tvPriceBefore.setText(priceBeforeString);
//            holder.tvPriceAfter.setText(priceAfterString);
//            holder.tvDiscount.setText(discountString);
//            holder.tvDescription.setText(liveStream.getProduct().getDescription());
//            Picasso.get()
//                    .load(liveStream.getProduct().getDisplayImage())
//                    .into(holder.ivProductImage);
//
//            if (liveStream.getProduct().isLimited()) {
//                holder.tvLimited.setVisibility(View.VISIBLE);
//            } else {
//                holder.tvLimited.setVisibility(View.GONE);
//            }

            String username = liveStream.getUsername();
            String content = liveStream.getCaption();
            String itemValue = username + " " + content;
            SpannableStringBuilder SS = new SpannableStringBuilder(itemValue);
//            SS.setSpan(new CustomTypefaceSpan("", FontsManager.getDefaultTypeface()), 0, itemValue.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            int startPos = itemValue.toLowerCase().indexOf(username.toLowerCase());
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {

                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            if (startPos != -1) {
                int endPos = startPos + username.length();
                SS.setSpan(clickableSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                SS.setSpan(new ForegroundColorSpan(this.context.getResources().getColor(R.color.colorHeader)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                SS.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            holder.tvContent.setText(SS);

            if (holder.timer == null) {
                final int randomTime = (new Random().nextInt(59 - 10 + 1) + 10) * 60 * 1000;
                holder.timer = new CountDownTimer(randomTime, 1000) {
                    public void onTick(long millisUntilFinished) {
                        long min = (millisUntilFinished / 1000) / 60;
                        long sec = (millisUntilFinished / 1000) % 60;
                        holder.tvCountDownTimer.setText((min < 10 ? "0" + min : min) + ":" + (sec < 10 ? "0" + sec : sec));
                    }

                    public void onFinish() {
                        holder.tvCountDownTimer.setAnimation(null);
                        holder.tvCountDownTimer.setText(context.getString(R.string.my_user_sold));
                        holder.ivPriceTag.setImageResource(R.drawable.ic_price_tag_sold);
                    }
                }.start();
            }

            if (!holder.tvCountDownTimer.getText().toString().equalsIgnoreCase(context.getString(R.string.my_user_sold))) {
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500); //You can manage the blinking time with this parameter
                anim.setStartOffset(0);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                holder.tvCountDownTimer.startAnimation(anim);
            }

            holder.tvPriceBefore.setPaintFlags(holder.tvPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

    }

    private void openUserDetail(int position) {
        LiveStream liveStream = (LiveStream) list.get(position);
        Intent intent = new Intent(context, DetailActivity.class);
        User user = new User();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_USER_ID, user.getId());
        intent.putExtras(args);
        // Sender usage
        DetailType.PROFILE.attachTo(intent);
        context.startActivity(intent);
    }

    @Override
    public int getItemViewType(int position) {
        LiveStream liveStream = (LiveStream) list.get(position);
        if (liveStream == null) return VIEW_TYPE_LOAD_MORE;
        else if (liveStream.getType().equalsIgnoreCase(HomeFeedType.LIVE.getType()))
            return VIEW_TYPE_LIVE;
        else if (liveStream.getType().equalsIgnoreCase(HomeFeedType.VIDEO.getType()))
            return VIEW_TYPE_VIDEO;
        else return VIEW_TYPE_EMPTY;
    }

//    public void changeItemType(int position) {
//        LiveStream liveStream = liveStreams.get(position);
//        if (liveStream.getType().equals(HomeFeedType.LIVE.getType())) {
//            liveStream.setType(HomeFeedType.PRODUCT.getType());
//        } else if (liveStream.getType().equals(HomeFeedType.PRODUCT.getType())) {
//            liveStream.setType(HomeFeedType.LIVE.getType());
//        }
//        notifyItemChanged(position);
//    }
}
