package com.airvtinginc.airvtinglive.gui.adapter.viewholder;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.gui.components.CircleImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletGiftHolder extends BaseViewHolder {

    public View mView;
    public @BindView(R.id.ivGift)
    CircleImageView ivGift;
    public @BindView(R.id.tvQuantityGift)
    TextView tvQuantityGift;
    public @BindView(R.id.tvNameGift)
    TextView tvNameGift;
    public @BindView(R.id.pbGift)
    ProgressBar pbGift;

    public WalletGiftHolder(View paramView) {
        super(paramView);
        mView = paramView;
        ButterKnife.bind(this, mView);
    }
}