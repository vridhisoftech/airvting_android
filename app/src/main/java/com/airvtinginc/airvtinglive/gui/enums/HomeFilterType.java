package com.airvtinginc.airvtinglive.gui.enums;

public enum HomeFilterType {
    FOLLOW("follow"),
    NEW("new"),
    HOT("hot"),
    FEATURED("featured");

    private String type;

    HomeFilterType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
