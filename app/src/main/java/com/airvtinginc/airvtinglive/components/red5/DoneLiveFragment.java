package com.airvtinginc.airvtinglive.components.red5;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.constants.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoneLiveFragment extends Red5LiveDetailFragment {

    @BindView(R.id.test_detail)
    TextView testDetail;

    private int status;

    public DoneLiveFragment() {
    }

    public static DoneLiveFragment newInstance(int status) {
        DoneLiveFragment fragment = new DoneLiveFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_LIVE_STATUS, status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        if (getArguments() != null) {
            this.status = getArguments().getInt(Constants.EXTRA_LIVE_STATUS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.red5_fragment_done_live_detail, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        switch (status) {
            case -1:
                break;
            case 0:
                testDetail.setText(getString(R.string.live_available));
                break;
            case 1:
                testDetail.setText(getString(R.string.live_end));
                break;
        }
    }
}
