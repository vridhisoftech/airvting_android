package com.airvtinginc.airvtinglive.components.red5;

/**
 * Created by toddanderson on 8/10/17.
 */

public interface PublishTestListener {
    void onPublishFlushBufferStart();

    void onPublishFlushBufferComplete();
}
