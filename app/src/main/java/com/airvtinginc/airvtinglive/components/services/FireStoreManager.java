package com.airvtinginc.airvtinglive.components.services;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.enums.CommentType;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.Red5ProConfig;
import com.airvtinginc.airvtinglive.models.response.Comment;
import com.airvtinginc.airvtinglive.models.response.GiftInFireStore;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FireStoreManager {
    private static final String TAG = FireStoreManager.class.getSimpleName();

    private static FireStoreManager instance;

    private FirebaseFirestore db;
    private CollectionReference commentsRef;
    private CollectionReference viewersRef;
    private CollectionReference postRef;
    private CollectionReference giftRef;
    private CollectionReference configRef;

    public FirebaseFirestore getDb() {
        return db;
    }

    public CollectionReference getCommentsRef() {
        return commentsRef;
    }

    public CollectionReference getViewersRef() {
        return viewersRef;
    }

    public CollectionReference getPostRef() {
        return postRef;
    }

    public CollectionReference getGiftRef() {
        return giftRef;
    }

    public CollectionReference getConfigRef() {
        return configRef;
    }

    public FireStoreManager() {
        db = FirebaseFirestore.getInstance();
        commentsRef = db.collection(Constants.COLLECTION_COMMENTS);
        viewersRef = db.collection(Constants.COLLECTION_VIEWERS);
        postRef = db.collection(Constants.COLLECTION_POSTS);
        giftRef = db.collection(Constants.COLLECTION_GIFTS);
        configRef = db.collection(Constants.COLLECTION_CONFIGS);
    }

    public static FireStoreManager getInstance() {
        if (instance == null) {
            instance = new FireStoreManager();
        }
        return instance;
    }

    public void addComment(Context context, String postId, String cmt, String type) {
        Comment comment = new Comment(SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_ID, ""),
                SharedPreferencesManager.getInstance(context).getString(Constants.PREF_CURRENT_USER_USERNAME, ""), postId, cmt, type, new Date(), new Date());

        commentsRef.add(comment)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "commentsRef added with ID: " + documentReference.getId());
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error adding document", e));
    }

    public void addGift(Context context, GiftInFireStore giftInFireStore) {
        giftRef.add(giftInFireStore)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "giftRef added with ID: " + documentReference.getId());
//                    addComment(context, giftInFireStore.getPostId(), context.getString(R.string.give_a) + giftName);
                    addComment(context, giftInFireStore.getPostId(), giftInFireStore.getFeaturedImage(), CommentType.GIFT.getType());
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error adding document", e));
    }

    public void setIsLive(String postId, boolean isLive, boolean isCreated) {
        if (TextUtils.isEmpty(postId)) {
            return;
        }
        Map<String, Object> post = new HashMap<>();
        post.put(Constants.FIELD_IS_LIVE, isLive);

        if (isCreated) {
            FireStoreManager.getInstance().getPostRef().document(postId).set(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setIsLive DocumentSnapshot set");
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "setIsLive Error set document", e);
                    });
        } else {
            FireStoreManager.getInstance().getPostRef().document(postId).update(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setIsLive DocumentSnapshot update");
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "setIsLive Error update document", e);
                        setIsLive(postId, isLive, true);
                    });
        }
    }

    public void setCountComment(String postId, int countComment, boolean isCreated) {
        if (TextUtils.isEmpty(postId)) {
            return;
        }
        Map<String, Object> post = new HashMap<>();
        post.put(Constants.FIELD_COUNT_COMMENTS, countComment);

        if (isCreated) {
            post.put(Constants.FIELD_IS_LIVE, true);
            FireStoreManager.getInstance().getPostRef().document(postId).set(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setCountComment DocumentSnapshot set");
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "setCountComment Error set document", e));
        } else {
            FireStoreManager.getInstance().getPostRef().document(postId).update(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setCountComment DocumentSnapshot update");
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "setCountComment Error update document", e);
                        setCountComment(postId, countComment, true);
                    });
        }
    }

    public void setCountViewer(String postId, int countViewer, boolean isCreated) {
        if (TextUtils.isEmpty(postId)) {
            return;
        }
        Map<String, Object> post = new HashMap<>();
        post.put(Constants.FIELD_COUNT_VIEWERS, countViewer);

        if (isCreated) {
            post.put(Constants.FIELD_IS_LIVE, true);
            FireStoreManager.getInstance().getPostRef().document(postId).set(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setCountViewer DocumentSnapshot set");
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "setCountViewer Error set document", e));
        } else {
            FireStoreManager.getInstance().getPostRef().document(postId).update(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setCountViewer DocumentSnapshot update");
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "setCountViewer Error update document", e);
                        setCountViewer(postId, countViewer, true);
                    });
        }
    }

    public void setIsLike(String viewerId, boolean isLike) {
        FireStoreManager.getInstance().getViewersRef().document(viewerId)
                .update(Constants.FIELD_IS_LIKE, isLike)
                .addOnSuccessListener(aVoid -> Log.d(TAG, "setIsLike DocumentSnapshot successfully updated!"))
                .addOnFailureListener(e -> Log.w(TAG, "setIsLike Error updating document", e));
    }

    public void setProduct(String postId, ProductInPost productInPost, boolean isCreated) {
        if (TextUtils.isEmpty(postId)) {
            return;
        }
        if (TextUtils.isEmpty(productInPost.getProductId())) {
            return;
        }
        Map<String, Object> mapProduct = new HashMap<>();
        mapProduct.put(Constants.FIELD_PRODUCT_ID, productInPost.getProductId());
        mapProduct.put(Constants.FIELD_PRODUCT_EXPIRE_AT, productInPost.getExpiredAt());
        mapProduct.put(Constants.FIELD_PRODUCT_DISCOUNT, productInPost.getDiscount());
        mapProduct.put(Constants.FIELD_PRODUCT_PRICE_DISCOUNT, productInPost.getPriceDiscount());
        mapProduct.put(Constants.FIELD_PRODUCT_QUANTITY, productInPost.getQuantity());
        Map<String, Object> post = new HashMap<>();
        post.put(Constants.FIELD_PRODUCT, mapProduct);

        if (isCreated) {
            FireStoreManager.getInstance().getPostRef().document(postId).set(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setProduct DocumentSnapshot set");
                        FireStoreManager.getInstance().setIsLive(postId, true, false);
                        FireStoreManager.getInstance().setCountComment(postId, 0, false);
                        FireStoreManager.getInstance().setCountViewer(postId, 0, false);
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "setProduct Error set document", e);
                    });
        } else {
            FireStoreManager.getInstance().getPostRef().document(postId).update(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "setProduct DocumentSnapshot update");
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "setProduct Error update document", e);
                        setProduct(postId, productInPost, true);
                    });
        }
    }

    public void getConfig(Context context) {
        configRef.document(Constants.DOCUMENT_RED_5_PRO).get().addOnCompleteListener(task -> {
            Log.d(TAG, "Get Red5Pro Success: ");
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                    Red5ProConfig red5ProConfig = document.toObject(Red5ProConfig.class);
                    if (red5ProConfig != null) {
                        Utils.saveRed5ProConfig(context, red5ProConfig);
                    }
                } else {
                    Log.d(TAG, "No such document");
                }
            } else {
                Log.e(TAG, "Get Red5Pro Error: " + task.getException());
            }
        });
    }
}
