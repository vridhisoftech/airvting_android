package com.airvtinginc.airvtinglive.components.services;

import android.util.Log;

import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.http.HttpResponseModel;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

public class MyAPISubscribe<Model> implements Observer<Model> {
    private APIResponseListener apiResponseListener;
    private RequestTarget requestTarget;
    private static final int SUCCESS = 201;
    private static final int TIMEOUT_TOKEN = 401;

    public MyAPISubscribe(APIResponseListener apiResponseListener, RequestTarget requestTarget) {
        this.apiResponseListener = apiResponseListener;
        this.requestTarget = requestTarget;
    }


    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Model model) {
        if (apiResponseListener != null) {
            ResponseModel responseModel = (ResponseModel) model;
            switch (responseModel.getStatusCode()) {
                case SUCCESS:
                    if (responseModel.isSuccess()) {
                        apiResponseListener.onResponseSuccess(responseModel, requestTarget);
                    } else {
                        apiResponseListener.onResponseFail(responseModel.getMessage(), EnumManager.StatusCode.ERR_API_CALL_FAIL, requestTarget);
                    }
                    break;
                case TIMEOUT_TOKEN:
                    Log.e("MyAPISubscribe", "onError: ERR_UNAUTHORIZED");
                    apiResponseListener.onResponseFail(responseModel.getMessage(), EnumManager.StatusCode.ERR_UNAUTHORIZED, requestTarget);
                    break;
            }
        }
    }

    @Override
    public void onError(Throwable t) {
        if (apiResponseListener != null) {
            try {
                ResponseBody errorBody = ((HttpException) t).response().errorBody();
                if (errorBody != null) {
                    handleError(errorBody);
                }

//            if (t instanceof HttpException) {
//                HttpException exception = (HttpException) t;
//                Log.e("APISubscribe", "HttpException: " + exception.code());
//                switch (exception.code()) {
//                    case TIMEOUT_TOKEN:
//                        Log.e("MyAPISubscribe", "onError: ERR_UNAUTHORIZED");
//                        apiResponseListener.onResponseFail(getErrorMessage(exception.response().errorBody()),
//                                EnumManager.StatusCode.ERR_UNAUTHORIZED, requestTarget);
//                        break;
//                    default:
//                        apiResponseListener.onResponseFail(getErrorMessage(exception.response().errorBody()),
//                                EnumManager.StatusCode.ERR_API_CALL_FAIL, requestTarget);
//                        break;
//                }
//            } else {
//                Log.e("APISubscribe", "onError: " + t.getLocalizedMessage());
//                apiResponseListener.onResponseFail(t.getLocalizedMessage(), EnumManager.StatusCode.ERR_API_CALL_FAIL, requestTarget);
//            }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onComplete() {
    }

    private void handleError(ResponseBody errorBody) {
        if (errorBody != null) {
            try {
                Gson gson = new GsonBuilder().create();
                HttpResponseModel httpResponseModel = gson.fromJson(errorBody.string(), HttpResponseModel.class);

                if (httpResponseModel != null) {
                    switch (httpResponseModel.getStatusCode()) {
                        case TIMEOUT_TOKEN:
                            Log.e("MyAPISubscribe", "onError: ERR_UNAUTHORIZED");
                            apiResponseListener.onResponseFail(httpResponseModel.getMessage(),
                                    EnumManager.StatusCode.ERR_UNAUTHORIZED, requestTarget);
                            break;
                        default:
                            apiResponseListener.onResponseFail(httpResponseModel.getMessage(),
                                    EnumManager.StatusCode.ERR_API_CALL_FAIL, requestTarget);
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
