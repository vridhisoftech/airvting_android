package com.airvtinginc.airvtinglive.components.red5.SubscribeTest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.red5.Red5PropertiesContent;
import com.airvtinginc.airvtinglive.components.red5.Red5LiveDetailFragment;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.red5pro.streaming.R5Connection;
import com.red5pro.streaming.R5Stream;
import com.red5pro.streaming.R5StreamProtocol;
import com.red5pro.streaming.config.R5Configuration;
import com.red5pro.streaming.event.R5ConnectionEvent;
import com.red5pro.streaming.event.R5ConnectionListener;
import com.red5pro.streaming.media.R5AudioController;
import com.red5pro.streaming.view.R5VideoView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscribeTestFragment extends Red5LiveDetailFragment implements R5ConnectionListener {
    private static final String TAG = SubscribeTestFragment.class.getSimpleName();

    @BindView(R.id.videoView)
    R5VideoView display;

    private R5Stream subscribe;

    private String streamName;
    private String deviceName;

    public SubscribeTestFragment() {
        // Required empty public constructor
    }

    public static SubscribeTestFragment newInstance(String postId, String deviceName) {
        SubscribeTestFragment fragment = new SubscribeTestFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_POST_ID, postId);
        args.putString(Constants.EXTRA_DEVICE_NAME, deviceName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onConnectionEvent(R5ConnectionEvent event) {
        Log.d("Subscriber", ":onConnectionEvent " + event.name());
        if (event.name() == R5ConnectionEvent.LICENSE_ERROR.name()) {
            Handler h = new Handler(Looper.getMainLooper());
            h.post(() -> showDialog("License is Invalid"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        if (getArguments() != null) {
            this.streamName = getArguments().getString(Constants.EXTRA_POST_ID);
            this.deviceName = getArguments().getString(Constants.EXTRA_DEVICE_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.red5_subscribe_test, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Subscribe();
    }

    public void Subscribe() {

        //Create the configuration from the tests.xml
        R5Configuration config = new R5Configuration(R5StreamProtocol.RTSP,
                Red5PropertiesContent.GetPropertyString("host"),
                Red5PropertiesContent.GetPropertyInt("port"),
                Red5PropertiesContent.GetPropertyString("context"),
                Red5PropertiesContent.GetPropertyFloat("subscribe_buffer_time"));
        config.setLicenseKey(SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_RED5PRO_LICENSE_KEY, ""));
        config.setBundleID(getActivity().getPackageName());

        R5Connection connection = new R5Connection(config);

        //setup a new stream using the connection
        subscribe = new R5Stream(connection);

        //Some devices can't handle rapid reuse of the audio controller, and will crash
        //Recreation of the controller assures that the example will always be stable
        subscribe.audioController = new R5AudioController();
        subscribe.audioController.sampleRate = Red5PropertiesContent.GetPropertyInt("sample_rate");

        subscribe.client = this;
        subscribe.setListener(this);

        //show all logging
        subscribe.setLogLevel(R5Stream.LOG_LEVEL_DEBUG);

        //set Scale Mode
        if (TextUtils.isEmpty(deviceName)) {
            subscribe.setScaleMode(0);
        } else {
            if (deviceName.equals("iPhone 6") || deviceName.equals("iPhone 6 Plus") || deviceName.equals("iPhone 6s") || deviceName.equals("iPhone 6s Plus")) {
                subscribe.setScaleMode(2);
            } else {
                subscribe.setScaleMode(0);
            }
        }

        //display.setZOrderOnTop(true);
        display.attachStream(subscribe);

        display.showDebugView(Red5PropertiesContent.GetPropertyBool("debug_view"));

        if (streamName == null || streamName.equals("")) {
            streamName = Red5PropertiesContent.GetPropertyString("stream1");
        }
        subscribe.play(streamName);
    }

    protected void updateOrientation(int value) {
        value += 90;
        Log.d("SubscribeTestFragment", "update orientation to: " + value);
        display.setStreamRotation(value);
    }

    public void onMetaData(String metadata) {
        Log.d("SubscribeTestFragment", "Metadata receieved: " + metadata);
        String[] props = metadata.split(";");
        for (String s : props) {
            String[] kv = s.split("=");
            if (kv[0].equalsIgnoreCase("orientation")) {
                updateOrientation(Integer.parseInt(kv[1]));
            }
        }
    }

    public void onStreamSend(String msg) {
        Log.d("SubscribeTestFragment", "GOT MSG");
    }

    public R5Stream getSubscribe() {
        return subscribe;
    }

    @Override
    public void onStop() {
        if (subscribe != null) {
            subscribe.stop();
        }

        super.onStop();
    }
}
