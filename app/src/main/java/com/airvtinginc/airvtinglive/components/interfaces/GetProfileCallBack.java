package com.airvtinginc.airvtinglive.components.interfaces;

public interface GetProfileCallBack {
    void onGetProfile();
}