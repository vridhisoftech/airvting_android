package com.airvtinginc.airvtinglive.components.interfaces;

public interface ResultListener<T> {
    void onSucceed(T result, String msg);

    void onError(int code, String msg);
}
