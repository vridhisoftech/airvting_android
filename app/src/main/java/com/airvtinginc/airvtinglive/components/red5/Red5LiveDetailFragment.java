package com.airvtinginc.airvtinglive.components.red5;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.tools.DialogUtils;

// import com.red5pro.streaming.event.R5StreamEvent;
// import com.red5pro.streaming.event.R5StreamListener;

public class Red5LiveDetailFragment extends Fragment {

    public Context context;

    public Red5LiveDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.red5_fragment_test_detail, container, false);
        return rootView;
    }

    public Boolean isPublisherTest() {
        return false;
    }

    public Boolean shouldClean() {
        return true;
    }

    public void showDialog(String message) {
        DialogUtils.showMessageDialog(getContext(), getFragmentManager(),
                getString(R.string.error_live_license_title), message);
    }
}
