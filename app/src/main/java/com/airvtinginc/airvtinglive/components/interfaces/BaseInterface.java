package com.airvtinginc.airvtinglive.components.interfaces;


import com.airvtinginc.airvtinglive.components.enums.RequestTarget;

public interface BaseInterface {
    <T> void requestApi(T model, boolean isLoading, RequestTarget requestTarget, APIResponseListener listener);
}
