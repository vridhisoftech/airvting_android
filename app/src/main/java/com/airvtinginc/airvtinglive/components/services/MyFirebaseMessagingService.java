package com.airvtinginc.airvtinglive.components.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.interfaces.BaseInterface;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.activities.BaseActivity;
import com.airvtinginc.airvtinglive.gui.activities.DetailActivity;
import com.airvtinginc.airvtinglive.gui.activities.LiveStreamActivity;
import com.airvtinginc.airvtinglive.gui.enums.DetailType;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.gui.enums.MediaType;
import com.airvtinginc.airvtinglive.gui.enums.NotiActivityType;
import com.airvtinginc.airvtinglive.models.PostInNotification;
import com.airvtinginc.airvtinglive.models.ProductInPost;
import com.airvtinginc.airvtinglive.models.response.Message;
import com.airvtinginc.airvtinglive.models.response.Notification;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.User;
import com.airvtinginc.airvtinglive.tools.DateTimeUtils;
import com.airvtinginc.airvtinglive.tools.DialogUtils;
import com.airvtinginc.airvtinglive.tools.ImageUtils;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MyFirebaseMessagingService extends FirebaseMessagingService
        implements BaseInterface, APIResponseListener {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            checkReceiveData(remoteMessage.getData());
        }
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "onNewToken: " + token);
        SharedPreferencesManager.getInstance(this).putString(Constants.PREF_CURRENT_FCM_TOKEN, token);
    }

    private void checkReceiveData(Map<String, String> data) {
        String currentUserId = SharedPreferencesManager.getInstance(this).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (data.containsKey(ServiceConstants.NOTIFICATION)) {
            String notificationData = data.get(ServiceConstants.NOTIFICATION);
            if (!TextUtils.isEmpty(notificationData)) {
                Gson gson = new Gson();
                Notification notification = gson.fromJson(notificationData, Notification.class);
                if (notification != null && currentUserId.equals(notification.getReceiverId())) {
                    sendNotification(notification);
                }
            }
        }
    }

    private void sendNotification(Notification notification) {
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.item_notification_activity);
        try {
            User notifier = notification.getNotifier();
            String username = notifier.getUsername();
            String featuredImage = notifier.getAvatar();

            if (!TextUtils.isEmpty(username)) {
                String displayUsernameTitle = "@" + username;
                contentView.setTextViewText(R.id.tv_notification_activity_username, displayUsernameTitle);
            }

            if (!TextUtils.isEmpty(featuredImage)) {
                Bitmap bitmap = Glide.with(this)
                        .asBitmap()
                        .load(featuredImage)
                        .submit(512, 512)
                        .get();
                Bitmap roundedBitmap = ImageUtils.getCroppedBitmap(bitmap);
                contentView.setImageViewBitmap(R.id.iv_notification_activity_avatar, roundedBitmap);
                contentView.setViewVisibility(R.id.tv_notification_activity_default_avatar, View.GONE);
                contentView.setViewVisibility(R.id.iv_notification_activity_border_avatar, View.GONE);
            } else {
                contentView.setImageViewResource(R.id.iv_notification_activity_avatar, R.drawable.bg_circle_avatar_grey_border);
                contentView.setTextViewText(R.id.tv_notification_activity_default_avatar, Utils.getDefaultAvatarText(notifier));
                contentView.setViewVisibility(R.id.tv_notification_activity_default_avatar, View.VISIBLE);
                contentView.setViewVisibility(R.id.iv_notification_activity_border_avatar, View.VISIBLE);
            }

            if (!TextUtils.isEmpty(notification.getNotifyMessage())) {
                contentView.setTextViewText(R.id.tv_notification_activity_status, notification.getNotifyMessage());
            }

            contentView.setTextViewText(R.id.tv_notification_time, DateTimeUtils.getCurrentTime(DateTimeUtils.TIME_HOUR_MINUTE_24_FORMAT));
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        Intent intent = getIntentFromNotification(notification);
        if (intent == null) {
            return;
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = getString(R.string.default_notification_channel_id);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setContent(contentView)
                        .setDefaults(android.app.Notification.DEFAULT_VIBRATE)
                        .setPriority(android.app.Notification.PRIORITY_HIGH); // Set for Heads-up notification

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setVibrate(new long[0]);
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        Date now = new Date();
        int uniqueId = Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.US).format(now));
        notificationManager.notify(uniqueId, notificationBuilder.build());
    }

    private Intent getIntentFromNotification(Notification notification) {
        Intent intent = new Intent(this, DetailActivity.class);
        Bundle args = new Bundle();

        User notifier = notification.getNotifier();
        ProductInPost product = notification.getProduct();
        PostInNotification post = notification.getPost();
        Message message = notification.getMessage();

        boolean isValidNotifier = notifier != null && !TextUtils.isEmpty(notifier.getUserId());
        boolean isValidProduct = product != null && !TextUtils.isEmpty(product.getProductId());
        boolean isValidPost = post != null && !TextUtils.isEmpty(post.getPostId());

        String notificationType = notification.getType();
        if (!TextUtils.isEmpty(notificationType)) {
            if (notificationType.equals(NotiActivityType.FOLLOW.getType())) {
                if (isValidNotifier) {
                    args.putString(Constants.EXTRA_USER_ID, notifier.getUserId());
                    intent.putExtras(args);
                    DetailType.PROFILE.attachTo(intent);
                }
            } else if (notificationType.equals(NotiActivityType.OFFER.getType())) {
                DetailType.WALLET.attachTo(intent);
            } else if (notificationType.equals(NotiActivityType.LIKE.getType())) {
                if (isValidProduct) {
                    args.putString(Constants.EXTRA_PRODUCT_ID, product.getProductId());
                    intent.putExtras(args);
                    DetailType.PRODUCT.attachTo(intent);
                }
            } else if (notificationType.equals(NotiActivityType.LIVE_STREAM.getType())) {
                if (isValidPost && isValidNotifier) {
                    intent = new Intent(this, LiveStreamActivity.class);
                    args.putString(Constants.EXTRA_USER_ID, notifier.getUserId());
                    args.putString(Constants.EXTRA_POST_ID, post.getPostId());
                    args.putString(Constants.EXTRA_MEDIA_URL, post.getMediaUrl());
                    intent.putExtras(args);
                }
            } else if (notificationType.equals(NotiActivityType.COMMENT.getType())) {
                if (isValidPost) {
                    args.putString(Constants.EXTRA_GALLERY_ID, post.getPostId());
                    intent.putExtras(args);
                    DetailType.GALLERY.attachTo(intent);
                }
            } else if (notificationType.equals(NotiActivityType.TAGGED.getType())) {
                if (isValidPost) {
                    if (post.getType().equals(MediaType.STREAM.getType())) {
                        if (isValidNotifier) {
                            intent = new Intent(this, LiveStreamActivity.class);
                            args.putString(Constants.EXTRA_USER_ID, notifier.getUserId());
                            args.putString(Constants.EXTRA_POST_ID, post.getPostId());
                            args.putString(Constants.EXTRA_MEDIA_URL, post.getMediaUrl());
                            intent.putExtras(args);
                        }
                    } else {
                        args.putString(Constants.EXTRA_GALLERY_ID, post.getPostId());
                        intent.putExtras(args);
                        DetailType.GALLERY.attachTo(intent);
                    }
                }
            } else if (notificationType.equals(NotiActivityType.MESSAGE.getType())) {
                if (message != null && !TextUtils.isEmpty(message.getMessageId())) {
                    intent.putExtra(Constants.EXTRA_CONVERSATION_ID, message.getMessageId());
                    DetailType.CONVERSATIONS_DETAIL.attachTo(intent);
                }
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return intent;
        }
        return null;
    }

    private void readNotification(String notificationId) {
        Notification notification = new Notification();
        notification.setId(notificationId);
        requestApi(notification, false, RequestTarget.READ_NOTIFICATION, this);
    }

    @Override
    public <T> void requestApi(T model, boolean isLoading, RequestTarget requestTarget, APIResponseListener listener) {
//        if (!Utils.isNetworkConnectionAvailable()) {
//            DialogUtils.hideLoadingProgress();
//            if (listener != null)
//                listener.onResponseFail(getString(R.string.error_no_internet_connection), EnumManager.StatusCode.ERR_NO_INTERNET_CONNECTION, requestTarget);
//            return;
//        }
//
//        if (requestTarget == null) {
//            return;
//        }
//
//        if (isLoading) {
//            DialogUtils.showLoadingProgress(this, false);
//        }
//
//        processRequestApi(model, requestTarget, listener);
        ((BaseActivity) getApplicationContext()).requestApi(model, isLoading, requestTarget, listener);
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        if (response.isSuccess()) {
            switch (requestTarget) {
                case READ_NOTIFICATION:
                    Log.d(TAG, "onResponseSuccess: Read notification activity");
                    break;
            }
        } else {
            Log.e(TAG, requestTarget.toString() + ": fail, failMessage: " + response.getMessage());
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        DialogUtils.hideLoadingProgress();
        switch (requestTarget) {
            case READ_NOTIFICATION:
                Log.d(TAG, "onResponseFail: Read notification activity - failMessage: " + failMessage);
                break;
        }
    }
}
