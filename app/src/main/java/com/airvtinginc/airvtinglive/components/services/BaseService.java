package com.airvtinginc.airvtinglive.components.services;

import android.app.Service;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.components.interfaces.APIManager;
import com.airvtinginc.airvtinglive.components.interfaces.APIResponseListener;
import com.airvtinginc.airvtinglive.components.interfaces.BaseInterface;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.PostRequest;
import com.airvtinginc.airvtinglive.models.response.PostDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.tools.Utils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.airvtinginc.airvtinglive.app.MainApplication.getContext;

public abstract class BaseService extends Service implements BaseInterface, APIResponseListener {

    protected final String TAG = this.getClass().getName();

    @Override
    public void onCreate() {
        super.onCreate();
        onStartService();
        initializeApiManager();
        Log.i(TAG, "onCreate(): Service Started.");
    }

    @Override
    public final int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStarCommand(): Received id " + startId + ": " + intent);
        if (intent == null) {
            onTurnOff();
        }
        return START_STICKY; // run until explicitly stopped.
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onStopService();
        Log.i(TAG, "Service Stopped.");
    }

    public abstract void onStartService();

    public abstract void onStopService();

    public abstract void onTurnOff();

    public abstract void onReceiveMessage(Message msg);

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.i(TAG, "AS onTaskRemoved called");
        super.onTaskRemoved(rootIntent);
    }

    private APIManager apiManager = null;
    private MyAPISubscribe myAPISubscribe;

    private void initializeApiManager() {
        apiManager = RetrofitGenerator.createService(APIManager.class);
    }

    @Override
    public <T> void requestApi(T model, boolean isLoading, RequestTarget requestTarget, APIResponseListener listener) {
        if (!Utils.isNetworkConnectionAvailable()) {
            if (listener != null)
                listener.onResponseFail(getString(R.string.error_no_internet_connection), EnumManager.StatusCode.ERR_NO_INTERNET_CONNECTION, requestTarget);
            return;
        }

        if (requestTarget == null) {
            return;
        }
        processRequestApi(model, requestTarget, listener);
    }

    private <T> void processRequestApi(T model, RequestTarget requestTarget, APIResponseListener listener) {
        switch (requestTarget) {
            case UPDATE_STATUS:
                String url = String.format(requestTarget.toString(), false);
                myAPISubscribe = new MyAPISubscribe<ResponseModel<UserResponse>>(listener, requestTarget);
                apiManager.updateStatus(url, getAuthorization())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
            case END_STREAM:
                PostRequest postRequest = (PostRequest) model;
                String endStreamUrl = String.format(requestTarget.toString(), postRequest.getPostId());
                myAPISubscribe = new MyAPISubscribe<ResponseModel<PostDetailResponse>>(listener, requestTarget);
                apiManager.endStream(endStreamUrl, getAuthorization(), postRequest.getSaveStreamRequest())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myAPISubscribe);
                break;
        }
    }

    private String getAuthorization() {
        String tokenId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_TOKEN_ID, "");
        System.out.println(ServiceConstants.HEADER_AUTHORIZATION_PREFIX + tokenId);
        return ServiceConstants.HEADER_AUTHORIZATION_PREFIX + tokenId;
    }

    public void cancelRequestApi() {
        if (myAPISubscribe != null) {
//            myAPISubscribe.(AndroidSchedulers.mainThread());
        }
    }
}