package com.airvtinginc.airvtinglive.components.interfaces;

import com.airvtinginc.airvtinglive.components.services.ServiceConstants;
import com.airvtinginc.airvtinglive.models.request.AddCardModelRequest;
import com.airvtinginc.airvtinglive.models.request.AddConversationsRequest;
import com.airvtinginc.airvtinglive.models.request.BuyAirTokenRequestModel;
import com.airvtinginc.airvtinglive.models.request.BuyGiftRequest;
import com.airvtinginc.airvtinglive.models.request.CartProductsRequest;
import com.airvtinginc.airvtinglive.models.request.CheckoutRequest;
import com.airvtinginc.airvtinglive.models.request.ConfigRequest;
import com.airvtinginc.airvtinglive.models.request.ContactUsRequest;
import com.airvtinginc.airvtinglive.models.request.LoginModelRequest;
import com.airvtinginc.airvtinglive.models.request.SaveStreamRequest;
import com.airvtinginc.airvtinglive.models.request.SendFCMTokenRequest;
import com.airvtinginc.airvtinglive.models.request.SendGiftRequest;
import com.airvtinginc.airvtinglive.models.request.SignUpModelRequest;
import com.airvtinginc.airvtinglive.models.request.SocicalSignInModelRequest;
import com.airvtinginc.airvtinglive.models.response.AirTokenExchangeResponse;
import com.airvtinginc.airvtinglive.models.response.AirTokenListResponse;
import com.airvtinginc.airvtinglive.models.response.BuyAirTokenResponse;
import com.airvtinginc.airvtinglive.models.response.CategoryListModelResponse;
import com.airvtinginc.airvtinglive.models.response.CheckoutResponse;
import com.airvtinginc.airvtinglive.models.response.ConfigsResponse;
import com.airvtinginc.airvtinglive.models.response.ContactUsResponse;
import com.airvtinginc.airvtinglive.models.response.ConversationDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ConversationReplyResponse;
import com.airvtinginc.airvtinglive.models.response.ConversationsListResponse;
import com.airvtinginc.airvtinglive.models.response.DeleteCardResponse;
import com.airvtinginc.airvtinglive.models.response.AdminFeeResponse;
import com.airvtinginc.airvtinglive.models.response.DeleteResponse;
import com.airvtinginc.airvtinglive.models.response.GiftDetailListResponse;
import com.airvtinginc.airvtinglive.models.response.LeftMenuResponse;
import com.airvtinginc.airvtinglive.models.response.LikeProductResponse;
import com.airvtinginc.airvtinglive.models.response.MediaModelResponse;
import com.airvtinginc.airvtinglive.models.response.NotificationsResponse;
import com.airvtinginc.airvtinglive.models.response.PostDetailResponse;
import com.airvtinginc.airvtinglive.models.response.PostResponse;
import com.airvtinginc.airvtinglive.models.response.ProductDetailResponse;
import com.airvtinginc.airvtinglive.models.response.ProductsResponse;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.airvtinginc.airvtinglive.models.response.SearchModelResponse;
import com.airvtinginc.airvtinglive.models.response.SearchUserModelResponse;
import com.airvtinginc.airvtinglive.models.response.UploadImageResponse;
import com.airvtinginc.airvtinglive.models.response.WalletRespone;
import com.airvtinginc.airvtinglive.models.response.UserResponse;
import com.airvtinginc.airvtinglive.models.response.UsersResponse;
import com.airvtinginc.airvtinglive.models.response.PaymentDetailResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface APIManager {
    @Multipart
    @POST()
    Observable<ResponseModel<MediaModelResponse>> postMedia(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @PartMap Map<String, RequestBody> params, @Part MultipartBody.Part photo);

    @Multipart
    @POST()
    Observable<ResponseModel<PostDetailResponse>> postComment(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @PartMap Map<String, RequestBody> params);

    @Multipart
    @POST()
    Observable<ResponseModel<MediaModelResponse>> postLive(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @PartMap Map<String, RequestBody> params);

    @Multipart
    @POST()
    Observable<ResponseModel<UsersResponse>> postReport(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @PartMap Map<String, RequestBody> params);

    @POST()
    Observable<ResponseModel<SearchUserModelResponse>> searchUser(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST()
    Observable<ResponseModel<UserResponse>> login(@Url String url, @Body LoginModelRequest loginModelRequest);

    @POST()
    Observable<ResponseModel<UserResponse>> socialSignIn(@Url String url, @Body SocicalSignInModelRequest socicalSignInModelRequest);

    @POST()
    Observable<ResponseModel<UserResponse>> signUp(@Url String url, @Body SignUpModelRequest signUpModelRequest);

    @GET
    Observable<ResponseModel<UserResponse>> logout(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @PUT
    Observable<ResponseModel<UserResponse>> updateStatus(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @Multipart
    @POST
    Observable<ResponseModel<UserResponse>> updateUserProfile(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @PartMap Map<String, RequestBody> params, @Part MultipartBody.Part avatar, @Part MultipartBody.Part cover);

    @GET
    Observable<ResponseModel<UserResponse>> getUserProfile(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<CategoryListModelResponse>> getCategory(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<ProductsResponse>> getListMyStore(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<PostDetailResponse>> bookmarkPost(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<ProductDetailResponse>> getProductDetail(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<LikeProductResponse>> likeProduct(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @Multipart
    @POST
    Observable<ResponseModel<PostDetailResponse>> addNewProduct(@Url String url,
                                                                @Header(ServiceConstants.HEADER_AUTHORIZATION) String token,
                                                                @PartMap Map<String, RequestBody> params,
                                                                @Part MultipartBody.Part images0,
                                                                @Part MultipartBody.Part images1,
                                                                @Part MultipartBody.Part images2,
                                                                @Part MultipartBody.Part images3,
                                                                @Part MultipartBody.Part images4,
                                                                @Part MultipartBody.Part images5,
                                                                @Part MultipartBody.Part images6,
                                                                @Part MultipartBody.Part images7,
                                                                @Part MultipartBody.Part images8,
                                                                @Part MultipartBody.Part images9);

    @Multipart
    @POST
    Observable<ResponseModel<PostDetailResponse>> editProduct(@Url String url,
                                                              @Header(ServiceConstants.HEADER_AUTHORIZATION) String token,
                                                              @PartMap Map<String, RequestBody> params,
                                                              @Part MultipartBody.Part images0,
                                                              @Part MultipartBody.Part images1,
                                                              @Part MultipartBody.Part images2,
                                                              @Part MultipartBody.Part images3,
                                                              @Part MultipartBody.Part images4,
                                                              @Part MultipartBody.Part images5,
                                                              @Part MultipartBody.Part images6,
                                                              @Part MultipartBody.Part images7,
                                                              @Part MultipartBody.Part images8,
                                                              @Part MultipartBody.Part images9);

    @GET
    Observable<ResponseModel<UsersResponse>> getExplorePeople(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<PostResponse>> getPost(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<ProductsResponse>> getProduct(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<PostDetailResponse>> getExpireProductInPost(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<WalletRespone>> getWallets(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET()
    Observable<ResponseModel<SearchModelResponse>> search(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET()
    Observable<ResponseModel<ConversationsListResponse>> searchConversations(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST()
    Observable<ResponseModel<ConversationDetailResponse>> addConversations(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body AddConversationsRequest conversationsRequest);

    @GET()
    Observable<ResponseModel<ConversationDetailResponse>> getConversationDetail(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST()
    Observable<ResponseModel<ConversationReplyResponse>> replyConversations(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body AddConversationsRequest conversationsRequest);

    @GET
    Observable<ResponseModel<PostDetailResponse>> getPostDetail(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @Multipart
    @PUT
    Observable<ResponseModel<PostDetailResponse>> updatePost(@Url String url,
                                                             @Header(ServiceConstants.HEADER_AUTHORIZATION) String token,
                                                             @PartMap Map<String, RequestBody> params,
                                                             @Part MultipartBody.Part image);

    @GET
    Observable<ResponseModel<GiftDetailListResponse>> getGiftStore(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<GiftDetailListResponse>> getUserGifts(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST
    Observable<ResponseModel<GiftDetailListResponse>> buyGift(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token,
                                                              @Body BuyGiftRequest buyGiftRequest);

    @POST
    Observable<ResponseModel<GiftDetailListResponse>> sendGift(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token,
                                                               @Body SendGiftRequest sendGiftRequest);

    @POST
    Observable<ResponseModel<PostDetailResponse>> likePost(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<LeftMenuResponse>> getLeftMenuInfo(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @PUT
    Observable<ResponseModel<PostDetailResponse>> endStream(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token,
                                                            @Body SaveStreamRequest saveStreamRequest);

    @GET
    Observable<ResponseModel<UserResponse>> followUser(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<PaymentDetailResponse>> getListMyCards(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST
    Observable<ResponseModel<PaymentDetailResponse>> addCard(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body AddCardModelRequest modelRequest);

    @PUT
    Observable<ResponseModel<PaymentDetailResponse>> updateCard(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body AddCardModelRequest modelRequest);

    @DELETE
    Observable<ResponseModel<DeleteCardResponse>> deleteCard(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<AirTokenListResponse>> getAirTokens(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST
    Observable<ResponseModel<BuyAirTokenResponse>> buyAirToken(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body BuyAirTokenRequestModel modelRequest);

    @POST
    Observable<ResponseModel<CheckoutResponse>> cartCheckout(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body CheckoutRequest modelRequest);

    @GET
    Observable<ResponseModel<AirTokenExchangeResponse>> exchangeAirToken(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<NotificationsResponse>> getNotifications(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST
    Observable<ResponseModel<UserResponse>> sendFCMToken(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body SendFCMTokenRequest modelRequest);

    @POST
    Observable<ResponseModel<ProductsResponse>> getCartProducts(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body CartProductsRequest modelRequest);

    @POST
    Observable<ResponseModel<UserResponse>> resendVerifyEmail(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST
    Observable<ResponseModel<UserResponse>> forgotPassword(@Url String url, @Body LoginModelRequest loginModelRequest);

    @POST
    Observable<ResponseModel<UserResponse>> deactivateAccount(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @POST
    Observable<ResponseModel<ContactUsResponse>> sendContact(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body ContactUsRequest modelRequest);

    @POST
    Observable<ResponseModel<ConfigsResponse>> configSettings(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token, @Body ConfigRequest modelRequest);

    @GET
    Observable<ResponseModel<NotificationsResponse>> readNotification(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<AdminFeeResponse>> getAdminFee(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<UsersResponse>> getFollowers(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @GET
    Observable<ResponseModel<UsersResponse>> getFollowing(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);

    @Multipart
    @POST
    Observable<ResponseModel<UploadImageResponse>> uploadImage(@Url String url,
                                                               @Header(ServiceConstants.HEADER_AUTHORIZATION) String token,
                                                               @Part List<MultipartBody.Part> images);

    @DELETE
    Observable<ResponseModel<DeleteResponse>> delete(@Url String url, @Header(ServiceConstants.HEADER_AUTHORIZATION) String token);
}
