package com.airvtinginc.airvtinglive.components.enums;

public enum FormatType {
    FormBody,
    Json
}
