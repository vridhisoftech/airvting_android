package com.airvtinginc.airvtinglive.components.interfaces;

import com.airvtinginc.airvtinglive.components.services.ServiceConstants;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;


public interface ApiService {

    @GET
    Call<String> request(@Url String url);

    @PUT
    Call<String> requestPut(@Url String url, @Body RequestBody body);

    @POST
    Call<String> requestPost(@Url String url, @Body RequestBody body);

    @DELETE
    Call<String> requestDelete(@Url String url, @Body RequestBody body);

    @Multipart
    @POST
    Call<String> requestUploadPhoto(@Url String url, @Part MultipartBody.Part image);

    @Multipart
    @POST
    Call<String> requestUploadUserPost(@Url String url, @Part("postCaption") RequestBody caption, @Part("photoCaptions") RequestBody captions, @Part("photoTags") RequestBody tags, @Part MultipartBody.Part[] images, @Part("postImagesCount") RequestBody postImagesCount);

    @GET("{fb_user_id}/picture?type=large")
    Call<ResponseBody> downLoadFacebookProfileImage(@Path("fb_user_id") String fbUserId);

    @Multipart
    @POST
    Call<String> requestLeadForm(@Url String url, @Part("body") RequestBody body, @Part MultipartBody.Part file);

    @GET(ServiceConstants.URL_GET_USER_PROFILE)
    Call<String> requestGetUserProfile(@Path(ServiceConstants.USER_ID) String userId);

    @GET(ServiceConstants.URL_GET_POSTS_BY_FILTER)
    Call<String> requestGetPostsByFilter(@Query(ServiceConstants.FILTER) String filter,
                                         @Query(ServiceConstants.PAGINATE) int paginate,
                                         @Query(ServiceConstants.PER_PAGE) int quantity,
                                         @Query(ServiceConstants.MAX_ID) String maxId,
                                         @Query(ServiceConstants.TYPE_POST) String[] typePost,
                                         @Query(ServiceConstants.IS_SHOW_ADVER) boolean isShowAdver);

    @GET(ServiceConstants.URL_GET_POST_BY_USER_ID)
    Call<String> requestGetPostsByUserId(@Path(ServiceConstants.USER_ID) String userId,
                                         @Query(ServiceConstants.PAGINATE) int paginate,
                                         @Query(ServiceConstants.PER_PAGE) int quantity,
                                         @Query(ServiceConstants.MAX_ID) String maxId,
                                         @Query(ServiceConstants.TYPE_POST) String[] typePost,
                                         @Query(ServiceConstants.BOOKMARKS) int bookmarks);

    @GET(ServiceConstants.URL_GET_LIST_PRODUCT)
    Call<String> requestGetListProduct(@Path(ServiceConstants.USER_ID) String userId,
                                       @Query(ServiceConstants.CATEGORY_ID) String categoryId,
                                       @Query(ServiceConstants.PAGINATE) int paginate,
                                       @Query(ServiceConstants.PER_PAGE) int quantity);

    @GET(ServiceConstants.URL_GET_PRODUCT_DETAIL)
    Call<String> requestGetProductDetail(@Path(ServiceConstants.PRODUCT_ID) String productId);

    @POST(ServiceConstants.URL_LIKE_PRODUCT)
    Call<String> requestLikeProduct(@Path(ServiceConstants.PRODUCT_ID) String productId,
                                    @Body RequestBody body);

    @Multipart
    @PUT(ServiceConstants.URL_UPDATE_USER_PROFILE)
    Call<String> requestUpdateProfile(@PartMap Map<String, RequestBody> params);
//                                      @Part MultipartBody.Part avatar,
//                                      @Part MultipartBody.Part cover);
}
