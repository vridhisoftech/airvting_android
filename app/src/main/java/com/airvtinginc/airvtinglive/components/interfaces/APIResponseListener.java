package com.airvtinginc.airvtinglive.components.interfaces;

import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;

public interface APIResponseListener {
    void onResponseSuccess(ResponseModel response, RequestTarget requestTarget);
    void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget);
}
