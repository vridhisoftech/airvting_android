package com.airvtinginc.airvtinglive.components.red5.VodStream;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.components.red5.Red5LiveDetailFragment;
import com.airvtinginc.airvtinglive.constants.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VodTestFragment extends Red5LiveDetailFragment {
    private static final String TAG = VodTestFragment.class.getSimpleName();

    @BindView(R.id.videoView)
    VideoView display;
    @BindView(R.id.detail_add_video_iv_play)
    ImageView ivPlay;
    @BindView(R.id.pbLoadMedia)
    ProgressBar pbLoadMedia;

    private String url;
    private MediaController mediaController;

    public VodTestFragment() {
        // Required empty public constructor
    }

    public static VodTestFragment newInstance(String url) {
        VodTestFragment fragment = new VodTestFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_MEDIA_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.url = getArguments().getString(Constants.EXTRA_MEDIA_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.red5_play_vod, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        playVod();
    }

    @Override
    public void onResume() {
        super.onResume();
        playVod();
    }

    private void playVod() {
        Log.d(TAG, "URL Media: " + url);
        display.setVideoURI(Uri.parse(url));
        if (mediaController == null) {
            mediaController = new MediaController(getActivity());
        }
        display.setMediaController(mediaController);
        display.setOnCompletionListener(mediaPlayer -> {
            ivPlay.setVisibility(View.VISIBLE);
            display.seekTo(100);
        });
        display.setOnPreparedListener(mp -> {
            pbLoadMedia.setVisibility(View.GONE);
        });

        mediaController.setAnchorView(display);
        ivPlay.setOnClickListener(v -> {
            ivPlay.setVisibility(View.GONE);
            display.start();
        });
        start();
    }

    private void start() {
        pbLoadMedia.setVisibility(View.VISIBLE);
        display.start();
    }
}
