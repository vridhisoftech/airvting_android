package com.airvtinginc.airvtinglive.components.services;

import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.app.MainApplication;
import com.airvtinginc.airvtinglive.components.interfaces.ResultListener;
import com.airvtinginc.airvtinglive.tools.AppLog;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class CallbackHandler<T> implements Callback<String> {
    private static final Gson GSON = new Gson();
    private final ResultListener<T> listener;
    private final Type type;
    private boolean parseFormat = true;

    CallbackHandler(ResultListener<T> listener, Type type) {
        this(listener, type, true);
    }

    CallbackHandler(ResultListener<T> listener, Type type, boolean parseFormat) {
        this.listener = listener;
        this.type = type;
        this.parseFormat = parseFormat;
    }

    @Override
    public void onResponse(Call<String> call, retrofit2.Response<String> response) {
        if (listener != null) {
            AppLog.e("Response " + response.body());
            if (response.isSuccessful()) {
                AppLog.e("Response Body " + response.body());
                try {
                    String data = response.body();
                    if (parseFormat) {
                        parseData(data, listener, type);
                    } else {
                        listener.onSucceed((T) data, "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onError(HttpRequest.ERROR_DATA_CODE, MainApplication.getContext().getString(R.string.services_internal_error));
                }

            } else {
                listener.onError(response.code(), getErrorMessage(response.errorBody()));
            }
        }
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        if (listener != null) {
            listener.onError(HttpRequest.ERROR_NETWORK_CODE, MainApplication.getContext().getString(R.string.services_network_error_check_ur_connection));
        }
    }

    private static <T> void parseData(String responseData, ResultListener<T> listener, Type type) throws Exception {
        AppLog.d("resp: " + responseData);

        JSONObject res = new JSONObject(responseData);
        int code = res.optInt(ServiceConstants.RESPONSE_STATUS_CODE, 0);
        if (code != ServiceConstants.RESPONSE_STATUS_CODE_SUCCESS) {
            String msg = res.optString("message", "");
            listener.onError(code, msg);
            return;
        }
        String data = res.get(ServiceConstants.RESPONSE_DATA).toString();
        String msg = res.get(ServiceConstants.RESPONSE_MESSAGE).toString();
        if (type.toString().toLowerCase().equals(String.class.toString().toLowerCase())) {
            listener.onSucceed((T) data, msg);
            return;
        }
        Object oj = GSON.fromJson(data, type);
        listener.onSucceed((T) oj, msg);
    }

    private String getErrorMessage(ResponseBody errorBody) {
        String message = "";
        if (errorBody != null) {
            try {
                JSONObject errorJsonObject = new JSONObject(errorBody.string());
                message = errorJsonObject.getString(ServiceConstants.MESSAGE);
            } catch (JSONException e) {
                AppLog.e(e.getMessage());
            } catch (IOException e) {
                AppLog.e(e.getMessage());
            }
        }
        return message;
    }
}
