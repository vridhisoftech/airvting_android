package com.airvtinginc.airvtinglive.components.services;

import com.airvtinginc.airvtinglive.BuildConfig;

public class ServiceConstants {
    // TODO: Sign in - Sign up URL
    public static final String URL_SIGN_IN = "api/v1/signIn";
    public static final String URL_SIGN_UP = "api/v1/signUp";
    public static final String URL_SIGN_UP_SOCIAL = "api/v1/socialSignIn";
    public static final String URL_LOG_OUT = "api/v1/signOut";
    public static final String URL_GET_VERIFY_CODE = "api/v1/get_code";
    public static final String URL_VERIFY_CODE = "api/v1/verify";
    public static final String URL_GET_USER_PROFILE = "api/v1/users/{userId}";

    // TODO: Home URL
    public static final String URL_GET_POSTS_BY_FILTER = "api/v1/posts";
    public static final String URL_GET_HOME_FEED = "";
    public static final String URL_GET_NEW_LIVES = "";
    public static final String URL_GET_HOT_LIVES = "";
    public static final String URL_GET_FEATURED_LIVES = "";
    public static final String URL_GET_HOME_BANNERS = "";

    // TODO: Explore URL
    public static final String URL_GET_PEOPLE_GROUP = "";
    public static final String URL_GET_PEOPLE_SUGGESTED_FOLLOWS = "";

    // TODO: Profile URL
    public static final String URL_GET_GALLERY_MEDIA = "";
    public static final String URL_GET_POST_BY_USER_ID = "api/v1/users/{userId}/posts";
    public static final String URL_UPDATE_USER_PROFILE = "api/v1/users";

    // TODO: Help and FAQ
    public static final String URL_HELP_AND_FAQ = BuildConfig.BASE_URL_WEBVIEW + "faqs.html";
    public static final String URL_TERMS_AND_CONDITIONS = BuildConfig.BASE_URL_WEBVIEW + "privacy.html";
    public static final String URL_PRIVACY_POLICY = BuildConfig.BASE_URL_WEBVIEW + "privacy-policy.html";
    public static final String URL_OUR_COMPANY = BuildConfig.BASE_URL_WEBVIEW + "our-company.html";
    public static final String URL_USER_AGREEMENT = BuildConfig.BASE_URL_WEBVIEW + "user-agreement.html";
    public static final String URL_STRIPE = "https://stripe.com/";

    // TODO: Product
    public static final String URL_GET_LIST_PRODUCT = "api/v1/users/{userId}/products";
    public static final String URL_GET_PRODUCT_DETAIL = "api/v1/products/{productId}";
    public static final String URL_LIKE_PRODUCT = "api/v1/products/{productId}/like";

    public static final String VERIFY_CODE = "code";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String BIRTHDAY = "birth";
    public static final String PASSWORD_CONFIRM = "";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String DISPLAY_NAME = "displayName";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String GENDER = "gender";
    public static final String USER_ID = "userId";
    public static final String FILTER = "filter";
    public static final String PAGINATE = "paginate";
    public static final String MAX_ID = "maxId";
    public static final String PER_PAGE = "perPage";
    public static final String IS_SHOW_ADVER = "isShowAdver";
    public static final String TYPE_POST = "typePost";
    public static final String BOOKMARKS = "bookmarks";
    public static final String FOLLOW = "follow";
    public static final String NEW = "new";
    public static final String HOT = "hot";
    public static final String FEATURED = "featured";
    public static final String CATEGORY_ID = "categoryId";
    public static final String HEADER_AUTHORIZATION = "authorization";
    public static final String HEADER_AUTHORIZATION_PREFIX = "Bearer ";
    public static final String RESPONSE_STATUS_CODE = "statusCode";
    public static final String RESPONSE_DATA = "data";
    public static final String RESPONSE_MESSAGE = "message";
    public static final String BIRTHDAY_FORMAT = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String TIME_FORMAT = "HH:mmaa";
    public static final String MESSAGE = "message";
    public static final String MESSAGE_ID = "messageId";
    public static final String SOCIAL_TYPE_FACEBOOK = "facebook";
    public static final String SOCIAL_TYPE_GOOGLE = "google";
    public static final String SOCIAL_ID = "socialId";
    public static final String SOCIAL_TYPE = "socialType";
    public static final String SOCIAL_TOKEN = "socialToken";
    public static final String FEATURED_IMAGE = "featuredImage";
    public static final String COVER_IMAGE = "coverImage";
    public static final String LOCATION = "location";
    public static final String DESCRIPTION = "description";
    public static final String PRODUCT_ID = "productId";
    public static final String POST_ID = "postId";
    public static final String TYPE = "type";
    public static final String IS_LIKE = "isLike";
    public static final String PART_FEATURED_IMAGE = "featuredImageName";
    public static final String PART_COVER_IMAGE = "coverImageName";
    public static final String TITLE = "title";
    public static final String PRICE = "price";
    public static final String PRICE_SALE = "priceSale";
    public static final String AIR_TOKEN = "airToken";
    public static final String PRODUCT_CATEGORIES = "productCategories";
    public static final String FEATURE_IMAGES = "featuredImages";
    public static final String FEATURE_IMAGES_OLD = "featuredImagesOld";
    public static final String FEATURE_IMAGES_MESSAGE = "image";
    public static final String CONDITION = "condition";
    public static final String CONDITION_NEW = "new";
    public static final String CONDITION_USED = "used";
    public static final String TIMER = "timer";
    public static final String STARTED_AT = "startedAt";
    public static final String EXPIRE_AT = "expiredAt";
    public static final String DISCOUNT = "discount";
    public static final String PRODUCT = "product";
    public static final String POST = "post";
    public static final String ONLINE = "online";
    public static final String NOTIFICATION = "notification";
    public static final String RECEIVER_ID = "receiverId";
    public static final String NOTIFY_MESSAGE = "notifyMessage";
    public static final String NOTIFIER = "notifier";
    public static final String NOTIFICATION_LIVE_STREAM = "liveStream";
    public static final String NOTIFICATION_MESSAGE = "message";
    public static final String NOTIFICATION_SYSTEM = "system";
    public static final String CURRENCY = "S$";
    public static final int RESPONSE_STATUS_CODE_SUCCESS = 201;
    public static final float MINIMUM_ADMIN_FEE = 0.5f;


    public static final int NUMBER_OF_ITEMS_PER_LOADING = 10;
    public static final int NUMBER_OF_GIFT_STORE_ITEMS_PER_LOADING = 30;
    public static final int NUMBER_OF_PHOTO_IN_SUGGESTED_LOADING = 5;
    public static final int NUMBER_OF_BANNERS_PER_LOADING = 6;
    public static final int ERROR_CODE_WRONG_USERNAME_OR_PASSWORD = 111;
    public static final int ERROR_CODE_NOT_MATCH_PASSWORD = 222;
    public static final int ERROR_CODE_INVALID_EMAIL = 333;
    public static final int ERROR_CODE_INVALID_PHONE_NUMBER = 444;
    public static final int ERROR_CODE_INVALID_VERIFY_CODE = 555;
    public static final int ERROR_CODE_REQUEST_TIME_OUT = 408;
    public static final String ERROR_UNAUTHORIZED = "Unauthorized";
    public static final String ERROR_MSG_REQUEST_TIME_OUT = "Request Timeout";

    public static final int GENDER_MALE = 0;
    public static final int GENDER_FEMALE = 1;
    public static final String FACEBOOK_GENDER_MALE = "male";
    public static final String FACEBOOK_GENDER_FEMALE = "female";

    public static final int REQUEST_CODE_SIGN_IN_GOOGLE_PLUS = 100;
}
