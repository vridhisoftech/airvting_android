package com.airvtinginc.airvtinglive.components.services;

import android.support.annotation.NonNull;
import android.util.Log;

import com.airvtinginc.airvtinglive.BuildConfig;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.tools.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitGenerator {

    public static <S> S createService(@NonNull Class<S> serviceClass) {
        try {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .connectTimeout(300, TimeUnit.SECONDS).retryOnConnectionFailure(true)
                    .writeTimeout(300, TimeUnit.SECONDS)
                    .readTimeout(300, TimeUnit.SECONDS)
                    .addInterceptor(chain -> {
                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("DeviceTypeId", String.valueOf(Constants.DEVICE_TYPE_ID_ANDROID))
                                .header("UdId", Utils.getUIIDDevice())
                                .header("DeviceName",android.os.Build.MODEL)
                                .header("Content-Type", "application/json")
                                .method(original.method(), original.body());

                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    })
                    .addInterceptor(logging)
                    .build();

            Gson gson = new GsonBuilder().setLenient().create();
            Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient)
                    .build();
            return retrofit.create(serviceClass);
        } catch (Exception e) {
            Log.e("Retrofit error: ", e.toString());
        }
        return null;
    }

}
