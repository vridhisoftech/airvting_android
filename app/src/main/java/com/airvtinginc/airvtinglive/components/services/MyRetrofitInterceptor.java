package com.airvtinginc.airvtinglive.components.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class MyRetrofitInterceptor implements Interceptor {
    private HashMap<String, String> headers;

    public MyRetrofitInterceptor() {
    }

    public MyRetrofitInterceptor(HashMap<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request org = chain.request();
        Request.Builder builder = org.newBuilder();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                builder.addHeader(key, value);
            }
        }
        Request request = builder.build();

        Response response = chain.proceed(request);
        return response;
    }
}
