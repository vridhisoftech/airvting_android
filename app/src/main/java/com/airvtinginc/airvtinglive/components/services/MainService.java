package com.airvtinginc.airvtinglive.components.services;

import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.airvtinginc.airvtinglive.components.enums.RequestTarget;
import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;
import com.airvtinginc.airvtinglive.gui.enums.EnumManager;
import com.airvtinginc.airvtinglive.models.request.PostRequest;
import com.airvtinginc.airvtinglive.models.request.SaveStreamRequest;
import com.airvtinginc.airvtinglive.models.response.ResponseModel;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import static com.airvtinginc.airvtinglive.app.MainApplication.getContext;

public class MainService extends BaseService {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStartService() {
    }

    @Override
    public void onStopService() {

    }

    @Override
    public void onTurnOff() {
        setIdOffLine();
        setIsLive(false, false);
        setSaveLive();
        setViewerView();
    }

    @Override
    public void onReceiveMessage(Message msg) {

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    private void setIdOffLine() {
        if (!TextUtils.isEmpty(SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, ""))) {
            requestApi(null, false, RequestTarget.UPDATE_STATUS, this);
        }
    }

    private void setSaveLive() {
        String postId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_POST_ID, "");
        Log.d(TAG, "postId setSaveLive: " + postId);
        if (!TextUtils.isEmpty(postId)) {
            PostRequest postRequest = new PostRequest(postId);
            SaveStreamRequest saveStreamRequest = new SaveStreamRequest();
            saveStreamRequest.setSaveStream(false);
            postRequest.setSaveStreamRequest(saveStreamRequest);

            requestApi(postRequest, false, RequestTarget.END_STREAM, this);
        }
    }

    public void setIsLive(boolean isLive, boolean isCreated) {
        String userId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        if (TextUtils.isEmpty(userId)) {
            Log.d(TAG, "userId isEmpty: ");
            return;
        }
        Log.d(TAG, "userId: " + userId);

        String postId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_POST_ID, "");
        if (TextUtils.isEmpty(postId)) {
            Log.d(TAG, "postId isEmpty: ");
            return;
        }
        Log.d(TAG, "postId: " + postId);

        CollectionReference postRef = FirebaseFirestore.getInstance().collection(Constants.COLLECTION_POSTS);

        Map<String, Object> post = new HashMap<>();
        post.put(Constants.FIELD_IS_LIVE, isLive);

        if (isCreated) {
            postRef.document(postId).set(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "DocumentSnapshot set");
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "Error adding document", e));
        } else {
            postRef.document(postId).update(post)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "DocumentSnapshot set");
                    })
                    .addOnFailureListener(e -> {
                        Log.w(TAG, "Error adding document", e);
                        setIsLive(isLive, true);
                    });
        }
    }

    public void setViewerView() {
        String viewerId = SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_VIEWER_ID, "");
        Log.d(TAG, "viewerId: " + viewerId);
        if (!TextUtils.isEmpty(viewerId)) {
            FirebaseFirestore.getInstance().collection(Constants.COLLECTION_VIEWERS).document(viewerId)
                    .update(Constants.FIELD_IS_VIEW, false)
                    .addOnSuccessListener(aVoid -> {
                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                        SharedPreferencesManager.getInstance(getContext()).putString(Constants.PREF_CURRENT_VIEWER_ID, "");
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
        }
    }

    @Override
    public void onResponseSuccess(ResponseModel response, RequestTarget requestTarget) {
        switch (requestTarget) {
            case UPDATE_STATUS:
                Log.i(TAG, "UPDATE_STATUS: onResponseSuccess");
                break;
            case END_STREAM:
                if (response.isSuccess()) {
                    Log.i(TAG, "END_STREAM: onResponseSuccess");
                    SharedPreferencesManager.getInstance(getContext()).putString(Constants.PREF_CURRENT_POST_ID, "");
                } else {
                    Log.i(TAG, "END_STREAM: onResponseFail");
                }
                break;
        }
    }

    @Override
    public void onResponseFail(String failMessage, EnumManager.StatusCode statusCode, RequestTarget requestTarget) {
        switch (requestTarget) {
            case UPDATE_STATUS:
                Log.i(TAG, "UPDATE_STATUS: onResponseFail: " + failMessage);
                break;
            case END_STREAM:
                Log.i(TAG, "END_STREAM: onResponseFail: " + failMessage);
                break;
        }
    }
}