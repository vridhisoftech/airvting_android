package com.airvtinginc.airvtinglive.components.services;

import com.airvtinginc.airvtinglive.BuildConfig;
import com.airvtinginc.airvtinglive.R;
import com.airvtinginc.airvtinglive.app.MainApplication;
import com.airvtinginc.airvtinglive.components.enums.FormatType;
import com.airvtinginc.airvtinglive.components.enums.RequestMethod;
import com.airvtinginc.airvtinglive.components.interfaces.ApiService;
import com.airvtinginc.airvtinglive.components.interfaces.ResultListener;
import com.airvtinginc.airvtinglive.tools.AppLog;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class HttpRequest {
    private static final String END_POINT = BuildConfig.BASE_URL;

    public static final int ERROR_DATA_CODE = -2;
    public static final int ERROR_NETWORK_CODE = -1;

    public static <T> void request(String url, final ResultListener<T> listener, final Type type) {
        AppLog.e("URL: "+url);
        request(url, null, RequestMethod.GET, null, null, listener, type, true);
    }

    public static <T> void request(String url, FormatType formatType, RequestMethod method, JSONObject params, final ResultListener<T> listener, final Type type, boolean parseFormat) {
        request(url, formatType, method, null, params, listener, type, parseFormat);
    }

    public static <T> void request(String url, FormatType formatType, RequestMethod method, HashMap<String, String> headers, JSONObject params, ResultListener<T> listener, final Type type, boolean parseFormat) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

        ApiService service = retrofit.create(ApiService.class);
        RequestBody body = serializeBody(formatType, params);
        Call<String> request = null;
        if (method == null) {
            method = RequestMethod.GET;
        }

        switch (method) {
            case GET:
                request = service.request(url);
                break;
            case POST:
                request = service.requestPost(url, body);
                break;
            case PUT:
                request = service.requestPut(url, body);
                break;
            case DELETE:
                request = service.requestDelete(url, body);
                break;
        }


        if (params != null) {
            AppLog.d("params: " + params.toString());
        }
        if (headers != null) {
            AppLog.d("header: " + headers.toString());
        }

        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, parseFormat));
        }
    }

    public static <T> void requestLocationLatLng(String url, final ResultListener<T> listener) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://maps.googleapis.com/maps/api/")
                .addConverterFactory(new ToStringConverterFactory())
                .build();
        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.request(url);
        if (request != null) {
            request.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    if (listener != null) {
                        if (response.isSuccessful()) {
                            AppLog.e("HttpResponseModel Body " + response.body());
                            try {
                                String data = response.body();
                                listener.onSucceed((T) data, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onError(ERROR_DATA_CODE, MainApplication.getContext().getString(R.string.services_internal_error));
                            }
                        } else {
                            listener.onError(response.code(), response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (listener != null) {
                        listener.onError(ERROR_NETWORK_CODE, MainApplication.getContext().getString(R.string.services_network_error_check_ur_connection));
                    }
                }
            });
        }
    }

    public static <T> void requestUploadPhoto(String url, MultipartBody.Part image, ResultListener<T> listener, Type type) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestUploadPhoto(url, image);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestUploadUserPost(String url, String caption, String captions, String tags, MultipartBody.Part[] images, String postImagesCount, ResultListener<T> listener, Type type) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestUploadUserPost(url, RequestBody.create(MediaType.parse("text/plain"), caption), RequestBody.create(MediaType.parse("text/plain"), captions), RequestBody.create(MediaType.parse("text/plain"), tags), images, RequestBody.create(MediaType.parse("text/plain"), postImagesCount));
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestLeadform(String url, JSONObject params, MultipartBody.Part file, ResultListener<T> listener, Type type) {
        AppLog.e("Lead form function HR");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestLeadForm(url, RequestBody.create(MediaType.parse("text/plain"), params.toString()), file);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    private static RequestBody serializeBody(FormatType formatType, JSONObject params) {
        RequestBody body;
        if (formatType != null && params != null) {
            if (formatType == FormatType.FormBody) {
                FormBody.Builder builder = new FormBody.Builder();
                Iterator<String> it = params.keys();
                try {
                    while (it.hasNext()) {
                        String key = it.next();
                        builder.add(key, params.getString(key));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                body = builder.build();
                return body;
            }

            if (formatType == FormatType.Json) {
                body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), params.toString());
                return body;
            }
        }
        return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "{}");
    }

    private static OkHttpClient generateClient(HashMap<String, String> headers) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(new MyRetrofitInterceptor(headers));

        OkHttpClient client = builder.build();
        return client;
    }

    public static <T> void requestGetUserProfile(String userId, HashMap<String, String> headers, ResultListener<T> listener, final Type type) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestGetUserProfile(userId);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestGetPostsByFilter(String filter, int paginate, int quantity,
                                                   String maxId, String[] typePost, boolean isShowAdver,
                                                   HashMap<String, String> headers, ResultListener<T> listener, final Type type) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestGetPostsByFilter(filter, paginate, quantity, maxId, typePost, isShowAdver);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestGetPostsByUserId(String userId, int paginate, int quantity,
                                                   String maxId, String[] typePost, int bookmarks,
                                                   HashMap<String, String> headers, ResultListener<T> listener, final Type type) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestGetPostsByUserId(userId, paginate, quantity, maxId, typePost, bookmarks);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestGetListProduct(String userId, String categoryId, int paginate, int quantity,
                                                 HashMap<String, String> headers, ResultListener<T> listener, final Type type) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestGetListProduct(userId, categoryId, paginate, quantity);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestGetProductDetail(String productId, HashMap<String, String> headers,
                                                   ResultListener<T> listener, final Type type) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestGetProductDetail(productId);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestLikeProduct(String productId, HashMap<String, String> headers,
                                              JSONObject params, ResultListener<T> listener, final Type type) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

        RequestBody body = serializeBody(FormatType.FormBody, params);

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestLikeProduct(productId, body);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }

    public static <T> void requestUpdateProfile(HashMap<String, String> headers,  Map<String, RequestBody> userMap,
//                                                @Part MultipartBody.Part avatar, @Part MultipartBody.Part cover,
                                                ResultListener<T> listener, final Type type) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(new ToStringConverterFactory())
                .client(generateClient(headers))
                .build();

//        RequestBody body = serializeBody(FormatType.FormBody, params);

        ApiService service = retrofit.create(ApiService.class);
        Call<String> request = service.requestUpdateProfile(userMap);
        if (request != null) {
            request.enqueue(new CallbackHandler<T>(listener, type, true));
        }
    }
}
