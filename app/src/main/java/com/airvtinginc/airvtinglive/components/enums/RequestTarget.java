package com.airvtinginc.airvtinglive.components.enums;

import com.airvtinginc.airvtinglive.constants.Constants;
import com.airvtinginc.airvtinglive.data.SharedPreferencesManager;

import static com.airvtinginc.airvtinglive.app.MainApplication.getContext;

public enum RequestTarget {
    SIGN_IN {
        @Override
        public String toString() {
            return "api/v1/signIn";
        }
    },
    SIGN_UP {
        @Override
        public String toString() {
            return "api/v1/signUp";
        }
    },
    SIGN_IN_SOCIAL {
        @Override
        public String toString() {
            return "api/v1/socialSignIn";
        }
    },
    LOG_OUT {
        @Override
        public String toString() {
            return "api/v1/signOut";
        }
    },
    UPDATE_STATUS {
        @Override
        public String toString() {
            return "api/v1/users/status?online=%1$s";
        }
    },
    GET_VERIFY_CODE {
        @Override
        public String toString() {
            return "api/v1/get_code";
        }
    },
    VERIFY_CODE {
        @Override
        public String toString() {
            return "api/v1/verify";
        }
    },
    GET_USER_PROFILE {
        @Override
        public String toString() {
            return "api/v1/users/%1$s";
        }
    },
    UPDATE_USER_PROFILE {
        @Override
        public String toString() {
            return "api/v1/users/"+ SharedPreferencesManager.getInstance(getContext()).getString(Constants.PREF_CURRENT_USER_ID, "");
        }
    },
    GET_POSTS_BY_FILTER {
        @Override
        public String toString() {
            return "api/v1/posts?filter=%1$s&paginate=%2$s&perPage=%3$s&maxId=%4$s&typePost=video,stream&isShowAdver=%7$b";
        }
    },
    GET_POST_BY_USER_ID {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/posts?paginate=%2$s&perPage=%3$s&typePost=%4$s&typePost=%5$s&typePost=%6$s&bookmarks=%7$s";
        }
    },
    GET_BOOKMARK_BY_USER_ID {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/bookmark_posts?paginate=%2$s&perPage=%3$s&typePost=stream,video,image&bookmarks=%7$s";
        }
    },


    ADD_POST {
        @Override
        public String toString() {
            return "api/v1/posts";
        }
    },
    ADD_POST_VIDEO {
        @Override
        public String toString() {
            return "";
        }
    },
    SEARCH_USER {
        @Override
        public String toString() {
            return "api/v1/users?username=%1$s&paginate=%2$s&perPage=%3$s&maxId=%4$s";
        }
    },
    //    GET_CATEGORY {
//        @Override
//        public String toString() {
//            return "api/v1/categories?type=%1$s";
//        }
//    }
    GET_CATEGORY {
        @Override
        public String toString() {
            return "api/v1/get_post_categories";
        }
    },
    //    GET_POST_CATEGORY {
//        @Override
//        public String toString() {
//            return "api/v1/get_post_categories";
//        }
//    },
    GET_LIST_PRODUCT {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/products?paginate=%2$s&perPage=%3$s&maxId=%4$s";
        }
    },
    ADD_NEW_PRODUCT {
        @Override
        public String toString() {
            return "api/v1/products";
        }
    },
    EDIT_PRODUCT {
        @Override
        public String toString() {
            return "api/v1/products/%1$s";
        }
    },
    GET_CATEGORY_POSTS {
        @Override
        public String toString() {
            return "api/v1/get_post_categories/%1$s/posts?typePost=video,stream&paginate=%4$s&perPage=%5$s&maxId=%6$s";
        }
    },
    GET_CATEGORY_PRODUCT {
        @Override
        public String toString() {
            return "api/v1/categories/%1$s/products?paginate=%2$s&perPage=%3$s";
        }
    },
    BOOKMARK_POST {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s/bookmark";
        }
    },
    GET_EXPLORE_PEOPLE {
        @Override
        public String toString() {
            return "api/v1/users/explore?paginate=%1$s&perPage=%2$s&type=%3$s";
        }
    },
    SEARCH {
        @Override
        public String toString() {
            return "api/v1/search?type=%1$s&keyword=%2$s&paginate=%3$s&perPage=%4$s&maxId=%5$s";
        }
    },
    GET_PRODUCT_DETAIL {
        @Override
        public String toString() {
            return "api/v1/products/%1$s";
        }
    },
    LIKE_PRODUCT {
        @Override
        public String toString() {
            return "api/v1/products/%1$s/like";
        }
    },
    SEARCH_CONVERSATION {
        @Override
        public String toString() {
            return "api/v1/conversations?paginate=%1$s&perPage=%2$s&maxId=%3$s";
        }
    },
    ADD_CONVERSATION {
        @Override
        public String toString() {
            return "api/v1/conversations";
        }
    },

    GET_CONVERSATIONS_DETAIL {
        @Override
        public String toString() {
            return "api/v1/conversations/%1$s?paginate=%2$s&perPage=%3$s&maxId=%4$s";
        }
    },
    REPLY_CONVERSATION {
        @Override
        public String toString() {
            return "api/v1/conversations/%1$s/messages";
        }
    },
    UPLOAD_NEW_IMAGE {
        @Override
        public String toString() {
            return "upload";
        }
    },
    GET_POST_DETAIL {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s";
        }
    },
    UPDATE_POST {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s";
        }
    },
    GET_GIFT_STORE {
        @Override
        public String toString() {
            return "api/v1/gifts?paginate=%1$s&perPage=%2$s";
        }
    },
    GET_USER_GIFTS {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/gifts?paginate=%2$s&perPage=%3$s";
        }
    },
    BUY_GIFT {
        @Override
        public String toString() {
            return "api/v1/gifts/buy";
        }
    },
    SEND_GIFT {
        @Override
        public String toString() {
            return "api/v1/gifts/send";
        }
    },
    LIKE_POST {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s/like";
        }
    },
    GET_LEFT_MENU_INFO {
        @Override
        public String toString() {
            return "api/v1/leftMenu";
        }
    },
    END_STREAM {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s/end?typeDevice=android";
        }
    },
    FOLLOW_USER {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/fallow";
        }
    },
    GET_USER_LIST_CARDS {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/paymentMethods?paginate=%2$s&perPage=%3$s";
        }
    },
    ADD_CARDS {
        @Override
        public String toString() {
            return "api/v1/paymentMethods";
        }
    },
    UPDATE_CARDS {
        @Override
        public String toString() {
            return "api/v1/paymentMethods/%1$s";
        }
    },
    DELETE_CARDS {
        @Override
        public String toString() {
            return "api/v1/paymentMethods/%1$s";
        }
    },
    GET_AIR_TOKENS {
        @Override
        public String toString() {
            return "api/v1/airTokens?paginate=%1$s&perPage=%2$s";
        }
    },
    GET_TRANSACTIONS {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/transactions?paginate=%2$s&perPage=%3$s";
        }
    },
    GET_GIFTS {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/gifts?paginate=%2$s&perPage=%3$s";
        }
    },
    GET_GIFT_HISTORY {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/gifts/%2$s/history?paginate=%3$s&perPage=%4$s";
        }
    },
    GET_COMMENTS {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s/comments?paginate=%2$s&perPage=%3$s&maxId=%4$s";
        }
    },
    ADD_COMMENT {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s/comments";
        }
    },
    BUY_AIR_TOKEN {
        @Override
        public String toString() {
            return "api/v1/airTokens/buy";
        }
    },
    PRE_CHECKOUT {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/preCheckout";
        }
    },
    CHECKOUT {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/checkout";
        }
    },
    EXCHANGE_AIR_TOKEN {
        @Override
        public String toString() {
            return "api/v1/airTokens/exchange";
        }
    },
    GET_NOTIFICATIONS {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/notifications?paginate=%2$s&perPage=%3$s&isMessage=%4$s";
        }
    },
    EXPIRE_POST_PRODUCT {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s/expire?type=time&uniqueProductId=%2$s&productId=%3$s";
        }
    },
    REPORT {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/report";
        }
    },
    SEND_FCM_TOKEN {
        @Override
        public String toString() {
            return "api/v1/users/token";
        }
    },
    GET_CART_PRODUCTS {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/cart";
        }
    },
    RESEND_VERIFY_EMAIL {
        @Override
        public String toString() {
            return "api/v1/users/resendVerified";
        }
    },
    FORGOT_PASSWORD {
        @Override
        public String toString() {
            return "api/v1/forgotPassword";
        }
    },
    DEACTIVATE_ACCOUNT {
        @Override
        public String toString() {
            return "api/v1/users/deActive";
        }
    },
    SEND_CONTACT {
        @Override
        public String toString() {
            return "api/v1/contact";
        }
    },
    CONFIG_SETTINGS {
        @Override
        public String toString() {
            return "api/v1/users/configs";
        }
    },
    READ_NOTIFICATION {
        @Override
        public String toString() {
            return "api/v1/notifications/%1$s/read";
        }
    },
    GET_PRODUCT_ADMIN_FEE {
        @Override
        public String toString() {
            return "api/v1/products/feeAdmin";
        }
    },
    GET_FOLLOWERS {
        @Override
        public String toString() {
            return "api/v1/users/relationships?paginate=%1$s&perPage=%2$s&userId=%3$s&type=fallowers";
        }
    },
    GET_FOLLOWING {
        @Override
        public String toString() {
            return "api/v1/users/relationships?paginate=%1$s&perPage=%2$s&userId=%3$s&type=fallowing";
        }
    },
    DELETE_POST {
        @Override
        public String toString() {
            return "api/v1/posts/%1$s";
        }
    },
    DELETE_PRODUCT {
        @Override
        public String toString() {
            return "api/v1/products/%1$s";
        }
    },
    DELETE_CONVERSATION {
        @Override
        public String toString() {
            return "api/v1/conversations/%1$s";
        }
    },
    DELETE_COMMENT {
        @Override
        public String toString() {
            return "api/v1/comments/%1$s";
        }
    },
    DELETE_NOTIFICATION {
        @Override
        public String toString() {
            return "api/v1/users/%1$s/notifications?isMessage=%2$s";
        }
    },
}

